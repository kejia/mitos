#include "ns.h"
#include <inc/lib.h>
#include <kern/e1000.h>

#define SLEEPTIME 16

extern union Nsipc nsipcbuf;

void sleep(int millisecond) {
	unsigned now = sys_time_msec();
	unsigned end = now + millisecond;
	if ((int)now < 0 && (int)now > -MAXERROR) panic("sys_time_msec: %e", (int)now);
	if (end < now) panic("sleep: wrap");
	while (sys_time_msec() < end) sys_yield();
}

void
input(envid_t ns_envid)
{
	binaryname = "ns_input";

	// LAB 6: Your code here:
	// 	- read a packet from the device driver
	//	- send it to the network server
	// Hint: When you IPC a page to the network server, it will be
	// reading from it for a while, so don't immediately receive
	// another packet in to the same physical page.
	uint8_t inputBuffer[E1000_BUFFER_SIZE];
	int r, i;
	while (1) {
		memset(inputBuffer, 0, sizeof(inputBuffer));
		// read a packet from the receive descriptors queue
		while ((r = sys_receive(inputBuffer, sizeof(inputBuffer))) == -E1000_ERROR_RECEIVE_QUEUE_EMPTY) sys_yield();
		// panic if the received packet length < 0
		if (r < 0) panic("ERROR: failed to receive packet");
		// send it to the network server
		nsipcbuf.pkt.jp_len = r;
		memcpy(nsipcbuf.pkt.jp_data, inputBuffer, r);
		ipc_send(ns_envid, NSREQ_INPUT, &nsipcbuf, PTE_P | PTE_U);
		// wait for IPC
		sleep(SLEEPTIME);
	}
}
