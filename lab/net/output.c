#include "ns.h"
#include <kern/e1000.h>

extern union Nsipc nsipcbuf;

void
output(envid_t ns_envid)
{
	binaryname = "ns_output";
	// LAB 6: Your code here:
	// 	- read a packet from the network server
	//	- send the packet to the device driver
	uint32_t ipc_val, whom;
	int r;
	while (1) {
		// receive a packet message from the network server. see `lib/ipc.c' for `ipc_recv'.
		ipc_val = ipc_recv((int32_t *) &whom, &nsipcbuf, NULL);
		if (ipc_val == NSREQ_OUTPUT) {
			// send the network packet to the device driver
			while ((r = sys_transmit(nsipcbuf.pkt.jp_data, nsipcbuf.pkt.jp_len)) == -E1000_ERROR_TRANSMIT_QUEUE_FULL)
				sys_yield(); // wait when the transmit queue is full
			if (r == -E1000_ERROR_PACKET_BUFFER_TOO_LARGE)
				panic("ERROR: %s: packet is too large (%d bytes) to be sent\n", binaryname, nsipcbuf.pkt.jp_len);
			else if (r < 0)
				panic("ERROR: the network failed to send transmit packats: the return value is %d\n", r);
		}
	}
}
