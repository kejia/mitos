#include <kern/e1000.h>
#include <kern/pmap.h>
#include <kern/pci.h>
#include <inc/string.h>

static void e1000_tx_init() {
	/* init the transmit queue: */
	int i = 0;
	memset(e1000_tx_queue, 0, sizeof(e1000_tx_queue));
	for (i = 0; i < E1000_TRANSMIT_SIZE; i++) e1000_tx_queue[i].addr = PADDR(e1000_tx_buffer[i]);

	/* init transmit descriptor registers: */
	E1000_ADDR(E1000_TDBAL) = PADDR(e1000_tx_queue);
	E1000_ADDR(E1000_TDBAH) = 0;
	E1000_ADDR(E1000_TDLEN) = sizeof(e1000_tx_queue);
	E1000_ADDR(E1000_TDH) = 0;
	E1000_ADDR(E1000_TDT) = 0;

	/* init transmit control registers */
	E1000_ADDR(E1000_TCTL) |= E1000_TCTL_EN | E1000_TCTL_PSP; // set the Enable (`TCTL.EN`) bit to `1`b for normal operation, and set the Pad Short Packets (`TCTL.PSP`) bit to `1`b.
	/// E1000_ADDR(E1000_TCTL) &= ~(E1000_TCTL_CT | E1000_TCTL_COLD); // prepare to configure the Collision Threshold (`TCTL.CT`) and the Collision Threshold (`TCTL.CT`). unclear...
	E1000_ADDR(E1000_TCTL) |= ((0x10 << 4) | (0x40 << 12)); // configure the Collision Threshold (`TCTL.CT`) and the Collision Threshold (`TCTL.CT`)
	/// E1000_ADDR(E1000_TIPG) &= ~(0x000003ff | 0x000ffc00 | 0x3ff00000); // prepare to program the Transmit IPG (`TIPG`) register
	E1000_ADDR(E1000_TIPG) |= (10 | (4 << 10) | (6 << 20)); // program the Transmit IPG (`TIPG`) register
}

int e1000_transmit(const void *buffer, size_t size) {
	if (size > E1000_PACKET_BUFFER_SIZE) return -E1000_ERROR_PACKET_BUFFER_TOO_LARGE;
	uint32_t tail = E1000_ADDR(E1000_TDT);
	if ((e1000_tx_queue[tail].cmd & E1000_TXD_CMD_RS) &&
	    !(e1000_tx_queue[tail].status & E1000_TXD_STAT_DD))
		return -E1000_ERROR_TRANSMIT_QUEUE_FULL; // the transmit queue is full.
	e1000_tx_queue[tail].status &= ~E1000_TXD_STAT_DD; // unset the `DD` bit to re-use the descriptor
	memcpy(e1000_tx_buffer[tail], buffer, size);
	e1000_tx_queue[tail].length = size;
	e1000_tx_queue[tail].cmd |= (E1000_TXD_CMD_RS | E1000_TXD_CMD_EOP);
	E1000_ADDR(E1000_TDT) = (tail + 1) % E1000_TRANSMIT_SIZE;
	return 0;
}

static void e1000_rx_init() {
	/* init the receive queue: */
	int i = 0;
	memset(e1000_rx_queue, 0, sizeof(e1000_rx_queue));
	for (i = 0; i < E1000_RECEIVE_SIZE; i++) e1000_rx_queue[i].addr = PADDR(e1000_rx_buffer[i]);

	/* init the receive address */
	/* E1000_ADDR(E1000_RAL) = QEMU_MAC_LOW; */
	/* E1000_ADDR(E1000_RAH) = QEMU_MAC_HIGH; */
	E1000_ADDR(E1000_RAH) |= E1000_RAH_AV; // set the `Address Valid' bit in `RAH'

	/* init receive descriptor registers: */
	E1000_ADDR(E1000_RDBAL) = PADDR(e1000_rx_queue);
	E1000_ADDR(E1000_RDBAH) = 0;
	E1000_ADDR(E1000_RDLEN) = sizeof(e1000_rx_queue);
	E1000_ADDR(E1000_RDH) = 0;
	E1000_ADDR(E1000_RDT) = E1000_RECEIVE_SIZE - 1;

	/* init receive control registers */
	E1000_ADDR(E1000_RCTL) |= E1000_RCTL_EN; // set the Enable (`TCTL.EN`) bit to `1`b for normal operation.
	E1000_ADDR(E1000_RCTL) &= ~(E1000_RCTL_LBM); // loopback Mode (`RCTL.LBM`) should be set to `00`b for normal operation.
	E1000_ADDR(E1000_RCTL) &= ~(E1000_RCTL_RDMTS); // configure the *Receive Descriptor Minimum Threshold Size* (`RCTL.RDMTS') bits to 0.
	E1000_ADDR(E1000_RCTL) |= E1000_RCTL_BAM; // set the *Broadcast Accept Mode* (`RCTL.BAM') bit to 1 allowing the hardware to accept broadcast packets.
	E1000_ADDR(E1000_RCTL) &= ~E1000_RCTL_BSEX; // the recive buffer size is `1024` bytes, so `RCTL.BSEX = 0`b and `RCTL.BSIZE = 01`b.
	E1000_ADDR(E1000_RCTL) |= (E1000_RCTL_SZ & 0x00010000); // the recive buffer size is `1024` bytes, so `RCTL.BSEX = 0`b and `RCTL.BSIZE = 01`b.
	E1000_ADDR(E1000_RCTL) |= E1000_RCTL_SECRC; // strip Ethernet CRC (`RCTL.SECRC = 1`)
}

int e1000_receive(void *buffer, size_t size) {
	uint32_t next_to_tail = ((E1000_ADDR(E1000_RDT) + 1) % E1000_RECEIVE_SIZE);
	if (!(e1000_rx_queue[next_to_tail].status & E1000_RXD_STAT_DD)) return -E1000_ERROR_RECEIVE_QUEUE_EMPTY; // the receive queue is empty
	if (e1000_rx_queue[next_to_tail].length > size) return -E1000_ERROR_PACKET_BUFFER_TOO_LARGE;
	memcpy(buffer, e1000_rx_buffer[next_to_tail], e1000_rx_queue[next_to_tail].length);
	e1000_rx_queue[next_to_tail].status &= ~E1000_RXD_STAT_DD; // unset the `DD` bit to re-use the descriptor
	E1000_ADDR(E1000_RDT) = next_to_tail; // move the tail
	return e1000_rx_queue[next_to_tail].length;
}

static uint16_t read_eeprom(uint8_t addr) {
	E1000_ADDR(E1000_EERD) &= ~(E1000_EERD_ADDR_MASK | E1000_EERD_DATA_MASK); // reset to prepare
	E1000_ADDR(E1000_EERD) |= (addr << E1000_EERD_ADDR_SHIFT); // write the address to read the *Read Address* (`EERD.ADDR`) field
	E1000_ADDR(E1000_EERD) |= E1000_EERD_START; // write a `1`b to the *Start Read bit* (`EERD.START`)
	while (!(E1000_ADDR(E1000_EERD) & E1000_EERD_DONE));  // poll the *EEPROM Read register* until it sees the `EERD.DONE` bit set
	return (E1000_ADDR(E1000_EERD) & E1000_EERD_DATA_MASK) >> E1000_EERD_DATA_SHIFT; // read the data in the *Read Data field* (`EERD.DATA`)
}

int64_t get_mac_from_eeprom() {
	return read_eeprom(E1000_EEPROM_MAC_0) |
		(read_eeprom(E1000_EEPROM_MAC_1) << 16) |
		((uint64_t)read_eeprom(E1000_EEPROM_MAC_2) << 32);
}

// LAB 6: Your driver code here
int e1000_attach(struct pci_func *pciFunc) {
	pci_func_enable(pciFunc);
	e1000_mmio_base = (uintptr_t) mmio_map_region(pciFunc->reg_base[0], pciFunc->reg_size[0]);
	cprintf("e1000 status register: 0x%08x\n", (*(uint32_t *)(e1000_mmio_base + 8))); // test mmio: the dump value should be 0x80080783
	e1000_tx_init();
	e1000_rx_init();
	return 0;
}
