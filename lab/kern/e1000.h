#include <kern/pci.h>

#ifndef JOS_KERN_E1000_H

#define JOS_KERN_E1000_H
#define E1000_VENDOR_ID            0x8086
#define E1000_DEVICE_ID_82540EM    0x100E

volatile uintptr_t e1000_mmio_base;

int e1000_attach(struct pci_func *pciFunc);

#define E1000_TCTL     0x00400  /* TX Control - RW */
#define E1000_TIPG     0x00410  /* TX Inter-packet gap -RW */
#define E1000_TDBAL    0x03800  /* TX Descriptor Base Address Low - RW */
#define E1000_TDBAH    0x03804  /* TX Descriptor Base Address High - RW */
#define E1000_TDLEN    0x03808  /* TX Descriptor Length - RW */
#define E1000_TDH      0x03810  /* TX Descriptor Head - RW */
#define E1000_TDT      0x03818  /* TX Descripotr Tail - RW */

/* Transmit Control */
#define E1000_TCTL_EN   0x00000002  /* enable tx */
#define E1000_TCTL_PSP  0x00000008  /* pad short packets */
#define E1000_TCTL_CT   0x00000ff0  /* collision threshold */
#define E1000_TCTL_COLD 0x003ff000  /* collision distance */

/* Collision related configuration parameters */
#define E1000_COLLISION_THRESHOLD   0x10
#define E1000_CT_SHIFT              4

/* Collision distance is a 0-based value that applies to half-duplex-capable hardware only. */
#define E1000_COLLISION_DISTANCE    0x40
#define E1000_COLD_SHIFT            12

/* Default values for the transmit IPG register */
#define E1000_DEFAULT_TIPG_IPGT     10
#define E1000_DEFAULT_TIPG_IPGR1    4
#define E1000_DEFAULT_TIPG_IPGR2    6
#define E1000_TIPG_IPGT_MASK        0x000003FF
#define E1000_TIPG_IPGR1_MASK       0x000FFC00
#define E1000_TIPG_IPGR2_MASK       0x3FF00000
#define E1000_TIPG_IPGR1_SHIFT      10
#define E1000_TIPG_IPGR2_SHIFT      20

struct e1000_tx_desc {
	volatile uint64_t addr;
	volatile uint16_t length;
	volatile uint8_t cso;
	volatile uint8_t cmd;
	volatile uint8_t status;
	volatile uint8_t css;
	volatile uint16_t special;
} __attribute__((packed));

#define E1000_BUFFER_SIZE 1024
#define E1000_TRANSMIT_SIZE 64

static struct e1000_tx_desc e1000_tx_queue[E1000_TRANSMIT_SIZE] __attribute__((aligned(16)));
static uint8_t e1000_tx_buffer[E1000_TRANSMIT_SIZE][E1000_BUFFER_SIZE];

#define E1000_ADDR(offset) (*(volatile uint32_t *)(e1000_mmio_base + offset))
static void e1000_tx_init();

/* Transmit Descriptor bit definitions */
#define E1000_TXD_CMD_EOP   0x01  /* End of Packet */
#define E1000_TXD_CMD_RS    0x08  /* Report Status */
#define E1000_TXD_STAT_DD   0x01  /* Descriptor Done */

#define E1000_PACKET_BUFFER_SIZE 2048
#define E1000_ERROR_PACKET_BUFFER_TOO_LARGE 1
#define E1000_ERROR_TRANSMIT_QUEUE_FULL 2
#define E1000_ERROR_RECEIVE_QUEUE_EMPTY 3

int e1000_transmit(const void *buffer, size_t size);

/* Receive Address */
#define E1000_RAL     0x00005400  /* Receive Address Low - RW */
#define E1000_RAH     0x00005404  /* Receive Address High - RW */
#define E1000_RAH_AV  0x80000000	/* Receive descriptor valid */

/* RX Descriptor */
#define E1000_RDBAL    0x02800  /* RX Descriptor Base Address Low - RW */
#define E1000_RDBAH    0x02804  /* RX Descriptor Base Address High - RW */
#define E1000_RDLEN    0x02808  /* RX Descriptor Length - RW */
#define E1000_RDH      0x02810  /* RX Descriptor Head - RW */
#define E1000_RDT      0x02818  /* RX Descriptor Tail - RW */

/* Receive Control */
#define E1000_RCTL          0x00000100  /* RX Control - RW */
#define E1000_RCTL_EN       0x00000002  /* enable */
#define E1000_RCTL_LBM      0x000000c0  /* loopback mode */
#define E1000_RCTL_RDMTS    0x00000300  /* rx desc min threshold size */
#define E1000_RCTL_BAM      0x00008000	/* broadcast enable */
#define E1000_RCTL_SZ       0x00030000  /* rx buffer size */
#define E1000_RCTL_SECRC    0x04000000  /* strip ethernet CRC */
#define E1000_RCTL_BSEX     0x02000000  /* Buffer size extension */

/* Receive Descriptor bit definitions */
#define E1000_RXD_STAT_DD	0x01  /* Descriptor Done */

struct e1000_rx_desc {
         volatile uint64_t addr;
         volatile uint16_t length;
         volatile uint16_t checksum;
         volatile uint8_t status;
         volatile uint8_t errors;
         volatile uint16_t special;
 } __attribute__((packed));

#define E1000_RECEIVE_SIZE 128

static struct e1000_rx_desc e1000_rx_queue[E1000_RECEIVE_SIZE] __attribute__((aligned(16)));
static uint8_t e1000_rx_buffer[E1000_RECEIVE_SIZE][E1000_BUFFER_SIZE];

#define QEMU_MAC_LOW  0x12005452
#define QEMU_MAC_HIGH 0x00005634

static void e1000_rx_init();

int e1000_receive(void *buffer, size_t size);

#define E1000_EERD     0x00014	/* EEPROM Read - RW */

/* EEPROM Read */
#define E1000_EERD_START      0x00000001	/* Start Read */
#define E1000_EERD_DONE       0x00000010	/* Read Done */
#define E1000_EERD_ADDR_SHIFT 8
#define E1000_EERD_ADDR_MASK  0x0000FF00	/* Read Address */
#define E1000_EERD_DATA_SHIFT 16
#define E1000_EERD_DATA_MASK  0xFFFF0000	/* Read Data */

/* EEPROM position of MAC Address */
#define E1000_EEPROM_MAC_0 0x00000000
#define E1000_EEPROM_MAC_1 0x00000001
#define E1000_EEPROM_MAC_2 0x00000002

static uint16_t read_eeprom(uint8_t addr);
int64_t get_mac_from_eeprom();

#endif  // SOL >= 6
