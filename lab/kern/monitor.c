// Simple command-line kernel monitor useful for
// controlling the kernel and exploring the system interactively.

#include <inc/stdio.h>
#include <inc/string.h>
#include <inc/memlayout.h>
#include <inc/assert.h>
#include <inc/x86.h>

#include <kern/console.h>
#include <kern/env.h>
#include <kern/monitor.h>
#include <kern/kdebug.h>
#include <kern/trap.h>

#define CMDBUF_SIZE	80	// enough for one VGA text line


struct Command {
	const char *name;
	const char *desc;
	// return -1 to force monitor to exit
	int (*func)(int argc, char** argv, struct Trapframe* tf);
};

static struct Command commands[] = {
	{ "help", "Display this list of commands", mon_help },
	{ "kerninfo", "Display information about the kernel", mon_kerninfo },
	{ "backtrace", "call backtrace", mon_backtrace },
	{ "ple183", "play lab1 exercise 8.3", play_lab_1_exercise_8_3 },
	{ "ple184", "play lab1 exercise 8.4", play_lab_1_exercise_8_4 },
	{ "stepi", "play lab3 challenge following Exercise 6", mon_stepi },
};

/***** Implementations of basic kernel monitor commands *****/

int
mon_help(int argc, char **argv, struct Trapframe *tf)
{
	int i;

	for (i = 0; i < ARRAY_SIZE(commands); i++)
		cprintf("%s - %s\n", commands[i].name, commands[i].desc);
	return 0;
}

int
mon_kerninfo(int argc, char **argv, struct Trapframe *tf)
{
	extern char _start[], entry[], etext[], edata[], end[];

	cprintf("Special kernel symbols:\n");
	cprintf("  _start                  %08x (phys)\n", _start);
	cprintf("  entry  %08x (virt)  %08x (phys)\n", entry, entry - KERNBASE);
	cprintf("  etext  %08x (virt)  %08x (phys)\n", etext, etext - KERNBASE);
	cprintf("  edata  %08x (virt)  %08x (phys)\n", edata, edata - KERNBASE);
	cprintf("  end    %08x (virt)  %08x (phys)\n", end, end - KERNBASE);
	cprintf("Kernel executable memory footprint: %dKB\n",
		ROUNDUP(end - entry, 1024) / 1024);
	return 0;
}

int mon_backtrace(int argc, char **argv, struct Trapframe *tf) {
	uint32_t* pebp  = (uint32_t*) read_ebp();
	cprintf("Stack backtrace:\n");
	while (pebp) {
		cprintf("  ebp %08x  eip %08x  args %08x %08x %08x %08x %08x\n",
			pebp, pebp[1], pebp[2], pebp[3], pebp[4], pebp[5], pebp[6]);
		struct Eipdebuginfo info;
		if (debuginfo_eip(pebp[1], &info) == 0) cprintf("         %s:%d: %.*s+%d\n",  // example: kern/monitor.c:143: monitor+106
								info.eip_file, info.eip_line, info.eip_fn_namelen, info.eip_fn_name, pebp[1] - info.eip_fn_addr); // N.B.: eip == pebp[1] is containing the address of the next instruction after the function returns.
		else cprintf("ERROR: tracing function");
		pebp = (uint32_t *) (*pebp);
	}
	return 0;
}

int play_lab_1_exercise_8_3(int argc, char **argv, struct Trapframe *tf)
{
	int x = 1, y = 3, z = 4;
	cprintf("x %d, y %x, z %d\n", x, y, z);
	return 0;
}

int play_lab_1_exercise_8_4(int argc, char **argv, struct Trapframe *tf)
{
	unsigned int i = 0x00646c72;
	cprintf("H%x Wo%s", 57616, &i);
	return 0;
}

int mon_stepi(int argc, char **argv, struct Trapframe *tf) {
	if(tf == NULL){
		cprintf("cannot execute `stepi`. is there any breakpoint set?\n");
		return 0;
	}
	tf->tf_eflags |= FL_TF;
	cprintf("DEBUG> eip: \t%08x\n", tf->tf_eip);
//	tf->tf_eflags &= ~FL_TF;
	env_run(curenv);
	return 0;
}

/***** Kernel monitor command interpreter *****/

#define WHITESPACE "\t\r\n "
#define MAXARGS 16

static int
runcmd(char *buf, struct Trapframe *tf)
{
	int argc;
	char *argv[MAXARGS];
	int i;

	// Parse the command buffer into whitespace-separated arguments
	argc = 0;
	argv[argc] = 0;
	while (1) {
		// gobble whitespace
		while (*buf && strchr(WHITESPACE, *buf))
			*buf++ = 0;
		if (*buf == 0)
			break;

		// save and scan past next arg
		if (argc == MAXARGS-1) {
			cprintf("Too many arguments (max %d)\n", MAXARGS);
			return 0;
		}
		argv[argc++] = buf;
		while (*buf && !strchr(WHITESPACE, *buf))
			buf++;
	}
	argv[argc] = 0;

	// Lookup and invoke the command
	if (argc == 0)
		return 0;
	for (i = 0; i < ARRAY_SIZE(commands); i++) {
		if (strcmp(argv[0], commands[i].name) == 0)
			return commands[i].func(argc, argv, tf);
	}
	cprintf("Unknown command '%s'\n", argv[0]);
	return 0;
}

void
monitor(struct Trapframe *tf)
{
	char *buf;

	cprintf("Welcome to the JOS kernel monitor!\n");
	cprintf("Type 'help' for a list of commands.\n");

	if (tf != NULL)
		print_trapframe(tf);

	while (1) {
		buf = readline("K> ");
		if (buf != NULL)
			if (runcmd(buf, tf) < 0)
				break;
	}
}
