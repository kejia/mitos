// implement fork from user space

#include <inc/string.h>
#include <inc/lib.h>

// PTE_COW marks copy-on-write page table entries.
// It is one of the bits explicitly allocated to user processes (PTE_AVAIL).
#define PTE_COW		0x800

//
// Custom page fault handler - if faulting page is copy-on-write,
// map in our own private writable copy.
//
static void pgfault(struct UTrapframe *utf) {
	void *addr = (void *) utf->utf_fault_va;
	uint32_t err = utf->utf_err;
	int r;
	
	// Check that the faulting access was (1) a write, and (2) a
	// copy-on-write page.  If not, panic.
	// Hint:
	//   Use the read-only page table mappings at uvpt
	//   (see <inc/memlayout.h>).

	// LAB 4: Your code here.
	pte_t pte = uvpt[PGNUM(addr)];
	if (((err & FEC_WR) == 0) ||
	    ((pte & PTE_COW) == 0)) panic("the faulting access is not write or copy-on-write");
	
	// Allocate a new page, map it at a temporary location (PFTEMP),
	// copy the data from the old page to the new page, then move the new
	// page to the old page's address.
	// Hint:
	//   You should make three system calls.

	// LAB 4: Your code here.
	envid_t envid = sys_getenvid();
	r = sys_page_alloc(envid, PFTEMP, PTE_W | PTE_U | PTE_P);
	if (r != 0) panic("failed to allocate page for a child");
	memcpy(PFTEMP, ROUNDDOWN(addr, PGSIZE), PGSIZE);
	r = sys_page_map(envid, PFTEMP, envid, ROUNDDOWN(addr, PGSIZE), PTE_W | PTE_U | PTE_P);
	if (r != 0) panic("failed to copy data to the new page for a child");
	r = sys_page_unmap(envid, PFTEMP);
	if (r != 0) panic("failed to unmap temp location");
}

//
// Map our virtual page pn (address pn*PGSIZE) into the target envid
// at the same virtual address.  If the page is writable or copy-on-write,
// the new mapping must be created copy-on-write, and then our mapping must be
// marked copy-on-write as well.
//
// Returns: 0 on success, < 0 on error.
// It is also OK to panic on error.
//
static int duppage(envid_t envid, unsigned pn) {
	int r;
	envid_t envid_parent = sys_getenvid();
	void *va = (void *)(pn * PGSIZE);
	if ((uvpt[pn] & PTE_SHARE) == PTE_SHARE) { // copy the mapping directly
		r = sys_page_map(envid_parent, va, envid, va, uvpt[pn] & PTE_SYSCALL);
		if (r != 0) panic("ERROR: failed to copy the mapping directly");
	} else 	if (((uvpt[pn] & PTE_W) == PTE_W)
		    || ((uvpt[pn] & PTE_COW) == PTE_COW)) { // the page is writable or copy-on-write
		r = sys_page_map(envid_parent, va, envid, va, PTE_COW | PTE_U | PTE_P);
		if (r != 0) panic("failed to map copy-on-write page for a child");
		r = sys_page_map(envid_parent, va, envid_parent, va, PTE_COW | PTE_U | PTE_P); // the parent page is marked copy-on-write
		if (r != 0) panic("failed to set parent page copy-on-write");
	} else {
		r = sys_page_map(envid_parent, va, envid, va, PTE_U | PTE_P);
		if (r != 0) panic("failed to map page for a child");
	}
	return 0;
}

//
// User-level fork with copy-on-write.
// Set up our page fault handler appropriately.
// Create a child.
// Copy our address space and page fault handler setup to the child.
// Then mark the child as runnable and return.
//
// Returns: child's envid to the parent, 0 to the child, < 0 on error.
// It is also OK to panic on error.
//
// Hint:
//   Use uvpd, uvpt, and duppage.
//   Remember to fix "thisenv" in the child process.
//   Neither user exception stack should ever be marked copy-on-write,
//   so you must allocate a new page for the child's user exception stack.
//
envid_t fork(void) {
	// LAB 4: Your code here.
	set_pgfault_handler(pgfault);
	envid_t envid_child = sys_exofork();
	if (envid_child < 0) panic("failed to fork");
	else if (envid_child == 0) {
		thisenv = &envs[ENVX(sys_getenvid())];
		return 0;
	}
	// map the address space for the child
	uint32_t addr;
	for (addr = 0; addr < USTACKTOP; addr += PGSIZE)
		if (((uvpd[PDX(addr)] & PTE_P) == PTE_P) &&
		    ((uvpt[PGNUM(addr)] & PTE_P) == PTE_P))
			duppage(envid_child, PGNUM(addr));

	// allocate a new page for the child's user exception stack
	void _pgfault_upcall();
	int return_code = sys_page_alloc(envid_child, (void *)(UXSTACKTOP - PGSIZE), PTE_W | PTE_U | PTE_P);
	if (return_code != 0) panic("failed to allocate exeption stack for the child");
	return_code = sys_env_set_pgfault_upcall(envid_child, _pgfault_upcall);
	if (return_code != 0) panic("failed to hook user-level page-fault handler for the child");

	// mark child RUNNABLE
	return_code = sys_env_set_status(envid_child, ENV_RUNNABLE);
	if (return_code != 0) panic("failed to set the child RUNNABLE");
	
	return envid_child;
}

// Challenge!
int
sfork(void)
{
	panic("sfork not implemented");
	return -E_INVAL;
}
