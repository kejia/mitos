# mitos

**http://gitlab.com/kejia/mitos**
**http://app.vagrantup.com/WIZARDELF/boxes/mitos**

This is for the [MIT 6.828 Operating System Engineering](http://pdos.csail.mit.edu/6.828/2018/schedule.html) course work around 2020.

Vagrant is used in this project. The vagrant box is based on `ubuntu/xenial32` and has the essential dev environment deployed. The easiest way to use it is retrieving the box from [Vagrant Cloud](http://app.vagrantup.com/WIZARDELF/boxes/mitos):
```bash
$ vagrant init WIZARDELF/mitos \
  --box-version 1.0.0
$ vagrant up
```

# Init

## Make the Project Dir

```bash
$ mkdir mitos
$ cd mitos
```

## Create Vagrant Box

```bash
$ vagrant init ubuntu/xenial32 \
  --box-version 20200822.0.0
```

Edit the `Vagrantfile` file to set the sync dir:
```ruby
config.vm.synced_folder ".", "/home/vagrant/mitos"
```

```bash
$ vagrant up
$ vagrant ssh
```

## Prepare Tools

How to setup the essentials tools for this course is presented in [Tools Used in 6.828](https://pdos.csail.mit.edu/6.828/2018/tools.html).

The following operations are executed in the vagrant box:

```bash
$ sudo apt update
$ sudo apt upgrade
$ sudo apt install -y binutils gcc build-essential gdb
$ sudo apt install -y pkg-config libsdl1.2-dev libtool-bin libglib2.0-dev libz-dev libpixman-1-dev
$ sudo apt install -y python
```

Make the qemu dir:
```bash
$ mkdir qemu
$ cd qemu
$ mkdir dist
```
Retrieve the course-specific qemu:
```bash
$ git clone https://github.com/mit-pdos/6.828-qemu.git source
```

Configure and make qemu:
```bash
$ cd source
$ ./configure --disable-kvm --disable-werror --prefix=/home/vagrant/mitos/qemu/dist --target-list="i386-softmmu"
$ make && make install
```

## Save the Box

It is time to save the vagrant box:
```bash
$ vagrant package
```

# Lab 1

## Lab 1 Specification

See [Lab 1 specification](http://pdos.csail.mit.edu/6.828/2018/labs/lab1/).

### Retrieve Lab 1

```bash
$ cd ~/mitos
$ git clone https://pdos.csail.mit.edu/6.828/2018/jos.git lab
$ cd lab
```

## Lab 1 Solution

See [Lab 1 solution](doc/lab_1.md).

## Specify Qemu Path

In the Vagrant box, update the `QEMU` env var of the script `lab/conf/env.mk`:
```bash
QEMU=/home/vagrant/mitos/qemu/dist/bin/qemu-system-i386
```

## Make the Skeletal JOS Kernel

This box does not have VGA installed, so make without VGA:
```bash
$ make qemu-nox
```

Now the JOS kernel should be booted. Type `Ctrl-a x` to quit.

# Homework 1

## Homework 1 Specification

See [Homework 1 specification](http://pdos.csail.mit.edu/6.828/2018/homework/xv6-boot.html).

## Homework 1 Solution

See [Homework 1 solution](doc/homework_1.md).

# Homework 2

## Homework 2 Specification

See [Homework 2 specification](http://pdos.csail.mit.edu/6.828/2018/homework/xv6-shell.html).

## Homework 2 Solution

See [Homework 2 solution](doc/homework_shell.md).

# Lab 2

## Lab 2 Specification

See [Lab 2 specification](https://pdos.csail.mit.edu/6.828/2018/labs/lab2).

## Lab 2 Solution

See [Lab 2 solution](doc/lab_2.md).

# Homework 3

## Homework 3 Specification

See [Homework 3 specification](http://pdos.csail.mit.edu/6.828/2018/homework/xv6-syscall.html).

## Homework 3 Solution

See [Homework 3 solution](doc/homework_3_system_call.md).

# Homework 4

## Homework 4 Specification

See [Homework 4 specification](http://pdos.csail.mit.edu/6.828/2018/homework/xv6-zero-fill.html).

## Homework 4 Solution

See [Homework 4 solution](doc/homework_4_xv6_lazy_page_allocation.md).

# Homework 5

## Homework 5 Specification

See [Homework 5 specification](http://pdos.csail.mit.edu/6.828/2018/homework/xv6-alarm.html).

## Homework 5 Solution

See [Homework 5 solution](doc/homework_5_cpu_alarm.md).

# Lab 3

## Lab 3 Specification

See [Lab 3 specification](http://pdos.csail.mit.edu/6.828/2018/labs/lab3/).

## Lab 3 Solution

See [Lab 3 solution](doc/lab_3.md).

# Homework 6

## Homework 6 Specification

See [Homework 6 Threads and Locking](http://pdos.csail.mit.edu/6.828/2018/homework/lock.html).

## Homework 6 Solution

See [Homework 6 solution](doc/homework_6_threads_and_locking.md).

# Homework 7

## Homework 7 Specification

See [Homework 7 xv6 Locking](http://pdos.csail.mit.edu/6.828/2018/homework/xv6-lock.html).

## Homework 7 Solution

See [Homework 7 solution](doc/hhomework_7_xv6_locking.md).

# Lab 4

## Lab 4 Specification

See [Lab 4 specification](http://pdos.csail.mit.edu/6.828/2018/labs/lab4/).

## Lab 4 Solution

See [Lab 4 solution](doc/lab_4.md).

# Homework 8

## Homework 8 Specification

See [Homework 8 User-level Threads](http://pdos.csail.mit.edu/6.828/2018/homework/xv6-uthread.html).

## Homework 8 Solution

See [Homework 8 solution](doc/homework_8_user-level_threads.md).

# Homework 9

## Homework 9 Specification

See [Homework 9 Barriers](http://pdos.csail.mit.edu/6.828/2018/homework/barrier.html).

## Homework 9 Solution

See [Homework 9 solution](doc/homework_9_barriers.md).

# Homework 10

## Homework 10 Specification

See [Homework 10 Bigger Files for xv6](http://pdos.csail.mit.edu/6.828/2018/homework/xv6-big-files.html).

## Homework 10 Solution

See [Homework 10 solution](doc/homework_10_bigger_files_for_xv6.md).

# Homework 11

## Homework 11 Specification

See [Homework 11 xv6 Log](http://pdos.csail.mit.edu/6.828/2018/homework/xv6-new-log.html).

## Homework 11 Solution

See [Homework 11 solution](doc/homework_11_xv6_log.md).

# Lab 5

## Lab 5 Specification

See [Lab 5 specification](http://pdos.csail.mit.edu/6.828/2018/labs/lab5/).

## Lab 5 Solution

See [Lab 5 solution](doc/lab_5.md).

# Homework 12

## Homework 12 Specification

See [Homework 12 mmap](http://pdos.csail.mit.edu/6.828/2018/homework/mmap.html).

## Homework 12 Solution

See [Homework 12 solution](doc/homework_12_mmap.md).

# Lab 6

## Lab 6 Specification

See [Lab 6 specification](http://pdos.csail.mit.edu/6.828/2018/labs/lab6/).

## Lab 6 Solution

See [Lab 6 solution](doc/lab_6.md).
