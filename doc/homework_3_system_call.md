
[Homework 3](http://pdos.csail.mit.edu/6.828/2018/homework/xv6-syscall.html)

# Homework: xv6 System Calls

## Part One: System Call Tracing

The first task is to modify the xv6 kernel to print out a line for each system call invocation:
```shell
	⋮
fork -> 2
exec -> 0
open -> 3
close -> 0
$write -> 1
 write -> 1
```

Modify the file `homework/xv6-public/syscall.c`: add the following array:
```c
static char* syscalls_name[] = {
[SYS_fork]    "fork",
[SYS_exit]    "exit",
[SYS_wait]    "wait",
[SYS_pipe]    "pipe",
[SYS_read]    "read",
[SYS_kill]    "kill",
[SYS_exec]    "exec",
[SYS_fstat]   "fstat",
[SYS_chdir]   "chdir",
[SYS_dup]     "dup",
[SYS_getpid]  "getpid",
[SYS_sbrk]    "sbrk",
[SYS_sleep]   "sleep",
[SYS_uptime]  "uptime",
[SYS_open]    "open",
[SYS_write]   "write",
[SYS_mknod]   "mknod",
[SYS_unlink]  "unlink",
[SYS_link]    "link",
[SYS_mkdir]   "mkdir",
[SYS_close]   "close",
};
```
This is a way to initialize c array, e.g. `int a[6] = { [4] = 29, [2] = 15 };`, and `=` is allowed between index and value. Here, the index is defined in `homework/xv6-public/syscall.h`. See [Designated Initializers of c99](http://gcc.gnu.org/onlinedocs/gcc/Designated-Inits.html).

Also, modify the `syscall` function:
```c
void syscall(void) {
  int num;
  struct proc *curproc = myproc();
  num = curproc->tf->eax;
  if(num > 0 && num < NELEM(syscalls) && syscalls[num]) {
    curproc->tf->eax = syscalls[num]();
    cprintf("%s -> %d\n", syscalls_name[num], curproc->tf->eax);
  } else {
    cprintf("%d %s: unknown sys call %d\n",
            curproc->pid, curproc->name, num);
    curproc->tf->eax = -1;
  }
}
```
One line is added: `cprintf("%s -> %d\n", syscalls_name[num], curproc->tf->eax);`.

In the directory `homework/xv6-public` run the following command to start the modified xv6 shell:
```shell
$ make clean && make qemu-nox
```

## Part Two: Date System Call

This task requires to implement the system call `date`. The source changes should be similar to the system call `uptime`:
```shell
$ ag uptime homework/xv6-public/

homework/xv6-public/sysproc.c
83:sys_uptime(void)

homework/xv6-public/usys.S
31:SYSCALL(uptime)

homework/xv6-public/syscall.c
105:extern int sys_uptime(void);
122:[SYS_uptime]  sys_uptime,

homework/xv6-public/user.h
25:int uptime(void);

homework/xv6-public/syscall.h
15:#define SYS_uptime 14
```
Do the same work as `uptime`:
```diff
diff --git a/homework/xv6-public/syscall.c b/homework/xv6-public/syscall.c
index ee85261..488126e 100644
--- a/homework/xv6-public/syscall.c
+++ b/homework/xv6-public/syscall.c
@@ -103,6 +103,7 @@ extern int sys_unlink(void);
 extern int sys_wait(void);
 extern int sys_write(void);
 extern int sys_uptime(void);
+extern int sys_date(void);

 static int (*syscalls[])(void) = {
 [SYS_fork]    sys_fork,
@@ -119,6 +120,7 @@ static int (*syscalls[])(void) = {
 [SYS_sbrk]    sys_sbrk,
 [SYS_sleep]   sys_sleep,
 [SYS_uptime]  sys_uptime,
+[SYS_date]    sys_date,
 [SYS_open]    sys_open,
 [SYS_write]   sys_write,
 [SYS_mknod]   sys_mknod,
diff --git a/homework/xv6-public/syscall.h b/homework/xv6-public/syscall.h
index bc5f356..1a620b9 100644
--- a/homework/xv6-public/syscall.h
+++ b/homework/xv6-public/syscall.h
@@ -20,3 +20,4 @@
 #define SYS_link   19
 #define SYS_mkdir  20
 #define SYS_close  21
+#define SYS_date   22
diff --git a/homework/xv6-public/sysproc.c b/homework/xv6-public/sysproc.c
index 0686d29..7249b56 100644
--- a/homework/xv6-public/sysproc.c
+++ b/homework/xv6-public/sysproc.c
@@ -89,3 +89,11 @@ sys_uptime(void)
   release(&tickslock);
   return xticks;
 }
+
+// for the system call `date'
+int sys_date(void) {
+  struct rtcdate* r;
+  if (argptr(0, (char**)&r, sizeof(*r)) < 0) return -1; // `argptr' fetches the first argument to the system call `date', and then `r' points at it.
+  cmostime(r);
+  return 0;
+}
diff --git a/homework/xv6-public/user.h b/homework/xv6-public/user.h
index 4f99c52..3e791dd 100644
--- a/homework/xv6-public/user.h
+++ b/homework/xv6-public/user.h
@@ -23,6 +23,7 @@ int getpid(void);
 char* sbrk(int);
 int sleep(int);
 int uptime(void);
+int date(struct rtcdate*);

 // ulib.c
 int stat(const char*, struct stat*);
diff --git a/homework/xv6-public/usys.S b/homework/xv6-public/usys.S
index 8bfd8a1..ba76d54 100644
--- a/homework/xv6-public/usys.S
+++ b/homework/xv6-public/usys.S
@@ -29,3 +29,4 @@ SYSCALL(getpid)
 SYSCALL(sbrk)
 SYSCALL(sleep)
 SYSCALL(uptime)
+SYSCALL(date)
```
The change in `sysproc.c` is as follows:
```c
// for the system call `date'
int sys_date(void) {
  struct rtcdate* r;
  if (argptr(0, (char**)&r, sizeof(*r)) < 0) return -1; // `argptr' fetches the first argument to the system call `date', and then `r' points at it.
  cmostime(r);
  return 0;
}
```
The entry of `date` is as follows:
```c
#include "types.h"
#include "user.h"
#include "date.h"

int main(int argc, char* argv[]) {
  struct rtcdate r;
  if (date(&r)) {
    printf(2, "date failed\n");
    exit();
  }
  printf(1, "UTC %d-%d-%d %d:%d:%d\n", r.year, r.month, r.day, r.hour, r.minute, r.second);
  exit();
}
```

The `SYSCALL` macro is defined in `usys.S`:
```c
define SYSCALL(name) .globl name; name: movl $SYS_ ## name, %eax; int $T_SYSCALL; ret
```
Thus, `SYSCALL(date)` is expanded to:
```x86asm
.globl date; date: movl $SYS_date, %eax; int $T_SYSCALL; ret
```

In the `date` entry, when `date(&r)` is executed, the address of `r` is pushed to the stack, and then `int` operation calls `sys_date`. In `sys_date`, `argptr` gets the first argument `&r` of `date(&r)` from the stack and assigns it to the local `struct rtcdate* r`, and then `cmostime(r)` is called to set `r`.

The statement on the function `argprt` can be found in the [xv6 Manual](http://pdos.csail.mit.edu/6.828/2018/xv6/book-rev11.pdf):
>argptr fetches thenth system call argument and checks that this argument is avalid user-space pointer. Note that two checks occur during a call toargptr. First, the user stack pointer is checked during the fetching of the argument. Then the argument, itself a user pointer, is checked.

In the directory `homework/xv6-public` run the command to start the xv6 shell:
```shell
$ make clean && make qemu-nox
```
In xv6 shell run `date`:
```shell
$ date
UTC 2020-11-12 23:29:55
```
