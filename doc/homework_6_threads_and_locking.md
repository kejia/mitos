
[Homework 6](http://pdos.csail.mit.edu/6.828/2018/homework/lock.html)

# Homework: Threads and Locking

In this module, the program maintains a hash-table `table` as follows:
```c
struct entry {
  int key;
  int value;
  struct entry *next;
};
struct entry *table[NBUCKET];
```
The `table` has `NBUCKET` buckets to handle the hashing-collision. `insert` always add a new `entry` to the head position:
```c
static void insert(int key, int value, struct entry **p, struct entry *n) {
  struct entry *e = malloc(sizeof(struct entry));
  e->key = key;
  e->value = value;
  e->next = n;
  *p = e;
}
```

Assuming `table[i]` has only one `entry`, the memory layout looks as follows:

![table[2] layout](../_resources/1991e06e3b524e8484a2ab7fb19abc10.png)

Here, `&table[i] = 100`.

The `get` function may return `0` if fails to look up the specified `key`.

The `put` function is defined as follows:
```c
static void put(int key, int value) {
  int i = key % NBUCKET;
  insert(key, value, &table[i], table[i]);
}
```

At the same time, there may be multiple threads call the `put`. Build the program and execute it with `1` thread:
```shell
$ gcc -g -O2 ph.c -pthread
$ ./a.out 1
0: put time = 0.006481
0: get time = 3.779566
0: 0 keys missing
completion time = 3.786202
```
Everything looks good. Now execute the program with `2` threads:
```shell
$ ./a.out 2
1: put time = 0.004580
0: put time = 0.004603
1: get time = 3.973064
1: 16846 keys missing
0: get time = 3.984983
0: 16846 keys missing
completion time = 3.989790
```
With the same `key` set, the `get` failed `16846` times. That is caused by concurrent `put` operations. For example, thread 1 allocates a new `entry` pointer `300`, thread 2 allocates a new `entry` pointer `400`, both have the `next` pointing to the existing `entry` pointer `200`, and either of them has not executed `*p = e` yet. The memory layout for now looks as follows:

![two new entries point next to 200](../_resources/43036fcd654a46d2ac2b4facd19667d2.png)

This is possible because the whole execution of the function `put` is not atomic and the new `entry`s are being inserted into `table[0]` (given that `NBUCKET = 5`). In the subsequent operations, thread 1 firstly executes `*p = e`, and the memory layout looks as follows:

![table[0] points to 300](../_resources/7463060c4b0e4e4aba1fc6730b3e59e6.png)

Here, `p = &table[0] = 100` and `*p = table[0] = 300`. Then, thread 2 secondly executes `*p = e`, and the memory layout looks as follows:

![table[0] points to 400](../_resources/4e38b7fd150d4c0288a69d45fa0a9adc.png)

Here, `p = &table[0] = 100` and `*p = table[0] = 400`. That is to say the new `entry` at `300` created by thread 1 is lost.

The solution to fixing the bug is adding a spin-lock arount the `insert` in the `put` function so that the `insert` operation becomes atomic:
```diff
git diff                                                                                                                                                                                                                                                                         16:34:24
diff --git a/homework/ph.c b/homework/ph.c
index c9c34fc..42492c9 100644
--- a/homework/ph.c
+++ b/homework/ph.c
@@ -9,6 +9,8 @@
 #define NBUCKET 5
 #define NKEYS 100000

+pthread_mutex_t lock;
+
 struct entry {
   int key;
   int value;
@@ -56,7 +58,9 @@ static
 void put(int key, int value)
 {
   int i = key % NBUCKET;
+  pthread_mutex_lock(&lock);
   insert(key, value, &table[i], table[i]);
+  pthread_mutex_unlock(&lock);
 }

 static struct entry*
@@ -110,6 +114,8 @@ main(int argc, char *argv[])
   long i;
   double t1, t0;

+  pthread_mutex_init(&lock, NULL);
+
   if (argc < 2) {
     fprintf(stderr, "%s: %s nthread\n", argv[0], argv[0]);
     exit(-1);
```

`get` does not write the `table`, so it is unnecessary to add a lock on it. Now everything goes well:
```shell
$ ./a.out 2
1: put time = 0.015716
0: put time = 0.017148
1: get time = 3.808014
1: 0 keys missing
0: get time = 3.814711
0: 0 keys missing
completion time = 3.831996
```

It shows that the spin-lock ensures both the parallelism and the performance.

✌️
