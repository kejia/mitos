
[Homework 1](http://pdos.csail.mit.edu/6.828/2018/homework/xv6-boot.html)

# Finding and Breaking at an Address

Find the entry address of the kernel, which is the address of `_start`:
```shell
$ make
	⋮
$ nm kernel | grep _start
8010a48c D _binary_entryother_start
8010a460 D _binary_initcode_start
0010000c T _start
```
The command `nm` is used to display information from object files.

Before proceeding the next step, users of this Vagrant work need to specify QEMU path in `mitos/homework/xv6-public/Makefile`:
```shell
QEMU = /home/vagrant/mitos/qemu/dist/bin/qemu-system-i386
```
This path points to QEMU built in Lab 1.

Run the kernel in QEMU-GDB:
```shell
$ make qemu-nox-gdb
```
To make GDB work, the users need to add the following line to the file `/home/vagrant/.gdbinit`:
```plaintext
set auto-load safe-path /home/vagrant/mitos/
```
Then, in another terminal, run GDB, and set a breakpoint at `_start`:
```shell
$ gdb
	⋮
(gdb) b *0x0010000c
Breakpoint 1 at 0x10000c
(gdb) c
Continuing.
The target architecture is assumed to be i386
=> 0x10000c:	mov    %cr4,%eax

Thread 1 hit Breakpoint 1, 0x0010000c in ?? ()
(gdb)
```

# Exercise: What Is on the Stack?

> While stopped at the above breakpoint, look at the registers and the stack contents:
> ```shell
> (gdb) info reg
> 	⋮
> (gdb) x/24x $esp
> 	⋮
> (gdb)
> ```
>
> Write a short (3-5 word) comment next to each non-zero value on the stack explaining what it is. Which part of the stack printout is actually the stack?

- Begin by restarting qemu and gdb, and set a break-point at `0x7c00`, the start of the boot block (bootasm.S). Single step through the instructions (type si at the gdb prompt). Where in bootasm.S is the stack pointer initialized? (Single step until you see an instruction that moves a value into %esp, the register for the stack pointer.)
	- In *bootasm.S*, the stack pointer is initialized by `movl $start, %esp`, which is followed by `call bootmain`. The debug info is as the following:
	```shell
		⋮
	(gdb) b *0x7c00
		⋮
	(gdb) c
		⋮
	(gdb) si
	=> 0x7c43:	mov    $0x7c00,%esp
	0x00007c43 in ?? ()
	(gdb) info reg
	eax            0x0	0
	ecx            0x0	0
	edx            0x80	128
	ebx            0x0	0
	esp            0x6f20	0x6f20
	ebp            0x0	0x0
	esi            0x0	0
	edi            0x0	0
	eip            0x7c43	0x7c43
	eflags         0x6	[ PF ]
	cs             0x8	8
	ss             0x10	16
	ds             0x10	16
	es             0x10	16
	fs             0x0	0
	gs             0x0	0
	(gdb) si
	=> 0x7c48:	call   0x7d3b
	0x00007c48 in ?? ()
	(gdb) info reg
	eax            0x0	0
	ecx            0x0	0
	edx            0x80	128
	ebx            0x0	0
	esp            0x7c00	0x7c00
	ebp            0x0	0x0
	esi            0x0	0
	edi            0x0	0
	eip            0x7c48	0x7c48
	eflags         0x6	[ PF ]
	cs             0x8	8
	ss             0x10	16
	ds             0x10	16
	es             0x10	16
	fs             0x0	0
	gs             0x0	0
	(gdb)
	```
- Single step through the `call` to bootmain; what is on the stack now? 
	- The `call` instruction pushes to the stack the return address of the calling, `0x00007c4d`:
	```shell
		⋮
	(gdb) si
	=> 0x7d3b:	push   %ebp
	(gdb) info reg
	eax            0x0	0
	ecx            0x0	0
	edx            0x80	128
	ebx            0x0	0
	esp            0x7bfc	0x7bfc
	ebp            0x0	0x0
	esi            0x0	0
	edi            0x0	0
	eip            0x7d3b	0x7d3b
	eflags         0x6	[ PF ]
	cs             0x8	8
	ss             0x10	16
	ds             0x10	16
	es             0x10	16
	fs             0x0	0
	gs             0x0	0
	(gdb) x/x $esp
	0x7bfc:	0x00007c4d
	```
	The stack pointer points to `0x7bfc` between which and the stack botton `0x7c00` there are `4` bytes:
	```shell
	(gdb) x/x $esp
	0x7bfc:	0x00007c4d
	(gdb) x/1b $esp
	0x7bfc:	0x4d
	```
	According to the GCC calling conventions, `%esp` points at the return address of the call, so it is `0x00007c4d` on the stack now.
- What do the first assembly instructions of bootmain do to the stack? Look for bootmain in bootblock.asm.
	- Check the code of `bootblock.asm`:
	```x86asm
	bootmain(void)
	{
		7d3b:	55                   	push   %ebp
		7d3c:	89 e5                	mov    %esp,%ebp
		7d3e:	57                   	push   %edi
			⋮
	```
- Continue tracing via gdb (using breakpoints if necessary -- see hint below) and look for the call that changes eip to 0x10000c. What does that call do to the stack? (Hint: Think about what this call is trying to accomplish in the boot sequence and try to identify this point in bootmain.c, and the corresponding instruction in the bootmain code in bootblock.asm. This might help you set suitable breakpoints to speed things up.) 
	- Recall the GCC calling convention: `%eip` is updated when a function is called or when a function call is returned. Thus, set breakpoints at function calls in the `bootmain` function. It shows that when `entry()` is executed (at `0x7dae` by checking `bootblock.asm`), `%eip` is set to `0x10000c`:
	```shell
	(gdb) b *0x7dae
	Breakpoint 2 at 0x7dae
	(gdb) c
	Continuing.
	=> 0x7dae:	call   *0x10018

	Thread 1 hit Breakpoint 2, 0x00007dae in ?? ()
	(gdb) info reg
	eax            0x0	0
	ecx            0x0	0
	edx            0x1f0	496
	ebx            0x10094	65684
	esp            0x7be0	0x7be0
	ebp            0x7bf8	0x7bf8
	esi            0x10094	65684
	edi            0x0	0
	eip            0x7dae	0x7dae
	eflags         0x46	[ PF ZF ]
	cs             0x8	8
	ss             0x10	16
	ds             0x10	16
	es             0x10	16
	fs             0x0	0
	gs             0x0	0
	(gdb) si
	=> 0x10000c:	mov    %cr4,%eax
	0x0010000c in ?? ()
	(gdb) info reg
	eax            0x0	0
	ecx            0x0	0
	edx            0x1f0	496
	ebx            0x10094	65684
	esp            0x7bdc	0x7bdc
	ebp            0x7bf8	0x7bf8
	esi            0x10094	65684
	edi            0x0	0
	eip            0x10000c	0x10000c
	eflags         0x46	[ PF ZF ]
	cs             0x8	8
	ss             0x10	16
	ds             0x10	16
	es             0x10	16
	fs             0x0	0
	gs             0x0	0
	(gdb)
	```
	The stack pointer changes from `0x7be0` to `0x7bdc` because the call return address `0x00007db4` is pushed to the stack:
	```shell
	(gdb) x/wx $esp
	0x7bdc:	0x00007db4
	```
- Besed on the above analysis, the homework questions is answered. The stack starts at `0x7c00` when the breakpoint is hit:
```shell
(gdb) b *0x0010000c
Breakpoint 1 at 0x10000c
(gdb) c
Continuing.
The target architecture is assumed to be i386
=> 0x10000c:	mov    %cr4,%eax

Thread 1 hit Breakpoint 1, 0x0010000c in ?? ()
(gdb) info reg
eax            0x0	0
ecx            0x0	0
edx            0x1f0	496
ebx            0x10094	65684
esp            0x7bdc	0x7bdc
ebp            0x7bf8	0x7bf8
esi            0x10094	65684
edi            0x0	0
eip            0x10000c	0x10000c
eflags         0x46	[ PF ZF ]
cs             0x8	8
ss             0x10	16
ds             0x10	16
es             0x10	16
fs             0x0	0
gs             0x0	0
(gdb) x/24x $esp
0x7bdc:	0x00007db4	0x00000000	0x00000000	0x00000000
0x7bec:	0x00000000	0x00000000	0x00000000	0x00000000
0x7bfc:	0x00007c4d	0x8ec031fa	0x8ec08ed8	0xa864e4d0
0x7c0c:	0xb0fa7502	0xe464e6d1	0x7502a864	0xe6dfb0fa
0x7c1c:	0x16010f60	0x200f7c78	0xc88366c0	0xc0220f01
0x7c2c:	0x087c31ea	0x10b86600	0x8ed88e00	0x66d08ec0
(gdb)
```
Now the stack pointer `%esp` points at `0x7bdc` (exclusion), so the contents between `0x7bdc` (inclusion) and `0x7c00` (exclusion) are stack ones:
```x86asm
0x7bdc:	0x00007db4 # the return address of the function call `entry()`
0x7be0:	0x00000000
0x7be4:	0x00000000
0x7be8:	0x00000000
0x7bec:	0x00000000
0x7bf0:	0x00000000
0x7bf4:	0x00000000
0x7bf8:	0x00000000
0x7bfc:	0x00007c4d # the return address of the function call `bootmain`
```
N.B.: `0x7c00` is not part of the stack.

✌️
