
[Homework 10](http://pdos.csail.mit.edu/6.828/2018/homework/xv6-big-files.html)

# Preliminaries

Prepare as instructed:

```shell
git diff                                                                                                                                                            18:24:49
diff --git a/homework/xv6-public/Makefile b/homework/xv6-public/Makefile
index 1849799..7f95b47 100644
--- a/homework/xv6-public/Makefile
+++ b/homework/xv6-public/Makefile
@@ -223,8 +223,9 @@ QEMUGDB = $(shell if $(QEMU) -help | grep -q '^-gdb'; \
        then echo "-gdb tcp::$(GDBPORT)"; \
        else echo "-s -p $(GDBPORT)"; fi)
 ifndef CPUS
-CPUS := 2
+CPUS := 1
 endif
+QEMUEXTRA = -snapshot
 QEMUOPTS = -drive file=fs.img,index=1,media=disk,format=raw -drive file=xv6.img,index=0,media=disk,format=raw -smp $(CPUS) -m 512 $(QEMUEXTRA)

 qemu: fs.img xv6.img
diff --git a/homework/xv6-public/param.h b/homework/xv6-public/param.h
index a7e90ef..fe38bfb 100644
--- a/homework/xv6-public/param.h
+++ b/homework/xv6-public/param.h
@@ -10,5 +10,5 @@
 #define MAXOPBLOCKS  10  // max # of blocks any FS op writes
 #define LOGSIZE      (MAXOPBLOCKS*3)  // max data blocks in on-disk log
 #define NBUF         (MAXOPBLOCKS*3)  // size of disk block cache
-#define FSSIZE       1000  // size of file system in blocks
+#define FSSIZE       20000  // size of file system in blocks
diff --git a/homework/xv6-public/Makefile b/homework/xv6-public/Makefile
index 7f95b47..10ec63f 100644
--- a/homework/xv6-public/Makefile
+++ b/homework/xv6-public/Makefile
@@ -170,7 +170,8 @@ mkfs: mkfs.c fs.h
 .PRECIOUS: %.o

 UPROGS=\
-  _cat\
+  _big\
+  _cat\
   _date\
   _echo\
```

Start xv6 in the directory *homework/xv6-public*:
```shell
$ make clean && make qemu-nox
    ⋮
$ big
.
wrote 140 sectors
done; ok
$
```

# Design

The inode data structure has the following layout in xv6:

![xv6 Inode Data Structure Layout](../_resources/8e1a5206f224445598658fb3f20f9053.png)

An inode contains the first `12` *direct* block numbers and `1` *singly-indirect* block number. The *singly-indirect* block number refers to a block that holds up to `128` more block numbers. So, here are `12 + 128 = 140` block numbers in total, `512` bytes for each and `71680` bytes totally.

In order to support `16523` block numbers (i.e. `8459776` bytes in total), this assignment requires to enlarge the total block numbers by making the following adjustment:

- `11` *direct* block numbers;
- `1` *singly-indirect* block number;
- `1` *doubly-indirect* block number.

The first two changes do not introduce essential modification and contribute `139` block numbers. The *doubly-indirect* block number is supposed to contribute the other `16384` block numbers. One way to achieve it is adopting the *two-layer design*, which is similar to the multiple-level `paging` technique.

One implementation may be as follows: the first layer of the *doubly-indirect* block number consists of `128` block numbers each of which contains `128` block numbers as the second layer. Thus, the *doubly-indirect* block number references to `128 * 128 = 16384` block numbers in total.

Therefore, the new inode data structure can support at most `11 + 128 + 16384 = 16523` block numbers. Its layout looks like the following:

![The New Inode Data Structure Layout](../_resources/9711ccd1df044e01b87d3e3ad45bdbe3.png)

So, three spots need to be changed:

- the sizes of *direct*, *singly-indirect*, and *doubly-indirect* block numbers;
- `struct inode` and `struct dinode`;
- handling the new *doubly-indirect* block numbers in `bmap`.

# Implementation

The modifications should be done in the files *homework/xv6-public/fs.h*, *homework/xv6-public/fs.c*, and *homework/xv6-public/file.h*:

```diff
diff --git a/homework/xv6-public/fs.h b/homework/xv6-public/fs.h
index 3214f1d..ffc7d5e 100644
--- a/homework/xv6-public/fs.h
+++ b/homework/xv6-public/fs.h
@@ -21,9 +21,10 @@ struct superblock {
   uint bmapstart;    // Block number of first free map block
 };
 
-#define NDIRECT 12
+#define NDIRECT 11
 #define NINDIRECT (BSIZE / sizeof(uint))
-#define MAXFILE (NDIRECT + NINDIRECT)
+#define NINDIRECTDOUBLY (NINDIRECT * NINDIRECT)
+#define MAXFILE (NDIRECT + NINDIRECT + NINDIRECTDOUBLY)
 
 // On-disk inode structure
 struct dinode {
@@ -32,7 +33,7 @@ struct dinode {
   short minor;          // Minor device number (T_DEV only)
   short nlink;          // Number of links to inode in file system
   uint size;            // Size of file (bytes)
-  uint addrs[NDIRECT+1];   // Data block addresses
+  uint addrs[NDIRECT+2];   // Data block addresses
 };
 
 // Inodes per block.
diff --git a/homework/xv6-public/file.h b/homework/xv6-public/file.h
index 0990c82..eeb87cf 100644
--- a/homework/xv6-public/file.h
+++ b/homework/xv6-public/file.h
@@ -22,7 +22,7 @@ struct inode {
   short minor;
   short nlink;
   uint size;
-  uint addrs[NDIRECT+1];
+  uint addrs[NDIRECT+2];
 };
 
 // table mapping major device number to
diff --git a/homework/xv6-public/fs.c b/homework/xv6-public/fs.c
index f77275f..d2793e8 100644
--- a/homework/xv6-public/fs.c
+++ b/homework/xv6-public/fs.c
@@ -396,6 +396,34 @@ bmap(struct inode *ip, uint bn)
     return addr;
   }
 
+  bn -= NINDIRECT;
+  if(bn < NINDIRECTDOUBLY) {
+    // load doubly-indirect block, allocating if necessary.
+    if((addr = ip->addrs[NINDIRECT + 1]) == 0)
+      ip->addrs[NINDIRECT + 1] = addr = balloc(ip->dev);
+
+    // handle layer one:
+    bp = bread(ip->dev, addr);
+    a = (uint*)bp->data;
+    uint offset_doubly_indirect_layer_one = bn / NINDIRECT;
+    if((addr = a[offset_doubly_indirect_layer_one]) == 0){
+      a[offset_doubly_indirect_layer_one] = addr = balloc(ip->dev);
+      log_write(bp);
+    }
+    brelse(bp);
+
+    // handle layer two:
+    bp = bread(ip->dev, addr);
+    a = (uint*)bp->data;
+    uint offset_doubly_indirect_layer_two = bn % NINDIRECT;
+    if((addr = a[offset_doubly_indirect_layer_two]) == 0){
+      a[offset_doubly_indirect_layer_two] = addr = balloc(ip->dev);
+      log_write(bp);
+    }
+    brelse(bp);
+    return addr;
+  }
+
   panic("bmap: out of range");
 }
```

The whole `bmap` function is implemented as follows:

```c
//PAGEBREAK!
// Inode content
//
// The content (data) associated with each inode is stored
// in blocks on the disk. The first NDIRECT block numbers
// are listed in ip->addrs[].  The next NINDIRECT blocks are
// listed in block ip->addrs[NDIRECT].

// Return the disk block address of the nth block in inode ip.
// If there is no such block, bmap allocates one.
static uint bmap(struct inode *ip, uint bn) {
  uint addr, *a;
  struct buf *bp;

  if(bn < NDIRECT){
    if((addr = ip->addrs[bn]) == 0)
      ip->addrs[bn] = addr = balloc(ip->dev);
    return addr;
  }
  bn -= NDIRECT;

  if(bn < NINDIRECT){
    // Load indirect block, allocating if necessary.
    if((addr = ip->addrs[NDIRECT]) == 0)
      ip->addrs[NDIRECT] = addr = balloc(ip->dev);
    bp = bread(ip->dev, addr);
    a = (uint*)bp->data;
    if((addr = a[bn]) == 0){
      a[bn] = addr = balloc(ip->dev);
      log_write(bp);
    }
    brelse(bp);
    return addr;
  }

  bn -= NINDIRECT;
  if(bn < NINDIRECTDOUBLY) {
    // load doubly-indirect block, allocating if necessary.
    if((addr = ip->addrs[NINDIRECT + 1]) == 0)
      ip->addrs[NINDIRECT + 1] = addr = balloc(ip->dev);

    // handle layer one:
    bp = bread(ip->dev, addr);
    a = (uint*)bp->data;
    uint offset_doubly_indirect_layer_one = bn / NINDIRECT;
    if((addr = a[offset_doubly_indirect_layer_one]) == 0){
      a[offset_doubly_indirect_layer_one] = addr = balloc(ip->dev);
      log_write(bp);
    }
    brelse(bp);

    // handle layer two:
    bp = bread(ip->dev, addr);
    a = (uint*)bp->data;
    uint offset_doubly_indirect_layer_two = bn % NINDIRECT;
    if((addr = a[offset_doubly_indirect_layer_two]) == 0){
      a[offset_doubly_indirect_layer_two] = addr = balloc(ip->dev);
      log_write(bp);
    }
    brelse(bp);
    return addr;
  }

  panic("bmap: out of range");
}
```

Verify the work in the directory *homework/xv6-public*:
```shell
$ make clean && make qemu-nox
    ⋮
xv6...
cpu0: starting 0
sb: size 20000 nblocks 19937 ninodes 200 nlog 30 logstart 2 inodestart 32 bmap start 58
init: starting sh
$ big
.....................................................................................................................................................................
wrote 16523 sectors
done; ok
$
```

✌️
