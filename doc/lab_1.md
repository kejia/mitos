
# Part 1: PC Bootstrap

## Address Layout

An **IA-32** *personal computer (PC)* is used in this cours. A PC's general address space layout is:
```
+------------------+  <- 0xFFFFFFFF (4GB)
|      32-bit      |
|  memory mapped   |
|     devices      |
|                  |
/\/\/\/\/\/\/\/\/\/\

/\/\/\/\/\/\/\/\/\/\
|                  |
|      Unused      |
|                  |
+------------------+  <- depends on amount of RAM
|                  |
|                  |
| Extended Memory  |
|                  |
|                  |
+------------------+  <- 0x00100000 (1MB)
|     BIOS ROM     |
+------------------+  <- 0x000F0000 (960KB)
|  16-bit devices, |
|  expansion ROMs  |
+------------------+  <- 0x000C0000 (768KB)
|   VGA Display    |
+------------------+  <- 0x000A0000 (640KB)
|                  |
|    Low Memory    |
|                  |
+------------------+  <- 0x00000000
```

## GDB Disassembly

The first instruction is:
```x86asm
[f000:fff0] 0xffff0:	ljmp   $0xf000,$0xe05b
```

`[f000:fff0] 0xffff0` indicates the address of the instruction:

>	 16 * 0xf000 + 0xfff0   # in hex multiplication by 16 is
>
>	= 0xf0000 + 0xfff0     # easy--just append a 0.
>
>	= 0xffff0 

The addressing follows the formula: $\mathit{address} = 16 \times \mathit{segment} + \mathit{offset}$. Here, the 8088 processor used is designed to start at `0xffff0`.

The first instruction can be explained as:
- The PC starts executing at physical address `0x000ffff0`, which is at the very top of the 64KB area reserved for the ROM BIOS.
- The PC starts executing with `CS = 0xf000` and `IP = 0xfff0`.
- The first instruction to be executed is a `jmp` instruction, which jumps to the segmented address `CS = 0xf000` and `IP = 0xe05b`.

# Part 2: The Boot Loader

A storage device, CD-ROM or Hard Disk, is devided into *sectors*. Each sector has fixed size, e.g. 512 bytes. A sector is the device's minimum transfer granularity: each read and write must be one or multiple sectors in size and aligned on a sector boundary. If a device is bootable, its first sector is the *boot sector*, where boot loader code resides. When the BIOS finds a bootable device, it loads the 512-byte boot sector into memory at physical address `0x7c00` through `0x7dff`, and then uses a `jmp` instruction to set the `CS:IP` to `0000:7c00`, passing control to the boot loader. These actions and addressing are standadized for PCs.

The conventional hard drive boot mechanism is used in this course. Hard drives use a sector size of 512 bytes, which means that the boot loader must fit into 512 bytes. The boot loader consists of one assembly language source file, *boot/boot.S*, and one C source file, *boot/main.c*. The file *lab/obj/boot/boot.asm* is the disassembly of the boot loader, which makes it easy to see exactly where in physical memory all of the boot loader's code resides, and makes it easier to track what's happening while stepping through the boot loader in GDB. The boot loader performs two functions:
- the boot loader switches the processor from *real mode* to *procteced mode*, and it is only in this mode that software can access all the memory above 1MB in the processor's physical address space. In protected mode, translation of segmented addresses (segment:offset pairs) into physical addresses changes the *offset* from 16 bits to 32 bits.
- the boot loader reads the kernel from the hard disk by directly accessing the IDE disk device registers via the x86's special I/O instructions.

> Q: How is the boot loader stored in the first sector of the hard drive in the lab?
>
> A: The boot loader file is *lab/obj/boot/boot*, compiled and linked from *lab/boot/boot.S* and *lab/boot/main.c*, and the script *lab/boot/sign.pl* stores the boot loader content to the first sector of the hard drive. See *lab/boot/Makefrag*.

A reference material for I/O port addresses: [PORTS.LST](http://bochs.sourceforge.net/techspec/PORTS.LST).

## Exercise 3

> Take a look at the lab tools guide, especially the section on GDB commands. Even if you're familiar with GDB, this includes some esoteric GDB commands that are useful for OS work.
>
> Set a breakpoint at address 0x7c00, which is where the boot sector will be loaded. Continue execution until that breakpoint. Trace through the code in boot/boot.S, using the source code and the disassembly file obj/boot/boot.asm to keep track of where you are. Also use the x/i command in GDB to disassemble sequences of instructions in the boot loader, and compare the original boot loader source code with both the disassembly in obj/boot/boot.asm and GDB.
>
> Trace into bootmain() in boot/main.c, and then into readsect(). Identify the exact assembly instructions that correspond to each of the statements in readsect(). Trace through the rest of readsect() and back out into bootmain(), and identify the begin and end of the for loop that reads the remaining sectors of the kernel from the disk. Find out what code will run when the loop is finished, set a breakpoint there, and continue to that breakpoint. Then step through the remainder of the boot loader.

- At what point does the processor start executing 32-bit code? What exactly causes the switch from 16- to 32-bit mode?
	- After the instruction, `0x7c2d: ljmp $0x8,$0x7c32` (`ljmp $PROT_MODE_CSEG, $protcseg` in source), is executed, the processor starts executing 32-bit code. The addressing mechanism in 32-bit mode is different from that in 16-bit mode.
- What is the last instruction of the boot loader executed, and what is the first instruction of the kernel it just loaded?
	- The last instruction of the boot loader executed is `0x7d6b: call *(%di)` (`((void (*)(void)) (ELFHDR->e_entry))();` in source).
	- The first instruction of the kernel it loaded is `0x10000c:	movw $0x1234,0x472`.
- Where is the first instruction of the kernel?
	- The first instruction of the kernel can be found by searching the string `0x1234` in the lab directory, and it locates in `lab/kern/entry.S`: `movw $0x1234,0x472 # warm boot`.
- How does the boot loader decide how many sectors it must read in order to fetch the entire kernel from disk? Where does it find this information?
	- See `main.c`: in the function `bootmain(void)`, `ph` and `eph` are constructed via `ELFHDR` and determine how many *segment* should be handled. Then, in the function `readseg(uint32_t pa, uint32_t count, uint32_t offset)`, `pa` and `end_pa` determine how many *sector* needs to be loaded. This function is called as `readseg(ph->p_pa, ph->p_memsz, ph->p_offset)`, and all parameters are from the pointer `ph` to the `Proghdr` struct which is also from the `ELFHDR`.
	- So, all the information is kept in the `Elf` pointer `ELFHDR`.

## Exercise  4

> Read about programming with pointers in C. The best reference for the C language is The C Programming Language by Brian Kernighan and Dennis Ritchie (known as 'K&R'). We recommend that students purchase this book (here is an Amazon Link) or find one of MIT's 7 copies.
>
>Read 5.1 (Pointers and Addresses) through 5.5 (Character Pointers and Functions) in K&R. Then download the code for [pointers.c](https://pdos.csail.mit.edu/6.828/2018/labs/lab1/pointers.c), run it, and make sure you understand where all of the printed values come from. In particular, make sure you understand where the pointer addresses in printed lines 1 and 6 come from, how all the values in printed lines 2 through 4 get there, and why the values printed in line 5 are seemingly corrupted.

`pointers.c`'s content is listed as the following:
```c
#include <stdio.h>
#include <stdlib.h>

void f(void)
{
    int a[4];
    int *b = malloc(16);
    int *c;
    int i;

    printf("1: a = %p, b = %p, c = %p\n", a, b, c);

    c = a;
    for (i = 0; i < 4; i++)
	a[i] = 100 + i;
    c[0] = 200;
    printf("2: a[0] = %d, a[1] = %d, a[2] = %d, a[3] = %d\n",
	   a[0], a[1], a[2], a[3]);

    c[1] = 300;
    *(c + 2) = 301;
    3[c] = 302;
    printf("3: a[0] = %d, a[1] = %d, a[2] = %d, a[3] = %d\n",
	   a[0], a[1], a[2], a[3]);

    c = c + 1;
    *c = 400;
    printf("4: a[0] = %d, a[1] = %d, a[2] = %d, a[3] = %d\n",
	   a[0], a[1], a[2], a[3]);

    c = (int *) ((char *) c + 1);
    *c = 500;
    printf("5: a[0] = %d, a[1] = %d, a[2] = %d, a[3] = %d\n",
	   a[0], a[1], a[2], a[3]);

    b = (int *) a + 1;
    c = (int *) ((char *) a + 1);
    printf("6: a = %p, b = %p, c = %p\n", a, b, c);
}

int main(int ac, char **av)
{
    f();
    return 0;
}
```

Compile it on the PC and run:
```shell
$ gcc pointers.c -o pointers
$ ./pointers
```
The following outputs are printed out:
```shell
1: a = 0xbf9a79bc, b = 0x98f9008, c = 0xb7712244
2: a[0] = 200, a[1] = 101, a[2] = 102, a[3] = 103
3: a[0] = 200, a[1] = 300, a[2] = 301, a[3] = 302
4: a[0] = 200, a[1] = 400, a[2] = 301, a[3] = 302
5: a[0] = 200, a[1] = 128144, a[2] = 256, a[3] = 302
6: a = 0xbf9a79bc, b = 0xbf9a79c0, c = 0xbf9a79bd
```

A weird pointer usage: `3[c] = 302;`. `c` is a pointer that points to the beginning of the array `a`, `3[c]` equals to `c[3]`, `*(c + 3)`, and `a[3]`.

The most confusing part may lie at this part:

```c
    c = (int *) ((char *) c + 1);
    *c = 500;
    printf("5: a[0] = %d, a[1] = %d, a[2] = %d, a[3] = %d\n",
	   a[0], a[1], a[2], a[3]);
```

The sizes of `char` and `int` in this lab are 1-byte and 4-byte respectively.
`c = (int *) ((char *) c + 1);` firstly makes `c` be addressed as `char`, move `c` forward by one byte to the next address, and re-cast `c` to an `int` pointer. Then, `*c` is assigned to `500`, which overwrites the first byte of `a[2]`. The memory allocation process is illustrated as the following:

![pointer.png](_resources/af6716af3aea4fa29dbe81b79152785e.png)

`b = (int *) a + 1;` casts `a` to an `int` pointer and moves forward 4 bytes, and so `b` points to the place 4 bytes more than `a`'s. `c = (int *) ((char *) a + 1);` addresses a as a `char` pointer firstly, increases it by 1 byte, then casts to an `int` pointer, and so `c` points to an `int` with the address one byte forward than `a`.

## Exercise 5

> Trace through the first few instructions of the boot loader again and identify the first instruction that would "break" or otherwise do the wrong thing if you were to get the boot loader's link address wrong. Then change the link address in boot/Makefrag to something wrong, run make clean, recompile the lab with make, and trace into the boot loader again to see what happens. Don't forget to change the link address back and make clean again afterward!

After changing the link address, the following instruction is executed rather than the kernel:
```x86asm
Program received signal SIGTRAP, Trace/breakpoint trap.
The target architecture is assumed to be i8086
[   0:7c2d] => 0x7c2d:	ljmp   $0x8,$0x6c32
```

## Exercise 6

> We can examine memory using GDB's x command. The GDB manual has full details, but for now, it is enough to know that the command x/Nx ADDR prints N words of memory at ADDR. (Note that both 'x's in the command are lowercase.) Warning: The size of a word is not a universal standard. In GNU assembly, a word is two bytes (the 'w' in xorw, which stands for word, means 2 bytes).
>
> Reset the machine (exit QEMU/GDB and start them again). Examine the 8 words of memory at 0x00100000 at the point the BIOS enters the boot loader, and then again at the point the boot loader enters the kernel. Why are they different? What is there at the second breakpoint? (You do not really need to use QEMU to answer this question. Just think.)

The bootloader start address is `0x7c00`, and the first instruction address of the kernel is `0x10000c`. Set breakpoints there, and check with `x/8x 0x00100000` the contents of 8 words following `0x00100000` when the breakpoints are hit. When the first breakpoint is hit, the contents are:
```x86asm
(gdb) x/8x 0x00100000
0x100000:	0x00000000	0x00000000	0x00000000	0x00000000
0x100010:	0x00000000	0x00000000	0x00000000	0x00000000
```
At this time, there're no instructions loaded to them. When the second breakpoint is hit, the contents are:
```x86asm
(gdb) x/8x 0x00100000
0x100000:	0x1badb002	0x00000000	0xe4524ffe	0x7205c766
0x100010:	0x34000004	0x0000b812	0x220f0011	0xc0200fd8
```
Now, the kernel instructions are filled in them.

## Exercise 7

> Use QEMU and GDB to trace into the JOS kernel and stop at the movl %eax, %cr0. Examine memory at 0x00100000 and at 0xf0100000. Now, single step over that instruction using the stepi GDB command. Again, examine memory at 0x00100000 and at 0xf0100000. Make sure you understand what just happened.
>
> What is the first instruction after the new mapping is established that would fail to work properly if the mapping weren't in place? Comment out the movl %eax, %cr0 in kern/entry.S, trace into it, and see if you were right.

The kernel entry starts at `0x0010000c`, and set a breakpoint there:
```shell
(gdb) b *0x0010000c
```
From that breakpoint, trace through several steps and stop at `mov %eax, %cr0`. Examine `0x00100000` and `0xf0100000`:
```shell
(gdb) x 0x00100000
0x100000:	0x1badb002
(gdb) x 0xf0100000
0xf0100000 <_start+4026531828>:	0x00000000
```
The contents stored in them are different, because they are different physical addresses in the real mode. Then step one:
```shell
(gdb) si
```
Now the PC is in the proteced mode, and examine `0x00100000` and `0xf0100000`:
```shell
(gdb) x 0x00100000
0x100000:	0x1badb002
(gdb) x 0xf0100000
0xf0100000 <_start+4026531828>:	0x1badb002
```
Their contents are the same because they are the same address. In the protected mode, virtual addresses need to be translated to physical addresses. Here, *kern/entrypgdir.c* translates virtual addresses in the range `0xf0000000` through `0xf0400000` to physical addresses `0x00000000` through `0x00400000`, as well as virtual addresses `0x00000000` through `0x00400000` to physical addresses `0x00000000` through `0x00400000`.

## Exercise 8

> Read through kern/printf.c, lib/printfmt.c, and kern/console.c, and make sure you understand their relationship. It will become clear in later labs why printfmt.c is located in the separate lib directory. We have omitted a small fragment of code---the code necessary to print octal numbers using patterns of the form "%o". Find and fill in this code fragment.

The functions of *kern/console.c* are declared in *kern/console.h*, the functions of *kern/printf.c* and *lib/printfmt.c* are declared in *inc/stdio.h*, and the headers are included in *kern/init.c*. The function `console::cons_init` is called to initialize the console, and the functions `vcprintf` and `cprintf` defined in *kern/printf.c* are called. The print functions use the `vprintfmt` function defined in *lib/printfmt.c*.

The missed fragment of code is in *lib/printfmt.c*. Study the code for the formatter of outputting decimal numbers, and the missed octal formatter can be fulfilled as:
```diff
$ git diff
diff --git a/lab/lib/printfmt.c b/lab/lib/printfmt.c
index 28e01c9..b1de635 100644
--- a/lab/lib/printfmt.c
+++ b/lab/lib/printfmt.c
@@ -205,11 +205,9 @@ vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)

                // (unsigned) octal
                case 'o':
-                       // Replace this with your code.
-                       putch('X', putdat);
-                       putch('X', putdat);
-                       putch('X', putdat);
-                       break;
+                       num = getuint(&ap, lflag);
+                       base = 8;
+                       goto number;

                // pointer
                case 'p':
```

1. Explain the interface between printf.c and console.c. Specifically, what function does console.c export? How is this function used by printf.c?
	- The function `console::cputchar` is exported and is used by *printf::putch* to output a character in the console.
2. Explain the following from console.c:
	```c
	      if (crt_pos >= CRT_SIZE) { // when the current output position exceeds the screen size
	              int i;
	              memmove(crt_buf, crt_buf + CRT_COLS, (CRT_SIZE - CRT_COLS) * sizeof(uint16_t)); // move each screen row up by one from the second row to the last row
	              for (i = CRT_SIZE - CRT_COLS; i < CRT_SIZE; i++) // for the last row of the console screen
	                    crt_buf[i] = 0x0700 | ' '; // (0x0700 | ' ') == 0x0720 => HEX color #000720 == rgba(0, 7, 32, 1), almost black
	              crt_pos -= CRT_COLS; // the current output position is set to the beginning of the last row
	      }
	```
	- `void *memmove(void *str1, const void *str2, size_t n)` copies n characters from str2 to str1.
3. For the following questions you might wish to consult the notes for Lecture 2. These notes cover GCC's calling convention on the x86. Trace the execution of the following code step-by-step: In the call to `cprintf()`, to what does `fmt` point? To what does `ap` point? List (in order of execution) each call to `cons_putc`, `va_arg`, and `vcprintf`. For `cons_putc`, list its argument as well. For `va_arg`, list what `ap` points to before and after the call. For `vcprintf` list the values of its two arguments.
	```c
	int x = 1, y = 3, z = 4;
	cprintf("x %d, y %x, z %d\n", x, y, z);
	```
	- In JOS, execute `help`, and the outputs give some clue on where to play the code: *lab/kern/monitor.c*. Place the exercise code there:
		```diff
		$ git diff lab/kern/monitor*
		
		diff --git a/lab/kern/monitor.c b/lab/kern/monitor.c
		index e137e92..2b4f32f 100644
		--- a/lab/kern/monitor.c
		+++ b/lab/kern/monitor.c
		@@ -24,6 +24,7 @@ struct Command {
		 static struct Command commands[] = {
		        { "help", "Display this list of commands", mon_help },
		        { "kerninfo", "Display information about the kernel", mon_kerninfo },
		+       { "ple183", "play lab1 exercise 8.3", play_lab_1_exercise_8_3 },
		 };
		
		 /***** Implementations of basic kernel monitor commands *****/
		@@ -61,7 +62,12 @@ mon_backtrace(int argc, char **argv, struct Trapframe *tf)
		        return 0;
		 }
		
		-
		+int play_lab_1_exercise_8_3(int argc, char **argv, struct Trapframe *tf)
		+{
		+       int x = 1, y = 3, z = 4;
		+       cprintf("x %d, y %x, z %d\n", x, y, z);
		+       return 0;
		+}
		
		 /***** Kernel monitor command interpreter *****/
		
		diff --git a/lab/kern/monitor.h b/lab/kern/monitor.h
		index 0aa0f26..5673d06 100644
		--- a/lab/kern/monitor.h
		+++ b/lab/kern/monitor.h
		@@ -14,6 +14,7 @@ void monitor(struct Trapframe *tf);
		 // Functions implementing monitor commands.
		 int mon_help(int argc, char **argv, struct Trapframe *tf);
		 int mon_kerninfo(int argc, char **argv, struct Trapframe *tf);
		+int play_lab_1_exercise_8_3(int argc, char **argv, struct Trapframe *tf);
		 int mon_backtrace(int argc, char **argv, struct Trapframe *tf);
		```
		Then set two breakpoints in terms of the calling chain around `cprintf`:
		```shell
		(gdb) break monitor.c:67
		(gdb) break printf.c:21
		```
		At the second breakpoint, check `fmt` and `ap`:
		```shell
		(gdb) p fmt
		$3 = 0xf0101b55 "x %d, y %x, z %d\n"
		(gdb) x/3x ap
		0xf010ff54:	0x00000001	0x00000003	0x00000004
		(gdb) x/3d ap
		0xf010ff54:	1	3	4
		```
	- It's not easy to trace `ap` of `va_arg`, which is a macro rather than a function:
		```c
		#define va_arg(ap, type) __builtin_va_arg(ap, type)
		```
		Here, `__builtin_va_arg` is kind of *primitive* code and should be provided by the compiler. The calling chain is `cprintf` $\rightarrow$ `vcprintf` $\rightarrow$ `vprintfmt([putch` $\rightarrow$ `cputchar` $\rightarrow$ `cons_putc])`. With the parameters `%d` and `%x`, `vprintfmt` calls `getuint` and `getint`, where `va_arg` is called. So, set two breakpoints there:
		```diff
		$ git diff lab/lib/printfmt.c
		
		diff --git a/lab/lib/printfmt.c b/lab/lib/printfmt.c
		index 28e01c9..dfd8dc2 100644
		--- a/lab/lib/printfmt.c
		+++ b/lab/lib/printfmt.c
		@@ -189,7 +189,7 @@ vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
		
		                // (signed) decimal
		                case 'd':
		-                       num = getint(&ap, lflag);
		+                       num = getint(&ap, lflag); // trace ap of va_arg
		                        if ((long long) num < 0) {
		                                putch('-', putdat);
		                                num = -(long long) num;
		@@ -222,7 +220,7 @@ vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
		
		                // (unsigned) hexadecimal
		                case 'x':
		-                       num = getuint(&ap, lflag);
		+                       num = getuint(&ap, lflag); // trace ap of va_arg
		                        base = 16;
		                number:
		                        printnum(putch, putdat, num, base, width, padc);
		```
		Set 4 breakpoints in total and install a watch for `ap`:
		```shell
		(gdb) b cons_putc
		Breakpoint 1 at 0xf01002fd: file kern/console.c, line 70.
		(gdb) b vcprintf
		Breakpoint 2 at 0xf01008f9: file kern/printf.c, line 19.
		(gdb) b printfmt.c:192
		Breakpoint 3 at 0xf0100fe8: file lib/printfmt.c, line 192.
		(gdb) b printfmt.c:223
		Breakpoint 4 at 0xf0101060: file lib/printfmt.c, line 223.
		```
		Instead of tracing `getuint` and `getint`, another way is using *gdb* *watch* to trace `ap` around `va_arg` in `vprintfmt`. Here, use this way:
		```shell
		(gdb) b cons_putc
		(gdb) b vcprintf
		(gdb) b vprintfmt
		```
		As the `vprintfmt` breakpoint is hit at the first time, install the watch:
		```shell
		(gdb) watch ap
		```
		The calling order is as the following:
		```shell
		(gdb) c
		Continuing.
		=> 0xf01002fd <cons_putc+11>:	mov    $0x0,%ebx
		
		Breakpoint 1, cons_putc (c=10) at kern/console.c:70
		70		for (i = 0;
		(gdb)
		Continuing.
		=> 0xf01008f9 <vcprintf+6>:	movl   $0x0,-0xc(%ebp)
		
		Breakpoint 2, vcprintf (fmt=0xf0101b55 "x %d, y %x, z %d\n", ap=0xf010ff54 "\001") at kern/printf.c:19
		19		int cnt = 0;
		(gdb)
		Continuing.
		=> 0xf0100d2b <vprintfmt+20>:	test   %eax,%eax
		
		Breakpoint 3, vprintfmt (putch=0xf01008e0 <putch>, putdat=0xf010ff1c, fmt=0xf0101b55 "x %d, y %x, z %d\n", ap=0xf010ff54 "\001") at lib/printfmt.c:93
		warning: Source file is more recent than executable.
		93				if (ch == '\0')
		(gdb) watch ap
		Hardware watchpoint 4: ap
		(gdb) c
		Continuing.
		=> 0xf01002fd <cons_putc+11>:	mov    $0x0,%ebx
		
		Breakpoint 1, cons_putc (c=120) at kern/console.c:70
		70		for (i = 0;
		(gdb)
		Continuing.
		=> 0xf0100d2b <vprintfmt+20>:	test   %eax,%eax
		
		Breakpoint 3, vprintfmt (putch=0xf01008e0 <putch>, putdat=0xf010ff1c, fmt=0xf0101b55 "x %d, y %x, z %d\n", ap=0xf010ff54 "\001") at lib/printfmt.c:93
		93				if (ch == '\0')
		(gdb)
		Continuing.
		=> 0xf01002fd <cons_putc+11>:	mov    $0x0,%ebx
		
		Breakpoint 1, cons_putc (c=32) at kern/console.c:70
		70		for (i = 0;
		(gdb)
		Continuing.
		=> 0xf0100fdb <vprintfmt+708>:	mov    (%eax),%eax
		
		Hardware watchpoint 4: ap
		
		Old value = (va_list) 0xf010ff54 "\001"
		New value = (va_list) 0xf010ff58 "\003"
		0xf0100fdb in vprintfmt (putch=0xf01008e0 <putch>, putdat=0xf010ff1c, fmt=0xf0101b55 "x %d, y %x, z %d\n", ap=0xf010ff58 "\003") at lib/printfmt.c:75
		75			return va_arg(*ap, int);
		(gdb)
		Continuing.
		=> 0xf01002fd <cons_putc+11>:	mov    $0x0,%ebx
		
		Breakpoint 1, cons_putc (c=49) at kern/console.c:70
		70		for (i = 0;
		(gdb)
		Continuing.
		=> 0xf0100d2b <vprintfmt+20>:	test   %eax,%eax
		
		Breakpoint 3, vprintfmt (putch=0xf01008e0 <putch>, putdat=0xf010ff1c, fmt=0xf0101b55 "x %d, y %x, z %d\n", ap=0xf010ff58 "\003") at lib/printfmt.c:93
		93				if (ch == '\0')
		(gdb)
		Continuing.
		=> 0xf01002fd <cons_putc+11>:	mov    $0x0,%ebx
		
		Breakpoint 1, cons_putc (c=44) at kern/console.c:70
		70		for (i = 0;
		(gdb)
		Continuing.
		=> 0xf0100d2b <vprintfmt+20>:	test   %eax,%eax
		
		Breakpoint 3, vprintfmt (putch=0xf01008e0 <putch>, putdat=0xf010ff1c, fmt=0xf0101b55 "x %d, y %x, z %d\n", ap=0xf010ff58 "\003") at lib/printfmt.c:93
		93				if (ch == '\0')
		(gdb)
		Continuing.
		=> 0xf01002fd <cons_putc+11>:	mov    $0x0,%ebx
		
		Breakpoint 1, cons_putc (c=32) at kern/console.c:70
		70		for (i = 0;
		(gdb)
		Continuing.
		=> 0xf0100d2b <vprintfmt+20>:	test   %eax,%eax
		
		Breakpoint 3, vprintfmt (putch=0xf01008e0 <putch>, putdat=0xf010ff1c, fmt=0xf0101b55 "x %d, y %x, z %d\n", ap=0xf010ff58 "\003") at lib/printfmt.c:93
		93				if (ch == '\0')
		(gdb)
		Continuing.
		=> 0xf01002fd <cons_putc+11>:	mov    $0x0,%ebx
		
		Breakpoint 1, cons_putc (c=121) at kern/console.c:70
		70		for (i = 0;
		(gdb)
		Continuing.
		=> 0xf0100d2b <vprintfmt+20>:	test   %eax,%eax
		
		Breakpoint 3, vprintfmt (putch=0xf01008e0 <putch>, putdat=0xf010ff1c, fmt=0xf0101b55 "x %d, y %x, z %d\n", ap=0xf010ff58 "\003") at lib/printfmt.c:93
		93				if (ch == '\0')
		(gdb)
		Continuing.
		=> 0xf01002fd <cons_putc+11>:	mov    $0x0,%ebx
		
		Breakpoint 1, cons_putc (c=32) at kern/console.c:70
		70		for (i = 0;
		(gdb)
		Continuing.
		=> 0xf0100cd4 <getuint+49>:	mov    (%edx),%eax
		
		Hardware watchpoint 4: ap
		
		Old value = (va_list) 0xf010ff58 "\003"
		New value = (va_list) 0xf010ff5c "\004"
		0xf0100cd4 in getuint (ap=0xf010ff0c, lflag=-267321512) at lib/printfmt.c:62
		62			return va_arg(*ap, unsigned int);
		(gdb)
		Continuing.
		=> 0xf01002fd <cons_putc+11>:	mov    $0x0,%ebx
		
		Breakpoint 1, cons_putc (c=51) at kern/console.c:70
		70		for (i = 0;
		(gdb)
		Continuing.
		=> 0xf0100d2b <vprintfmt+20>:	test   %eax,%eax
		
		Breakpoint 3, vprintfmt (putch=0xf01008e0 <putch>, putdat=0xf010ff1c, fmt=0xf0101b55 "x %d, y %x, z %d\n", ap=0xf010ff5c "\004") at lib/printfmt.c:93
		93				if (ch == '\0')
		(gdb)
		Continuing.
		=> 0xf01002fd <cons_putc+11>:	mov    $0x0,%ebx
		
		Breakpoint 1, cons_putc (c=44) at kern/console.c:70
		70		for (i = 0;
		(gdb)
		Continuing.
		=> 0xf0100d2b <vprintfmt+20>:	test   %eax,%eax
		
		Breakpoint 3, vprintfmt (putch=0xf01008e0 <putch>, putdat=0xf010ff1c, fmt=0xf0101b55 "x %d, y %x, z %d\n", ap=0xf010ff5c "\004") at lib/printfmt.c:93
		93				if (ch == '\0')
		(gdb)
		Continuing.
		=> 0xf01002fd <cons_putc+11>:	mov    $0x0,%ebx
		
		Breakpoint 1, cons_putc (c=32) at kern/console.c:70
		70		for (i = 0;
		(gdb)
		Continuing.
		=> 0xf0100d2b <vprintfmt+20>:	test   %eax,%eax
		
		Breakpoint 3, vprintfmt (putch=0xf01008e0 <putch>, putdat=0xf010ff1c, fmt=0xf0101b55 "x %d, y %x, z %d\n", ap=0xf010ff5c "\004") at lib/printfmt.c:93
		93				if (ch == '\0')
		(gdb)
		Continuing.
		=> 0xf01002fd <cons_putc+11>:	mov    $0x0,%ebx
		
		Breakpoint 1, cons_putc (c=122) at kern/console.c:70
		70		for (i = 0;
		(gdb)
		Continuing.
		=> 0xf0100d2b <vprintfmt+20>:	test   %eax,%eax
		
		Breakpoint 3, vprintfmt (putch=0xf01008e0 <putch>, putdat=0xf010ff1c, fmt=0xf0101b55 "x %d, y %x, z %d\n", ap=0xf010ff5c "\004") at lib/printfmt.c:93
		93				if (ch == '\0')
		(gdb)
		Continuing.
		=> 0xf01002fd <cons_putc+11>:	mov    $0x0,%ebx
		
		Breakpoint 1, cons_putc (c=32) at kern/console.c:70
		70		for (i = 0;
		(gdb)
		Continuing.
		=> 0xf0100fdb <vprintfmt+708>:	mov    (%eax),%eax
		
		Hardware watchpoint 4: ap
		
		Old value = (va_list) 0xf010ff5c "\004"
		New value = (va_list) 0xf010ff60 "\001"
		0xf0100fdb in vprintfmt (putch=0xf01008e0 <putch>, putdat=0xf010ff1c, fmt=0xf0101b55 "x %d, y %x, z %d\n", ap=0xf010ff60 "\001") at lib/printfmt.c:75
		75			return va_arg(*ap, int);
		(gdb)
		Continuing.
		=> 0xf01002fd <cons_putc+11>:	mov    $0x0,%ebx
		
		Breakpoint 1, cons_putc (c=52) at kern/console.c:70
		70		for (i = 0;
		(gdb)
		Continuing.
		=> 0xf0100d2b <vprintfmt+20>:	test   %eax,%eax
		
		Breakpoint 3, vprintfmt (putch=0xf01008e0 <putch>, putdat=0xf010ff1c, fmt=0xf0101b55 "x %d, y %x, z %d\n", ap=0xf010ff60 "\001") at lib/printfmt.c:93
		93				if (ch == '\0')
		(gdb)
		Continuing.
		=> 0xf01002fd <cons_putc+11>:	mov    $0x0,%ebx
		
		Breakpoint 1, cons_putc (c=10) at kern/console.c:70
		70		for (i = 0;
		(gdb)
		Continuing.
		=> 0xf0100d2b <vprintfmt+20>:	test   %eax,%eax
		
		Breakpoint 3, vprintfmt (putch=0xf01008e0 <putch>, putdat=0xf010ff1c, fmt=0xf0101b55 "x %d, y %x, z %d\n", ap=0xf010ff60 "\001") at lib/printfmt.c:93
		93				if (ch == '\0')
		(gdb)
		Continuing.
		
		Watchpoint 4 deleted because the program has left the block in
		which its expression is valid.
		=> 0xf0100914 <vcprintf+33>:	mov    -0xc(%ebp),%eax
		vcprintf (fmt=0xf0101b55 "x %d, y %x, z %d\n", ap=0xf010ff54 "\001") at kern/printf.c:23
		23	}
		(gdb)
		Continuing.
		=> 0xf01008f9 <vcprintf+6>:	movl   $0x0,-0xc(%ebp)
		
		Breakpoint 2, vcprintf (fmt=0xf0101d6a "%s", ap=0xf010ff44 "g\033\020\360h\377\020\360\233\a\020\360U\033\020\360\001") at kern/printf.c:19
		19		int cnt = 0;
		(gdb)
		Continuing.
		=> 0xf01002fd <cons_putc+11>:	mov    $0x0,%ebx
		
		Breakpoint 1, cons_putc (c=75) at kern/console.c:70
		70		for (i = 0;
		(gdb)
		Continuing.
		=> 0xf01002fd <cons_putc+11>:	mov    $0x0,%ebx
		
		Breakpoint 1, cons_putc (c=62) at kern/console.c:70
		70		for (i = 0;
		(gdb)
		Continuing.
		=> 0xf01002fd <cons_putc+11>:	mov    $0x0,%ebx
		
		Breakpoint 1, cons_putc (c=32) at kern/console.c:70
		70		for (i = 0;
		(gdb)
		Continuing.
		=> 0xf0100d2b <vprintfmt+20>:	test   %eax,%eax
		
		Breakpoint 3, vprintfmt (putch=0xf01008e0 <putch>, putdat=0xf010ff0c, fmt=0xf0101d6a "%s", ap=0xf010ff48 "h\377\020\360\233\a\020\360U\033\020\360\001") at lib/printfmt.c:93
		93				if (ch == '\0')
		(gdb)
		Continuing.
		```
		There are 3 value-changes of `ap` during the process:
		```shell
		Hardware watchpoint 4: ap
		Old value = (va_list) 0xf010ff54 "\001"
		New value = (va_list) 0xf010ff58 "\003"
		⋮
		Hardware watchpoint 4: ap
		Old value = (va_list) 0xf010ff58 "\003"
		New value = (va_list) 0xf010ff5c "\004"
		⋮
		Hardware watchpoint 4: ap
		Old value = (va_list) 0xf010ff5c "\004"
		New value = (va_list) 0xf010ff60 "\001"
		```
		`cons_putc` outputs one character to the console every time. List the calls to `cons_putc` in order:
		```shell
		cons_putc (c=10)	# \n
		cons_putc (c=120)	# x
		cons_putc (c=32)	# ␣
		cons_putc (c=49)	# 1
		cons_putc (c=44)	# ,
		cons_putc (c=32)	# ␣
		cons_putc (c=121)	# y
		cons_putc (c=32)	# ␣
		cons_putc (c=51)	# 3
		cons_putc (c=44)	# ,
		cons_putc (c=32)	# ␣
		cons_putc (c=122)	# z
		cons_putc (c=32)	# ␣
		cons_putc (c=52)	# 4
		cons_putc (c=10)	# \n
		cons_putc (c=75)	# K
		cons_putc (c=62)	# >
		cons_putc (c=32)	# ␣
		```
		The debug for this part is tricky. For instance, input `ple183` in JOS firstly, then set up *gdb*, and hit enter to debug lastly:
		```shell
		Welcome to the JOS kernel monitor!
		Type 'help' for a list of commands.
		K> ple183
		```

4. Run the following code.
	```c
	unsigned int i = 0x00646c72;
	cprintf("H%x Wo%s", 57616, &i);
	```
	What is the output? Explain how this output is arrived at in the step-by-step manner of the previous exercise. The output depends on that fact that the x86 is little-endian. If the x86 were instead big-endian what would you set `i` to in order to yield the same output? Would you need to change `57616` to a different value?

	- The output is:
		```shell
		He110 World
		```
		The immediate number `57616` is output as its hex representation, `e110`, and this is easily seen. The variable `i`'s address is inputed to `cprintf` and is output as a string. It means that `i` is chunked as a set of `char` for output. The `unsigned int` number `0x 00 64 6c 72 == 0b 00000000 01100100 01101100 01110010 == 6581362`, is stored in 4 bytes. The x86 is little-endian, so this number looks in memory like the following:
		```x86asm
		&i	:	0111 0010
		&i + 1	:	0110 1100
		&i + 2	:	0110 0100
		&i + 3	:	0000 0000
		```
		Represent in decimal:
		```x86asm
		&i	:	114
		&i + 1	:	108
		&i + 2	:	100
		&i + 3	:	0
		```		
		`vprintfmt` reads one `char`, i.e. one byte, each time and outputs it to the console: `114`, `108`, `100`, and `0` are `r`, `l`, `d`, and `\0` respectively in ASCII encoding. Add the code to `monitor.c` as being done for the exercise 8.3, and trace `conssole.c::cons_putc`:
		```shell
		cons_putc (c=72)	# H
		cons_putc (c=101)	# e
		cons_putc (c=49)	# 1
		cons_putc (c=49)	# 1
		cons_putc (c=48)	# 0
		cons_putc (c=32)	# ␣
		cons_putc (c=87)	# W
		cons_putc (c=111)	# o
		cons_putc (c=114)	# r
		cons_putc (c=108)	# l
		cons_putc (c=100)	# d
		```
	- In a big-endian system, the following 4-byte memory represents `0b 01110010 01101100 01100100 00000000 == 1919706112`:
		```x86asm
		&i	:	0111 0010
		&i + 1	:	0110 1100
		&i + 2	:	0110 0100
		&i + 3	:	0000 0000
		```
		So `i` needs to be set to that value: `unsigned int i = 1919706112`. To output a hexadecimal representation, `vprintfmt` calls this function:
		```c
		static void printnum(void (*putch)(int, void*), void *putdat,
			 unsigned long long num, unsigned base, int width, int padc)
		{
			// first recursively print all preceding (more significant) digits
			if (num >= base) {
				printnum(putch, putdat, num / base, base, width - 1, padc);
			} else {
				// print any needed pad characters before first digit
				while (--width > 0)
					putch(padc, putdat);
			}
		
			// then print this (the least significant) digit
			putch("0123456789abcdef"[num % base], putdat);
		}
		```
		The operation is based on arithmetic calculation, as is unrelated to the memory allocation way, so there is no need to change `57616` here.
		
5. In the following code, what is going to be printed after 'y='? (note: the answer is not a specific value.) Why does this happen?
	```c
	cprintf("x=%d y=%d", 3);
	```
	- The output may be `x=3 y=-1559526`. `y`'s value may be any integer. `cprintf` calls `vprintfmt`, which enumerates all `fmt`:
		```c
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
				return;
			putch(ch, putdat);
		}
		```
		It firstly outputs `x=3`, which is easily understood. Then, `vprintfmt` calls `getint` again:
		```c
		static long long getint(va_list *ap, int lflag)
		{
			if (lflag >= 2)
				return va_arg(*ap, long long);
			else if (lflag)
				return va_arg(*ap, long);
			else
				return va_arg(*ap, int);
		}
		```
		Here, `lflag == 0`, so it fetches an `int` at `ap`. However, the programe does not allocate for any integer, so the pointer `ap` actually points to a memory filled with arbitrary values. Therefore, the output following `y=` may be any `int` value.

6. Let's say that GCC changed its calling convention so that it pushed arguments on the stack in declaration order, so that the last argument is pushed last. How would you have to change cprintf or its interface so that it would still be possible to pass it a variable number of arguments?
	- Nothing should be changed. The underlying addressing depends on the implementation of GCC: if it changes calling convention to store arguments in another way, it should change its addressing mechanism to retrieve correct arguments.

## Exercise 9

See [xv6: a simple, Unix-like teaching operating system](https://pdos.csail.mit.edu/6.828/2019/xv6/book-riscv-rev0.pdf) for explanation on *kernel stack*. N.B.: in *xv6* each process has a *kernel stack*, but there is only one for all processes in *JOS*.

> Determine where the kernel initializes its stack, and exactly where in memory its stack is located. How does the kernel reserve space for its stack? And at which "end" of this reserved area is the stack pointer initialized to point to?

- The kernel stack initialization is as the following (see `kern/entry.S`):
	```x86asm
	# Set the stack pointer
	movl $(bootstacktop), %esp
	```
- Check the disassembly `obj/kern/kernel.asm`:
	```x86asm
	mov $0xf0110000, %esp
	```
	The kernel stack top is located to `0xf0110000`, which is mapped to the physical address `0x00110000`.
- The lecture says:
	> The x86 stack pointer (esp register) points to the lowest location on the stack that is currently in use. Everything below that location in the region reserved for the stack is free. Pushing a value onto the stack involves decreasing the stack pointer and then writing the value to the place the stack pointer points to. Popping a value from the stack involves reading the value the stack pointer points to and then increasing the stack pointer.
	
	Thus, `esp` points to the higher address of the kernel stack area. See the disassembly `obj/kern/kernel.asm`:
	```x86asm
	.data
	###################################################################
	# boot stack
	###################################################################
		.p2align	PGSHIFT		# force page alignment
		.globl		bootstack
	bootstack:
		.space		KSTKSIZE
		.globl		bootstacktop   
	bootstacktop:
	```
	Before allocating for `bootstacktop` in the `.data` segment, `.space` for `bootstack` is allocated `KSTKSIZE`. `KSTKSIZE` is defined in `inc/memlayout.h`:
	```c
	#define KSTKSIZE	(8*PGSIZE)   		// size of a kernel stack
	```
	`PGSIZE` is defined in `inc/mmu.h`:
	```c
	#define PGSIZE		4096		// bytes mapped by a page
	```
	Therefore, `KSTKSIZE` $= 4096 \times 8 = 32768 \; \mathit{bytes} = 32 \; \mathit{m}$.
- Based on the above analysis, the end of the kernel stack is located `32`$\mathit{m}$ lower than the stack top, `0xf0110000`, which is `0xf0108000` and is mapped to the physical address `0x00108000`. Therefore, the memory area `0x00108000`~`0x00110000` is reserved for the kernel stack.

## Exercise 10

See [6.828 Lecture Notes: x86 and PC architecture](https://pdos.csail.mit.edu/6.828/2018/lec/l-x86.html) for *GCC Calling Conventions for JOS*. Some points:
- stack frames:
	>		       +------------+   |
	>		       | arg 2      |   \
	>		       +------------+    >- previous function's stack frame
	>		       | arg 1      |   /
	>		       +------------+   |
	>		       | ret %eip   |   /
	>		       +============+   
	>		       | saved %ebp |   \
	>		%ebp-> +------------+   |
	>		       |            |   |
	>		       |   local    |   \
	>		       | variables, |    >- current function's stack frame
	>		       |    etc.    |   /
	>		       |            |   |
	>		       |            |   |
	>		%esp-> +------------+   /
- prologue and epilogue:
	```x86asm
	pushl %ebp
	movl %esp, %ebp
	⋮
	movl %ebp, %esp
	popl %ebp
	```
	Arguments and locals are at fixed offsets from `ebp`.
- `eax` contains return value.
- `ebp` isn not strictly necessary, but *JOS* and *xv6* is compiled this way for convenience of walking up the stack.

It is handy to use [TUI](http://sourceware.org/gdb/current/onlinedocs/gdb/TUI-Commands.html#TUI-Commands) to debug in GDB, e.g. `layout asm`, and `layout regs`.

> To become familiar with the C calling conventions on the x86, find the address of the test_backtrace function in obj/kern/kernel.asm, set a breakpoint there, and examine what happens each time it gets called after the kernel starts. How many 32-bit words does each recursive nesting level of test_backtrace push on the stack, and what are those words?
>
> Note that, for this exercise to work properly, you should be using the patched version of QEMU available on the tools page or on Athena. Otherwise, you'll have to manually translate all breakpoint and memory addresses to linear addresses.
- The `test_backtrace` function is defined in `lab/kern/init.c/`:
	```c
	// Test the stack backtrace function (lab 1 only)
	void test_backtrace(int x)
	{
		cprintf("entering test_backtrace %d\n", x);
		if (x > 0)
			test_backtrace(x - 1);
		else
			mon_backtrace(0, 0, 0);
		cprintf("leaving test_backtrace %d\n", x);
	}
	```
	In `lab/obj/kern/kernel.asm`, the address of the `test_backtrace` is `f0100040`. Set a breakpoint there, and each time it gets called, the following routine is always executed before the `cprintf`:
	```x86asm
	=> 0xf0100040 <test_backtrace>:	push   %ebp
	
	Breakpoint 1, test_backtrace (x=4) at kern/init.c:13
	13	{
	(gdb) si
	=> 0xf0100041 <test_backtrace+1>:	mov    %esp,%ebp
	0xf0100041	13	{
	(gdb)
	=> 0xf0100043 <test_backtrace+3>:	push   %ebx
	0xf0100043	13	{
	(gdb)
	=> 0xf0100044 <test_backtrace+4>:	sub    $0xc,%esp
	0xf0100044	13	{
	(gdb)
	=> 0xf0100047 <test_backtrace+7>:	mov    0x8(%ebp),%ebx
	```
- At the breakpoint, check registers:
	```x86asm
	(gdb) info registers
	eax            0x0	0
	ecx            0x3d4	980
	edx            0x3d5	981
	ebx            0x5	5
	esp            0xf010ffc0	0xf010ffc0
	ebp            0xf010ffd8	0xf010ffd8
	esi            0x10094	65684
	edi            0x0	0
	eip            0xf0100050	0xf0100050 <test_backtrace+16>
	eflags         0x92	[ AF SF ]
	cs             0x8	8
	ss             0x10	16
	ds             0x10	16
	es             0x10	16
	fs             0x10	16
	gs             0x10	16
	```
	Check the following instructions from the breakpoints:
	```x86asm
	(gdb) x/32i 0xf0100040
	   0xf0100040 <test_backtrace>:	push   %ebp
	   0xf0100041 <test_backtrace+1>:	mov    %esp,%ebp
	   0xf0100043 <test_backtrace+3>:	push   %ebx
	   0xf0100044 <test_backtrace+4>:	sub    $0xc,%esp
	   0xf0100047 <test_backtrace+7>:	mov    0x8(%ebp),%ebx
	   0xf010004a <test_backtrace+10>:	push   %ebx
	   0xf010004b <test_backtrace+11>:	push   $0xf0101840
	   0xf0100050 <test_backtrace+16>:	call   0xf0100928 <cprintf>
	   0xf0100055 <test_backtrace+21>:	add    $0x10,%esp
	   0xf0100058 <test_backtrace+24>:	test   %ebx,%ebx
	   0xf010005a <test_backtrace+26>:	jle    0xf010006d <test_backtrace+45>
	   0xf010005c <test_backtrace+28>:	sub    $0xc,%esp
	   0xf010005f <test_backtrace+31>:	lea    -0x1(%ebx),%eax
	   0xf0100062 <test_backtrace+34>:	push   %eax
	   0xf0100063 <test_backtrace+35>:	call   0xf0100040 <test_backtrace>
	   0xf0100068 <test_backtrace+40>:	add    $0x10,%esp
	   0xf010006b <test_backtrace+43>:	jmp    0xf010007e <test_backtrace+62>
	   0xf010006d <test_backtrace+45>:	sub    $0x4,%esp
	   0xf0100070 <test_backtrace+48>:	push   $0x0
	   0xf0100072 <test_backtrace+50>:	push   $0x0
	   0xf0100074 <test_backtrace+52>:	push   $0x0
	   0xf0100076 <test_backtrace+54>:	call   0xf01007b1 <mon_backtrace>
	   0xf010007b <test_backtrace+59>:	add    $0x10,%esp
	   0xf010007e <test_backtrace+62>:	sub    $0x8,%esp
	   0xf0100081 <test_backtrace+65>:	push   %ebx
	   0xf0100082 <test_backtrace+66>:	push   $0xf010185c
	   0xf0100087 <test_backtrace+71>:	call   0xf0100928 <cprintf>
	   0xf010008c <test_backtrace+76>:	add    $0x10,%esp
	   0xf010008f <test_backtrace+79>:	mov    -0x4(%ebp),%ebx
	   0xf0100092 <test_backtrace+82>:	leave
	   0xf0100093 <test_backtrace+83>:	ret
	   0xf0100094 <i386_init>:	push   %ebp
	```
	There are $6$ stack-push operations for each recursively calling (N.B.: a `call` instruction pushes its return address (address immediately after the `call` instruction, not the return value's address) on the stack.):
	```x86asm
	(gdb) x/32i 0xf0100040
	   0xf0100040 <test_backtrace>:	push   %ebp
	   0xf0100043 <test_backtrace+3>:	push   %ebx
			⋮
	   0xf010004a <test_backtrace+10>:	push   %ebx
	   0xf010004b <test_backtrace+11>:	push   $0xf0101840
	   0xf0100050 <test_backtrace+16>:	call   0xf0100928 <cprintf>
			⋮
	   0xf0100063 <test_backtrace+35>:	call   0xf0100040 <test_backtrace>
			⋮
	```
	Every operation pushes $4$ bytes to the stack. There is another instruction that moves the stack-top pointer $12$ bytes for reserved space: `0xf0100055 <test_backtrace+21>:	add    $0x10,%esp`. Thus, $36$ bytes, i.e. $9$ 32-bit words, are pushed to the stack in each recursive call. For registers, the values of pushed to the stack can be displayed by checking the registers and the stack. For example, in the call `test_backtrace (x=3)`, the stack looks like the following after `sub    $0xc,%esp`:
	```x86asm
	(gdb) x/8d $esp
	0xf010ff88:	-267321416	0	-267384593	4
	0xf010ff98:	-267321416	-267386776	3	4
	```
	Here, the register `esp` points to the stack top, `0xf010ff88`, and `0xf010ff98` is the register `ebp` value, pointing to the stack bottom. In the call `test_backtrace (x=2)`, the stack looks like the following after `sub    $0xc,%esp`:
	```x86asm
	(gdb) x/8d $esp
	0xf010ff68:	-267321448	0	-267384593	3
	0xf010ff78:	-267321448	-267386776	2	3
	```
	At the same time, the registers' values are:
	```x86asm
	(gdb) info registers
	eax            0x2	2
	ecx            0x3d4	980
	edx            0x3d5	981
	ebx            0x3	3
	esp            0xf010ff68	0xf010ff68
	ebp            0xf010ff78	0xf010ff78
	esi            0x10094	65684
	edi            0x0	0
	eip            0xf0100047	0xf0100047 <test_backtrace+7>
	eflags         0x92	[ AF SF ]
	cs             0x8	8
	ss             0x10	16
	ds             0x10	16
	es             0x10	16
	fs             0x10	16
	gs             0x10	16
	```

## Exercise 11

> Implement the backtrace function as specified above. Use the same format as in the example, since otherwise the grading script will be confused. When you think you have it working right, run make grade to see if its output conforms to what our grading script expects, and fix it if it doesn't. After you have handed in your Lab 1 code, you are welcome to change the output format of the backtrace function any way you like.
>
> If you use read_ebp(), note that GCC may generate "optimized" code that calls read_ebp() before mon_backtrace()'s function prologue, which results in an incomplete stack trace (the stack frame of the most recent function call is missing). While we have tried to disable optimizations that cause this reordering, you may want to examine the assembly of mon_backtrace() and make sure the call to read_ebp() is happening after the function prologue.

A frame stack looks like the following:
>		       +------------+   |
>		       | arg 2      |   \
>		       +------------+    >- previous function's stack frame
>		       | arg 1      |   /
>		       +------------+   |
>		       | ret %eip   |   /
>		       +============+   
>		       | saved %ebp |   \
>		%ebp-> +------------+   |
>		       |            |   |
>		       |   local    |   \
>		       | variables, |    >- current function's stack frame
>		       |    etc.    |   /
>		       |            |   |
>		       |            |   |
>		%esp-> +------------+   /
The required backtrace info can be got by accessing `ebp`. `read_ebp`, which is defined in `inc/x86.h`, can return a pointer pointing to `ebp`. Thus, the `mon_backtrace` is implemented as the following:
```c
int mon_backtrace(int argc, char **argv, struct Trapframe *tf)
{
	uint32_t* pebp  = (uint32_t*) read_ebp();
	cprintf("Stack backtrace:\n");
	while (pebp)
	{
		cprintf("  ebp %08x  eip %08x  args %08x %08x %08x %08x %08x\n",
			pebp, pebp[1], pebp[2], pebp[3], pebp[4], pebp[5], pebp[6]);
		pebp = (uint32_t*) (*pebp);
	}
	return 0;
}
```

## Exercise 12

> Modify your stack backtrace function to display, for each eip, the function name, source file name, and line number corresponding to that eip.
>
> In debuginfo_eip, where do __STAB_* come from? This question has a long answer; to help you to discover the answer, here are some things you might want to do:
>
>	- look in the file kern/kernel.ld for __STAB_*
>	- run `objdump -h obj/kern/kernel`
>	- run `objdump -G obj/kern/kernel`
>	- run `gcc -pipe -nostdinc -O2 -fno-builtin -I. -MD -Wall -Wno-format -DJOS_KERNEL -gstabs -c -S kern/init.c`, and look at init.s.
>	- see if the bootloader loads the symbol table in memory as part of loading the kernel binary
>
> Complete the implementation of debuginfo_eip by inserting the call to stab_binsearch to find the line number for an address.
>
> Add a backtrace command to the kernel monitor, and extend your implementation of mon_backtrace to call debuginfo_eip and print a line for each stack frame of the form:
>
>	```shell
>	K> backtrace
>	Stack backtrace:
>	  ebp f010ff78  eip f01008ae  args 00000001 f010ff8c 00000000 f0110580 00000000
>	         kern/monitor.c:143: monitor+106
>	  ebp f010ffd8  eip f0100193  args 00000000 00001aac 00000660 00000000 00000000
>	         kern/init.c:49: i386_init+59
>	  ebp f010fff8  eip f010003d  args 00000000 00000000 0000ffff 10cf9a00 0000ffff
>	         kern/entry.S:70: <unknown>+0
>	K> 
>	```
> Each line gives the file name and line within that file of the stack frame's eip, followed by the name of the function and the offset of the eip from the first instruction of the function (e.g., monitor+106 means the return eip is 106 bytes past the beginning of monitor).
>
> Be sure to print the file and function names on a separate line, to avoid confusing the grading script.
>
> Tip: cprintf format strings provide an easy, albeit obscure, way to print non-null-terminated strings like those in STABS tables. cprintf("%.*s", length, string) prints at most length characters of string. Take a look at the printf man page to find out why this works.
>
> You may find that some functions are missing from the backtrace. For example, you will probably see a call to monitor() but not to runcmd(). This is because the compiler in-lines some function calls. Other optimizations may cause you to see unexpected line numbers. If you get rid of the -O2 from GNUMakefile, the backtraces may make more sense (but your kernel will run more slowly).

- In *debuginfo_eip*, `do _STAB*` are provided in `lab/kern/kernel.ld`, which is a link script for the command `ld`. *stabs* is a data format for storing debugging information. Debugging information is stored in *symbol table strings*.
- Get the headers from *obj/kern/kernel*:
	```shell
	$ objdump -h obj/kern/kernel
	
	obj/kern/kernel:     file format elf32-i386
	
	Sections:
	Idx Name          Size      VMA       LMA       File off  Algn
	  0 .text         00001861  f0100000  00100000  00001000  2**4
	                  CONTENTS, ALLOC, LOAD, READONLY, CODE
	  1 .rodata       000007f0  f0101880  00101880  00002880  2**5
	                  CONTENTS, ALLOC, LOAD, READONLY, DATA
	  2 .stab         000039cd  f0102070  00102070  00003070  2**2
	                  CONTENTS, ALLOC, LOAD, READONLY, DATA
	  3 .stabstr      00001943  f0105a3d  00105a3d  00006a3d  2**0
	                  CONTENTS, ALLOC, LOAD, READONLY, DATA
	  4 .data         0000a300  f0108000  00108000  00009000  2**12
	                  CONTENTS, ALLOC, LOAD, DATA
	  5 .bss          00000648  f0112300  00112300  00013300  2**5
	                  CONTENTS, ALLOC, LOAD, DATA
	  6 .comment      00000035  00000000  00000000  00013948  2**0
	                  CONTENTS, READONLY
	```
- Display the stabs contents:
	```shell
	$ objdump -G obj/kern/kernel
	
	obj/kern/kernel:     file format elf32-i386
	
	Contents of .stab section:
	
	Symnum n_type n_othr n_desc n_value  n_strx String
	
	-1     HdrSym 0      1232   00001942 1
	0      SO     0      0      f0100000 1      {standard input}
	1      SOL    0      0      f010000c 18     kern/entry.S
		⋮
	396    SO     0      2      f0100686 3688   kern/monitor.c
	397    OPT    0      0      00000000 49     gcc2_compiled.
	398    LSYM   0      0      00000000 64     int:t(0,1)=r(0,1);-2147483648;2147483647;
	399    LSYM   0      0      00000000 106    char:t(0,2)=r(0,2);0;127;
	400    LSYM   0      0      00000000 132    long int:t(0,3)=r(0,3);-2147483648;2147483647;
	401    LSYM   0      0      00000000 179    unsigned int:t(0,4)=r(0,4);0;4294967295;
	402    LSYM   0      0      00000000 220    long unsigned int:t(0,5)=r(0,5);0;4294967295;
	403    LSYM   0      0      00000000 266    __int128:t(0,6)=r(0,6);0;-1;
	404    LSYM   0      0      00000000 295    __int128 unsigned:t(0,7)=r(0,7);0;-1;
	405    LSYM   0      0      00000000 333    long long int:t(0,8)=r(0,8);-0;4294967295;
	406    LSYM   0      0      00000000 376    long long unsigned int:t(0,9)=r(0,9);0;-1;
	407    LSYM   0      0      00000000 419    short int:t(0,10)=r(0,10);-32768;32767;
	408    LSYM   0      0      00000000 459    short unsigned int:t(0,11)=r(0,11);0;65535;
	409    LSYM   0      0      00000000 503    signed char:t(0,12)=r(0,12);-128;127;
	410    LSYM   0      0      00000000 541    unsigned char:t(0,13)=r(0,13);0;255;
	411    LSYM   0      0      00000000 578    float:t(0,14)=r(0,1);4;0;
	412    LSYM   0      0      00000000 604    double:t(0,15)=r(0,1);8;0;
	413    LSYM   0      0      00000000 631    long double:t(0,16)=r(0,1);12;0;
	414    LSYM   0      0      00000000 664    _Decimal32:t(0,17)=r(0,1);4;0;
	415    LSYM   0      0      00000000 695    _Decimal64:t(0,18)=r(0,1);8;0;
	416    LSYM   0      0      00000000 726    _Decimal128:t(0,19)=r(0,1);16;0;
	417    LSYM   0      0      00000000 759    void:t(0,20)=(0,20)
	418    EXCL   0      0      00000000 2724   ./inc/stdio.h
	419    EXCL   0      0      00000650 2738   ./inc/stdarg.h
	420    EXCL   0      0      00000000 2781   ./inc/string.h
	421    EXCL   0      0      000060d4 791    ./inc/types.h
	422    EXCL   0      0      000008c7 2548   ./inc/memlayout.h
	423    EXCL   0      0      000159c2 779    ./inc/mmu.h
	424    BINCL  0      0      0000300f 3703   ./kern/kdebug.h
	425    LSYM   0      0      00000000 3719   Eipdebuginfo:T(7,1)=s24eip_file:(7,2)=*(0,2),0,32;eip_line:(0,1),32,32;eip_fn_name:(7,2),64,32;eip_fn_namelen:(0,1),96,32;eip_fn_addr:(4,13),128,32;eip_fn_narg:(0,1),160,32;;
		⋮
	453    FUN    0      0      f010076d 4116   mon_backtrace:F(0,1)
	454    PSYM   0      0      00000008 3997   argc:p(0,1)
	455    PSYM   0      0      0000000c 4081   argv:p(0,24)
	456    PSYM   0      0      00000010 4094   tf:p(0,25)
		⋮
	```
- Make the assembly of *lab/kern/init.c*:
	```shell
	$ gcc -pipe -nostdinc -O2 -fno-builtin -I. -MD -Wall -Wno-format -DJOS_KERNEL -gstabs -c -S kern/init.c
	```
	The header of *init.s*:
	```x86asm
		.file	"init.c"
		.stabs	"kern/init.c",100,0,2,.Ltext0
		.text
	.Ltext0:
		.stabs	"gcc2_compiled.",60,0,0,0
		.stabs	"int:t(0,1)=r(0,1);-2147483648;2147483647;",128,0,0,0
		.stabs	"char:t(0,2)=r(0,2);0;127;",128,0,0,0
		.stabs	"long int:t(0,3)=r(0,3);-2147483648;2147483647;",128,0,0,0
		.stabs	"unsigned int:t(0,4)=r(0,4);0;4294967295;",128,0,0,0
		⋮
	```
- From the above output of `objdump -h obj/kern/kernel`, the address of `stabstr` is `0xf0105a3d`:
	```shell
	$ objdump -h obj/kern/kernel
	
	obj/kern/kernel:     file format elf32-i386
	
	Sections:
	Idx Name          Size      VMA       LMA       File off  Algn
		⋮
	  3 .stabstr      00001943  f0105a3d  00105a3d  00006a3d  2**0
		⋮
	```
	Set a breakpoint to check whether the stabs is loaded by the bootloader:
	```shell
	(gdb) b printf.c:cprintf
	```
	As the breakpoint is hit, check the memory contents:
	```shell
	(gdb) x/8s 0xf0105a3d
	0xf0105a3d:	""
	0xf0105a3e:	"{standard input}"
	0xf0105a4f:	"kern/entry.S"
	0xf0105a5c:	"kern/entrypgdir.c"
	0xf0105a6e:	"gcc2_compiled."
	0xf0105a7d:	"int:t(0,1)=r(0,1);-2147483648;2147483647;"
	0xf0105aa7:	"char:t(0,2)=r(0,2);0;127;"
	0xf0105ac1:	"long int:t(0,3)=r(0,3);-2147483648;2147483647;"	
	```
	The memory contents are the same as the header of *init.s*, so the debug information stabs is loaded by the bootloader.
- The function `kedebug.c:debuginfo_eip` needs to be completed with tracing line-number. It uses two important `struct`, `lab/kern/kdebug.h:Eipdebuginfo` and `lab/inc/stab.h:Stab`:
	```c
	// Debug information about a particular instruction pointer
	struct Eipdebuginfo {
		const char *eip_file;		// Source code filename for EIP
		int eip_line;			// Source code linenumber for EIP

		const char *eip_fn_name;	// Name of function containing EIP
						//  - Note: not null terminated!
		int eip_fn_namelen;		// Length of function name
		uintptr_t eip_fn_addr;		// Address of start of function
		int eip_fn_narg;		// Number of function arguments
	};

	// Entries in the STABS table are formatted as follows.
	struct Stab {
		uint32_t n_strx;	// index into string table of name
		uint8_t n_type;         // type of symbol
		uint8_t n_other;        // misc info (usually empty)
		uint16_t n_desc;        // description field
		uintptr_t n_value;	// value of symbol
	};
	```
	The code for tracing line-number may be:
	```c
		// Search within [lline, rline] for the line number stab.
		// If found, set info->eip_line to the right line number.
		// If not found, return -1.
		//
		// Hint:
		//	There's a particular stabs type used for line numbers.
		//	Look at the STABS documentation and <inc/stab.h> to find
		//	which one.
		// Your code here.

		stab_binsearch(stabs, &lline, &rline, N_SLINE, addr);
		if (lline <= rline && stabs[lline].n_strx < stabstr_end - stabstr)
			info->eip_line = stabs[lline].n_desc;
	```	
- Now `lab/kern/monitor.c:mon_backtrace` can call `kedebug.c:debuginfo_eip` to trace a function name and line-number:
	```c
	int mon_backtrace(int argc, char **argv, struct Trapframe *tf)
	{
		uint32_t* pebp  = (uint32_t*) read_ebp();
		cprintf("Stack backtrace:\n");
		while (pebp)
		{
			cprintf("  ebp %08x  eip %08x  args %08x %08x %08x %08x %08x\n",
				pebp, pebp[1], pebp[2], pebp[3], pebp[4], pebp[5], pebp[6]);
			struct Eipdebuginfo info;
			if (debuginfo_eip(pebp[1], &info) == 0)
				cprintf("         %s:%d: %.*s+%d\n",  // example: kern/monitor.c:143: monitor+106
					info.eip_file, info.eip_line, info.eip_fn_namelen, info.eip_fn_name, pebp[1] - info.eip_fn_addr); // N.B.: eip == pebp[1] is containing the address of the next instruction after the function returns.
			else
				cprintf("ERROR: tracing function");
			pebp = (uint32_t*) (*pebp);
		}
		return 0;
	}
	```
	Here, `Eipdebuginfo.eip_fn_name` is not null terminated, so the problem hints to use `cprintf("%.*s", length, string)` to handle it. In the programme, `eip` $=$ `pebp[1]` is containing the address of the next instruction after the function returns.
- Hook the function as the command `backtrace` in *lab/kern/monitor.c*:
	```c
	static struct Command commands[] = {
		{ "help", "Display this list of commands", mon_help },
		{ "kerninfo", "Display information about the kernel", mon_kerninfo },
		{ "backtrace", "call backtrace", mon_backtrace },
		{ "ple183", "play lab1 exercise 8.3", play_lab_1_exercise_8_3 },
		{ "ple184", "play lab1 exercise 8.4", play_lab_1_exercise_8_4 },
	};
	```
	Verify the new command:
	```shell
	K> backtrace
	Stack backtrace:
	  ebp f010ff68  eip f0100944  args 00000001 f010ff80 00000000 f010ffc8 f0112540
			 kern/monitor.c:150: monitor+256
	  ebp f010ffd8  eip f01000e1  args 00000000 00001aac 00000640 00000000 00000000
			 kern/init.c:43: i386_init+77
	  ebp f010fff8  eip f010003e  args 00111021 00000000 00000000 00000000 00000000
			 kern/entry.S:83: <unknown>+0
	```
- Check the final work:
	```shell
	vagrant@mitos:~/mitos/lab$ make grade
	make: Warning: File 'obj/.deps' has modification time 1.3 s in the future
	make clean
	make[1]: Entering directory '/home/vagrant/mitos/lab'
	make[1]: Warning: File 'obj/.deps' has modification time 1.2 s in the future
	rm -rf obj .gdbinit jos.in qemu.log
	make[1]: warning:  Clock skew detected.  Your build may be incomplete.
	make[1]: Leaving directory '/home/vagrant/mitos/lab'
	./grade-lab1
	make[1]: Entering directory '/home/vagrant/mitos/lab'
	make[1]: Warning: File 'obj/.deps' has modification time 1.1 s in the future
	+ as kern/entry.S
	+ cc kern/entrypgdir.c
	+ cc kern/init.c
	+ cc kern/console.c
	+ cc kern/monitor.c
	+ cc kern/printf.c
	+ cc kern/kdebug.c
	+ cc lib/printfmt.c
	+ cc lib/readline.c
	+ cc lib/string.c
	+ ld obj/kern/kernel
	ld: warning: section `.bss' type changed to PROGBITS
	+ as boot/boot.S
	+ cc -Os boot/main.c
	+ ld boot/boot
	boot block is 390 bytes (max 510)
	+ mk obj/kern/kernel.img
	make[1]: warning:  Clock skew detected.  Your build may be incomplete.
	make[1]: Leaving directory '/home/vagrant/mitos/lab'
	running JOS: make[1]: Warning: File 'obj/boot/boot.d' has modification time 0.86 s in the future
	make[1]: Warning: File 'obj/.deps' has modification time 0.8 s in the future
	make[1]: warning:  Clock skew detected.  Your build may be incomplete.
	(0.6s)
	  printf: OK
	  backtrace count: OK
	  backtrace arguments: OK
	  backtrace symbols: OK
	  backtrace lines: OK
	Score: 50/50
	make: warning:  Clock skew detected.  Your build may be incomplete.
	```
✌️
