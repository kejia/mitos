
# Solution

This directory contains my solutions to the lab works and homeworks.

## Lab 1

My solutions to *Lab 1* is [lab_1.md](lab_1.md).

## Homework 1

My solutions to *Homework 1* is [homework_1.md](homework_1.md).

## Homework 2

My solutions to *Homework 2* is [homework_shell.md](homework_shell.md).

## Lab 2

My solutions to *Lab 2* is [lab_2.md](lab_2.md).

## Homework 3

My solutions to *Homework 3* is [homework_3_system_call.md](homework_3_system_call.md).

## Homework 4

My solutions to *Homework 4* is [homework_4_xv6_lazy_page_allocation.md](homework_4_xv6_lazy_page_allocation.md).

## Homework 5

My solutions to *Homework 5* is [homework_5_cpu_alarm.md](homework_5_cpu_alarm.md).

## Lab 3

My solutions to *Lab 3* is [lab_3.md](lab_3.md).

## Homework 6

My solutions to *Homework 6* is [homework_6_threads_and_locking.md](homework_6_threads_and_locking.md).

## Homework 7

My solutions to *Homework 7* is [homework_7_xv6_locking.md](homework_7_xv6_locking.md).

## Lab 4

My solutions to *Lab 4* is [lab_4.md](lab_4.md).

## Homework 8

My solutions to *Homework 8* is [homework_8_user-level_threads.md](homework_8_user-level_threads.md).

## Homework 9

My solutions to *Homework 9* is [homework_9_barriers.md](homework_9_barriers.md).

## Homework 10

My solutions to *Homework 10* is [homework_10_bigger_files_for_xv6.md](homework_10_bigger_files_for_xv6.md).

## Homework 11

My solutions to *Homework 11* is [homework_11_xv6_log.md](homework_11_xv6_log.md).

## Lab 5

My solutions to *Lab 5* is [lab_5.md](lab_5.md).

## Homework 12

My solutions to *Homework 12* is [homework_12_mmap.md](homework_12_mmap.md).

## Lab 6

My solutions to *Lab 6* is [lab_6.md](lab_6.md).
