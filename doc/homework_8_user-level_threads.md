
[Homework 8](http://pdos.csail.mit.edu/6.828/2018/homework/xv6-uthread.html)

# Homework: User-level Threads

> Complete a simple user-level thread package by implementing the code to perform context switching between threads.

Download [*uthread.c*](http://pdos.csail.mit.edu/6.828/2018/homework/uthread.c) and [*uthread_switch.S*](http://pdos.csail.mit.edu/6.828/2018/homework/uthread_switch.S) into the directory *homework/xv6-public*, and set up the `Makefile` as what the assignment describes.

Start xv6 shell in the directory *homework/xv6-public*:

```shell
$ make clean && make CPUS=1 qemu-nox
    ⋮
cpu0: starting 0
sb: size 1000 nblocks 941 ninodes 200 nlog 30 logstart 2 inodestart 32 bmap start 58
init: starting sh
$
```

Run `uthread`:

```shell
$ uthread
pid 3 uthread: trap 14 err 5 on cpu 0 eip 0xffffffff addr 0xffffffff--kill proc
```

To complete the fix, one needs to understand how *uthread.c* uses *thread_switch.S*. *uthread.c* has two global variables `current_thread` and `next_thread`. Each is a pointer to a `thread` structure:

```c
struct thread {
  int        sp;                /* saved stack pointer */
  char stack[STACK_SIZE];       /* the thread's stack */
  int        state;             /* FREE, RUNNING, RUNNABLE */
};
```

The `thread` structure has a stack for a thread and a saved stack pointer (`sp`, which points into the thread's stack). The job of `uthread_switch` is to save the current thread state into the structure pointed to by `current_thread`, restore next_thread's state, and make `current_thread` point to where `next_thread` was pointing to, so that when `uthread_switch` returns `next_thread` is running and is the `current_thread`.

The function `thread_create` in the file *homework/xv6-public/uthread.c* sets up the initial stack for a new thread. The comments hints about what `thread_switch` should do:

```c
void thread_create(void (*func)()) {
  thread_p t;
  for (t = all_thread; t < all_thread + MAX_THREAD; t++) {
    if (t->state == FREE) break;
  }
  t->sp = (int) (t->stack + STACK_SIZE);   // set sp to the top of the stack
  t->sp -= 4;                              // space for return address
  *(int *) (t->sp) = (int)func;           // push return address on stack
  t->sp -= 32;                             // space for registers that thread_switch expects
  t->state = RUNNABLE;
}
```

The intent is that `thread_switch` uses the assembly instructions `popal` to restore and save all eight x86 registers. Note that `thread_create` simulates eight pushed registers (32 bytes) on a new thread's stack.

To write the assembly in `thread_switch`, one needs to know how the C compiler lays out `struct thread` in memory:

```
    --------------------
    | 4 bytes for state|
    --------------------
    | STACK_SIZE bytes |
    | for stack        |
    --------------------
    | 4 bytes for sp   |
    --------------------  <--- current_thread
         ......

         ......
    --------------------
    | 4 bytes for state|
    --------------------
    | STACK_SIZE bytes |
    | for stack        |
    --------------------
    | 4 bytes for sp   |
    --------------------  <--- next_thread
```

The variables `next_thread` and `current_thread` each contain the address of a `struct thread`.

To write the `sp` field of the `struct` that `current_thread` points to, one should write assembly like this:

```x86asm
   movl current_thread, %eax
   movl %esp, (%eax)
```

This saves `%esp` in `current_thread->sp`. This works because `sp` is at offset `0` in the `struct`. One can study the assembly the compiler generates for *uthread.c* by looking at *uthread.asm*.

The implementation of `thread_switch` of *homework/xv6-public/thread_switch.S* is as follows:
```x86asm
	.text

/* Switch from current_thread to next_thread. Make next_thread
 * the current_thread, and set next_thread to 0.
 * Use eax as a temporary register; it is caller saved.
 */
	.globl thread_switch
thread_switch:
	/* save all current_thread's registers */
	pushal 
	/* update current_thread's sp */
	movl current_thread, %eax
	movl %esp, (%eax)
	/* restore all next_thread's registers */
	movl next_thread, %eax
	movl (%eax), %esp
	popal
	/* assign next_thread to current_thread and reset next_thread */
	movl next_thread, %eax
	movl %eax, current_thread
	movl $0, %eax
	movl %eax, next_thread
	ret				/* pop return address from stack */
```

Verify the work in the directory *homework/xv6-public*:
```shell
$ make clean && make CPUS=1 qemu-nox
    ⋮
cpu0: starting 0
sb: size 1000 nblocks 941 ninodes 200 nlog 30 logstart 2 inodestart 32 bmap start 58
init: starting sh
$ uthread
my thread running
my thread 0x2D08
my thread running
my thread 0x4D10
my thread 0x2D08
    ⋮
my thread 0x2D08
my thread 0x4D10
my thread: exit
my thread: exit
thread_schedule: no runnable threads
$
```


✌️
