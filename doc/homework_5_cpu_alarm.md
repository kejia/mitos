
[Homework 5](http://pdos.csail.mit.edu/6.828/2018/homework/xv6-alarm.html)

# Homework: xv6 CPU Alarm

The first step is adding 3 new fields in `struct proc` of *homework/xv6-public/proc.h*:
```diff
diff --git a/homework/xv6-public/proc.h b/homework/xv6-public/proc.h
index 1647114..49a9bb7 100644
--- a/homework/xv6-public/proc.h
+++ b/homework/xv6-public/proc.h
@@ -49,6 +49,9 @@ struct proc {
   struct file *ofile[NOFILE];  // Open files
   struct inode *cwd;           // Current directory
   char name[16];               // Process name (debugging)
+  int alarmticks;
+  void(* alarmhandler)();
+  int curalarmticks;
 };
```

The second step is implementing the `alarm` system call:
```diff
diff --git a/homework/xv6-public/syscall.c b/homework/xv6-public/syscall.c
index de7fe29..7e3493a 100644
--- a/homework/xv6-public/syscall.c
+++ b/homework/xv6-public/syscall.c
@@ -104,6 +104,7 @@ extern int sys_wait(void);
 extern int sys_write(void);
 extern int sys_uptime(void);
 extern int sys_date(void);
+extern int sys_alarm(void);

 static int (*syscalls[])(void) = {
 [SYS_fork]    sys_fork,
@@ -128,6 +129,7 @@ static int (*syscalls[])(void) = {
 [SYS_link]    sys_link,
 [SYS_mkdir]   sys_mkdir,
 [SYS_close]   sys_close,
+[SYS_alarm]   sys_alarm,
 };

 /* static char* syscalls_name[] = { */
diff --git a/homework/xv6-public/syscall.h b/homework/xv6-public/syscall.h
index 1a620b9..c32611f 100644
--- a/homework/xv6-public/syscall.h
+++ b/homework/xv6-public/syscall.h
@@ -21,3 +21,4 @@
 #define SYS_mkdir  20
 #define SYS_close  21
 #define SYS_date   22
+#define SYS_alarm  23
diff --git a/homework/xv6-public/sysproc.c b/homework/xv6-public/sysproc.c
index 597783a..bd9fb17 100644
--- a/homework/xv6-public/sysproc.c
+++ b/homework/xv6-public/sysproc.c
@@ -97,3 +97,14 @@ int sys_date(void) {
   cmostime(r);
   return 0;
 }
+
+// system call `alarm'
+int sys_alarm(void) {
+  int ticks;
+  void (*handler)();
+  if(argint(0, &ticks) < 0) return -1;
+  if(argptr(1, (char**)&handler, 1) < 0) return -1;
+  myproc()->alarmticks = ticks;
+  myproc()->alarmhandler = handler;
+  return 0;
+}
diff --git a/homework/xv6-public/user.h b/homework/xv6-public/user.h
index 3e791dd..f74b5cd 100644
--- a/homework/xv6-public/user.h
+++ b/homework/xv6-public/user.h
@@ -24,6 +24,7 @@ char* sbrk(int);
 int sleep(int);
 int uptime(void);
 int date(struct rtcdate*);
+int alarm(int ticks, void(*hander)());
diff --git a/homework/xv6-public/usys.S b/homework/xv6-public/usys.S
index ba76d54..6762624 100644
--- a/homework/xv6-public/usys.S
+++ b/homework/xv6-public/usys.S
@@ -30,3 +30,4 @@ SYSCALL(sbrk)
 SYSCALL(sleep)
 SYSCALL(uptime)
 SYSCALL(date)
+SYSCALL(alarm)
```

The third step is adding interrupt handler at `T_IRQ0 + IRQ_TIMER` of *homework/xv6-public/trap.c*:
```diff
diff --git a/homework/xv6-public/trap.c b/homework/xv6-public/trap.c
index 41c66eb..4a4540d 100644
--- a/homework/xv6-public/trap.c
+++ b/homework/xv6-public/trap.c
@@ -54,6 +54,15 @@ trap(struct trapframe *tf)
       wakeup(&ticks);
       release(&tickslock);
     }
+    if (myproc() && (tf->cs & 3) == 3) {
+      myproc()->curalarmticks++;
+      if (myproc()->alarmticks == myproc()->curalarmticks) {
+        myproc()->curalarmticks = 0;
+        tf->esp -= 4;
+        *((uint *)(tf->esp)) = tf->eip;
+        tf->eip = (uint) myproc()->alarmhandler;
+      }
+    }
     lapiceoi();
     break;
```

The firth step is adding the test program *homework/xv6-public/alarmtest.c*:
```c
#include "types.h"
#include "stat.h"
#include "user.h"

void periodic();

int main(int argc, char *argv[]) {
  int i;
  printf(1, "alarmtest starting\n");
  alarm(10, periodic);
  for(i = 0; i < 25 * 500000 * 100; i++){
    if((i % 250000) == 0)
      write(2, ".", 1);
  }
  exit();
}

void periodic() {
  printf(1, "alarm!\n");
}
```
Here, the iteration is increased by 100 to get satisfying result, as is mentioned in the assignment:
>If you only see one "alarm!", try increasing the number of iterations in alarmtest.c by 10x.

Then, the last step is changing the *Makefile*:
```diff
diff --git a/homework/xv6-public/Makefile b/homework/xv6-public/Makefile
index 4d66066..0d228bb 100644
--- a/homework/xv6-public/Makefile
+++ b/homework/xv6-public/Makefile
@@ -166,6 +166,7 @@ mkfs: mkfs.c fs.h
 .PRECIOUS: %.o

 UPROGS=\
+  _alarmtest\
   _cat\
   _date\
   _echo\
```

Now make and start xv6 in *homework/xv6-public*, and execute the test command:
```shell
$ make clean && make CPUS=1 qemu-nox
	⋮
init: starting sh
$ alarmtest
alarmtest starting
...........................................................................................................................................alarm!
.......................................................................................................................alarm!
...................................................................................................................................alarm!
..................................................................................................................................................alarm!
......................................................................................................................................................................................alarm!
.........................................................................................................................................................................alarm!
....................................................................................................................................alarm!
.....................................................................................................................................alarm!
....................................................................................................................................alarm!
..................................................................................................................................................................alarm!
...........................................................................................................................................alarm!
............................................................................................................................alarm!
..............................................................................................................................alarm!
..........................................................................................................................alarm!
...............................................................................................................alarm!
.......................................................................................................................alarm!
..............................................................................................alarm!
............................................................................................................alarm!
.............................................................................................................................alarm!
......................................................................................................................alarm!
.......................................................................................................................alarm!
......................................................................................................................................alarm!
...................................................................................................................................alarm!
...................................................................................................................alarm!
..................................................................................................................alarm!
................................................................................................................................alarm!
.....................................................................................................................alarm!
....................................................................................................................................alarm!
........................................................................................................................alarm!
.....................................................................................................................alarm!
.......................................................................................................alarm!
................................................................................................................................alarm!
........................................................................................................................alarm!
....................................................................................................................alarm!
..................................................................................................................alarm!
..................................................................................................................alarm!
..................................................................................................................................................alarm!
.....................................................................................................................alarm!
.............................................................................................................................alarm!
...........................................................$
```

✌️
