
[Homework 7](http://pdos.csail.mit.edu/6.828/2018/homework/xv6-lock.html)

# Homework: xv6 Locking

## Don't Do This

> What will happen when executing the following snippet:
>
>```c
>  struct spinlock lk;
>  initlock(&lk, "test lock");
>  acquire(&lk);
>  acquire(&lk);
>```

The function `acquire` is defined in *homework/xv6-public/spinlock.c*:
```c
void acquire(struct spinlock *lk) {
  pushcli(); // disable interrupts to avoid deadlock.
  if(holding(lk))
    panic("acquire");

  // The xchg is atomic.
  while(xchg(&lk->locked, 1) != 0)
    ;

  // Tell the C compiler and the processor to not move loads or stores
  // past this point, to ensure that the critical section's memory
  // references happen after the lock is acquired.
  __sync_synchronize();

  // Record info about lock acquisition for debugging.
  lk->cpu = mycpu();
  getcallerpcs(&lk, lk->pcs);
}
```
The first call to it makes the lock `lk` is hold, and the second call to it happened before the lock is released, so the panic *acquire* is output.

# Interrupts in ide.c

>An acquire ensures that interrupts are off on the local processor using the cli instruction (via pushcli()), and that interrupts remain off until the release of the last lock held by that processor (at which point they are enabled using sti).
>
>Let's see what happens if we turn on interrupts while holding the ide lock. In iderw in ide.c, add a call to sti() after the acquire(), and a call to cli() just before the release(). Rebuild the kernel and boot it in QEMU. Chances are the kernel will panic soon after boot; try booting QEMU a few times if it doesn't.
>
>Explain in a few sentences why the kernel panicked.

Change the code as instruced:
```diff
diff --git a/homework/xv6-public/ide.c b/homework/xv6-public/ide.c
index b4c0b1f..aecbb1c 100644
--- a/homework/xv6-public/ide.c
+++ b/homework/xv6-public/ide.c
@@ -147,7 +147,7 @@ iderw(struct buf *b)
     panic("iderw: ide disk 1 not present");

   acquire(&idelock);  //DOC:acquire-lock
-
+  sti();
   // Append b to idequeue.
   b->qnext = 0;
   for(pp=&idequeue; *pp; pp=&(*pp)->qnext)  //DOC:insert-queue
@@ -163,6 +163,6 @@ iderw(struct buf *b)
     sleep(b, &idelock);
   }

-
+  cli();
   release(&idelock);
 }
```

Run the xv6 in the direction *homework/xv6-public*:
```shell
$ make clean && make qemu-nox
    ⋮
cpu1: starting 1
cpu0: starting 0
lapicid 0: panic: sched locks
 80103b91 80103d02 80105863 8010565c 80100183 80101435 801014af 80103694 8010565f 0QEMU: Terminated
```

The panic comes from the calling to the function `sched` in *homework//xv6-public/proc.c*:
```c
  if(mycpu()->ncli != 1)
    panic("sched locks");
```

When multiple processes can switch because interrupt is enabled, one process acquired the lock, and then the scheduler checkes the `ncli` field, which may be not `1`, so the panic is thrown.

# Interrupts in file.c

>Remove the sti() and cli() you added, rebuild the kernel, and make sure it works again.
>
>Now let's see what happens if we turn on interrupts while holding the file_table_lock. This lock protects the table of file descriptors, which the kernel modifies when an application opens or closes a file. In filealloc() in file.c, add a call to sti() after the call to acquire(), and a cli() just before each of the release()es. You will also need to add #include "x86.h" at the top of the file after the other #include lines. Rebuild the kernel and boot it in QEMU. It most likely will not panic.
>
>Submit: Explain in a few sentences why the kernel didn't panic. Why do file_table_lock and ide_lock have different behavior in this respect?

Now do the similar change in the function `filealloc` in *homework/xv6-public/file.c*:
```diff
diff --git a/homework/xv6-public/file.c b/homework/xv6-public/file.c
index 24b32c2..2385825 100644
--- a/homework/xv6-public/file.c
+++ b/homework/xv6-public/file.c
@@ -9,6 +9,7 @@
 #include "spinlock.h"
 #include "sleeplock.h"
 #include "file.h"
+#include "x86.h"

 struct devsw devsw[NDEV];
 struct {
@@ -29,13 +30,16 @@ filealloc(void)
   struct file *f;

   acquire(&ftable.lock);
+  sti();
   for(f = ftable.file; f < ftable.file + NFILE; f++){
     if(f->ref == 0){
       f->ref = 1;
+      cli();
       release(&ftable.lock);
       return f;
     }
   }
+  cli();
   release(&ftable.lock);
   return 0;
```

This time no panic is unlikely thrown. I really do not have a better answer to this and just follow others' explanation: The time window during which interrupt is enabled is too short, and the probability of switching processes that acquire the same lock is low.

# xv6 lock implementation

>Why does release() clear lk->pcs[0] and lk->cpu before clearing lk->locked? Why not wait until after?

The `release` is defined as follows:
```c
// Release the lock.
void release(struct spinlock *lk) {
  if(!holding(lk))
    panic("release");

  lk->pcs[0] = 0;
  lk->cpu = 0;

  // Tell the C compiler and the processor to not move loads or stores
  // past this point, to ensure that all the stores in the critical
  // section are visible to other cores before the lock is released.
  // Both the C compiler and the hardware may re-order loads and
  // stores; __sync_synchronize() tells them both not to.
  __sync_synchronize();

  // Release the lock, equivalent to lk->locked = 0.
  // This code can't use a C assignment, since it might
  // not be atomic. A real OS would use C atomics here.
  asm volatile("movl $0, %0" : "+m" (lk->locked) : );

  popcli();
}
```
And the `spinlock` is defined as follows:
```c
// Mutual exclusion lock.
struct spinlock {
  uint locked;       // Is the lock held?
  // For debugging:
  char *name;        // Name of lock.
  struct cpu *cpu;   // The cpu holding the lock.
  uint pcs[10];      // The call stack (an array of program counters)
                     // that locked the lock.
};
```
So, `release` function must clear the `cpu` and the `pcs` before complete releasing the lock; otherwise, e.g. clear them as follows:
```c
// Release the lock.
void release(struct spinlock *lk) {
  if(!holding(lk))
    panic("release");

  // Tell the C compiler and the processor to not move loads or stores
  // past this point, to ensure that all the stores in the critical
  // section are visible to other cores before the lock is released.
  // Both the C compiler and the hardware may re-order loads and
  // stores; __sync_synchronize() tells them both not to.
  __sync_synchronize();

  // Release the lock, equivalent to lk->locked = 0.
  // This code can't use a C assignment, since it might
  // not be atomic. A real OS would use C atomics here.
  asm volatile("movl $0, %0" : "+m" (lk->locked) : );

  lk->pcs[0] = 0;
  lk->cpu = 0;

  popcli();
}
```
Another process may have acquired the lock and may access the uncleared `pcs` and `cpu` before the current process clears them.

✌️
