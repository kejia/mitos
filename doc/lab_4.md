
[Lab 4](http://pdos.csail.mit.edu/6.828/2018/labs/lab4/)

# Part A: Multiprocessor Support and Cooperative Multitasking

*Symmetric multiprocessing (SMP)*: a mutiprocessor model in which all CPUs have equivalent access to system resources such as memory and I/O buses. While all CPUs are functionally identical in SMP, during the boot process they can be classified into two types: the bootstrap processor (BSP) is responsible for initializing the system and for booting the operating system; and the application processors (APs) are activated by the BSP only after the operating system is up and running. Which processor is the BSP is determined by the hardware and the BIOS.

In an SMP system, each CPU has an accompanying local APIC (LAPIC) unit. The LAPIC units are responsible for delivering interrupts throughout the system. The LAPIC also provides its connected CPU with a unique identifier. In this lab, the LAPIC unit is made used of the following basic functionality:

- reading the LAPIC identifier (APIC ID) to tell which CPU the code is currently running on (see `cpunum()`)
- sending the `STARTUP` interprocessor interrupt (IPI) from the BSP to the APs to bring up other CPUs (see `lapic_startap()`)
- in Part C, programming LAPIC's built-in timer to trigger clock interrupts to support preemptive multitasking (see `apic_init()`)

A processor accesses its LAPIC using memory-mapped I/O (MMIO). In MMIO, a portion of *physical* memory is hardwired to the registers of some I/O devices, so the same load/store instructions typically used to access memory can be used to acces device registers. The LAPIC lives in a hole starting at physical address `0xfe000000`.

## Exercise 1

> Implement mmio\_map\_region in kern/pmap.c. To see how this is used, look at the beginning of lapic\_init in kern/lapic.c. You'll have to do the next exercise, too, before the tests for mmio\_map_region will run.

I/O devices generally are made from two parts: mechanical part and device controller. Device controllers transform bit-stream to byte-block and do character-block checking. Each device controller has a set of registers to communicate with CPU.

There are two ways for CPU to communicate with device controller registers and data buffers. In the first method, each controller register is assigned with one I/O port, and all I/O ports consists of I/O port space, which is proteced from user programs. The second method, controller registers are mapped to memory space. Each controller register is mapped to a unique memory address. This way is called *memory-mapped I/O*.

```c
void *mmio_map_region(physaddr_t pa, size_t size) {
    // Where to start the next region.  Initially, this is the
    // beginning of the MMIO region.  Because this is static, its
    // value will be preserved between calls to mmio_map_region
    // (just like nextfree in boot_alloc).
    static uintptr_t base = MMIOBASE;
    // Reserve size bytes of virtual memory starting at base and
    // map physical pages [pa,pa+size) to virtual addresses
    // [base,base+size).  Since this is device memory and not
    // regular DRAM, you'll have to tell the CPU that it isn't
    // safe to cache access to this memory.  Luckily, the page
    // tables provide bits for this purpose; simply create the
    // mapping with PTE_PCD|PTE_PWT (cache-disable and
    // write-through) in addition to PTE_W.  (If you're interested
    // in more details on this, see section 10.5 of IA32 volume
    // 3A.)
    //
    // Be sure to round size up to a multiple of PGSIZE and to
    // handle if this reservation would overflow MMIOLIM (it's
    // okay to simply panic if this happens).
    //
    // Hint: The staff solution uses boot_map_region.
    //
    // Your code here:
    size_t rounded = ROUNDUP(size, PGSIZE);
    if (base + rounded >= MMIOLIM) panic("mmio mapping overflow");
    boot_map_region(kern_pgdir, base, rounded, pa, PTE_PCD | PTE_PWT | PTE_W);
    uintptr_t cur = base;
    base += rounded;
    return cur;
}
```

## Exercise 2

> Read boot\_aps() and mp\_main() in kern/init.c, and the assembly code in kern/mpentry.S. Make sure you understand the control flow transfer during the bootstrap of APs. Then modify your implementation of page\_init() in kern/pmap.c to avoid adding the page at MPENTRY\_PADDR to the free list, so that we can safely copy and run AP bootstrap code at that physical address. Your code should pass the updated check\_page\_free\_list() test (but might fail the updated check\_kern_pgdir() test, which we will fix soon).

Only change one line in the function `page_init` in *lab/kern/pmap.c*:

```diff
@@ -313,7 +311,7 @@ page_init(void)
        size_t io_start = IOPHYSMEM / PGSIZE - 1, io_end = PADDR(boot_alloc(0)) / PGSIZE;
        size_t i;
        for (i = 1; i < npages; i++) {
-               if (i > io_start && i < io_end) {
+               if ((i > io_start && i < io_end) || (i == MPENTRY_PADDR / PGSIZE)) {
                        pages[i].pp_ref = 1;
                        pages[i].pp_link = NULL;
                } else {
```

Verify the work:

```shell
$ make clean && make qemu-nox
    ⋮
Physical memory: 131072K available, base = 640K, extended = 130432K
check_page_free_list() succeeded!
check_page_alloc() succeeded!
check_page() succeeded!
kernel panic on CPU 0 at kern/pmap.c:794: assertion failed: check_va2pa(pgdir, base + KSTKGAP + i) == PADDR(percpu_kstacks[n]) + i
Welcome to the JOS kernel monitor!
Type 'help' for a list of commands.
K>	
```

## Question 1

> Compare kern/mpentry.S side by side with boot/boot.S. Bearing in mind that kern/mpentry.S is compiled and linked to run above KERNBASE just like everything else in the kernel, what is the purpose of macro MPBOOTPHYS? Why is it necessary in kern/mpentry.S but not in boot/boot.S? In other words, what could go wrong if it were omitted in kern/mpentry.S? Hint: recall the differences between the link address and the load address that we have discussed in Lab 1.

The code in *lab/kern/mpentry.S* will be copied to a high address `MPENTRY_PADDR`, which is out of the scope of the addresses of the real-mode. The macro `MPBOOTPHYS` in *lab/kern/mpentry.S* calculates the high address.

## Exercise 3

> Modify mem\_init\_mp() (in kern/pmap.c) to map per-CPU stacks starting at KSTACKTOP, as shown in inc/memlayout.h. The size of each stack is KSTKSIZE bytes plus KSTKGAP bytes of unmapped guard pages. Your code should pass the new check in check\_kern\_pgdir().

The per-CPU state is defined in *lab/kern/cpu.h*. `cpunum()` always returns the ID of the CPU that calls it. The macro `thiscpu` is shorthand for the current CPU's struct `CpuInfo`.

There is a separate kernel stack for each processor. The array `percpu_kstacks[NCPU][KSTKSIZE]` reserves space for the kernel stacks. CPU 0's stack will still grow down from `KSTACKTOP`; CPU 1's stack will start `KSTKGAP` bytes below the bottom of CPU 0's stack, and so on. *lab/inc/memlayout.h* shows the mapping layout.

The work is implemented as follows:

```c
// Modify mappings in kern_pgdir to support SMP
//   - Map the per-CPU stacks in the region [KSTACKTOP-PTSIZE, KSTACKTOP)
static void mem_init_mp(void) {
    // Map per-CPU stacks starting at KSTACKTOP, for up to 'NCPU' CPUs.
    //
    // For CPU i, use the physical memory that 'percpu_kstacks[i]' refers
    // to as its kernel stack. CPU i's kernel stack grows down from virtual
    // address kstacktop_i = KSTACKTOP - i * (KSTKSIZE + KSTKGAP), and is
    // divided into two pieces, just like the single stack you set up in
    // mem_init:
    //     * [kstacktop_i - KSTKSIZE, kstacktop_i)
    //          -- backed by physical memory
    //     * [kstacktop_i - (KSTKSIZE + KSTKGAP), kstacktop_i - KSTKSIZE)
    //          -- not backed; so if the kernel overflows its stack,
    //             it will fault rather than overwrite another CPU's stack.
    //             Known as a "guard page".
    //     Permissions: kernel RW, user NONE
    //
    // LAB 4: Your code here:
    int i = 0;
    uint32_t cur_kstacktop = -1;
    for (i = 0; i < NCPU; i++) {
        cur_kstacktop = KSTACKTOP - i * (KSTKSIZE + KSTKGAP);
        boot_map_region(kern_pgdir, cur_kstacktop - KSTKSIZE, KSTKSIZE, PADDR(percpu_kstacks[i]), PTE_W);
    }
}
```

## Exercise 4

> The code in trap\_init\_percpu() (kern/trap.c) initializes the TSS and TSS descriptor for the BSP. It worked in Lab 3, but is incorrect when running on other CPUs. Change the code so that it can work on all CPUs. (Note: your new code should not use the global ts variable any more.)

A per-CPU task state segment (TSS) is also needed in order to specify where each CPU's kernel stack lives. The TSS for CPU i is stored in `cpus[i].cpu_ts`, and the corresponding TSS descriptor is defined in the GDT entry `gdt[(GD_TSS0 >> 3) + i]`. The global `ts` variable defined in *lab/kern/trap.c* will no longer be useful.

Since each CPU can run different user process simultaneously, we redefined the symbol `curenv` to refer to `cpus[cpunum()].cpu_env` (or `thiscpu->cpu_env`), which points to the environment currently executing on the current CPU (the CPU on which the code is running).

All registers, including system registers, are private to a CPU. Therefore, instructions that initialize these registers, such as `lcr3()`, `ltr()`, `lgdt()`, `lidt()`, etc., must be executed once on each CPU. Functions `env_init_percpu()` and `trap_init_percpu()` are defined for this purpose.

The work is implemented as follows:

```c
// Initialize and load the per-CPU TSS and IDT
void trap_init_percpu(void) {
    // The example code here sets up the Task State Segment (TSS) and
    // the TSS descriptor for CPU 0. But it is incorrect if we are
    // running on other CPUs because each CPU has its own kernel stack.
    // Fix the code so that it works for all CPUs.
    //
    // Hints:
    //   - The macro "thiscpu" always refers to the current CPU's
    //     struct CpuInfo;
    //   - The ID of the current CPU is given by cpunum() or
    //     thiscpu->cpu_id;
    //   - Use "thiscpu->cpu_ts" as the TSS for the current CPU,
    //     rather than the global "ts" variable;
    //   - Use gdt[(GD_TSS0 >> 3) + i] for CPU i's TSS descriptor;
    //   - You mapped the per-CPU kernel stacks in mem_init_mp()
    //   - Initialize cpu_ts.ts_iomb to prevent unauthorized environments
    //     from doing IO (0 is not the correct value!)
    //
    // ltr sets a 'busy' flag in the TSS selector, so if you
    // accidentally load the same TSS on more than one CPU, you'll
    // get a triple fault.  If you set up an individual CPU's TSS
    // wrong, you may not get a fault until you try to return from
    // user space on that CPU.
    //
    // LAB 4: Your code here:
    int cpu_id = thiscpu->cpu_id;
    // Setup a TSS so that we get the right stack
    // when we trap to the kernel.
    thiscpu->cpu_ts.ts_esp0 = (uint32_t) percpu_kstacks[cpu_id] + KSTKSIZE; // ts.ts_esp0 = KSTACKTOP;
    thiscpu->cpu_ts.ts_ss0 = GD_KD; // ts.ts_ss0 = GD_KD;
    thiscpu->cpu_ts.ts_iomb = sizeof(struct Taskstate); // ts.ts_iomb = sizeof(struct Taskstate);
    // Initialize the TSS slot of the gdt.
    gdt[(GD_TSS0 >> 3) + cpu_id] = SEG16(STS_T32A, (uint32_t) (&thiscpu->cpu_ts), // SEG16(STS_T32A, (uint32_t) (&ts),
                    sizeof(struct Taskstate) - 1, 0);
    gdt[(GD_TSS0 >> 3) + cpu_id].sd_s = 0;
    // Load the TSS selector (like other segment selectors, the
    // bottom three bits are special; we leave them 0)
    ltr(GD_TSS0 + (cpu_id << 3)); // ltr(GD_TSS0); // why?
    // Load the IDT
    lidt(&idt_pd);
}
```

Verify the work:

```shell
$ make clean && make qemu-nox CPUS=4
    ⋮
Physical memory: 131072K available, base = 640K, extended = 130432K
check_page_free_list() succeeded!
check_page_alloc() succeeded!
check_page() succeeded!
check_kern_pgdir() succeeded!
check_page_free_list() succeeded!
check_page_installed_pgdir() succeeded!
SMP: CPU 0 found 4 CPU(s)
enabled interrupts: 1 2
SMP: CPU 1 starting
SMP: CPU 2 starting
SMP: CPU 3 starting
[00000000] new env 00001000
Incoming TRAP frame at 0xf011cf04
kernel panic on CPU 0 at kern/trap.c:317: page fault in kernel mode
Welcome to the JOS kernel monitor!
Type 'help' for a list of commands.
K>
```

## Exercise 5

> Apply the big kernel lock as described above, by calling lock\_kernel() and unlock\_kernel() at the proper locations.

A *big kernel lock* is a single global lock that is held whenever an environment enters kernel mode, and is released when the environment returns to user mode. In this model, envrionment in user mode can run concurrently on any available CPUs, but no more than one environment can run in kernel mode.

*lab/kern/spinlock.h* declares the big kernel lock, namely `kernel_lock`. It also provides `lock_kernel()` and `unlock_kernel()`, shortcuts to acquire and release the lock. This big kernel lock should be applied at four locations:

- In `i386_init()`, acquire the lock before the BSP wakes up the other CPUs.
- In `mp_main()`, acquire the lock after initializing the AP, and then call `sched_yield()` to start running environments on this AP.
- In `trap()`, acquire the lock when trapped from user mode. To determine whether a trap happened in user mode or in kernel mode, check the low bits of the `tf_cs`.
- In `env_run()`, release the lock *right before* switching to user mode.

```diff
diff --git a/lab/kern/env.c b/lab/kern/env.c
index 82b795a..8921a4f 100644
--- a/lab/kern/env.c
+++ b/lab/kern/env.c
@@ -514,6 +514,7 @@ void env_run(struct Env *e) {
 	curenv = e;
 	curenv->env_status = ENV_RUNNING;
 	curenv->env_runs++;
+	unlock_kernel();
 	lcr3(PADDR(curenv->env_pgdir));
 	env_pop_tf(&(curenv->env_tf));
 }
diff --git a/lab/kern/init.c b/lab/kern/init.c
index e5491ec..61bf47c 100644
--- a/lab/kern/init.c
+++ b/lab/kern/init.c
@@ -43,6 +43,7 @@ i386_init(void)
 
 	// Acquire the big kernel lock before waking up APs
 	// Your code here:
+	lock_kernel();
 
 	// Starting non-boot CPUs
 	boot_aps();
@@ -109,6 +110,8 @@ mp_main(void)
 	// only one CPU can enter the scheduler at a time!
 	//
 	// Your code here:
+	lock_kernel();
+	sched_yield();
 
 	// Remove this after you finish Exercise 6
 	for (;;);
diff --git a/lab/kern/trap.c b/lab/kern/trap.c
index 7fc7181..cab6f21 100644
--- a/lab/kern/trap.c
+++ b/lab/kern/trap.c
@@ -271,6 +271,7 @@ trap(struct Trapframe *tf)
 		// Acquire the big kernel lock before doing any
 		// serious kernel work.
 		// LAB 4: Your code here.
+		lock_kernel();
 		assert(curenv);
 
 		// Garbage collect if current enviroment is a zombie
```

## Question 2

> It seems that using the big kernel lock guarantees that only one CPU can run the kernel code at a time. Why do we still need separate kernel stacks for each CPU? Describe a scenario in which using a shared kernel stack will go wrong, even with the protection of the big kernel lock.

In the function `trap` of *lab/kern/trap.c*, before a CPU holds the kernel lock it may write data to the kernel stack. It is possible that another CPU does the same operation with different data. That means the shared kernel stack is polluted. Therefore, each CPU needs a separate kernel stacks.

## Exercise 6

> Implement round-robin scheduling in sched\_yield() as described above. Don't forget to modify syscall() to dispatch sys\_yield().
> 
> Make sure to invoke sched\_yield() in mp\_main.
> 
> Modify kern/init.c to create three (or more!) environments that all run the program user/yield.c.
> 
> Run make qemu. You should see the environments switch back and forth between each other five times before terminating, like below.
> 
> Test also with several CPUS: make qemu CPUS=2.
> 
> ```shell
> ...
> Hello, I am environment 00001000.
> Hello, I am environment 00001001.
> Hello, I am environment 00001002.
> Back in environment 00001000, iteration 0.
> Back in environment 00001001, iteration 0.
> Back in environment 00001002, iteration 0.
> Back in environment 00001000, iteration 1.
> Back in environment 00001001, iteration 1.
> Back in environment 00001002, iteration 1.
> ...
> ```
> 
> After the yield programs exit, there will be no runnable environment in the system, the scheduler should invoke the JOS kernel monitor. If any of this does not happen, then fix your code before proceeding.

This work requires to change the JOS kernel so that it can alternate between multiple environments in *round-robin* fashion. Round-robin scheduling in JOS works as follows:

- The function `sched_yield` in the new *lab/kern/sched.c* is responsible for selecting a new environment to run. It searches sequentially through the `envs[]` array in circular fashion, starting just after the previously running environment (or at the beginning of the array if there was no previously running environment), picks the first environment it finds with a status of `ENV_RUNNABLE` (see *lab/inc/env.h*), and calls `env_run()` to jump into that environment.
- `sched_yield()` must never run the same environment on two CPUs at the same time. It can tell that an environment is currently running on some CPU (possibly the current CPU) because that environment's status will be `ENV_RUNNING`.
- We have implemented a new system call for you, `sys_yield()`, which user environments can call to invoke the kernel's `sched_yield()` function and thereby voluntarily give up the CPU to a different environment.

The implementation is iterating environements of `envs[]` to find the first *runnable* one after `curenv`. one noticable corner case is `curenv == NULL`.

The implementaiton in *lab/kern/sched.c* is as follows:

```c
// Choose a user environment to run and run it.
void sched_yield(void) {
    struct Env *idle;
    // Implement simple round-robin scheduling.
    //
    // Search through 'envs' for an ENV_RUNNABLE environment in
    // circular fashion starting just after the env this CPU was
    // last running.  Switch to the first such environment found.
    //
    // If no envs are runnable, but the environment previously
    // running on this CPU is still ENV_RUNNING, it's okay to
    // choose that environment.
    //
    // Never choose an environment that's currently running on
    // another CPU (env_status == ENV_RUNNING). If there are
    // no runnable environments, simply drop through to the code
    // below to halt the cpu.
    // LAB 4: Your code here.
    idle = curenv;
    int idle_index = (curenv != NULL) ? ENVX(idle->env_id) : -1;
    int i;
    int found = 0;
    for (i = idle_index + 1; i < NENV; i++) // search from the next to the curenv
        if (envs[i].env_status == ENV_RUNNABLE) {
            found = 1;
            break;
        }
    if (!found) // search round from the first
        for (i = 0; i < idle_index; i++)
            if (envs[i].env_status == ENV_RUNNABLE) {
                found = 1;
                break;
            }
    if (found) env_run(&envs[i]);
    else if (idle != NULL && idle->env_status == ENV_RUNNING) env_run(idle);
    // sched_halt never returns
    sched_halt();
}
```

Other changes are also needed:

```diff
diff --git a/lab/kern/init.c b/lab/kern/init.c
index e5491ec..5eb324a 100644
@@ -52,7 +53,10 @@ i386_init(void)
 	ENV_CREATE(TEST, ENV_TYPE_USER);
 #else
 	// Touch all you want.
-	ENV_CREATE(user_primes, ENV_TYPE_USER);
+	ENV_CREATE(user_yield, ENV_TYPE_USER);
+	ENV_CREATE(user_yield, ENV_TYPE_USER);
+	ENV_CREATE(user_yield, ENV_TYPE_USER);
+	/// ENV_CREATE(user_primes, ENV_TYPE_USER);
 #endif // TEST*
 
 	// Schedule and run the first user environment!
@@ -109,9 +113,8 @@ mp_main(void)
    // Your code here:
    lock_kernel();
    sched_yield();-
-	// Remove this after you finish Exercise 6
-	for (;;);
 }
 
 /*
diff --git a/lab/kern/syscall.c b/lab/kern/syscall.c
index 3a987a8..0fe01c9 100644
--- a/lab/kern/syscall.c
+++ b/lab/kern/syscall.c
@@ -273,6 +273,7 @@ int32_t syscall(uint32_t syscallno, uint32_t a1, uint32_t a2, uint32_t a3, uint3
 	case SYS_cgetc: return sys_cgetc();
 	case SYS_getenvid: return sys_getenvid();
 	case SYS_env_destroy: return sys_env_destroy(a1);
+	case SYS_yield: sys_yield(); return 0;
 	default: return -E_INVAL;
 	}
 }
```

Verify the work:

```shell
$ make clean && make qemu-nox CPUS=4
    ⋮
check_page_installed_pgdir() succeeded!
SMP: CPU 0 found 4 CPU(s)
enabled interrupts: 1 2
SMP: CPU 1 starting
SMP: CPU 2 starting
SMP: CPU 3 starting
[00000000] new env 00001000
[00000000] new env 00001001
[00000000] new env 00001002
Incoming TRAP frame at 0xf0233fbc
Incoming TRAP frame at 0xf023bfbc
Incoming TRAP frame at 0xf0243fbc
Incoming TRAP frame at 0xf0243fbc
Hello, I am environment 00001002.
Incoming TRAP frame at 0xf0243fbc
Incoming TRAP frame at 0xf0233fbc
Hello, I am environment 00001000.
Incoming TRAP frame at 0xf023bfbc
Hello, I am environment 00001001.
Incoming TRAP frame at 0xf0243fbc
Back in environment 00001002, iteration 0.
Incoming TRAP frame at 0xf0233fbc
Incoming TRAP frame at 0xf0243fbc
Incoming TRAP frame at 0xf0233fbc
Back in environment 00001000, iteration 0.
Incoming TRAP frame at 0xf023bfbc
Incoming TRAP frame at 0xf0243fbc
Back in environment 00001002, iteration 1.
Incoming TRAP frame at 0xf0233fbc
Incoming TRAP frame at 0xf0243fbc
Incoming TRAP frame at 0xf0233fbc
Back in environment 00001000, iteration 1.
Incoming TRAP frame at 0xf0243fbc
Back in environment 00001002, iteration 2.
Incoming TRAP frame at 0xf0233fbc
Incoming TRAP frame at 0xf023bfbc
Back in environment 00001001, iteration 0.
Incoming TRAP frame at 0xf0243fbc
Incoming TRAP frame at 0xf0233fbc
Incoming TRAP frame at 0xf023bfbc
Incoming TRAP frame at 0xf0243fbc
Back in environment 00001002, iteration 3.
Back in environment 00001000, iteration 2.
Incoming TRAP frame at 0xf023bfbc
Back in environment 00001Incoming TRAP frame at 0xf0243fbc
Incoming TRAP frame at 0xf0233fbc
001, iteration 1.
Incoming TRAP frame at 0xf023bfbc
Incoming TRAP frame at 0xf0243fbc
Back in environment 00001002, iteration 4.
Incoming TRAP frame at 0xf0233fbc
Back in environment 00001000, iteration 3.
Incoming TRAP frame at 0xf023bfbc
Incoming TRAP frame at 0xf0243fbc
Back in environment 00001001, iteration 2.
All done in environment 00001002.
Incoming TRAP frame at 0xf0233fbc
Incoming TRAP frame at 0xf023bfbc
Incoming TRAP frame at 0xf0243fbc
[00001002] exiting gracefully
[00001002] free env 00001002
Incoming TRAP frame at 0xf0233fbc
Back in environment 00001000, iteration 4.
Incoming TRAP frame at 0xf023bfbc
Back in environment 00001001, iteration 3.
Incoming TRAP frame at 0xf0233fbc
Incoming TRAP frame at 0xf023bfbc
All done in environment 00001000.
Incoming TRAP frame at 0xf0233fbc
[00001000] exiting gracefully
[00001000] free env 00001000
Incoming TRAP frame at 0xf023bfbc
Back in environment 00001001, iteration 4.
Incoming TRAP frame at 0xf023bfbc
All done in environment 00001001.
Incoming TRAP frame at 0xf023bfbc
[00001001] exiting gracefully
[00001001] free env 00001001
No runnable environments in the system!
Welcome to the JOS kernel monitor!
Type 'help' for a list of commands.
K>
```

## Question 3

> In your implementation of env\_run() you should have called lcr3(). Before and after the call to lcr3(), your code makes references (at least it should) to the variable e, the argument to env\_run. Upon loading the %cr3 register, the addressing context used by the MMU is instantly changed. But a virtual address (namely e) has meaning relative to a given address context--the address context specifies the physical address to which the virtual address maps. Why can the pointer e be dereferenced both before and after the addressing switch?

In *lab/kern/env.c*, the comment of the function `env_setup_vm` says:

```c
    //    - The VA space of all envs is identical above UTOP
    //	(except at UVPT, which we've set below).
    //	See inc/memlayout.h for permissions and layout.
    //	Can you use kern_pgdir as a template?  Hint: Yes.
    //	(Make sure you got the permissions right in Lab 2.)
```

The variable `curenv` points at the kernel part mentioned above, so the virtual address spaces of all user environments are identical, and should keep unchanged after context switching.

## Question 4

> Whenever the kernel switches from one environment to another, it must ensure the old environment's registers are saved so they can be restored properly later. Why? Where does this happen?

Context switch is user-transparent, and each environment proceeds as that there's no context switch happens. This requires each environment resumes exactly with the same context (i.e. registers values) as the environment stops. When `sys_yield` system call is executed, the context registers are stored onto the stack, and then the trap handler, `trap()` in *lab/kern/trap.c*, stores them in `env_tf`. When `evn_run()` is called the context registers are restored by `env_pop_tf()`.

## Exercise 7

> Implement the system calls described below in *lab/kern/syscall.c* and make sure `syscall()` calls them. You will need to use various functions in *lab/kern/pmap.c* and *lab/kern/env.c*, particularly `envid2env()`. For now, whenever you call `envid2env()`, pass 1 in the `checkperm` parameter. Be sure you check for any invalid system call arguments, returning `-E_INVAL` in that case. Test your JOS kernel with *lab/user/dumbfork* and make sure it works before proceeding.

So far, context switching is implemented by the kernel initial setting, and this exercise requires to implement the necessary JOS system calls to allow *user environments* to start and to switch other new user environments.

The new system calls for JOS are as follows:

- `sys_exofork`: This system call creates a new environment with an almost blank slate: nothing is mapped in the user portion of its address space, and it is not runnable. The new environment will have the same register state as the parent environment at the time of the `sys_exofork` call. In the parent, `sys_exofork` will return the `envid_t` of the newly created environment (or a negative error code if the environment allocation failed). In the child, however, it will return `0`. (Since the child starts out marked as not runnable, `sys_exofork` will not actually return in the child until the parent has explicitly allowed this by marking the child runnable using....)
- `sys_env_set_status`: Sets the status of a specified environment to `ENV_RUNNABLE` or `ENV_NOT_RUNNABLE`. This system call is typically used to mark a new environment ready to run, once its address space and register state has been fully initialized.
- `sys_page_alloc`: Allocates a page of physical memory and maps it at a given virtual address in a given environment's address space.
- `sys_page_map`: Copy a page mapping (not the contents of a page!) from one environment's address space to another, leaving a memory sharing arrangement in place so that the new and the old mappings both refer to the same page of physical memory.
- `sys_page_unmap`: Unmap a page mapped at a given virtual address in a given environment.

For all of the system calls above that accept environment IDs, the JOS kernel supports the convention that a value of 0 means "the current environment." This convention is implemented by `envid2env()` in *lab/kern/env.c*.

The new system calls implement a similar functionality as `fork`. The test program *lab/user/dumbfork.c* provides a Unix-like `fork()`. This test program uses the above system calls to create and run a child environment with a copy of its own address space. The two environments then switch back and forth using `sys_yield` as in the previous exercise. The parent exits after `10` iterations, whereas the child exits after `20`.

The implementation is as follows:

```c
// Allocate a new environment.
// Returns envid of new environment, or < 0 on error.  Errors are (see env_alloc in kern/env.c):
//	-E_NO_FREE_ENV if no free environment is available.
//	-E_NO_MEM on memory exhaustion.
static envid_t sys_exofork(void) {
    // Create the new environment with env_alloc(), from kern/env.c.
    // It should be left as env_alloc created it, except that
    // status is set to ENV_NOT_RUNNABLE, and the register set is copied
    // from the current environment -- but tweaked so sys_exofork
    // will appear to return 0.

    // LAB 4: Your code here.
    struct Env *penv;
    int rv_alloc = env_alloc(&penv, curenv->env_id);
    if (rv_alloc != 0) return rv_alloc;
    penv->env_status = ENV_NOT_RUNNABLE;
    penv->env_tf = curenv->env_tf;
    penv->env_tf.tf_regs.reg_eax = 0;
    return penv->env_id;
}

// Set envid's env_status to status, which must be ENV_RUNNABLE
// or ENV_NOT_RUNNABLE.
//
// Returns 0 on success, < 0 on error.  Errors are:
//	-E_BAD_ENV if environment envid doesn't currently exist,
//		or the caller doesn't have permission to change envid.
//	-E_INVAL if status is not a valid status for an environment.
static int sys_env_set_status(envid_t envid, int status) {
    // Hint: Use the 'envid2env' function from kern/env.c to translate an
    // envid to a struct Env.
    // You should set envid2env's third argument to 1, which will
    // check whether the current environment has permission to set
    // envid's status.

    // LAB 4: Your code here.
    struct Env *penv;
    if (status != ENV_RUNNABLE && status != ENV_NOT_RUNNABLE) return -E_INVAL;
    int rv_envid2env = envid2env(envid, &penv, 1);
    if (rv_envid2env != 0) return rv_envid2env;
    penv->env_status = status;
    return 0;
}

// Allocate a page of memory and map it at 'va' with permission
// 'perm' in the address space of 'envid'.
// The page's contents are set to 0.
// If a page is already mapped at 'va', that page is unmapped as a
// side effect.
//
// perm -- PTE_U | PTE_P must be set, PTE_AVAIL | PTE_W may or may not be set,
//         but no other bits may be set.  See PTE_SYSCALL in inc/mmu.h.
//
// Return 0 on success, < 0 on error.  Errors are:
//	-E_BAD_ENV if environment envid doesn't currently exist,
//		or the caller doesn't have permission to change envid.
//	-E_INVAL if va >= UTOP, or va is not page-aligned.
//	-E_INVAL if perm is inappropriate (see above).
//	-E_NO_MEM if there's no memory to allocate the new page,
//		or to allocate any necessary page tables.
static int sys_page_alloc(envid_t envid, void *va, int perm) {
    // Hint: This function is a wrapper around page_alloc() and
    //   page_insert() from kern/pmap.c.
    //   Most of the new code you write should be to check the
    //   parameters for correctness.
    //   If page_insert() fails, remember to free the page you
    //   allocated!

    // LAB 4: Your code here.
    if (((uint32_t)va >= UTOP || PGOFF(va) != 0) ||
        ((perm & (PTE_U | PTE_P)) != (PTE_U | PTE_P)) ||
        ((perm & ~(PTE_SYSCALL)) != 0)) return -E_INVAL;
    struct Env *penv;
    int rv_envid2env = envid2env(envid, &penv, 1);
    if (rv_envid2env != 0) return rv_envid2env;
    struct PageInfo *page_info = page_alloc(perm);
    if (page_info == NULL) return -E_NO_MEM;
    int rv_page_insert = page_insert(penv->env_pgdir, page_info, va, perm);
    if (rv_page_insert != 0) {
        page_free(page_info);
        return -E_NO_MEM;
    }
    return 0;
}

// Map the page of memory at 'srcva' in srcenvid's address space
// at 'dstva' in dstenvid's address space with permission 'perm'.
// Perm has the same restrictions as in sys_page_alloc, except
// that it also must not grant write access to a read-only
// page.
//
// Return 0 on success, < 0 on error.  Errors are:
//	-E_BAD_ENV if srcenvid and/or dstenvid doesn't currently exist,
//		or the caller doesn't have permission to change one of them.
//	-E_INVAL if srcva >= UTOP or srcva is not page-aligned,
//		or dstva >= UTOP or dstva is not page-aligned.
//	-E_INVAL is srcva is not mapped in srcenvid's address space.
//	-E_INVAL if perm is inappropriate (see sys_page_alloc).
//	-E_INVAL if (perm & PTE_W), but srcva is read-only in srcenvid's
//		address space.
//	-E_NO_MEM if there's no memory to allocate any necessary page tables.
static int sys_page_map(envid_t srcenvid, void *srcva,
         envid_t dstenvid, void *dstva, int perm) {
    // Hint: This function is a wrapper around page_lookup() and
    //   page_insert() from kern/pmap.c.
    //   Again, most of the new code you write should be to check the
    //   parameters for correctness.
    //   Use the third argument to page_lookup() to
    //   check the current permissions on the page.

    // LAB 4: Your code here.
    if (((uint32_t)srcva >= UTOP || PGOFF(srcva) != 0) ||
        ((uint32_t)dstva >= UTOP || PGOFF(dstva) != 0) ||
        ((perm & (PTE_U | PTE_P)) != (PTE_U | PTE_P)) ||
        ((perm & ~(PTE_SYSCALL)) != 0))
        return -E_INVAL;
    struct Env *srcenv, *dstenv;
    int rv_envid2env = envid2env(srcenvid, &srcenv, 1);
    if (rv_envid2env != 0) return rv_envid2env;
    rv_envid2env = envid2env(dstenvid, &dstenv, 1);
    if (rv_envid2env != 0) return rv_envid2env;
    pte_t *pte;
    struct PageInfo *page_info = page_lookup(srcenv->env_pgdir, srcva, &pte);
    if (page_info == NULL) return -E_INVAL;
    if ((*pte & PTE_W) == 0 && (perm & PTE_W) == PTE_W) return -E_INVAL;
    int rv_page_insert = page_insert(dstenv->env_pgdir, page_info, dstva, perm);
    if (rv_page_insert != 0) return rv_page_insert;
    return 0;
}

// Unmap the page of memory at 'va' in the address space of 'envid'.
// If no page is mapped, the function silently succeeds.
//
// Return 0 on success, < 0 on error.  Errors are:
//	-E_BAD_ENV if environment envid doesn't currently exist,
//		or the caller doesn't have permission to change envid.
//	-E_INVAL if va >= UTOP, or va is not page-aligned.
static int sys_page_unmap(envid_t envid, void *va) {
    // Hint: This function is a wrapper around page_remove().
    // LAB 4: Your code here.
    if ((uint32_t)va >= UTOP || PGOFF(va) != 0) return -E_INVAL;
    struct Env *penv;
    int rv_envid2env = envid2env(envid, &penv, 1);
    if (rv_envid2env != 0) return rv_envid2env;
    page_remove(penv->env_pgdir, va);
    return 0;
}
```

Hook them to the `syscall()` in *lab/kern/syscall.c*:

```diff
@@ -274,6 +308,11 @@ int32_t syscall(uint32_t syscallno, uint32_t a1, uint32_t a2, uint32_t a3, uint3
        case SYS_getenvid: return sys_getenvid();
        case SYS_env_destroy: return sys_env_destroy(a1);
        case SYS_yield: sys_yield(); return 0;
+       case SYS_exofork: return sys_exofork();
+       case SYS_env_set_status: return sys_env_set_status(a1, a2);
+       case SYS_page_alloc: return sys_page_alloc(a1, (void *)a2, a3);
+       case SYS_page_map: return sys_page_map(a1, (void *)a2, a3, (void *)a4, a5);
+       case SYS_page_unmap: return sys_page_unmap(a1, (void *)a2);
        default: return -E_INVAL;
        }
 }
```

Verify the work in *lab*:

```shell
$ make clean && make grade
    ⋮
dumbfork: OK (2.8s)
Part A score: 5/5
    ⋮
```

# Part B: Copy-on-write Fork

Unix provides the `fork()` system call as its primary process creation primitive. xv6 implements `fork()` by copying all data from the parent's pages into new pages allocated for the child. This is essentially the same approach that `dumbfork()` takes.

A call to `fork()` is frequently followed almost immediately by a call to `exec()` in the child process, which replaces the child's memory with a new program. In such a case, the time spent copying the parent's address space is largely wasted, because the child process will use very little of its memory before calling `exec()`.

For this reason, later versions of Unix took advantage of virtual memory hardware to allow the parent and child to *share* the memory mapped into their respective address spaces until one of the processes actually modifies it. This technique is known as *copy-on-write*.

## User-level Page Fault Handling

A user-level *copy-on-write* `fork()` needs to know about page faults on paging. Most Unix kernels initially map only a single page in a new process's stack region, and allocate and map additional stack pages later *on demand* as the process's stack consumption increases and causes page faults on stack addresses that are not yet mapped.

A typical Unix kernel must keep track of what action to take when a page fault occurs in each region of a process's space. For example, a fault in the stack region will typically allocate and map new page of physical memory, a fault in the program's BSS region wil typically allocate a new page, fill it with zeroes, and map it, and a fault in the text region will read the corresponding page of the binary off of disk and then map it.

In order to handle its own page faults, a user environment will need to register a *page fault handler entrypoint* with the JOS kernel. The user environment registers its page fault entrypoint via the new `sys_env_set_pgfault_upcall` system call. The `Env` field `env_pgfault_upcall` is recording this information.

## Exercise 8

> Implement the sys\_env\_set\_pgfault\_upcall system call. Be sure to enable permission checking when looking up the environment ID of the target environment, since this is a "dangerous" system call.

The function `sys_env_set_pgfault_upcall` is mainly retrieving the `struct Env` pointer by `envid_t` and is setting the handler function to the pointer. The implementation is as follows:
```c
// Set the page fault upcall for 'envid' by modifying the corresponding struct
// Env's 'env_pgfault_upcall' field.  When 'envid' causes a page fault, the
// kernel will push a fault record onto the exception stack, then branch to
// 'func'.
//
// Returns 0 on success, < 0 on error.  Errors are:
//	-E_BAD_ENV if environment envid doesn't currently exist,
//		or the caller doesn't have permission to change envid.
static int sys_env_set_pgfault_upcall(envid_t envid, void *func) {
    struct Env *env;
    if (envid2env(envid, &env, 1) != 0) return -E_BAD_ENV;
    env->env_pgfault_upcall = func;
    return 0;
}
```

And hook the syscall function:

```diff
diff --git a/lab/kern/syscall.c b/lab/kern/syscall.c
index 25ef9ac..86faca1 100644
--- a/lab/kern/syscall.c
+++ b/lab/kern/syscall.c
@@ -313,6 +313,7 @@ int32_t syscall(uint32_t syscallno, uint32_t a1, uint32_t a2, uint32_t a3, uint3
        case SYS_page_alloc: return sys_page_alloc(a1, (void *)a2, a3);
        case SYS_page_map: return sys_page_map(a1, (void *)a2, a3, (void *)a4, a5);
        case SYS_page_unmap: return sys_page_unmap(a1, (void *)a2);
+       case SYS_env_set_pgfault_upcall: return sys_env_set_pgfault_upcall(a1, (void *)a2);
        default: return -E_INVAL;
        }
 }
```

## Normal and Exception Stacks in User Environments

During normal execution, a user environment in JOS will run on the *normal* user stack: its `ESP` regitster starts out pointing at `USTACKTOP`, and the stack data it pusheds resides on the page between `USTACKTOP - PGSIZE` and `USTACKTOP - 1`, inclusive. However, when a page fault occurs in user mode, the kernel will restart the user environment running a designated user-level page fault handler on a different stack, namely the *user exception* stack. JOS must implement *stack switching*.

The JOS *exception stack* is one page in size, and its top is defined to be at virtual address *UXSTACKTOP*, so the valid bytes of the user exception stack are from `UXSTACKTOP - PGSIZE` through `UXSTACKTOP - 1`, inclusive. While running on this exception stack, the user-level page fault handler can use JOS's regular system calls to map new pages or adjust mappings. Then the user-level page fault handler returns, via an assembly language stub, to the faulting code on the original stack.

Each user environment that wants to support user-level page fault handling will need to allocate memory for its own exception stack, using the `sys_page_alloc()` system call.

## Invoking the User Page Fault Handler

The page fault handling code for user mode in *lab/kern/trap.c* should be changed as follows. The state of the user environment at the time of the fault is called the *trap-time* state.

If there is no page fault handler registered, JOS kernel destroyed the user environment as before; otherwise, the kernel sets up a trap frame on the exception stack that looks like a `struct UTrapframe` from *lab/inc/trap.h*:

```shell
                    <-- UXSTACKTOP
trap-time esp
trap-time eflags
trap-time eip
trap-time eax       start of struct PushRegs
trap-time ecx
trap-time edx
trap-time ebx
trap-time esp
trap-time ebp
trap-time esi
trap-time edi       end of struct PushRegs
tf_err (error code)
fault_va            <-- %esp when handler is run
```

The kernel then arranges for the user environment to resume execution with the page fault handler running on the exception stack with this stack frame. The `fault_va` is the virtual address that caused the page fault.

If the user environment is already running on the user exception stack when an exception occurs, then the page fault handler itself has faulted. In this case, you should start the new stack frame just under the current `tf->tf_esp` rather than at `UXSTACKTOP`. One should first push an empty 32-bit word and then a `struct UTrapframe`.

To test whether `tf->tf_esp` is already on the user exception stack, check whether it is in the range between `UXSTACKTOP - PGSIZE` and `UXSTACKTOP - 1`, inclusive.

## Exercise 9

> Implement the code in page_fault_handler in kern/trap.c required to dispatch page faults to the user-mode handler. Be sure to take appropriate precautions when writing into the exception stack. (What happens if the user environment runs out of space on the exception stack?)

The implementation is as follows:

```c
void page_fault_handler(struct Trapframe *tf) {
    uint32_t fault_va;

    // Read processor's CR2 register to find the faulting address
    fault_va = rcr2();

    // Handle kernel-mode page faults.

    // LAB 3: Your code here.
    if ((tf->tf_cs & 3) == 0) panic("page fault in kernel mode");

    // We've already handled kernel-mode exceptions, so if we get here,
    // the page fault happened in user mode.

    // Call the environment's page fault upcall, if one exists.  Set up a
    // page fault stack frame on the user exception stack (below
    // UXSTACKTOP), then branch to curenv->env_pgfault_upcall.
    //
    // The page fault upcall might cause another page fault, in which case
    // we branch to the page fault upcall recursively, pushing another
    // page fault stack frame on top of the user exception stack.
    //
    // It is convenient for our code which returns from a page fault
    // (lib/pfentry.S) to have one word of scratch space at the top of the
    // trap-time stack; it allows us to more easily restore the eip/esp. In
    // the non-recursive case, we don't have to worry about this because
    // the top of the regular user stack is free.  In the recursive case,
    // this means we have to leave an extra word between the current top of
    // the exception stack and the new stack frame because the exception
    // stack _is_ the trap-time stack.
    //
    // If there's no page fault upcall, the environment didn't allocate a
    // page for its exception stack or can't write to it, or the exception
    // stack overflows, then destroy the environment that caused the fault.
    // Note that the grade script assumes you will first check for the page
    // fault upcall and print the "user fault va" message below if there is
    // none.  The remaining three checks can be combined into a single test.
    //
    // Hints:
    //   user_mem_assert() and env_run() are useful here.
    //   To change what the user environment runs, modify 'curenv->env_tf'
    //   (the 'tf' variable points at 'curenv->env_tf').

    // LAB 4: Your code here.
    if (curenv->env_pgfault_upcall != NULL) {
        struct UTrapframe *uframe;
        if ((tf->tf_esp >= UXSTACKTOP - PGSIZE) && (tf->tf_esp < UXSTACKTOP)) { // grow an extra layer of stack frame
            *(uint32_t *)(tf->tf_esp - 4) = 0;  // push an empty 32-bit word
            uframe = (struct UTrapframe *)(tf->tf_esp - 4 - sizeof(struct UTrapframe));
        } else {
            uframe = (struct UTrapframe *)(UXSTACKTOP - sizeof(struct UTrapframe));
        }
        
        user_mem_assert(curenv, (void *)uframe, sizeof(struct UTrapframe), PTE_W | PTE_U);
        
        uframe->utf_esp = tf->tf_esp;
        uframe->utf_eflags = tf->tf_eflags;
        uframe->utf_eip = tf->tf_eip;
        uframe->utf_regs = tf->tf_regs;
        uframe->utf_err = tf->tf_err;
        uframe->utf_fault_va = fault_va;

        // environment context switch
        tf->tf_esp = (uint32_t)uframe;
        tf->tf_eip = (uint32_t)curenv->env_pgfault_upcall;
        env_run(curenv);
    }
    // Destroy the environment that caused the fault.
    cprintf("[%08x] user fault va %08x ip %08x\n",
        curenv->env_id, fault_va, tf->tf_eip);
    print_trapframe(tf);
    env_destroy(curenv);
}
```

See the code of `user_mem_assert` to answer the question: `env_destroy` is called if any address space is not accessible.

## Exercise 10

> Implement the \_pgfault\_upcall routine in lib/pfentry.S. The interesting part is returning to the original point in the user code that caused the page fault. You'll return directly there, without going back through the kernel. The hard part is simultaneously switching stacks and re-loading the EIP.

The assembly routine `_pgfault_upcall` in *lab/lib/pfentry.S* takes care of calling the C page fault handler and resume execution at the original faulting instaruction. This assembly routine is the handler that is registered with the kernel using `sys_env_set_pgfault_upcall()`.

The implementiona is as follows (I cannot implement this exercise and copy other's work):

```x86asm
_pgfault_upcall:
    // Call the C page fault handler.
    pushl %esp			// function argument: pointer to UTF
    movl _pgfault_handler, %eax
    call *%eax
    addl $4, %esp			// pop function argument
    
    // Now the C page fault handler has returned and you must return
    // to the trap time state.
    // Push trap-time %eip onto the trap-time stack.
    //
    // Explanation:
    //   We must prepare the trap-time stack for our eventual return to
    //   re-execute the instruction that faulted.
    //   Unfortunately, we can't return directly from the exception stack:
    //   We can't call 'jmp', since that requires that we load the address
    //   into a register, and all registers must have their trap-time
    //   values after the return.
    //   We can't call 'ret' from the exception stack either, since if we
    //   did, %esp would have the wrong value.
    //   So instead, we push the trap-time %eip onto the *trap-time* stack!
    //   Below we'll switch to that stack and call 'ret', which will
    //   restore %eip to its pre-fault value.
    //
    //   In the case of a recursive fault on the exception stack,
    //   note that the word we're pushing now will fit in the
    //   blank word that the kernel reserved for us.
    //
    // Throughout the remaining code, think carefully about what
    // registers are available for intermediate calculations.  You
    // may find that you have to rearrange your code in non-obvious
    // ways as registers become unavailable as scratch space.
    //
    // LAB 4: Your code here.
    movl 0x30(%esp), %ecx    // save trap-time esp in ecx
    subl $4, %ecx            // enlarge the previous stack for 4 bytes
    movl %ecx, 0x30(%esp)    // write the modified esp back
    movl 0x28(%esp), %edx    // save trap-time eip in edx
    movl %edx, (%ecx)        // save eip at new esp for return	
    // Restore the trap-time registers.  After you do this, you
    // can no longer modify any general-purpose registers.
    // LAB 4: Your code here.
    addl $8, %esp            // skip fault_va and tf_err
    popal                    // pop PushRegs
    // Restore eflags from the stack.  After you do this, you can
    // no longer use arithmetic operations or anything else that
    // modifies eflags.
    // LAB 4: Your code here.
    addl $4, %esp            // skip eip
    popfl                    // pop eflags
    // Switch back to the adjusted trap-time stack.
    // LAB 4: Your code here.
    pop %esp
    // Return to re-execute the instruction that faulted.
    // LAB 4: Your code here.
    ret
```

## Exercise 11

> Finish set\_pgfault\_handler() in lib/pgfault.c.

The user-level page fault handling is implemented as follows:

```c
//
// Set the page fault handler function.
// If there isn't one yet, _pgfault_handler will be 0.
// The first time we register a handler, we need to
// allocate an exception stack (one page of memory with its top
// at UXSTACKTOP), and tell the kernel to call the assembly-language
// _pgfault_upcall routine when a page fault occurs.
//
void set_pgfault_handler(void (*handler)(struct UTrapframe *utf)) {
    int r;
    if (_pgfault_handler == 0) {
        // First time through!
        // LAB 4: Your code here.
        r = sys_page_alloc(thisenv->env_id, (void *)(UXSTACKTOP - PGSIZE), PTE_W | PTE_U | PTE_P);
        if (r != 0) panic("failed to handle allocating user-level page");
        r = sys_env_set_pgfault_upcall(thisenv->env_id, _pgfault_upcall);
        if (r != 0) panic("failed to set user-level page-fault upcall");
    }
    // Save handler pointer for assembly to call.
    _pgfault_handler = handler;
}
```

Verify the work done in Exercise 8, 9, 10, and 11:

- `run-faultread-nox````shell
    $ make clean && make run-faultread-nox
        ⋮
    enabled interrupts: 1 2
    [00000000] new env 00001000
    Incoming TRAP frame at 0xf0234fbc
    Incoming TRAP frame at 0xf0234fbc
    [00001000] user fault va 00000000 ip 00800039
    TRAP frame at 0xf02af000 from CPU 0
      edi  0x00000000
      esi  0x00000000
      ebp  0xeebfdfd0
      oesp 0xf0234fdc
      ebx  0x00000000
      edx  0x00000000
      ecx  0x00000000
      eax  0xeec00000
      es   0x----0023
      ds   0x----0023
      trap 0x0000000e Page Fault
      cr2  0x00000000
      err  0x00000004 [user, read, not-present]
      eip  0x00800039
      cs   0x----001b
      flag 0x00000086
      esp  0xeebfdfc0
      ss   0x----0023
    [00001000] free env 00001000
    No runnable environments in the system!
    Welcome to the JOS kernel monitor!
    Type 'help' for a list of commands.
    K>
    ```
- `run-faultdie-nox````shell
    $ make clean && make run-faultdie-nox
        ⋮
    enabled interrupts: 1 2
    [00000000] new env 00001000
    Incoming TRAP frame at 0xf0234fbc
    Incoming TRAP frame at 0xf0234fbc
    Incoming TRAP frame at 0xf0234fbc
    Incoming TRAP frame at 0xf0234fbc
    Incoming TRAP frame at 0xf0234fbc
    i faulted at va deadbeef, err 6
    Incoming TRAP frame at 0xf0234fbc
    Incoming TRAP frame at 0xf0234fbc
    [00001000] exiting gracefully
    [00001000] free env 00001000
    No runnable environments in the system!
    Welcome to the JOS kernel monitor!
    Type 'help' for a list of commands.
    K>
    ```
- `run-faultalloc-nox````shell
    $ make clean && make run-faultalloc-nox
        ⋮
    enabled interrupts: 1 2
    [00000000] new env 00001000
    Incoming TRAP frame at 0xf0234fbc
    Incoming TRAP frame at 0xf0234fbc
    Incoming TRAP frame at 0xf0234fbc
    Incoming TRAP frame at 0xf0234fbc
    Incoming TRAP frame at 0xf0234fbc
    fault deadbeef
    Incoming TRAP frame at 0xf0234fbc
    Incoming TRAP frame at 0xf0234fbc
    this string was faulted in at deadbeef
    Incoming TRAP frame at 0xf0234fbc
    Incoming TRAP frame at 0xf0234fbc
    fault cafebffe
    Incoming TRAP frame at 0xf0234fbc
    Incoming TRAP frame at 0xf0234fbc
    Incoming TRAP frame at 0xf0234fbc
    fault cafec000
    Incoming TRAP frame at 0xf0234fbc
    Incoming TRAP frame at 0xf0234fbc
    this string was faulted in at cafebffe
    Incoming TRAP frame at 0xf0234fbc
    [00001000] exiting gracefully
    [00001000] free env 00001000
    No runnable environments in the system!
    Welcome to the JOS kernel monitor!
    Type 'help' for a list of commands.
    K>
    ```
- `run-faultallocbad-nox````shell
    $ make clean && make run-faultallocbad-nox
        ⋮
    enabled interrupts: 1 2
    [00000000] new env 00001000
    Incoming TRAP frame at 0xf0234fbc
    Incoming TRAP frame at 0xf0234fbc
    Incoming TRAP frame at 0xf0234fbc
    Incoming TRAP frame at 0xf0234fbc
    [00001000] user_mem_check assertion failure for va deadbeef
    [00001000] free env 00001000
    No runnable environments in the system!
    Welcome to the JOS kernel monitor!
    Type 'help' for a list of commands.
    K>
    ```

## Exercise 12

>Implement fork, duppage and pgfault in lib/fork.c.
>
>Test your code with the forktree program. It should produce the following messages, with interspersed 'new env', 'free env', and 'exiting gracefully' messages. The messages may not appear in this order, and the environment IDs may be different.
>
>```shell
>	1000: I am ''
>	1001: I am '0'
>	2000: I am '00'
>	2001: I am '000'
>	1002: I am '1'
>	3000: I am '11'
>	3001: I am '10'
>	4000: I am '100'
>	1003: I am '01'
>	5000: I am '010'
>	4001: I am '011'
>	2002: I am '110'
>	1004: I am '001'
>	1005: I am '111'
>	1006: I am '101'
>```	

Like `dumbfork()`, `fork()` should create a new environment, then scan throught the parent environment's entire address space and set up corresponding page mappings in the child. The key difference is that, while `dumbfork()` copied *pages*, `fork()` will initially only copy page *mappings*. `fork()` will copy each page only when one fo the environments tries to write it.

The basic control flow for `fork()` is as follows:
1. The parent installs `pgfault()` as the c-level page fault hander, using the `set_pgfault_handler()` function.
2. The parent calls `sys_exofork()` to create a child environment.
3. For each writable or copy-on-write page in its address space below `UTOP`, the parent calls `duppage`, which should map the page copy-on-write into the address space of the child and then *remap* the page copy-on-write in its own address space. `duppage` sets both `PTE`s so that the page is not writeable, and to contain `PTE_COW` in the `avail` field to distinguish copy-on-write pages from genuine read-only pages. The exception stack is not remapped this way, however. Instead one needs to allocate a fresh page in the child from the exception stak. Since the page fault handler will be doing the actual copying and the page fault handler runs on the exception stack, the exception stack cannot be made copy-on-write. `fork()` also needs to handle pages that are present, but not writable or copy-on-write.
4. The parent sets the user page fault entrypoint for the child to look like its own.
5. The child is now ready to run, so the parent marks it runnable.

Each time one of the environments writes a copy-on-write page that it hasn't yet written, it will take a page fault. Here's the control flow for the user page fault handler:
1. The kernel propagates the page fault to `_pgfault_upcall`, which calls `fork()`'s `pgfault()` handler.
2. `pgfault()` checks that the fault is a write (check for `FEC_WR` in the error code) and that the `PTE` for the page is marked `PTE_COW`. If not, panic.
3. `pgfault()` allocates a new page mapped at a temporary location and copies the contents of the faulting page into it. Then the fault handler maps the new page at the appropriate address with read/write permissions, in place of the old read-only mapping.

The user-level *lab/lib/fork.c* code must consult the environment's page tables for several of the operations above (e.g. that the `PTE` for a page is marked `PTE_COW`). The kernel maps the environment's page tables at `UVPT` exactly for this purpose. It uses a [clever mapping trick](http://pdos.csail.mit.edu/6.828/2018/labs/lab4/uvpt.html) to make it easy to lookup `PTE`s for user code. *lab/lib/entry.S* sets up `uvpt` and `uvpd` so that one can easily lookup page-table information in *lab/lib/fork.c*.

The implementation is as follows:
- `fork()
	```c
	// User-level fork with copy-on-write.
	// Set up our page fault handler appropriately.
	// Create a child.
	// Copy our address space and page fault handler setup to the child.
	// Then mark the child as runnable and return.
	//
	// Returns: child's envid to the parent, 0 to the child, < 0 on error.
	// It is also OK to panic on error.
	//
	// Hint:
	//   Use uvpd, uvpt, and duppage.
	//   Remember to fix "thisenv" in the child process.
	//   Neither user exception stack should ever be marked copy-on-write,
	//   so you must allocate a new page for the child's user exception stack.
	//
	envid_t fork(void) {
		// LAB 4: Your code here.
		set_pgfault_handler(pgfault);
		envid_t envid_child = sys_exofork();
		if (envid_child < 0) panic("failed to fork");
		else if (envid_child == 0) {
			thisenv = &envs[ENVX(sys_getenvid())];
			return 0;
		}
		// map the address space for the child
		uint32_t addr;
		for (addr = 0; addr < USTACKTOP; addr += PGSIZE)
			if (((uvpd[PDX(addr)] & PTE_P) == PTE_P) &&
				((uvpt[PGNUM(addr)] & PTE_P) == PTE_P))
				duppage(envid_child, PGNUM(addr));

		// allocate a new page for the child's user exception stack
		void _pgfault_upcall();
		int return_code = sys_page_alloc(envid_child, (void *)(UXSTACKTOP - PGSIZE), PTE_W | PTE_U | PTE_P);
		if (return_code != 0) panic("failed to allocate exeption stack for the child");
		return_code = sys_env_set_pgfault_upcall(envid_child, _pgfault_upcall);
		if (return_code != 0) panic("failed to hook user-level page-fault handler for the child");

		// mark child RUNNABLE
		return_code = sys_env_set_status(envid_child, ENV_RUNNABLE);
		if (return_code != 0) panic("failed to set the child RUNNABLE");

		return envid_child;
	}
	```
- `duppage`
	```c
	// Map our virtual page pn (address pn*PGSIZE) into the target envid
	// at the same virtual address.  If the page is writable or copy-on-write,
	// the new mapping must be created copy-on-write, and then our mapping must be
	// marked copy-on-write as well.
	//
	// Returns: 0 on success, < 0 on error.
	// It is also OK to panic on error.
	//
	static int duppage(envid_t envid, unsigned pn) {
		int r;
		// LAB 4: Your code here.
		envid_t envid_parent = sys_getenvid();
		void *va = (void *)(pn * PGSIZE);
		if (((uvpt[pn] & PTE_W) == PTE_W)
			|| ((uvpt[pn] & PTE_COW) == PTE_COW)) { // the page is writable or copy-on-write
			r = sys_page_map(envid_parent, va, envid, va, PTE_COW | PTE_U | PTE_P);
			if (r != 0) panic("failed to map copy-on-write page for a child");
			r = sys_page_map(envid_parent, va, envid_parent, va, PTE_COW | PTE_U | PTE_P); // the parent page is marked copy-on-write
			if (r != 0) panic("failed to set parent page copy-on-write");
		} else {
			r = sys_page_map(envid_parent, va, envid, va, PTE_U | PTE_P);
			if (r != 0) panic("failed to map page for a child");
		}
		return 0;
	}
	```
- `pgfault`
	```c
	// Custom page fault handler - if faulting page is copy-on-write,
	// map in our own private writable copy.
	//
	static void pgfault(struct UTrapframe *utf) {
		void *addr = (void *) utf->utf_fault_va;
		uint32_t err = utf->utf_err;
		int r;

		// Check that the faulting access was (1) a write, and (2) a
		// copy-on-write page.  If not, panic.
		// Hint:
		//   Use the read-only page table mappings at uvpt
		//   (see <inc/memlayout.h>).

		// LAB 4: Your code here.
		pte_t pte = uvpt[PGNUM(addr)];
		if (((err & FEC_WR) == 0) ||
			((pte & PTE_COW) == 0)) panic("the faulting access is not write or copy-on-write");

		// Allocate a new page, map it at a temporary location (PFTEMP),
		// copy the data from the old page to the new page, then move the new
		// page to the old page's address.
		// Hint:
		//   You should make three system calls.

		// LAB 4: Your code here.
		envid_t envid = sys_getenvid();
		r = sys_page_alloc(envid, PFTEMP, PTE_W | PTE_U | PTE_P);
		if (r != 0) panic("failed to allocate page for a child");
		memcpy(PFTEMP, ROUNDDOWN(addr, PGSIZE), PGSIZE);
		r = sys_page_map(envid, PFTEMP, envid, ROUNDDOWN(addr, PGSIZE), PTE_W | PTE_U | PTE_P);
		if (r != 0) panic("failed to copy data to the new page for a child");
		r = sys_page_unmap(envid, PFTEMP);
		if (r != 0) panic("failed to unmap temp location");
	}
	```

Verify the work:
```shell
$ make clean && make run-forktree-nox
	⋮
enabled interrupts: 1 2
[00000000] new env 00001000
Incoming TRAP frame at 0xf0238fbc
	⋮
1000: I am ''
Incoming TRAP frame at 0xf0238fbc
	⋮
[00001000] new env 00001001
Incoming TRAP frame at 0xf0238fbc
	⋮
[00001000] new env 00001002
Incoming TRAP frame at 0xf0238fbc
	⋮
[00001000] exiting gracefully
[00001000] free env 00001000
Incoming TRAP frame at 0xf0238fbc
	⋮
1001: I am '0'
Incoming TRAP frame at 0xf0238fbc
[00001001] new env 00002000
Incoming TRAP frame at 0xf0238fbc
	⋮
[00001001] new env 00001003
Incoming TRAP frame at 0xf0238fbc
	⋮
[00001001] exiting gracefully
[00001001] free env 00001001
Incoming TRAP frame at 0xf0238fbc
	⋮
2000: I am '00'
Incoming TRAP frame at 0xf0238fbc
[00002000] new env 00002001
Incoming TRAP frame at 0xf0238fbc
	⋮
[00002000] new env 00001004
Incoming TRAP frame at 0xf0238fbc
	⋮
[00002000] exiting gracefully
[00002000] free env 00002000
Incoming TRAP frame at 0xf0238fbc
	⋮
2001: I am '000'
Incoming TRAP frame at 0xf0238fbc
[00002001] exiting gracefully
[00002001] free env 00002001
Incoming TRAP frame at 0xf0238fbc
	⋮
1002: I am '1'
Incoming TRAP frame at 0xf0238fbc
[00001002] new env 00003001
Incoming TRAP frame at 0xf0238fbc
	⋮
[00001002] new env 00003000
Incoming TRAP frame at 0xf0238fbc
	⋮
[00001002] exiting gracefully
[00001002] free env 00001002
Incoming TRAP frame at 0xf0238fbc
	⋮
3000: I am '11'
Incoming TRAP frame at 0xf0238fbc
[00003000] new env 00002002
Incoming TRAP frame at 0xf0238fbc
	⋮
```

N.B.: The following change must be made; otherwise, the work will not pass the grade:
```diff
diff --git a/lab/kern/trap.c b/lab/kern/trap.c
index 8808ba0..ab5f537 100644
--- a/lab/kern/trap.c
+++ b/lab/kern/trap.c
@@ -280,8 +280,6 @@ trap(struct Trapframe *tf)
        // the interrupt path.
        assert(!(read_eflags() & FL_IF));

-       cprintf("Incoming TRAP frame at %p\n", tf);
-
        if ((tf->tf_cs & 3) == 3) {
                // Trapped from user mode.
                // Acquire the big kernel lock before doing any
```
Now run the grade task:
```shell
$ make clean && make grade
	⋮
faultread: OK (1.9s)
faultwrite: OK (2.4s)
faultdie: OK (2.1s)
faultregs: OK (2.5s)
faultalloc: OK (2.0s)
faultallocbad: OK (2.0s)
faultnostack: OK (2.0s)
faultbadhandler: OK (2.3s)
faultevilhandler: OK (1.6s)
forktree: OK (2.0s)
Part B score: 50/50
	⋮
```

# Part C: Preemptive Multitasking and Inter-Process Communication (IPC)

## Clock Interrupts and Preemption

For now run the `user/spin` test, This test program forks off a child environment, which simply spins forever in a tight loop once it receives control of the CPU. Neither the parent einvrionment nor the kernel ever regains the CPU. The JOS kernel should support external hardware interrupts from the clock hardware to retake control of the CPU.

## Exercise 13

>Modify kern/trapentry.S and kern/trap.c to initialize the appropriate entries in the IDT and provide handlers for IRQs 0 through 15. Then modify the code in env_alloc() in kern/env.c to ensure that user environments are always run with interrupts enabled.
>
>Also uncomment the sti instruction in sched_halt() so that idle CPUs unmask interrupts.
>
>The processor never pushes an error code when invoking a hardware interrupt handler. You might want to re-read section 9.2 of the [80386 Reference Manual](http://pdos.csail.mit.edu/6.828/2018/readings/i386/toc.htm), or section 5.8 of the [IA-32 Intel Architecture Software Developer's Manual, Volume 3](http://pdos.csail.mit.edu/6.828/2018/readings/ia32/IA32-3A.pdf), at this time.
>
>After doing this exercise, if you run your kernel with any test program that runs for a non-trivial length of time (e.g., spin), you should see the kernel print trap frames for hardware interrupts. While interrupts are now enabled in the processor, JOS isn't yet handling them, so you should see it misattribute each interrupt to the currently running user environment and destroy it. Eventually it should run out of environments to destroy and drop into the monitor. 

External interrupts (i.e. device interrupts) are referred to as IRQs. There are 16 pissible IRQs, numbered `0` to `15`. The mapping from IRQ nubmer to IDT entry is not fixed. `pic_init` in *picirq.c* maps IRQs `0`-`15` to IDT entries `IRQ_OFFSET` through `IRQ_OFFSET + 15`.

In *inc/trap.h*, `IRQ_OFFSET` is defined to be decimal `32`. Thus the `IDT` entries `32`-`47` correspond to the IRQs `0`-`15`. For example, the clock interrupt is IRQ `0`. Thus, `IDT[IRQ_OFFSET+0]` (i.e., `IDT[32]`) contains the address of the clock's interrupt handler routine in the kernel. This `IRQ_OFFSET` is chosen so that the device interrupts do not overlap with the processor exceptions, which could obviously cause confusion.

In JOS, we make a key simplification compared to xv6 Unix. External device interrupts are always disabled when in the kernel (and, like xv6, enabled when in user space). External interrupts are controlled by the `FL_IF` flag bit of the `%eflags` register (see *inc/mmu.h*). When this bit is set, external interrupts are enabled. While the bit can be modified in several ways, because of our simplification, we will handle it solely through the process of saving and restoring `%eflags` register as we enter and leave user mode.

The `FL_IF` flag is set in user environments when they run so that when an interrupt arrives, it gets passed through to the processor and handled by your interrupt code. Otherwise, interrupts are masked, or ignored until interrupts are re-enabled. We masked interrupts with the very first instruction of the bootloader, and so far we have never gotten around to re-enabling them.

Most implementation work is setting up the external interrupts (i.e. IRQs) and is similar to the Exercise 4 of Lab 3. One also needs to enable IRQs in user mode by setting the `FL_IF` flag bit of the `%eflags` register and to uncomment the `sti` instruction in `sched_halt()` in *lab/kern/sched.c*. The implementation is as follows:
```diff
diff --git a/lab/kern/env.c b/lab/kern/env.c
index 8921a4f..9bbf95d 100644
--- a/lab/kern/env.c
+++ b/lab/kern/env.c
@@ -253,7 +253,8 @@ env_alloc(struct Env **newenv_store, envid_t parent_id)
 
 	// Enable interrupts while in user mode.
 	// LAB 4: Your code here.
-
+	e->env_tf.tf_eflags |= FL_IF;
+	
 	// Clear the page fault handler until user installs one.
 	e->env_pgfault_upcall = 0;
 
diff --git a/lab/kern/sched.c b/lab/kern/sched.c
index fe08f60..e43fb0e 100644
--- a/lab/kern/sched.c
+++ b/lab/kern/sched.c
@@ -87,7 +87,7 @@ sched_halt(void)
 		"pushl $0\n"
 		"pushl $0\n"
 		// Uncomment the following line after completing exercise 13
-		//"sti\n"
+		"sti\n"
 		"1:\n"
 		"hlt\n"
 		"jmp 1b\n"
diff --git a/lab/kern/trap.c b/lab/kern/trap.c
index 7be5514..d550cb5 100644
--- a/lab/kern/trap.c
+++ b/lab/kern/trap.c
@@ -89,6 +89,12 @@ void trap_init(void) {
 	void machineCheckHandler();
 	void SIMDFloatingPointExceptionHandler();
 	void syscallHandler();
+	void timerHandler();
+	void kbdHandler();
+	void serialHandler();
+	void spuriousHandler();
+	void ideHandler();
+	void errorHandler();
 
 	SETGATE(idt[T_DIVIDE], 0, GD_KT, &divideErrorHandler, 0);
 	SETGATE(idt[T_DEBUG], 0, GD_KT, &debugHandler, 0);
@@ -109,7 +115,13 @@ void trap_init(void) {
 	SETGATE(idt[T_MCHK], 0, GD_KT, &machineCheckHandler, 0);
 	SETGATE(idt[T_SIMDERR], 0, GD_KT, &SIMDFloatingPointExceptionHandler, 0);
 	SETGATE(idt[T_SYSCALL], 0, GD_KT, &syscallHandler, 3);
-
+	SETGATE(idt[IRQ_OFFSET + IRQ_TIMER], 0, GD_KT, &timerHandler, 0);
+	SETGATE(idt[IRQ_OFFSET + IRQ_KBD], 0, GD_KT, &kbdHandler, 0);
+	SETGATE(idt[IRQ_OFFSET + IRQ_SERIAL], 0, GD_KT, &serialHandler, 0);
+	SETGATE(idt[IRQ_OFFSET + IRQ_SPURIOUS], 0, GD_KT, &spuriousHandler, 0);
+	SETGATE(idt[IRQ_OFFSET + IRQ_IDE], 0, GD_KT, &ideHandler, 0);
+	SETGATE(idt[IRQ_OFFSET + IRQ_ERROR], 0, GD_KT, &errorHandler, 0);
+	
 	// Per-CPU setup
 	trap_init_percpu();
 }
diff --git a/lab/kern/trapentry.S b/lab/kern/trapentry.S
index 1ef3c0c..8b34ed3 100644
--- a/lab/kern/trapentry.S
+++ b/lab/kern/trapentry.S
@@ -69,6 +69,12 @@ TRAPHANDLER_NOEC(alignmentCheckHandler, T_ALIGN);
 TRAPHANDLER_NOEC(machineCheckHandler, T_MCHK);
 TRAPHANDLER_NOEC(SIMDFloatingPointExceptionHandler, T_SIMDERR);
 TRAPHANDLER_NOEC(syscallHandler, T_SYSCALL);
+TRAPHANDLER_NOEC(timerHandler, IRQ_OFFSET + IRQ_TIMER);
+TRAPHANDLER_NOEC(kbdHandler, IRQ_OFFSET + IRQ_KBD);
+TRAPHANDLER_NOEC(serialHandler, IRQ_OFFSET + IRQ_SERIAL);
+TRAPHANDLER_NOEC(spuriousHandler, IRQ_OFFSET + IRQ_SPURIOUS);
+TRAPHANDLER_NOEC(ideHandler, IRQ_OFFSET + IRQ_IDE);
+TRAPHANDLER_NOEC(errorHandler, IRQ_OFFSET + IRQ_ERROR);
 
 /*
  * Lab 3: Your code here for _alltraps
```

## Exercise 14

>Modify the kernel's trap_dispatch() function so that it calls sched_yield() to find and run a different environment whenever a clock interrupt takes place.
>
>You should now be able to get the user/spin test to work: the parent environment should fork off the child, sys_yield() to it a couple times but in each case regain control of the CPU after one time slice, and finally kill the child environment and terminate gracefully. 

So far, the kernel has not been able to get control back when the `user/spin` is running. Now it is time to grogram the hardware to generate clock interrupts periodically, which will force control back to the kernel.

The calls to `lapic_init` in `i386_init` in *lab/kern/init.c* set up the clock and the interrupt controller to generate interrupts.

This exercise requires to write the code to handle these interrupts:
```diff
diff --git a/lab/kern/trap.c b/lab/kern/trap.c
index d550cb5..8808ba0 100644
--- a/lab/kern/trap.c
+++ b/lab/kern/trap.c
@@ -229,6 +229,10 @@ static void trap_dispatch(struct Trapframe *tf) {
 					      tf->tf_regs.reg_ecx, tf->tf_regs.reg_ebx,
 					      tf->tf_regs.reg_edi, tf->tf_regs.reg_esi);
 		return;
+	case IRQ_OFFSET + IRQ_TIMER:
+		lapic_eoi();
+		sched_yield();
+		return;
 	case IRQ_OFFSET + IRQ_SPURIOUS:
 	// Handle spurious interrupts
 	// The hardware sometimes raises these because of noise on the
```

Verify the work:
```shell
$ make clean && make CPUS=2 run-spin-nox
	⋮
SMP: CPU 0 found 2 CPU(s)
enabled interrupts: 1 2
SMP: CPU 1 starting
[00000000] new env 00001000
I am the parent.  Forking the child...
[00001000] new env 00001001
I am the parent.  Running the child...
I am the child.  Spinning...
I am the parent.  Killing the child...
[00001000] destroying 00001001
[00001001] free env 00001001
[00001000] exiting gracefully
[00001000] free env 00001000
No runnable environments in the system!
Welcome to the JOS kernel monitor!
Type 'help' for a list of commands.
K>
```
And run the grade in the directory *lab*:
```shell
$ make clean && make grade
	⋮
spin: OK (2.3s)
stresssched: OK (4.2s)
	⋮
```

## Exercise 15

>Implement sys_ipc_recv and sys_ipc_try_send in kern/syscall.c. Read the comments on both before implementing them, since they have to work together. When you call envid2env in these routines, you should set the checkperm flag to 0, meaning that any environment is allowed to send IPC messages to any other environment, and the kernel does no special permission checking other than verifying that the target envid is valid.
>
>Then implement the ipc_recv and ipc_send functions in lib/ipc.c.
>
>Use the user/pingpong and user/primes functions to test your IPC mechanism. user/primes will generate for each prime number a new environment until JOS runs out of environments. You might find it interesting to read user/primes.c to see all the forking and IPC going on behind the scenes. 

In JOS, the term *inter-process communication (IPC)* is equal to the term *inter-environment communication (IEC)*. This Unix pipe model is a canonical mechanism to allow programs to communicate with each other. There are many models for interprocess communication. This exercise requires to implement a simple IPC mechanism.

Two system calls, `sys_ipc_recv` and `sys_ipc_try_send`, need to be implemented. Two library wrappers `ipc_recv` and `ipc_send` are also needed to be implemented.

The *messages* consist of two components: a single `32`-bit value, and optionally a single page mapping. Allowing environments to pass page mappings in messages provides an efficient way to transfer more data that will fit into a single `32`-bit integer, and also allows environments to set up shared memory arrangements easily.

To receive a message, an environment calls `sys_ipc_recv`. This system call de-schedules the current environment and does not run it again until a message has been received. When an environment is waiting to receive a message, any other environment can send it a message---not just a particular environment, and not just environments that have a parent/child arrangement with the receiving environment. In other words, the permission checking implemented in Part A will not apply to IPC, because the IPC system calls are carefully designed so as to be *safe*: an environment cannot cause another environment to malfunction simply by sending it messages.

To try to send a value, an environment calls `sys_ipc_try_send` with both the receiver's environment id and the value to be sent. If the named environment is actually receiving (it has called `sys_ipc_recv` and not gotten a value yet), then the send delivers the message and returns `0`; otherwise the send returns `-E_IPC_NOT_RECV` to indicate that the target environment is not currently expecting to receive a value.

A library function `ipc_recv` in user space will take care of calling `sys_ipc_recv` and then looking up the information about the received values in the current environment's `struct Env`.

Similarly, a library function `ipc_send` will take care of repeatedly calling `sys_ipc_try_send` until the send succeeds.

When an environment calls `sys_ipc_recv` with a valid `dstva` parameter (below `UTOP`), the environment is stating that it is willing to receive a page mapping. If the sender sends a page, then that page should be mapped at `dstva` in the receiver's address space. If the receiver already had a page mapped at `dstva`, then that previous page is unmapped.

When an environment calls `sys_ipc_try_send` with a valid `srcva` (below `UTOP`), it means the sender wants to send the page currently mapped at `srcva` to the receiver, with permissions `perm`. After a successful IPC, the sender keeps its original mapping for the page at `srcva` in its address space, but the receiver also obtains a mapping for this same physical page at the `dstva` originally specified by the receiver, in the receiver's address space. As a result this page becomes shared between the sender and receiver.

If either the sender or the receiver does not indicate that a page should be transferred, then no page is transferred. After any IPC, the kernel sets the new field `env_ipc_perm` in the receiver's `Env` structure to the permissions of the page received, or zero if no page was received. 

`sys_ipc_recv` and `sys_ipc_try_send` in `lab/kern/syscall.c` are implemented as follows:
```c
// Block until a value is ready.  Record that you want to receive
// using the env_ipc_recving and env_ipc_dstva fields of struct Env,
// mark yourself not runnable, and then give up the CPU.
//
// If 'dstva' is < UTOP, then you are willing to receive a page of data.
// 'dstva' is the virtual address at which the sent page should be mapped.
//
// This function only returns on error, but the system call will eventually
// return 0 on success.
// Return < 0 on error.  Errors are:
//	-E_INVAL if dstva < UTOP but dstva is not page-aligned.
static int sys_ipc_recv(void *dstva) {
	// LAB 4: Your code here.
	if (((uint32_t)dstva < UTOP) &&
	    ((uint32_t)dstva & (PGSIZE - 1)))
		return -E_INVAL;
	curenv->env_status = ENV_NOT_RUNNABLE;
	curenv->env_ipc_recving = 1;
	curenv->env_ipc_dstva = dstva;
	return 0;
}

// Try to send 'value' to the target env 'envid'.
// If srcva < UTOP, then also send page currently mapped at 'srcva',
// so that receiver gets a duplicate mapping of the same page.
//
// The send fails with a return value of -E_IPC_NOT_RECV if the
// target is not blocked, waiting for an IPC.
//
// The send also can fail for the other reasons listed below.
//
// Otherwise, the send succeeds, and the target's ipc fields are
// updated as follows:
//    env_ipc_recving is set to 0 to block future sends;
//    env_ipc_from is set to the sending envid;
//    env_ipc_value is set to the 'value' parameter;
//    env_ipc_perm is set to 'perm' if a page was transferred, 0 otherwise.
// The target environment is marked runnable again, returning 0
// from the paused sys_ipc_recv system call.  (Hint: does the
// sys_ipc_recv function ever actually return?)
//
// If the sender wants to send a page but the receiver isn't asking for one,
// then no page mapping is transferred, but no error occurs.
// The ipc only happens when no errors occur.
//
// Returns 0 on success, < 0 on error.
// Errors are:
//	-E_BAD_ENV if environment envid doesn't currently exist.
//		(No need to check permissions.)
//	-E_IPC_NOT_RECV if envid is not currently blocked in sys_ipc_recv,
//		or another environment managed to send first.
//	-E_INVAL if srcva < UTOP but srcva is not page-aligned.
//	-E_INVAL if srcva < UTOP and perm is inappropriate
//		(see sys_page_alloc).
//	-E_INVAL if srcva < UTOP but srcva is not mapped in the caller's
//		address space.
//	-E_INVAL if (perm & PTE_W), but srcva is read-only in the
//		current environment's address space.
//	-E_NO_MEM if there's not enough memory to map srcva in envid's
//		address space.
static int sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, unsigned perm) {
	// LAB 4: Your code here.
	struct Env *penv;
	int r = envid2env(envid, &penv, 0);
	if (r != 0) return -E_IPC_NOT_RECV;
	if (penv->env_ipc_recving == 0) return -E_IPC_NOT_RECV;
	struct PageInfo *page_info;
	pte_t *pte;
	if ((uintptr_t)srcva < UTOP) {
		if (((uintptr_t)srcva & (PGSIZE - 1)) ||
		    ((perm & (PTE_U | PTE_P)) != (PTE_U | PTE_P)) ||
		    ((perm & ~(PTE_SYSCALL)) != 0) ||
		    ((page_info = page_lookup(curenv->env_pgdir, srcva, &pte)) == NULL) ||
		    (((*pte & PTE_W) == 0) && ((perm & PTE_W) == PTE_W)))
			return -E_INVAL;
		r = page_insert(penv->env_pgdir, page_info, penv->env_ipc_dstva, perm);
		if (r != 0) return r;
		penv->env_ipc_perm = perm;
	} else penv->env_ipc_perm = 0;
	penv->env_ipc_recving = 0;
	penv->env_ipc_from = curenv->env_id;
	penv->env_ipc_value = value;
	penv->env_status = ENV_RUNNABLE;
	return 0;
}
```
Do not forget hooking the dispatchers:
```diff
diff --git a/lab/kern/syscall.c b/lab/kern/syscall.c
index 86faca1..d30014d 100644
--- a/lab/kern/syscall.c
+++ b/lab/kern/syscall.c
@@ -314,6 +336,8 @@ int32_t syscall(uint32_t syscallno, uint32_t a1, uint32_t a2, uint32_t a3, uint3
        case SYS_page_map: return sys_page_map(a1, (void *)a2, a3, (void *)a4, a5);
        case SYS_page_unmap: return sys_page_unmap(a1, (void *)a2);
        case SYS_env_set_pgfault_upcall: return sys_env_set_pgfault_upcall(a1, (void *)a2);
+       case SYS_ipc_recv: return sys_ipc_recv((void *)a1);
+       case SYS_ipc_try_send: return sys_ipc_try_send(a1, a2, (void *)a3, a4);
        default: return -E_INVAL;
        }
 }
```

`ipc_recv` and `ipc_send` functions in `lab/lib/ipc.c` are implemented as follows:
```c
// Receive a value via IPC and return it.
// If 'pg' is nonnull, then any page sent by the sender will be mapped at
//	that address.
// If 'from_env_store' is nonnull, then store the IPC sender's envid in
//	*from_env_store.
// If 'perm_store' is nonnull, then store the IPC sender's page permission
//	in *perm_store (this is nonzero iff a page was successfully
//	transferred to 'pg').
// If the system call fails, then store 0 in *fromenv and *perm (if
//	they're nonnull) and return the error.
// Otherwise, return the value sent by the sender
//
// Hint:
//   Use 'thisenv' to discover the value and who sent it.
//   If 'pg' is null, pass sys_ipc_recv a value that it will understand
//   as meaning "no page".  (Zero is not the right value, since that's
//   a perfectly valid place to map a page.)
int32_t ipc_recv(envid_t *from_env_store, void *pg, int *perm_store) {
	// LAB 4: Your code here.
	int r;
	if (pg == NULL) pg = (void *)UTOP;
	r = sys_ipc_recv(pg);
	if (r < 0) {
		if (from_env_store != NULL) *from_env_store = 0;
		if (perm_store != NULL) *perm_store = 0;
		return r;
	} else {
		if (from_env_store != NULL) *from_env_store = thisenv->env_ipc_from;
		if (perm_store != NULL) *perm_store = thisenv->env_ipc_perm;
	}
	return thisenv->env_ipc_value;
}

// Send 'val' (and 'pg' with 'perm', if 'pg' is nonnull) to 'toenv'.
// This function keeps trying until it succeeds.
// It should panic() on any error other than -E_IPC_NOT_RECV.
//
// Hint:
//   Use sys_yield() to be CPU-friendly.
//   If 'pg' is null, pass sys_ipc_try_send a value that it will understand
//   as meaning "no page".  (Zero is not the right value.)
void ipc_send(envid_t to_env, uint32_t val, void *pg, int perm) {
	// LAB 4: Your code here.
	if (pg == NULL) pg = (void *)UTOP;
	int r = 1;
	while (r != 0) {
		r = sys_ipc_try_send(to_env, val, pg, perm);
		if (r < 0 && r != -E_IPC_NOT_RECV) panic("failed to do ipc_send");
		sys_yield();
	}
}
```

Verify the work in the directory `mitos/lab`:
```shell
$ make clean && make grade
	⋮
OK (2.7s)
Part A score: 5/5

faultread: OK (1.6s)
faultwrite: OK (2.1s)
faultdie: OK (1.5s)
faultregs: OK (1.9s)
faultalloc: OK (2.2s)
faultallocbad: OK (2.6s)
faultnostack: OK (2.0s)
faultbadhandler: OK (2.3s)
faultevilhandler: OK (1.5s)
forktree: OK (2.0s)
Part B score: 50/50

spin: OK (1.9s)
stresssched: OK (2.9s)
sendpage: OK (2.1s)
pingpong: OK (1.9s)
primes: OK (6.7s)
Part C score: 25/25

Score: 80/80
```

## Challenge

>Why does ipc_send have to loop? Change the system call interface so it doesn't have to. Make sure you can handle multiple environments trying to send to one environment at the same time.

IPC is implemented by sharing a page between environments in JOS. At the same time, one environment can only reveive one page shared by another environment. When multiple environemnts send to one environment at the same time, only one of the senders will succeed, and the others will go on re-trying until success. The loop used in `ipc_send` is for the *re-try* purpose. Besides loop, locking is another way to handle the case where multiple environments send to one environment at the same time.

The implementation involves in defining a new spin-lock in **, and in using it in the unit *lab/kern/syscall.c*:
```diff
diff --git a/lab/kern/spinlock.c b/lab/kern/spinlock.c
index bf4d2d2..aea35f6 100644
--- a/lab/kern/spinlock.c
+++ b/lab/kern/spinlock.c
@@ -16,6 +16,12 @@ struct spinlock kernel_lock = {
 #endif
 };
 
+struct spinlock ipc_lock = {
+#ifdef IPCDEBUG_SPINLOCK
+	.name = "ipc_lock"
+#endif
+};
+
 #ifdef DEBUG_SPINLOCK
 // Record the current call stack in pcs[] by following the %ebp chain.
 static void
diff --git a/lab/kern/spinlock.h b/lab/kern/spinlock.h
index 52d20b4..adeb0f8 100644
--- a/lab/kern/spinlock.h
+++ b/lab/kern/spinlock.h
@@ -4,7 +4,8 @@
 #include <inc/types.h>
 
 // Comment this to disable spinlock debugging
-#define DEBUG_SPINLOCK
+///#define DEBUG_SPINLOCK
+///#define IPCDEBUG_SPINLOCK
 
 // Mutual exclusion lock.
 struct spinlock {
@@ -26,6 +27,7 @@ void spin_unlock(struct spinlock *lk);
 #define spin_initlock(lock)   __spin_initlock(lock, #lock)
 
 extern struct spinlock kernel_lock;
+extern struct spinlock ipc_lock;
 
 static inline void
 lock_kernel(void)
diff --git a/lab/kern/syscall.c b/lab/kern/syscall.c
index d30014d..056b2cc 100644
--- a/lab/kern/syscall.c
+++ b/lab/kern/syscall.c
@@ -11,6 +11,7 @@
 #include <kern/syscall.h>
 #include <kern/console.h>
 #include <kern/sched.h>
+#include <kern/spinlock.h>
 
 // Print a string to the system console.
 // The string is exactly 'len' characters long.
@@ -274,9 +275,23 @@ static int sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, unsigned perm) {
 	// LAB 4: Your code here.
 	struct Env *penv;
-	int r = envid2env(envid, &penv, 0);
-	if (r != 0) return -E_IPC_NOT_RECV;
-	if (penv->env_ipc_recving == 0) return -E_IPC_NOT_RECV;
+	spin_lock(&ipc_lock);
+	int r = 1;
+	while (r != 0) {
+		spin_unlock(&ipc_lock);
+		sys_yield();
+		spin_lock(&ipc_lock);
+		r = envid2env(envid, &penv, 0);
+		if (r == 0 && penv->env_ipc_recving == 0) r = 1;
+	}
 	struct PageInfo *page_info;
 	pte_t *pte;
 	if ((uintptr_t)srcva < UTOP) {
		if (((uintptr_t)srcva & (PGSIZE - 1)) ||
 		    ((perm & (PTE_U | PTE_P)) != (PTE_U | PTE_P)) ||
 		    ((perm & ~(PTE_SYSCALL)) != 0) ||
 		    ((page_info = page_lookup(curenv->env_pgdir, srcva, &pte)) == NULL) ||
-		    (((*pte & PTE_W) == 0) && ((perm & PTE_W) == PTE_W)))
+		    (((*pte & PTE_W) == 0) && ((perm & PTE_W) == PTE_W))) {
+			spin_unlock(&ipc_lock);
 			return -E_INVAL;
+		}
 		r = page_insert(penv->env_pgdir, page_info, penv->env_ipc_dstva, perm);
-		if (r != 0) return r;
+		if (r != 0) {
+			spin_unlock(&ipc_lock);
+			return r;
+		}
 		penv->env_ipc_perm = perm;
 	} else penv->env_ipc_perm = 0;
 	penv->env_ipc_recving = 0;
 	penv->env_ipc_from = curenv->env_id;
 	penv->env_ipc_value = value;
 	penv->env_status = ENV_RUNNABLE;
+	spin_unlock(&ipc_lock);
 	return 0;
 }
 
@@ -310,12 +331,17 @@ static int sys_ipc_recv(void *dstva) {
 	// LAB 4: Your code here.
+	spin_lock(&ipc_lock);
 	if (((uint32_t)dstva < UTOP) &&
-	    ((uint32_t)dstva & (PGSIZE - 1)))
+	    ((uint32_t)dstva & (PGSIZE - 1))) {
+		spin_unlock(&ipc_lock);
 		return -E_INVAL;
+	}
 	curenv->env_status = ENV_NOT_RUNNABLE;
 	curenv->env_ipc_recving = 1;
 	curenv->env_ipc_dstva = dstva;
+	spin_unlock(&ipc_lock);
	sys_yield();
 	return 0;
 }
diff --git a/lab/lib/ipc.c b/lab/lib/ipc.c
index 5347ec7..8a7894f 100644
--- a/lab/lib/ipc.c
+++ b/lab/lib/ipc.c
@@ -47,10 +53,11 @@ void ipc_send(envid_t to_env, uint32_t val, void *pg, int perm) {
        // LAB 4: Your code here.
        if (pg == NULL) pg = (void *)UTOP;
        int r = 1;
-       while (r != 0) {
                r = sys_ipc_try_send(to_env, val, pg, perm);
                if (r < 0 && r != -E_IPC_NOT_RECV) panic("failed to do ipc_send");
-               sys_yield();
-       }
```

Unfortuantely, this implementation does not pass the grader. I do not know the cause.

✌️
