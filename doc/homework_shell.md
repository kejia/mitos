
[Homework 2](https://pdos.csail.mit.edu/6.828/2018/homework/xv6-shell.html)

# Homework: shell

## Preparation

The homework materials are in directory `homework/shell`:
```shell
$ tree homework/shell
homework/shell
├── sh.c
└── t.sh
```

Compile and run:
```shell
$ ./a.out < t.sh
redir not implemented
exec not implemented
pipe not implemented
exec not implemented
exec not implemented
pipe not implemented
exec not implemented
```

## Implementing `ls`

This part requires to implement the command `ls` via the function `execv`:
```c
int execv(const char* path, char* const argv[]);
```
It actually asks to re-use the host system's `ls` command. The first argument of `execv` is the full path of the `ls` command, and the second argument is the options of `ls`. `sh.c` has provided the skeleton of a full shell, and the `' '` case in the function `runcmd` is for executing the `ls` command. The following implementation searches all paths in the environment variable `PATH` for the command `ls`:
```c
void runcmd(struct cmd* cmd) {
	⋮
    ecmd = (struct execcmd*)cmd;
    if(ecmd->argv[0] == 0) _exit(0);
    // fprintf(stderr, "exec not implemented\n");
    // Your code here ...
    const char* PATH = getenv("PATH");
    const size_t len_path = strlen(PATH);
    // fprintf(stdout, "DEBUG PATH: %s. PATH length: %d\n", PATH, len_path);
    const char DELIMITER = ':';
    int cur, prev = 0;
    int done = -1;
    char** const argv = ecmd->argv;
    if (!argv[1] && !strcmp("ls", argv[0])) argv[1] = "."; // set the default option for `ls'
    size_t len_com = strlen(argv[0]);
    for (cur = 0; cur < len_path; cur++) { // search all PATH directories for the command
      if (PATH[cur] == DELIMITER || cur + 1 == len_path) {
        int k = cur - prev;
        if (cur + 1 == len_path) k++;
        char com[k + len_com + 2];
        memcpy(com, PATH + prev, k);
        strcpy(com + k, "/"); // append `/ls'
        strcpy(com + k + 1, argv[0]);
        com[k + len_com + 1] = '\0';
        // printf("DEBUG: %s\n", *(argv + 1));
        done = execv(com, argv);
        if (done != -1) break;
        prev = cur + 1;
      }
    }
    if (done == -1) fprintf(stderr, "The command `%s' does not exist.\n", ecmd->argv[0]);
    break;
  case '>':
	⋮
```
This implementation actually provides the shell with the capability of executing all host system's commands:
```shell
$ gcc sh.c && ./a.out

6.828$ pwd
/home/vagrant/mitos/homework/shell

6.828$ ls
a.out  sh.c  t.sh

6.828$ ls -l
total 28
-rwxr-xr-x 1 vagrant vagrant 12844 Oct 10 22:25 a.out
-rw-r--r-- 1 vagrant vagrant  7184 Oct 10 17:19 sh.c
-rw-r--r-- 1 vagrant vagrant    80 Oct  9 18:57 t.sh

6.828$ cat t.sh
ls > y
cat < y | sort | uniq | wc > y1
cat y1
rm y1
ls |  sort | uniq | wc
rm y
```
The implementation parses the `PATH` environment variable, and it is more handy to directly use the function `execvp`.

## I/O Redirection

Check the manual on how to use functions `close` and `open` to implement *I/O redirection*:

```shell
$ man close
	⋮
SEE ALSO
       ..., open(2), ...
	⋮
$ man 2 open
```

The logic is to use a new file-descriptor of the input file to replace the old one, which is the standard output, `stdout`, so a command's output will be re-directed to the input file.


The implementation is as follows:
```c
void runcmd(struct cmd* cmd) {
	⋮
  case '>':
  case '<':
    rcmd = (struct redircmd*)cmd;
    // fprintf(stderr, "redir not implemented\n");
    // Your code here ...
    int mode = S_IRWXU | S_IRWXG; // set for the mode argument
    close(rcmd->fd); // close the current file-descriptor, i.e. stdout
    if (open(rcmd->file, rcmd->flags, mode) < 0) printf("ERROR: redirection failed.\n"); // open a new file-descriptor to replace stdout with the file
    runcmd(rcmd->cmd); // go on running the command, and its output will be in the new file-descriptor
    break;
  case '|':
	⋮
```

N.B.: The assignment description says:

> ... Note that the `mode` field in redircmd contains access modes (e.g., O_RDONLY), ...

This is outdated: now there is a `flag` field rather than `mode`, and `flag` is consistent with the function `open`'s argument definition.

Run the shell and execute the I/O redirection operations:
```shell
$ gcc sh.c && ./a.out
6.828$ echo "6.828 is cool" > x.txt
6.828$ cat < x.txt
"6.828 is cool"
6.828$
```

## Implement Pipes

Use the system functions `pipe`, `fork`, `dup`, and `close` to implement the pipe in Unix. The scheme is to create a pipe with `pipe`, to attatch the pipe to `stdin` and `stdout` with `dup`, and to execute the commands on both sides of the pipe in different processes created with `fork`. File descriptors are limited resources, so it is good practice to release unused file descriptors with `close`. The implementation is as follows:

```c
void runcmd(struct cmd* cmd) {
	⋮
  case '|':
    pcmd = (struct pipecmd*)cmd;
    // fprintf(stderr, "pipe not implemented\n");
    // Your code here ...
    int flag_pipe = 0;
    flag_pipe = pipe(p); // create the pipe, and p is initialized as the file descriptors of the ends of the pipe.
    if (flag_pipe == 0) {
      pid_t pid = fork(); // fork another process
      if (pid == 0) { // child process to handle the pipe left for writing
        flag_pipe |= close(p[0]); // close the file descriptor on the left---good practice: release unused valuable resources.
        if (flag_pipe == 0) {
          flag_pipe |= (dup2(p[1], STDOUT_FILENO) < 0); // hook stdout to the left (write) file descriptor
          runcmd(pcmd->left); // run the left command
        }
      } else if (pid > 0) { // parent process to handle the pipe right for reading
        flag_pipe |= close(p[1]); // close the file descriptor on the right---good practice: release unused valuable resources.
        if (!flag_pipe) {
          flag_pipe |= (dup2(p[0], STDIN_FILENO) < 0); // hook stdin to the right (read) file descriptor
          runcmd(pcmd->right); // run the right command
        }
      } else {
        printf("ERROR: fork failed.\n");
        flag_pipe = 1;
      }
    }
    if (flag_pipe != 0) printf("ERROR: pipe failed.\n");
    break;
	⋮
}
```

Run a pipe instruction:

```shell
$ gcc sh.c && ./a.out
6.828$ ls | sort | uniq | wc
      4       4      22
6.828$
```

✌️
