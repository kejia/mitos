
[Lab 3](http://pdos.csail.mit.edu/6.828/2018/labs/lab3/)

In JOS, the kernel stack is common among all processes, as is different from xv6.

The most important part for running a process includes its registers content and its paging information.

# *lab/kern/env.c* Analysis

The `struct Segdesc` is for the *segment* technique on x86, but is only for switching privilege levels in JOS.

`envid2env` converts an `envid` to an `env` pointer.

`env_init_percpu` loads the *global descriptor table* and *segment descriptors*.

`env_init` marks all environments in `envs` as free, sets their `env_ids` to `0`, and inserts them into the `env_free_list`. Make sure the environments are in the free list in the same order they are in the `envs` array. `env_init_percpu` is called in this function.

`env_setup_vm` initializes the kernel virtual memory layout for an environment. It allocates a page directory, set `e_.env_pgdir` accordingly, and initializes the kernel portion of the new environment's address space. No physical address is mapped into the *user* portion of the environment's virtual address space for now.

`env_alloc` allocates and initializes a new environment and stores it in `*newenv_store`.

`region_alloc` allocates physical memory for environment `env` and maps it at virtual address `va` of the environment. Pages should be writable by both user and kernel.

`load_icode` sets up the initial program binary, stack, and processor flags for a user process. This function is only called during kernel initialization, before running the first user-mode environment. This function loads all loadable segments from the ELF binary image into the environment's user memory. It also clears to zero any portions of these segments that are marked in the program header as being mapped but not actually present in the ELF file, i.e. the program's *bss* section. Finally, this function maps one page for the program's initial stack.

`env_create` allocates a new `env` with `env_alloc`, loads the named ELF binary into it with `load_icode`, and sets its `env_type`. This function is only called during kernel initialization, before running the first user-mode environment. The new `env`'s parent ID is set to `0`.

`env_free` frees an `env` and all memory it uses.

`env_destroy` frees an environment by calling `env_free`.

`env_pop_tf` restores the register values in the `Trapframe` with the `iret` instruction. This exits the kernel and starts executing some environment's code.

`env_run` switches context from `curenv` to `env`. If this is the first time to call `evn_run`, `curenv` is `NULL`.

# Part A: User Environments and Exception Handling

## Exercise 1

> Modify mem_init() in kern/pmap.c to allocate and map the envs array. This array consists of exactly NENV instances of the Env structure allocated much like how you allocated the pages array. Also like the pages array, the memory backing envs should also be mapped user read-only at UENVS (defined in inc/memlayout.h) so user processes can read from this array.
> 
> You should run your code and make sure check\_kern\_pgdir() succeeds.

`mem_init` sets up *page directory* and *page tables*. This function only sets up the kernel part of the address space, i.e. addresses >= UTOP. The user part of the address space will be set up later. Also, From UTOP to ULIM, the user is allowed to read but not to write, and above ULIM the user cannot read or write.

The memory layout is define in *lab/inc/memlayout.h*:

```c
/*
 * Virtual memory map:                                Permissions
 *                                                    kernel/user
 *
 *    4 Gig -------->  +------------------------------+
 *                     |                              | RW/--
 *                     ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *                     :              .               :
 *                     :              .               :
 *                     :              .               :
 *                     |~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~| RW/--
 *                     |                              | RW/--
 *                     |   Remapped Physical Memory   | RW/--
 *                     |                              | RW/--
 *    KERNBASE, ---->  +------------------------------+ 0xf0000000      --+
 *    KSTACKTOP        |     CPU0's Kernel Stack      | RW/--  KSTKSIZE   |
 *                     | - - - - - - - - - - - - - - -|                   |
 *                     |      Invalid Memory (*)      | --/--  KSTKGAP    |
 *                     +------------------------------+                   |
 *                     |     CPU1's Kernel Stack      | RW/--  KSTKSIZE   |
 *                     | - - - - - - - - - - - - - - -|                 PTSIZE
 *                     |      Invalid Memory (*)      | --/--  KSTKGAP    |
 *                     +------------------------------+                   |
 *                     :              .               :                   |
 *                     :              .               :                   |
 *    MMIOLIM ------>  +------------------------------+ 0xefc00000      --+
 *                     |       Memory-mapped I/O      | RW/--  PTSIZE
 * ULIM, MMIOBASE -->  +------------------------------+ 0xef800000
 *                     |  Cur. Page Table (User R-)   | R-/R-  PTSIZE
 *    UVPT      ---->  +------------------------------+ 0xef400000
 *                     |          RO PAGES            | R-/R-  PTSIZE
 *    UPAGES    ---->  +------------------------------+ 0xef000000
 *                     |           RO ENVS            | R-/R-  PTSIZE
 * UTOP,UENVS ------>  +------------------------------+ 0xeec00000
 * UXSTACKTOP -/       |     User Exception Stack     | RW/RW  PGSIZE
 *                     +------------------------------+ 0xeebff000
 *                     |       Empty Memory (*)       | --/--  PGSIZE
 *    USTACKTOP  --->  +------------------------------+ 0xeebfe000
 *                     |      Normal User Stack       | RW/RW  PGSIZE
 *                     +------------------------------+ 0xeebfd000
 *                     |                              |
 *                     |                              |
 *                     ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *                     .                              .
 *                     .                              .
 *                     .                              .
 *                     |~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~|
 *                     |     Program Data & Heap      |
 *    UTEXT -------->  +------------------------------+ 0x00800000
 *    PFTEMP ------->  |       Empty Memory (*)       |        PTSIZE
 *                     |                              |
 *    UTEMP -------->  +------------------------------+ 0x00400000      --+
 *                     |       Empty Memory (*)       |                   |
 *                     | - - - - - - - - - - - - - - -|                   |
 *                     |  User STAB Data (optional)   |                 PTSIZE
 *    USTABDATA ---->  +------------------------------+ 0x00200000        |
 *                     |       Empty Memory (*)       |                   |
 *    0 ------------>  +------------------------------+                 --+
 *
 * (*) Note: The kernel ensures that "Invalid Memory" is *never* mapped.
 *     "Empty Memory" is normally unmapped, but user programs may map pages
 *     there if desired.  JOS user programs map pages temporarily at UTEMP.
 */
``````diff
diff --git a/lab/kern/pmap.c b/lab/kern/pmap.c
index ef9b219..738d1d9 100644
--- a/lab/kern/pmap.c
+++ b/lab/kern/pmap.c
@@ -8,6 +8,7 @@

 #include <kern/pmap.h>
 #include <kern/kclock.h>
+#include <kern/env.h>

 // These variables are set by i386_detect_memory()
 size_t npages;                 // Amount of physical memory (in pages)
@@ -151,6 +155,12 @@ void mem_init(void) {
        pages = (struct PageInfo*) boot_alloc(boot_alloc_size);
        memset(pages, 0, boot_alloc_size);

+       //////////////////////////////////////////////////////////////////////
+       // Make 'envs' point to an array of size 'NENV' of 'struct Env'.
+       // LAB 3: Your code here.
+       envs = (struct Env*) boot_alloc(sizeof(struct Env) * NENV);
+       memset(envs, 0, sizeof(struct Env) * NENV);
+
        //////////////////////////////////////////////////////////////////////
        // Now that we've allocated the initial kernel data structures, we set
@@ -174,7 +184,16 @@ void mem_init(void) {
        //    - pages itself -- kernel RW, user NONE
        // Your code goes here:
        boot_map_region(kern_pgdir, UPAGES, PTSIZE, PADDR(pages), PTE_U | PTE_P);
+       //////////////////////////////////////////////////////////////////////
+       // Map the 'envs' array read-only by the user at linear address UENVS
+       // (ie. perm = PTE_U | PTE_P).
+       // Permissions:
+       //    - the new image at UENVS  -- kernel R, user R
+       //    - envs itself -- kernel RW, user NONE
+       // LAB 3: Your code here.
+       boot_map_region(kern_pgdir, UENVS, PTSIZE, PADDR(envs), PTE_U | PTE_P);
+
        //////////////////////////////////////////////////////////////////////
        // Use the physical memory that 'bootstack' refers to as the kernel
        // stack.  The kernel stack grows down from virtual address KSTACKTOP.
```

Verify the work in directory *lab*:

```shell
$ make clean && make qemu-nox
    ⋮
check_kern_pgdir() succeeded!
    ⋮
```

## Exercise 2

> In the file env.c, finish coding the following functions:
> 
> - env\_init(): Initialize all of the Env structures in the envs array and add them to the env\_free\_list. Also calls env\_init_percpu, which configures the segmentation hardware with separate segments for privilege level 0 (kernel) and privilege level 3 (user).
> - env\_setup\_vm(): Allocate a page directory for a new environment and initialize the kernel portion of the new environment's address space.
> - region_alloc(): Allocates and maps physical memory for an environment
> - load_icode(): You will need to parse an ELF binary image, much like the boot loader already does, and load its contents into the user address space of a new environment.
> - env\_create(): Allocate an environment with env\_alloc and call load_icode to load an ELF binary into it.
> - env_run(): Start a given environment running in user mode.
> 
> As you write these functions, you might find the new cprintf verb %e useful -- it prints a description corresponding to an error code. For example,
> 
> r = -E\_NO\_MEM; panic("env_alloc: %e", r);
> 
> will panic with the message "env_alloc: out of memory".

This exercise uses `struct Env` which is defined in *lab/inc/env.h*:

```c
struct Env {
    struct Trapframe env_tf;	// Saved registers
    struct Env *env_link;		// Next free Env
    envid_t env_id;			// Unique environment identifier
    envid_t env_parent_id;		// env_id of this env's parent
    enum EnvType env_type;		// Indicates special system environments
    unsigned env_status;		// Status of the environment
    uint32_t env_runs;		// Number of times environment has run
    // Address space
    pde_t *env_pgdir;		// Kernel virtual address of page dir
};
```

The implementation is as follows:

```c
void env_init(void) {
    // Set up envs array
    env_free_list = envs;
    env_free_list->env_id = 0;
    struct Env *cur = envs, *start = envs, *end = envs + NENV; // the memory grows from bottom to top
    while (start < end) {
        cur->env_link = ++start;
        cur = cur->env_link;
        cur->env_id = 0;
    }
    // Per-CPU part of the initialization
    env_init_percpu();
}

static int env_setup_vm(struct Env *e) {
    int i;
    struct PageInfo *p = NULL;

    // Allocate a page for the page directory
    if (!(p = page_alloc(ALLOC_ZERO)))
        return -E_NO_MEM;

    // Now, set e->env_pgdir and initialize the page directory.
    //
    // Hint:
    //    - The VA space of all envs is identical above UTOP
    //	(except at UVPT, which we've set below).
    //	See inc/memlayout.h for permissions and layout.
    //	Can you use kern_pgdir as a template?  Hint: Yes.
    //	(Make sure you got the permissions right in Lab 2.)
    //    - The initial VA below UTOP is empty.
    //    - You do not need to make any more calls to page_alloc.
    //    - Note: In general, pp_ref is not maintained for
    //	physical pages mapped only above UTOP, but env_pgdir
    //	is an exception -- you need to increment env_pgdir's
    //	pp_ref for env_free to work correctly.
    //    - The functions in kern/pmap.h are handy.

    // LAB 3: Your code here.
    p->pp_ref++;
    e->env_pgdir = (pde_t*) page2kva(p);
    memcpy(e->env_pgdir, kern_pgdir, PGSIZE);

    // UVPT maps the env's own page table read-only.
    // Permissions: kernel R, user R
    e->env_pgdir[PDX(UVPT)] = PADDR(e->env_pgdir) | PTE_P | PTE_U;

    return 0;
}

static void region_alloc(struct Env* e, void* va, size_t len) {
    // LAB 3: Your code here.
    // (But only if you need it for load_icode.)
    //
    // Hint: It is easier to use region_alloc if the caller can pass
    //   'va' and 'len' values that are not page-aligned.
    //   You should round va down, and round (va + len) up.
    //   (Watch out for corner-cases!)
    void* start = ROUNDDOWN(va, PGSIZE);
    void* end = ROUNDUP(va + len, PGSIZE);
    if ((uint32_t) end > UTOP) panic("cannot allocate above UTOP");
    while (start < end) {
        struct PageInfo* page_info = page_alloc(0);
        if (page_info == NULL) panic("out of free memory");
        if (page_insert(e->env_pgdir, page_info, start, PTE_W | PTE_U) < 0) panic("failed to insert the new page");
        start += PGSIZE;
    }
}

static void load_icode(struct Env* e, uint8_t* binary) {
    // Hints:
    //  Load each program segment into virtual memory
    //  at the address specified in the ELF segment header.
    //  You should only load segments with ph->p_type == ELF_PROG_LOAD.
    //  Each segment's virtual address can be found in ph->p_va
    //  and its size in memory can be found in ph->p_memsz.
    //  The ph->p_filesz bytes from the ELF binary, starting at
    //  'binary + ph->p_offset', should be copied to virtual address
    //  ph->p_va.  Any remaining memory bytes should be cleared to zero.
    //  (The ELF header should have ph->p_filesz <= ph->p_memsz.)
    //  Use functions from the previous lab to allocate and map pages.
    //
    //  All page protection bits should be user read/write for now.
    //  ELF segments are not necessarily page-aligned, but you can
    //  assume for this function that no two segments will touch
    //  the same virtual page.
    //
    //  You may find a function like region_alloc useful.
    //
    //  Loading the segments is much simpler if you can move data
    //  directly into the virtual addresses stored in the ELF binary.
    //  So which page directory should be in force during
    //  this function?
    //
    //  You must also do something with the program's entry point,
    //  to make sure that the environment starts executing there.
    //  What?  (See env_run() and env_pop_tf() below.)

    // LAB 3: Your code here.
    struct Elf* elf_header = (struct Elf*) binary;
    struct Proghdr *ph, *eph;
    if (elf_header->e_magic != ELF_MAGIC) panic("not ELF");
    ph = (struct Proghdr *) ((uint8_t *) elf_header + elf_header->e_phoff);
    eph = ph + elf_header->e_phnum;
    lcr3(PADDR(e->env_pgdir));
    while (ph < eph) {
        if (ph->p_type == ELF_PROG_LOAD) {
            region_alloc(e, (void *) ph->p_va, ph->p_memsz);
            memset((void *)ph->p_va, 0, ph->p_memsz);
            memcpy((void *)ph->p_va, binary + ph->p_offset, ph->p_filesz);
        }
        ph++;
    }
    e->env_tf.tf_eip = elf_header->e_entry;
    // Now map one page for the program's initial stack
    // at virtual address USTACKTOP - PGSIZE.
    
    // LAB 3: Your code here.
    region_alloc(e, (void *)(USTACKTOP - PGSIZE), PGSIZE);
    lcr3(PADDR(kern_pgdir));
}

void env_create(uint8_t *binary, enum EnvType type) {
    // LAB 3: Your code here.
    struct Env *env;
    if (env_alloc(&env, 0) != 0) panic("failed to allocate env");
    load_icode(env, binary);
    env->env_type = type;
}

void env_run(struct Env *e) {
    // Step 1: If this is a context switch (a new environment is running):
    //	   1. Set the current environment (if any) back to
    //	      ENV_RUNNABLE if it is ENV_RUNNING (think about
    //	      what other states it can be in),
    //	   2. Set 'curenv' to the new environment,
    //	   3. Set its status to ENV_RUNNING,
    //	   4. Update its 'env_runs' counter,
    //	   5. Use lcr3() to switch to its address space.
    // Step 2: Use env_pop_tf() to restore the environment's
    //	   registers and drop into user mode in the
    //	   environment.

    // Hint: This function loads the new environment's state from
    //	e->env_tf.  Go back through the code you wrote above
    //	and make sure you have set the relevant parts of
    //	e->env_tf to sensible values.

    // LAB 3: Your code here.
    if (curenv != NULL && curenv->env_status == ENV_RUNNING) curenv->env_status = ENV_RUNNABLE;
    curenv = e;
    curenv->env_status = ENV_RUNNING;
    curenv->env_runs++;
    lcr3(PADDR(curenv->env_pgdir));
    env_pop_tf(&(curenv->env_tf));
}
```

For now the transition from user space into kernel space, so the processor catches that it has not set up to handle this system call interrupt: it will generate a general protection exception, find that it can't handle that, generate a double fault exception, find that it can't handle that either, and finally give up with what's known as a *triple fault*. Usually, you would then see the CPU reset and the system reboot. With the patched QEMU of this course, one will instead see a register dump and a *Triple fault.* message.

Verify the work in directory *lab*::

```shell
$ make clean && make qemu-nox
    ⋮
check_page_installed_pgdir() succeeded!
[00000000] new env 00001000
EAX=00000000 EBX=00000000 ECX=0000000d EDX=eebfde88
ESI=00000000 EDI=00000000 EBP=eebfde60 ESP=eebfde54
EIP=00800a9b EFL=00000092 [--S-A--] CPL=3 II=0 A20=1 SMM=0 HLT=0
ES =0023 00000000 ffffffff 00cff300 DPL=3 DS   [-WA]
CS =001b 00000000 ffffffff 00cffa00 DPL=3 CS32 [-R-]
SS =0023 00000000 ffffffff 00cff300 DPL=3 DS   [-WA]
DS =0023 00000000 ffffffff 00cff300 DPL=3 DS   [-WA]
FS =0023 00000000 ffffffff 00cff300 DPL=3 DS   [-WA]
GS =0023 00000000 ffffffff 00cff300 DPL=3 DS   [-WA]
LDT=0000 00000000 00000000 00008200 DPL=0 LDT
TR =0028 f017c680 00000067 00408900 DPL=0 TSS32-avl
GDT=     f011a320 0000002f
IDT=     f017be60 000007ff
CR0=80050033 CR2=00000000 CR3=003bc000 CR4=00000000
DR0=00000000 DR1=00000000 DR2=00000000 DR3=00000000
DR6=ffff0ff0 DR7=00000400
EFER=0000000000000000
Triple fault.  Halting for inspection via QEMU monitor.
```

## Exercise 3

> Read [Chapter 9, Exceptions and Interrupts](http://pdos.csail.mit.edu/6.828/2018/readings/i386/c09.htm) in the [80386 Programmer's Manual](http://pdos.csail.mit.edu/6.828/2018/readings/i386/toc.htm) (or Chapter 5 of the [IA-32 Developer's Manual](http://pdos.csail.mit.edu/6.828/2018/readings/ia32/IA32-3A.pdf)), if you haven't already.

In Intel's terminology, an *interrupt* is a *protected control transfer* that is caused by an *asynchronous* event usually external to the processor, such as notification of external device I/O activity. An *exception*, in contrast, is a *protected control transfer* caused *synchronously* by the currently running code, for example due to a divide by zero or an invalid memory access.

All of the synchronous exceptions that the x86 processor can generate internally use interrupt vectors between 0 and 31, and therefore map to IDT entries 0-31. For example, a page fault always causes an exception through vector 14. Interrupt vectors greater than 31 are only used by software interrupts, which can be generated by the int instruction, or asynchronous hardware interrupts, caused by external devices when they need attention.

The *nested exceptions* are those caused by code within the kernel itself.

On the x86, two mechanisms work together to protect the kernel space and the user one:

- The Interrupt Descriptor Table
    - The processor ensures that interrupts and exceptions can only cause the kernel to be entered at a few specific, well-defined entry-points determined by the kernel itself, and not by the code running when the interrupt or exception is taken.
    - The x86 allows up to 256 different interrupt or exception entry points into the kernel, each with a different interrupt vector. A vector is a number between 0 and 255. An interrupt's vector is determined by the source of the interrupt: different devices, error conditions, and application requests to the kernel generate interrupts with different vectors. The CPU uses the vector as an index into the processor's interrupt descriptor table (IDT), which the kernel sets up in kernel-private memory, much like the GDT. From the appropriate entry in this table the processor loads:
        - the value to load into the instruction pointer (EIP) register, pointing to the kernel code designated to handle that type of exception.
        - the value to load into the code segment (CS) register, which includes in bits 0-1 the privilege level at which the exception handler is to run. (In JOS, all exceptions are handled in kernel mode, privilege level 0.)
- The Task State Segment
    - The processor needs a place to save the old processor state before the interrupt or exception occurred, such as the original values of EIP and CS before the processor invoked the exception handler, so that the exception handler can later restore that old state and resume the interrupted code from where it left off. But this save area for the old processor state must in turn be protected from unprivileged user-mode code; otherwise buggy or malicious user code could compromise the kernel.
    - For this reason, when an x86 processor takes an interrupt or trap that causes a privilege level change from user to kernel mode, it also switches to a stack in the kernel's memory. A structure called the task state segment (TSS) specifies the segment selector and address where this stack lives. The processor pushes (on this new stack) SS, ESP, EFLAGS, CS, EIP, and an optional error code. Then it loads the CS and EIP from the interrupt descriptor, and sets the ESP and SS to refer to the new stack.
    - Although the TSS is large and can potentially serve a variety of purposes, JOS only uses it to define the kernel stack that the processor should switch to when it transfers from user to kernel mode. Since "kernel mode" in JOS is privilege level 0 on the x86, the processor uses the ESP0 and SS0 fields of the TSS to define the kernel stack when entering kernel mode. JOS doesn't use any other TSS fields.

The *IDT* looks like as follows:

```
      IDT                   trapentry.S         trap.c
   
+----------------+                        
|   &handler1    |---------> handler1:          trap (struct Trapframe *tf)
|                |             // do stuff      {
|                |             call trap          // handle the exception/interrupt
|                |             // ...           }
+----------------+
|   &handler2    |--------> handler2:
|                |            // do stuff
|                |            call trap
|                |            // ...
+----------------+
       .
       .
       .
+----------------+
|   &handlerX    |--------> handlerX:
|                |             // do stuff
|                |             call trap
|                |             // ...
+----------------+
```

It is defined in *lab/kern/trap.h* in this lab, `idt`.

## Exercise 4

> Edit trapentry.S and trap.c and implement the features described above. The macros TRAPHANDLER and TRAPHANDLER\_NOEC in trapentry.S should help you, as well as the T\_* defines in inc/trap.h. You will need to add an entry point in trapentry.S (using those macros) for each trap defined in inc/trap.h, and you'll have to provide \_alltraps which the TRAPHANDLER macros refer to. You will also need to modify trap\_init() to initialize the idt to point to each of these entry points defined in trapentry.S; the SETGATE macro will be helpful here.
> 
> Your _alltraps should:
> 
> - push values to make the stack look like a struct Trapframe
> - load GD_KD into %ds and %es
> - pushl %esp to pass a pointer to the Trapframe as an argument to trap()
> - call trap (can trap ever return?)
> 
> Consider using the pushal instruction; it fits nicely with the layout of the struct Trapframe.
> 
> Test your trap handling code using some of the test programs in the user directory that cause exceptions before making any system calls, such as user/divzero. You should be able to get make grade to succeed on the divzero, softint, and badsegment tests at this point.

Check [Table 5-1 of IA-32 Developer's Manual](http://pdos.csail.mit.edu/6.828/2018/readings/ia32/IA32-3A.pdf) for the existence of the *error codes*.

The implementation is as follows:

- *lab/kern/trapentry.S*

```c
/*
 * Lab 3: Your code here for generating entry points for the different traps.
 */
// Exceptions without error code.
TRAPHANDLER_NOEC(divideErrorHandler, T_DIVIDE);
TRAPHANDLER_NOEC(debugHandler, T_DEBUG);
TRAPHANDLER_NOEC(NMIHandler, T_NMI);
TRAPHANDLER_NOEC(breakpointHandler, T_BRKPT);
TRAPHANDLER_NOEC(overflowHandler, T_OFLOW);
TRAPHANDLER_NOEC(BOUNDRangeExceededHandler, T_BOUND);
TRAPHANDLER_NOEC(invalidOpcodeHandler, T_ILLOP);
TRAPHANDLER_NOEC(deviceNotAvailableHandler, T_DEVICE);
// exceptions with error code
TRAPHANDLER(doubleFaultHandler, T_DBLFLT);
TRAPHANDLER(invalidTSSHandler, T_TSS);
TRAPHANDLER(segmentNotPresentHandler, T_SEGNP);
TRAPHANDLER(stackFaultHandler, T_STACK);
TRAPHANDLER(generalProtectionHandler, T_GPFLT);
TRAPHANDLER(pageFaultHandler, T_PGFLT);
// Exceptions without error code.
TRAPHANDLER_NOEC(floatingPointErrorHandler, T_FPERR);
TRAPHANDLER_NOEC(alignmentCheckHandler, T_ALIGN);
TRAPHANDLER_NOEC(machineCheckHandler, T_MCHK);
TRAPHANDLER_NOEC(SIMDFloatingPointExceptionHandler, T_SIMDERR);

/*
 * Lab 3: Your code here for _alltraps
 */
_alltraps:
    pushl %ds
    pushl %es
    pushal
    movw $GD_KD, %ax
    movw %ax, %ds
    movw %ax, %es
    pushl %esp
    call trap

```

- *lab/kern/trap.c*

```c
void trap_init(void) {
    extern struct Segdesc gdt[];

    // LAB 3: Your code here.
    void divideErrorHandler();
    void debugHandler();
    void NMIHandler();
    void breakpointHandler();
    void overflowHandler();
    void BOUNDRangeExceededHandler();
    void invalidOpcodeHandler();
    void deviceNotAvailableHandler();
    void doubleFaultHandler();
    void invalidTSSHandler();
    void segmentNotPresentHandler();
    void stackFaultHandler();
    void generalProtectionHandler();
    void pageFaultHandler();
    void floatingPointErrorHandler();
    void alignmentCheckHandler();
    void machineCheckHandler();
    void SIMDFloatingPointExceptionHandler();

    SETGATE(idt[T_DIVIDE], 0, GD_KT, &divideErrorHandler, 0);
    SETGATE(idt[T_DEBUG], 0, GD_KT, &debugHandler, 0);
    SETGATE(idt[T_NMI], 0, GD_KT, &NMIHandler, 0);
    SETGATE(idt[T_BRKPT], 0, GD_KT, &breakpointHandler, 0);
    SETGATE(idt[T_OFLOW], 0, GD_KT, &overflowHandler, 0);
    SETGATE(idt[T_BOUND], 0, GD_KT, &BOUNDRangeExceededHandler, 0);
    SETGATE(idt[T_ILLOP], 0, GD_KT, &invalidOpcodeHandler, 0);
    SETGATE(idt[T_DEVICE], 0, GD_KT, &deviceNotAvailableHandler, 0);
    SETGATE(idt[T_DBLFLT], 0, GD_KT, &doubleFaultHandler, 0);
    SETGATE(idt[T_TSS], 0, GD_KT, &invalidTSSHandler, 0);
    SETGATE(idt[T_SEGNP], 0, GD_KT, &segmentNotPresentHandler, 0);
    SETGATE(idt[T_STACK], 0, GD_KT, &stackFaultHandler, 0);
    SETGATE(idt[T_GPFLT], 0, GD_KT, &generalProtectionHandler, 0);
    SETGATE(idt[T_PGFLT], 0, GD_KT, &pageFaultHandler, 0);
    SETGATE(idt[T_FPERR], 0, GD_KT, &floatingPointErrorHandler, 0);
    SETGATE(idt[T_ALIGN], 0, GD_KT, &alignmentCheckHandler, 0);
    SETGATE(idt[T_MCHK], 0, GD_KT, &machineCheckHandler, 0);
    SETGATE(idt[T_SIMDERR], 0, GD_KT, &SIMDFloatingPointExceptionHandler, 0);
 
    // Per-CPU setup 
    trap_init_percpu();
}
```

Verify the work in directory *lab*:

```shell
$ make clean && make grade
    ⋮
divzero: OK (2.0s)
softint: OK (1.0s)
badsegment: OK (2.1s)
Part A score: 30/30
    ⋮
```

### Question 1

> What is the purpose of having an individual handler function for each exception/interrupt? (i.e., if all exceptions/interrupts were delivered to the same handler, what feature that exists in the current implementation could not be provided?)

Each exception/interrupt has a unique code, and the CPU ensures only some exception/interrupt to have privileged access to the kernel. Without standalone handlers, such isolation cannot be fulfilled. Also, special flag bits may need to be set for an exception/interrupt. So, wrapping up the handling for each exception/interrupt is efficient for managing it.

### Question 2

> Did you have to do anything to make the user/softint program behave correctly? The grade script expects it to produce a general protection fault (trap 13), but softint's code says int $14. Why should this produce interrupt vector 13? What happens if the kernel actually allows softint's int $14 instruction to invoke the kernel's page fault handler (which is interrupt vector 14)?

The user/softint program here throws the *page fault* interrupt, which requires the kernel access privilege. See [Section 5.12.1.1, IA32 Software Developer's Manual, Volume 3A](http://pdos.csail.mit.edu/6.828/2018/readings/ia32/IA32-3A.pdf):

> The processor checks the DPL of the interrupt or trap gate only if an exception or interrupt is generated with an INT n, INT 3, or INTO instruction. Here, the CPL must be less than or equal to the DPL of the gate. This restriction prevents application programs or procedures running at privilege level 3 from using a software interrupt to access critical exception handlers, such as the page-fault handler, providing that those handlers are placed in more privileged code segments (numerically lower privilege level). For hardware-generated interrupts and processor-detected exceptions, the processor ignores the DPL of interrupt and trap gates.

So, set the DPL to `3` in the function `trap_init` in *lab/kern/trap.c* in order to trigger *page fault* rather than *general-protected exception*. If the kernel's page fault handler is allowed here, the kernel part of the virtual space may be broken by the user/softint program.

# Part B: Page Faults, Breakpoints Exceptions, and System Calls

## Exercise 5

> Modify trap\_dispatch() to dispatch page fault exceptions to page\_fault_handler(). You should now be able to get make grade to succeed on the faultread, faultreadkernel, faultwrite, and faultwritekernel tests. If any of them don't work, figure out why and fix them. Remember that you can boot JOS into a particular user program using make run-x or make run-x-nox. For instance, make run-hello-nox runs the hello user program.

The implementation is done in the *lab/kern/trap.c* and is as follows:

```c
static void trap_dispatch(struct Trapframe *tf) {
    // Handle processor exceptions.
    // LAB 3: Your code here.
    switch (tf->tf_trapno) {
    case T_PGFLT: page_fault_handler(tf); return;
    default:
        // Unexpected trap: The user process or the kernel has a bug.
        print_trapframe(tf);
        if (tf->tf_cs == GD_KT) panic("unhandled trap in kernel");
        else {
            env_destroy(curenv);
            return;
        }
    }	
}
    ⋮
void page_fault_handler(struct Trapframe *tf) {
    ⋮
    // LAB 3: Your code here.
    if ((tf->tf_cs & 3) == 0) panic("page fault in kernel mode");
    ⋮
}
```

Verify the work in directory *lab*:

```shell
$ make clean && make grade
    ⋮
faultread: OK (2.1s)
faultreadkernel: OK (2.7s)
faultwrite: OK (1.8s)
faultwritekernel: OK (1.9s)
    ⋮
```

## Exercise 6

> Modify trap_dispatch() to make breakpoint exceptions invoke the kernel monitor. You should now be able to get make grade to succeed on the breakpoint test.

Add one line for handling the `T_BRKPT` interrupt:

```c
static void trap_dispatch(struct Trapframe *tf) {
    // Handle processor exceptions.
    // LAB 3: Your code here.
    switch (tf->tf_trapno) {
    case T_PGFLT: page_fault_handler(tf); return;
    case T_BRKPT: monitor(tf); return;
    default:
        // Unexpected trap: The user process or the kernel has a bug.
        print_trapframe(tf);
        if (tf->tf_cs == GD_KT) panic("unhandled trap in kernel");
        else {
            env_destroy(curenv);
            return;
        }
    }	
}
```

Also, change the privilege level to `3` in the function `trap_init` in *lab/kern/trap.c*:

```c
SETGATE(idt[T_BRKPT], 0, GD_KT, &breakpointHandler, 3);
```

The Question 3 explains the reason on the above change. See *homework/xv6-public/mmu.h* for the definition of `SETGATE`:

```c
// Set up a normal interrupt/trap gate descriptor.
// - istrap: 1 for a trap (= exception) gate, 0 for an interrupt gate.
//   interrupt gate clears FL_IF, trap gate leaves FL_IF alone
// - sel: Code segment selector for interrupt/trap handler
// - off: Offset in code segment for interrupt/trap handler
// - dpl: Descriptor Privilege Level -
//        the privilege level required for software to invoke
//        this interrupt/trap gate explicitly using an int instruction.
#define SETGATE(gate, istrap, sel, off, d)                \
{                                                         \
  (gate).off_15_0 = (uint)(off) & 0xffff;                \
  (gate).cs = (sel);                                      \
  (gate).args = 0;                                        \
  (gate).rsv1 = 0;                                        \
  (gate).type = (istrap) ? STS_TG32 : STS_IG32;           \
  (gate).s = 0;                                           \
  (gate).dpl = (d);                                       \
  (gate).p = 1;                                           \
  (gate).off_31_16 = (uint)(off) >> 16;                  \
}
```

Verify the work in directory *lab*:

```shell
$ make clean && make grade
    ⋮
breakpoint: OK (1.9s)
    ⋮
```

### Challenge

> Modify the JOS kernel monitor so that you can 'continue' execution from the current location (e.g., after the int3, if the kernel monitor was invoked via the breakpoint exception), and so that you can single-step one instruction at a time. You will need to understand certain bits of the EFLAGS register in order to implement single-stepping.

This task needs to set the *EFLAGS* register. See the chapter, *3.4.3 EFLAGS Register*, of [IA32 Developer's Manual](http://pdos.csail.mit.edu/6.828/2018/readings/ia32/IA32-1.pdf):

![EFLAGS](../_resources/bb3849e7d0b64f7b84cb2d57e5fbf0ce.png)

The *TF* bit is used for single step debug:

> TF (bit 8)Trap flag — Set to enable single-step mode for debugging; clear to disable single-step mode.

This bit is set to `1` . The implementation is as follows:

```diff
diff --git a/lab/kern/monitor.c b/lab/kern/monitor.c
index 4139e3c..d256830 100644
--- a/lab/kern/monitor.c
+++ b/lab/kern/monitor.c
@@ -28,6 +28,7 @@ static struct Command commands[] = {
        { "backtrace", "call backtrace", mon_backtrace },
        { "ple183", "play lab1 exercise 8.3", play_lab_1_exercise_8_3 },
        { "ple184", "play lab1 exercise 8.4", play_lab_1_exercise_8_4 },
+       { "stepi", "play lab3 challenge following Exercise 6", mon_stepi },
 };

 /***** Implementations of basic kernel monitor commands *****/
@@ -87,6 +88,18 @@ int play_lab_1_exercise_8_4(int argc, char **argv, struct Trapframe *tf)
        return 0;
 }

+int mon_stepi(int argc, char **argv, struct Trapframe *tf) {
+       if(tf == NULL){
+               cprintf("cannot execute `stepi`. is there any breakpoint set?\n");
+               return 0;
+       }
+       tf->tf_eflags |= FL_TF;
+       cprintf("DEBUG> eip: \t%08x\n", tf->tf_eip);
+       env_run(curenv);
+       return 0;
+}
+
 /***** Kernel monitor command interpreter *****/

 #define WHITESPACE "\t\r\n "
diff --git a/lab/kern/monitor.h b/lab/kern/monitor.h
index 104bae4..27e3fc4 100644
--- a/lab/kern/monitor.h
+++ b/lab/kern/monitor.h
@@ -17,5 +17,9 @@ int mon_kerninfo(int argc, char **argv, struct Trapframe *tf);
 int mon_backtrace(int argc, char **argv, struct Trapframe *tf);
 int play_lab_1_exercise_8_3(int argc, char **argv, struct Trapframe *tf);
 int play_lab_1_exercise_8_4(int argc, char **argv, struct Trapframe *tf);
+int mon_stepi(int argc, char **argv, struct Trapframe *tf);
+
+extern struct Env *curenv;
+extern void env_run(struct Env *e);

#endif // !JOS_KERN_MONITOR_H
```

Verify the work in directory *lab*:

```shell
$ make clean && make run-breakpoint-nox
    ⋮
Welcome to the JOS kernel monitor!
Type 'help' for a list of commands.
TRAP frame at 0xf01bf000
  edi  0x00000000
  esi  0x00000000
  ebp  0xeebfdfd0
  oesp 0xefffffdc
  ebx  0x00000000
  edx  0x00000000
  ecx  0x00000000
  eax  0xeec00000
  es   0x----0023
  ds   0x----0023
  trap 0x00000003 Breakpoint
  err  0x00000000
  eip  0x00800037
  cs   0x----001b
  flag 0x00000082
  esp  0xeebfdfd0
  ss   0x----0023
K> stepi
DEBUG> eip: 	     00800037
Incoming TRAP frame at 0xefffffbc
TRAP frame at 0xf01bf000
  edi  0x00000000
  esi  0x00000000
  ebp  0xeebfdff0
  oesp 0xefffffdc
  ebx  0x00000000
  edx  0x00000000
  ecx  0x00000000
  eax  0xeec00000
  es   0x----0023
  ds   0x----0023
  trap 0x00000001 Debug
  err  0x00000000
  eip  0x00800038
  cs   0x----001b
  flag 0x00000182
  esp  0xeebfdfd4
  ss   0x----0023
[00001000] free env 00001000
Destroyed the only environment - nothing more to do!
Welcome to the JOS kernel monitor!
    ⋮
```

### Question 3

> The break point test case will either generate a break point exception or a general protection fault depending on how you initialized the break point entry in the IDT (i.e., your call to SETGATE from trap_init). Why? How do you need to set it up in order to get the breakpoint exception to work as specified above and what incorrect setup would cause it to trigger a general protection fault?

This question is similar to the Question 2, so just set `CPL <= DPL`. Change the privilege level to `3` in the function `trap_init` in *lab/kern/trap.c*:

```c
SETGATE(idt[T_BRKPT], 0, GD_KT, &breakpointHandler, 3);
```

### Question 4

> What do you think is the point of these mechanisms, particularly in light of what the user/softint test program does?

These mechanisms are necessary for ensuring the *isolation*. With them, a *user/softint* is restricted in manipulating the kernel space.

## Exercise 7

> Add a handler in the kernel for interrupt vector T\_SYSCALL. You will have to edit kern/trapentry.S and kern/trap.c's trap\_init(). You also need to change trap\_dispatch() to handle the system call interrupt by calling syscall() (defined in kern/syscall.c) with the appropriate arguments, and then arranging for the return value to be passed back to the user process in %eax. Finally, you need to implement syscall() in kern/syscall.c. Make sure syscall() returns -E\_INVAL if the system call number is invalid. You should read and understand lib/syscall.c (especially the inline assembly routine) in order to confirm your understanding of the system call interface. Handle all the system calls listed in inc/syscall.h by invoking the corresponding kernel function for each call.
> 
> Run the user/hello program under your kernel (make run-hello). It should print "hello, world" on the console and then cause a page fault in user mode. If this does not happen, it probably means your system call handler isn't quite right. You should also now be able to get make grade to succeed on the testbss test.

Firstly, declare and dispatch the system call interrupt:

```diff
diff --git a/lab/kern/trap.c b/lab/kern/trap.c
index ba33414..597a44e 100644
--- a/lab/kern/trap.c
+++ b/lab/kern/trap.c
@@ -81,7 +81,7 @@ void trap_init(void) {
        void alignmentCheckHandler();
        void machineCheckHandler();
        void SIMDFloatingPointExceptionHandler();
+       void syscallHandler();

        SETGATE(idt[T_DIVIDE], 0, GD_KT, &divideErrorHandler, 0);
        SETGATE(idt[T_DEBUG], 0, GD_KT, &debugHandler, 0);
@@ -101,7 +101,7 @@ void trap_init(void) {
        SETGATE(idt[T_ALIGN], 0, GD_KT, &alignmentCheckHandler, 0);
        SETGATE(idt[T_MCHK], 0, GD_KT, &machineCheckHandler, 0);
        SETGATE(idt[T_SIMDERR], 0, GD_KT, &SIMDFloatingPointExceptionHandler, 0);
+       SETGATE(idt[T_SYSCALL], 0, GD_KT, &syscallHandler, 3);

        // Per-CPU setup
        trap_init_percpu();
@@ -182,6 +182,11 @@ static void trap_dispatch(struct Trapframe *tf) {
        switch (tf->tf_trapno) {
        case T_PGFLT: page_fault_handler(tf); return;
        case T_BRKPT: monitor(tf); return;
+       case T_SYSCALL:
+               tf->tf_regs.reg_eax = syscall(tf->tf_regs.reg_eax, tf->tf_regs.reg_edx,
+                                             tf->tf_regs.reg_ecx, tf->tf_regs.reg_ebx,
+                                             tf->tf_regs.reg_edi, tf->tf_regs.reg_esi);
+               return;
        default:
                // Unexpected trap: The user process or the kernel has a bug.
                print_trapframe(tf);
diff --git a/lab/kern/trapentry.S b/lab/kern/trapentry.S
index 4f67184..0d52b85 100644
--- a/lab/kern/trapentry.S
+++ b/lab/kern/trapentry.S
@@ -67,6 +67,7 @@ TRAPHANDLER_NOEC(floatingPointErrorHandler, T_FPERR);
 TRAPHANDLER_NOEC(alignmentCheckHandler, T_ALIGN);
 TRAPHANDLER_NOEC(machineCheckHandler, T_MCHK);
 TRAPHANDLER_NOEC(SIMDFloatingPointExceptionHandler, T_SIMDERR);
+TRAPHANDLER_NOEC(syscallHandler, T_SYSCALL);
```

Then, complete `syscall` function in *lab/kern/syscall.c*:

```c
// Dispatches to the correct kernel function, passing the arguments.
int32_t syscall(uint32_t syscallno, uint32_t a1, uint32_t a2, uint32_t a3, uint32_t a4, uint32_t a5) {
    // Call the function corresponding to the 'syscallno' parameter.
    // Return any appropriate return value.
    // LAB 3: Your code here.
    switch (syscallno) {
    case SYS_cputs: sys_cputs((const char *)a1, a2); return 0;
    case SYS_cgetc: return sys_cgetc();
    case SYS_getenvid: return sys_getenvid();
    case SYS_env_destroy: return sys_env_destroy(a1);
    default: return -E_INVAL;
    }
}
```

Verify the work in directory *lab*:

```shell
$ make run-hello-nox
    ⋮
check_page_installed_pgdir() succeeded!
[00000000] new env 00001000
Incoming TRAP frame at 0xefffffbc
hello, world
Incoming TRAP frame at 0xefffffbc
[00001000] user fault va 00000048 ip 00800048
TRAP frame at 0xf01be000
  edi  0x00000000
  esi  0x00000000
  ebp  0xeebfdfd0
  oesp 0xefffffdc
  ebx  0x00000000
  edx  0xeebfde88
  ecx  0x0000000d
  eax  0x00000000
  es   0x----0023
  ds   0x----0023
  trap 0x0000000e Page Fault
  cr2  0x00000048
  err  0x00000004 [user, read, not-present]
  eip  0x00800048
  cs   0x----001b
  flag 0x00000092
  esp  0xeebfdfb8
  ss   0x----0023
[00001000] free env 00001000
Destroyed the only environment - nothing more to do!
Welcome to the JOS kernel monitor!
    ⋮
```

Grade the work in directory *lab* (probably run multiple times):

```shell
$ make clean && make grade
    ⋮
testbss: OK (2.3s)
    ⋮
```

todo: challenge

## Exercise 8

> Add the required code to the user library, then boot your kernel. You should see user/hello print "hello, world" and then print "i am environment 00001000". user/hello then attempts to "exit" by calling sys\_env\_destroy() (see lib/libmain.c and lib/exit.c). Since the kernel currently only supports one user environment, it should report that it has destroyed the only environment and then drop into the kernel monitor. You should be able to get make grade to succeed on the hello test.

The task is changing one line in *lab/lib/libmain.c*:

```diff
diff --git a/lab/lib/libmain.c b/lab/lib/libmain.c
index 8a14b29..2c57a4b 100644
--- a/lab/lib/libmain.c
+++ b/lab/lib/libmain.c
@@ -13,7 +13,7 @@ libmain(int argc, char **argv)
 {
        // set thisenv to point at our Env structure in envs[].
        // LAB 3: Your code here.
-       thisenv = 0;
+       thisenv = &envs[ENVX(sys_getenvid())];

        // save the name of the program so that panic() can use it
        if (argc > 0)
```

Verify the work in directory *lab* (probably run multiple times):

```shell
    ⋮
$ make clean && make qemu-nox
Incoming TRAP frame at 0xefffffbc
hello, world
Incoming TRAP frame at 0xefffffbc
i am environment 00001000
Incoming TRAP frame at 0xefffffbc
[00001000] exiting gracefully
[00001000] free env 00001000
Destroyed the only environment - nothing more to do!
Welcome to the JOS kernel monitor!
    ⋮
```

Grade the work in directory *lab*:

```shell
$ make grade
    ⋮
hello: OK (2.2s)
    ⋮
```

## Page Faults and Memory Protection

The *fixable* page faults can be recovered by the kernel. If a page fault is not fixable, the user program that causes it cannot continue.

A user program passes pointers to the kernel, which uses the pointers to fix page faults if possible. Kernel-level page faults may happen when the kernel tries to fix a user program page fault. In such a case, the kernel must have a way to know that the page fault is caused by a user program.

A user program may pass pointers that point at memory that the kernel can read and write but that the user program cannot. The kernel must protect itself in this case.

In both cases, the kernel must treat the pointers passed by user programs carefully. Thus, the kernel will never suffer a page fault due to dereferencing a user-supplied pointer. If the kernel does page fault, it should panic and terminate.

## Exercise 9

> Change kern/trap.c to panic if a page fault happens in kernel mode.
> 
> Hint: to determine whether a fault happened in user mode or in kernel mode, check the low bits of the tf_cs.
> 
> Read user\_mem\_assert in kern/pmap.c and implement user\_mem\_check in that same file.
> 
> Change kern/syscall.c to sanity check arguments to system calls.
> 
> Boot your kernel, running user/buggyhello. The environment should be destroyed, and the kernel should not panic. You should see:
> 
> ```
>     [00001000] user_mem_check assertion failure for va 00000001
>     [00001000] free env 00001000
>     Destroyed the only environment - nothing more to do!
> ```
> 
> Finally, change debuginfo\_eip in kern/kdebug.c to call user\_mem_check on usd, stabs, and stabstr. If you now run user/breakpoint, you should be able to run backtrace from the kernel monitor and see the backtrace traverse into lib/libmain.c before the kernel panics with a page fault. What causes this page fault? You don't need to fix it, but you should understand why it happens.

The following code in *lab/kern/trap.c* is throwing panic for a fault in kernel mode:

```c
void page_fault_handler(struct Trapframe *tf) {
    ⋮
    if ((tf->tf_cs & 3) == 0) panic("page fault in kernel mode");
    ⋮
```

Implement `user_mem_check` in *lab/kern/pmap.c*:

```c
// Check that an environment is allowed to access the range of memory
// [va, va+len) with permissions 'perm | PTE_P'.
// Normally 'perm' will contain PTE_U at least, but this is not required.
// 'va' and 'len' need not be page-aligned; you must test every page that
// contains any of that range.  You will test either 'len/PGSIZE',
// 'len/PGSIZE + 1', or 'len/PGSIZE + 2' pages.
//
// A user program can access a virtual address if (1) the address is below
// ULIM, and (2) the page table gives it permission.  These are exactly
// the tests you should implement here.
//
// If there is an error, set the 'user_mem_check_addr' variable to the first
// erroneous virtual address.
//
// Returns 0 if the user program can access this range of addresses,
// and -E_FAULT otherwise.
//
int user_mem_check(struct Env *env, const void *va, size_t len, int perm) {
    // LAB 3: Your code here.
    uint32_t virtual_address = (uint32_t)va, start = ROUNDDOWN(virtual_address, PGSIZE), end = ROUNDUP(virtual_address + len, PGSIZE);
    pte_t *pte;
    struct PageInfo *page_info;
    while (start < end) {
        page_info = page_lookup(env->env_pgdir, (void *)start, &pte);
        if (!page_info || start >= ULIM || pte == NULL || !(*pte & PTE_P) || (*pte & perm) != perm) {
            user_mem_check_addr = (start < virtual_address) ? virtual_address : start;
            return -E_FAULT;
        }
        start += PGSIZE;
    }
    return 0;
}
```

It is also required to complete `sys_cputs` in *lab/kern/syscall.c*:

```diff
diff --git a/lab/kern/syscall.c b/lab/kern/syscall.c
index ffdd448..b0ac29b 100644
--- a/lab/kern/syscall.c
+++ b/lab/kern/syscall.c
@@ -21,7 +21,7 @@ sys_cputs(const char *s, size_t len)
        // Destroy the environment if not.

        // LAB 3: Your code here.
-
+       user_mem_assert(curenv, s, len, PTE_U);
        // Print the string supplied by the user.
        cprintf("%.*s", len, s);
 }
```

Verify the work in directory *lab* (probably run multiple times):

```shell
$ make clean && make run-buggyhello-nox
    ⋮
check_page_installed_pgdir() succeeded!
[00000000] new env 00001000
Incoming TRAP frame at 0xefffffbc
Incoming TRAP frame at 0xefffffbc
[00001000] user_mem_check assertion failure for va 00000001
[00001000] free env 00001000
Destroyed the only environment - nothing more to do!
Welcome to the JOS kernel monitor!
    ⋮
```

Change `debuginfo_eip` in *kern/kdebug.c* to call `user_mem_check` on `usd`, `stabs`, and `stabstr`:

```diff
diff --git a/lab/kern/kdebug.c b/lab/kern/kdebug.c
index f4ee8ee..3d395c2 100644
--- a/lab/kern/kdebug.c
+++ b/lab/kern/kdebug.c
@@ -142,6 +142,7 @@ debuginfo_eip(uintptr_t addr, struct Eipdebuginfo *info)
                // Make sure this memory is valid.
                // Return -1 if it is not.  Hint: Call user_mem_check.
                // LAB 3: Your code here.
+               if (user_mem_check(curenv, usd, sizeof(struct UserStabData), PTE_U) < 0) return -1;

                stabs = usd->stabs;
                stab_end = usd->stab_end;
@@ -150,6 +151,8 @@ debuginfo_eip(uintptr_t addr, struct Eipdebuginfo *info)

                // Make sure the STABS and string table memory is valid.
                // LAB 3: Your code here.
+               if (user_mem_check(curenv, stabs, stab_end - stabs, PTE_U) < 0) return -1;
+               if (user_mem_check(curenv, stabstr, stabstr_end - stabstr, PTE_U) < 0) return -1;
        }

```

Verify the work in directory *lab* (probably run multiple times):

```shell
$ make clean && make run-breakpoint-nox
    ⋮
Welcome to the JOS kernel monitor!
Type 'help' for a list of commands.
TRAP frame at 0xf01bf000
  edi  0x00000000
  esi  0x00000000
  ebp  0xeebfdfd0
  oesp 0xefffffdc
  ebx  0x00000000
  edx  0x00000000
  ecx  0x00000000
  eax  0xeec00000
  es   0x----0023
  ds   0x----0023
  trap 0x00000003 Breakpoint
  err  0x00000000
  eip  0x00800037
  cs   0x----001b
  flag 0x00000082
  esp  0xeebfdfd0
  ss   0x----0023
K> backtrace
Stack backtrace:
  ebp efffff20  eip f0100912  args 00000001 efffff38 f01bf000 00000000 f017d700
         kern/monitor.c:0: monitor+276
  ebp efffff90  eip f010361c  args f01bf000 efffffbc 00000000 00000082 00000000
         kern/trap.c:0: trap+169
  ebp efffffb0  eip f0103725  args efffffbc 00000000 00000000 eebfdfd0 efffffdc
         kern/syscall.c:0: syscall+0
  ebp eebfdfd0  eip 00800073  args 00000000 00000000 eebfdff0 00800049 00000000
         lib/libmain.c:0: libmain+58
Incoming TRAP frame at 0xeffffebc
kernel panic at kern/trap.c:249: page fault in kernel mode
Welcome to the JOS kernel monitor!
Type 'help' for a list of commands.
K>
```

## Exercise 10

> Boot your kernel, running user/evilhello. The environment should be destroyed, and the kernel should not panic. You should see:
> 
> ```
>     [00000000] new env 00001000
>     ...
>     [00001000] user_mem_check assertion failure for va f010000c
>     [00001000] free env 00001000
> ```

All work has been done so far, so verify the work in directory *lab* (probably run multiple times):

```shell
$ make clean && make run-evilhello-nox
    ⋮
check_page_installed_pgdir() succeeded!
[00000000] new env 00001000
Incoming TRAP frame at 0xefffffbc
Incoming TRAP frame at 0xefffffbc
[00001000] user_mem_check assertion failure for va f010000c
[00001000] free env 00001000
Destroyed the only environment - nothing more to do!
Welcome to the JOS kernel monitor!
    ⋮
```

Make the final grade:

```shell
$ make clean && make grade
    ⋮
divzero: OK (2.0s)
softint: OK (1.7s)
badsegment: OK (1.6s)
Part A score: 30/30

faultread: OK (2.2s)
faultreadkernel: OK (1.7s)
faultwrite: OK (2.3s)
faultwritekernel: OK (1.6s)
breakpoint: OK (2.1s)
testbss: OK (2.3s)
hello: OK (1.5s)
buggyhello: OK (2.2s)
buggyhello2: OK (1.8s)
evilhello: OK (2.2s)
Part B score: 50/50

Score: 80/80
```

✌️
