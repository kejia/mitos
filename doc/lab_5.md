
[Lab 5](http://pdos.csail.mit.edu/6.828/2018/labs/lab5/)

# Merging Lab 4 and Lab 5

It is really difficult to merge the two labs works. I finally successfully merged them in steps.

## Basic Merge

Follow the instructions listed in assignment:

```shell
$ git checkout -b lab5 origin/lab5
$ git merge lab4
```

The start commit is *ac33d8243848efb747e0604e60c884fb09979f10*. At this point, the work done in Lab 4 is basically destroyed. I had to check each changed file to fix Lab 4.

## Fixing Lab 4

Lab 5 disables some work for Lab 4 in the file *lab/kern/env.c*, and let's restore it:

```diff
diff --git a/lab/kern/env.c b/lab/kern/env.c
index a3091b9..b766db4 100644
--- a/lab/kern/env.c
+++ b/lab/kern/env.c
@@ -265,7 +265,7 @@ env_alloc(struct Env **newenv_store, envid_t parent_id)
        env_free_list = e->env_link;
        *newenv_store = e;

-       // cprintf("[%08x] new env %08x\n", curenv ? curenv->env_id : 0, e->env_id);
+       cprintf("[%08x] new env %08x\n", curenv ? curenv->env_id : 0, e->env_id);
        return 0;
 }

@@ -406,7 +406,7 @@ env_free(struct Env *e)
                lcr3(PADDR(kern_pgdir));

        // Note the environment's demise.
-       // cprintf("[%08x] free env %08x\n", curenv ? curenv->env_id : 0, e->env_id);
+       cprintf("[%08x] free env %08x\n", curenv ? curenv->env_id : 0, e->env_id);

        // Flush all mapped pages in the user portion of the address space
        static_assert(UTOP % PTSIZE == 0);
```

Some work for Lab 4 is deleted in the file *lab/kern/syscall.c*, and let's restore it:

```diff
diff --git a/lab/kern/syscall.c b/lab/kern/syscall.c
index 687d35a..025d1be 100644
--- a/lab/kern/syscall.c
+++ b/lab/kern/syscall.c
@@ -55,6 +55,10 @@ sys_env_destroy(envid_t envid)

        if ((r = envid2env(envid, &e, 1)) < 0)
                return r;
+       if (e == curenv)
+               cprintf("[%08x] exiting gracefully\n", curenv->env_id);
+       else
+               cprintf("[%08x] destroying %08x\n", curenv->env_id, e->env_id);
        env_destroy(e);
        return 0;
 }
```

Also, comment out 2 lines as are mentioned in the assignment:

```diff
diff --git a/lab/kern/init.c b/lab/kern/init.c
index 0ec4314..97c9ae6 100644
--- a/lab/kern/init.c
+++ b/lab/kern/init.c
@@ -49,7 +49,7 @@ i386_init(void)
        boot_aps();

        // Start fs.
-       ENV_CREATE(fs_fs, ENV_TYPE_FS);
+       ///// ENV_CREATE(fs_fs, ENV_TYPE_FS);

 #if defined(TEST)
        // Don't touch -- used by grading script!
diff --git a/lab/lib/exit.c b/lab/lib/exit.c
index cee3336..b88b29a 100644
--- a/lab/lib/exit.c
+++ b/lab/lib/exit.c
@@ -4,7 +4,7 @@
 void
 exit(void)
 {
-       close_all();
+       ///// close_all();
        sys_env_destroy(0);
 }
```

Change the grader number for Lab 4:

```diff
diff --git a/lab/conf/lab.mk b/lab/conf/lab.mk
index 428f188..2378783 100644
--- a/lab/conf/lab.mk
+++ b/lab/conf/lab.mk
@@ -1,2 +1,2 @@
-LAB=5
+LAB=4
```

These are almost all works needed to pass Lab 4 grade. Unfortunately, I ran into another tough issue when I started the grader in the directory *mitos/lab* in the VirtualBox machine:

```shell
$ make grade
    ⋮
+ mk obj/fs/clean-fs.img
mmap obj/fs/clean-fs.img: Invalid argument
fs/Makefrag:64: recipe for target 'obj/fs/clean-fs.img' failed
make[1]: *** [obj/fs/clean-fs.img] Aborted (core dumped)
make[1]: *** Deleting file 'obj/fs/clean-fs.img'
make[1]: Leaving directory '/home/vagrant/mitos/lab'
GNUmakefile:210: recipe for target 'grade' failed
make: *** [grade] Error 1
```

The last step is to resolve this issue.

## Fixing the VirtualBox Bug (or Feature)

The build failure is thrown by the function `opendisk` in the file *lab/fs/fsformat.c*:

```c
void opendisk(const char *name) {
    int r, diskfd, nbitblocks;

    if ((diskfd = open(name, O_RDWR | O_CREAT, 0666)) < 0)
        panic("open %s: %s", name, strerror(errno));

    if ((r = ftruncate(diskfd, 0)) < 0
        || (r = ftruncate(diskfd, nblocks * BLKSIZE)) < 0)
        panic("truncate %s: %s", name, strerror(errno));

    if ((diskmap = mmap(NULL, nblocks * BLKSIZE, PROT_READ|PROT_WRITE,
                MAP_SHARED, diskfd, 0)) == MAP_FAILED)
        panic("mmap %s: %s", name, strerror(errno));

    close(diskfd);

    diskpos = diskmap;
    alloc(BLKSIZE);
    super = alloc(BLKSIZE);
    super->s_magic = FS_MAGIC;
    super->s_nblocks = nblocks;
    super->s_root.f_type = FTYPE_DIR;
    strcpy(super->s_root.f_name, "/");

    nbitblocks = (nblocks + BLKBITSIZE - 1) / BLKBITSIZE;
    bitmap = alloc(nbitblocks * BLKSIZE);
    memset(bitmap, 0xFF, nbitblocks * BLKSIZE);
}
```

This function is called by the build script to create a clean file system image (see *lab/fs/Makefrag*):

```make
    ⋮
$(OBJDIR)/fs/clean-fs.img: $(OBJDIR)/fs/fsformat $(FSIMGFILES)
    @echo + mk $(OBJDIR)/fs/clean-fs.img
    $(V)mkdir -p $(@D)
    $(V)$(OBJDIR)/fs/fsformat $(OBJDIR)/fs/clean-fs.img 1024 $(FSIMGFILES)
    ⋮
```

The problematic code is at:

```c
    if ((diskmap = mmap(NULL, nblocks * BLKSIZE, PROT_READ|PROT_WRITE,
                MAP_SHARED, diskfd, 0)) == MAP_FAILED)
        panic("mmap %s: %s", name, strerror(errno));
```

It catches `Invalid argument` error:

```shell
mmap obj/fs/clean-fs.img: Invalid argument
```

The program actually does not have bug, and this issue is related to a VirtualBox bug (or feature), and one who has interest may see:

- http://www.virtualbox.org/ticket/819
- http://github.com/alticelabs/kyoto/blob/master/Vagrantfile#L8
- http://stackoverflow.com/a/18421071/571828
- http://patchwork.kernel.org/project/linux-fsdevel/patch/20190418100412.19016-2-hdegoede@redhat.com/

In brief, in a VirtualBox machine, the `MAP_SHARED` parameter to the `mmap` system call is not allowed for a file that locates in a directory shared between the virtual system and the host system. In my Vagrant config, the file `$(OBJDIR)/fs/clean-fs.img` whose file descriptor, `diskfd`, is passed to `mmap` locates in the directory `/home/vagrant/mitos/lab/fs`, and the directory `/home/vagrant/mitos/lab` is such a shared one between the virtual system and the host system.

The fix is easy:

```diff
diff --git a/lab/fs/Makefrag b/lab/fs/Makefrag
index 748e065..e8c9107 100644
--- a/lab/fs/Makefrag
+++ b/lab/fs/Makefrag
@@ -60,14 +60,14 @@ $(OBJDIR)/fs/fsformat: fs/fsformat.c
        $(V)mkdir -p $(@D)
        $(V)$(NCC) $(NATIVE_CFLAGS) -o $(OBJDIR)/fs/fsformat fs/fsformat.c

-$(OBJDIR)/fs/clean-fs.img: $(OBJDIR)/fs/fsformat $(FSIMGFILES)
-       @echo + mk $(OBJDIR)/fs/clean-fs.img
+/home/vagrant/clean-fs.img: $(OBJDIR)/fs/fsformat $(FSIMGFILES)
+       @echo + mk /home/vagrant/clean-fs.img
        $(V)mkdir -p $(@D)
-       $(V)$(OBJDIR)/fs/fsformat $(OBJDIR)/fs/clean-fs.img 1024 $(FSIMGFILES)
+       $(V)$(OBJDIR)/fs/fsformat /home/vagrant/clean-fs.img 1024 $(FSIMGFILES)

-$(OBJDIR)/fs/fs.img: $(OBJDIR)/fs/clean-fs.img
-       @echo + cp $(OBJDIR)/fs/clean-fs.img $@
-       $(V)cp $(OBJDIR)/fs/clean-fs.img $@
+$(OBJDIR)/fs/fs.img: /home/vagrant/clean-fs.img
+       @echo + cp /home/vagrant/clean-fs.img $@
+       $(V)cp /home/vagrant/clean-fs.img $@

 all: $(OBJDIR)/fs/fs.img

diff --git a/lab/gradelib.py b/lab/gradelib.py
index 07a7a84..500914b 100644
--- a/lab/gradelib.py
+++ b/lab/gradelib.py
@@ -232,8 +232,8 @@ def color(name, text):
     return text

 def reset_fs():
-    if os.path.exists("obj/fs/clean-fs.img"):
-        shutil.copyfile("obj/fs/clean-fs.img", "obj/fs/fs.img")
+    if os.path.exists("/home/vagrant/clean-fs.img"):
+        shutil.copyfile("/home/vagrant/clean-fs.img", "obj/fs/fs.img")

 ##################################################################
 # Controllers
```

I re-direct `$(OBJDIR)/fs/clean-fs.img` to `/home/vagrant/clean-fs.img`: the directory `/home/vagrant` is not a shared one between the virtual system and the host one.

Now, the Lab 4 work can pass the grader again:

```shell
    ⋮
+ mk /home/vagrant/clean-fs.img
+ cp /home/vagrant/clean-fs.img obj/fs/fs.img
make[1]: Leaving directory '/home/vagrant/mitos/lab'
dumbfork: OK (2.1s)
Part A score: 5/5

faultread: OK (1.5s)
faultwrite: OK (2.1s)
faultdie: OK (2.0s)
faultregs: OK (1.9s)
faultalloc: OK (2.0s)
faultallocbad: OK (2.1s)
faultnostack: OK (1.3s)
faultbadhandler: OK (2.2s)
faultevilhandler: OK (2.2s)
forktree: OK (2.6s)
Part B score: 50/50

spin: OK (1.9s)
stresssched: OK (3.4s)
sendpage: OK (1.2s)
pingpong: OK (1.9s)
primes: OK (9.8s)
Part C score: 25/25

Score: 80/80
```

The fixing work is committed at: *4c77f9b822e1568c588941d6eb9eb9e8488fe2b6*.

## Preparing for Lab 5

Now restore some modifications done in the previous steps in order to prepare Lab 5.

Firstly, enable the Lab 5 code:

```diff
diff --git a/lab/conf/lab.mk b/lab/conf/lab.mk
index 2378783..428f188 100644
--- a/lab/conf/lab.mk
+++ b/lab/conf/lab.mk
@@ -1,2 +1,2 @@
-LAB=4
+LAB=5
 PACKAGEDATE=Wed Oct 24 20:44:37 EDT 2018
diff --git a/lab/kern/init.c b/lab/kern/init.c
index 97c9ae6..0ec4314 100644
--- a/lab/kern/init.c
+++ b/lab/kern/init.c
@@ -49,7 +49,7 @@ i386_init(void)
        boot_aps();

        // Start fs.
-       ///// ENV_CREATE(fs_fs, ENV_TYPE_FS);
+       ENV_CREATE(fs_fs, ENV_TYPE_FS);

 #if defined(TEST)
        // Don't touch -- used by grading script!
diff --git a/lab/lib/exit.c b/lab/lib/exit.c
index b88b29a..cee3336 100644
--- a/lab/lib/exit.c
+++ b/lab/lib/exit.c
@@ -4,7 +4,7 @@
 void
 exit(void)
 {
-       ///// close_all();
+       close_all();
        sys_env_destroy(0);
 }
```

Secondly, disable some unnecessary legacy code:

```diff
diff --git a/lab/kern/env.c b/lab/kern/env.c
index b766db4..a3091b9 100644
--- a/lab/kern/env.c
+++ b/lab/kern/env.c
@@ -265,7 +265,7 @@ env_alloc(struct Env **newenv_store, envid_t parent_id)
 	env_free_list = e->env_link;
 	*newenv_store = e;
 
-	cprintf("[%08x] new env %08x\n", curenv ? curenv->env_id : 0, e->env_id);
+	// cprintf("[%08x] new env %08x\n", curenv ? curenv->env_id : 0, e->env_id);
 	return 0;
 }
 
@@ -406,7 +406,7 @@ env_free(struct Env *e)
 		lcr3(PADDR(kern_pgdir));
 
 	// Note the environment's demise.
-	cprintf("[%08x] free env %08x\n", curenv ? curenv->env_id : 0, e->env_id);
+	// cprintf("[%08x] free env %08x\n", curenv ? curenv->env_id : 0, e->env_id);
 
 	// Flush all mapped pages in the user portion of the address space
 	static_assert(UTOP % PTSIZE == 0);
diff --git a/lab/kern/syscall.c b/lab/kern/syscall.c
index 025d1be..eab2159 100644
--- a/lab/kern/syscall.c
+++ b/lab/kern/syscall.c
@@ -55,10 +55,12 @@ sys_env_destroy(envid_t envid)
 
 	if ((r = envid2env(envid, &e, 1)) < 0)
 		return r;
+    /*
 	if (e == curenv)
 		cprintf("[%08x] exiting gracefully\n", curenv->env_id);
 	else
 		cprintf("[%08x] destroying %08x\n", curenv->env_id, e->env_id);
+    */
 	env_destroy(e);
 	return 0;
 }
```

The work as for now is committed at: *1171fc976087c87a814c64d219c62b901577d143*.

Oh, my weekend.

# File System Preliminaries

## Sectors and Blocks

Most disks perform reads and writes in units of *sectors*, and file systems actually allocate and use disk storage in units of *blocks*. *Sector size* is a property of disk hardware, whereas *block size* is an aspect of the operating system using the disk. A file system's block size must be a multiple of the sector size of the underlying disk.

In this lab, the block size is `4096` bytes.

## Superblocks

File systems typically reserve certain disk blocks at *easy-to-find* locations on the disk (such as the very start or the very end) to hold meta-data describing properties of the file system as a whole, such as the block size, disk size, any meta-data required to find the root directory, the time the file system was last mounted, the time the file system was last checked for errors, and so on. These special blocks are called *superblocks*.

Our file system will have exactly one superblock, which will always be at block 1 on the disk. Its layout is defined by `struct Super` in *lab/inc/fs.h*. Block `0` is typically reserved to hold boot loaders and partition tables, so file systems generally do not use the very first disk block. Many real file systems maintain multiple superblocks, replicated throughout several widely-spaced regions of the disk, so that if one of them is corrupted or the disk develops a media error in that region, the other superblocks can still be found and used to access the file system.

![e472fbad8a2fd0bb94f15cefc6677054.png](../_resources/93c854c10a51413f92ee4e6306a7978e.png)

## File Meta-data

The layout of the meta-data describing a file in our file system is described by `struct File` in *lab/inc/fs.h*. This meta-data includes the file's name, size, type (regular file or directory), and pointers to the blocks comprising the file. As mentioned above, we do not have inodes in this lab, so this meta-data is stored in a directory entry on disk. Unlike in most real file systems, for simplicity we will use this one `File` structure to represent file meta-data as it appears *both on disk and in memory*.

The `f_direct` array in `struct File` contains space to store the block numbers of the first `10` (`NDIRECT`) blocks of the file, which we call the file's *direct blocks*. For small files up to `10*4096 = 40`KB in size, this means that the block numbers of all of the file's blocks will fit directly within the File structure itself. For larger files, however, we need a place to hold the rest of the file's block numbers. For any file greater than `40`KB in size, therefore, we allocate an additional disk block, called the file's *indirect block*, to hold up to `4096 / 4 = 1024` additional block numbers. Our file system therefore allows files to be up to `1034` blocks, or just over `4` megabytes, in size. To support larger files, real file systems typically support double- and triple-indirect blocks as well.

![c7dc41e879649437553b76941ec3bfb4.png](../_resources/4e19105c6d2e4a79a320524f4e5adff0.png)

## Directories versus Regular Files

A File structure in our file system can represent either a *regular file* or a *directory*; these two types of *files* are distinguished by the `type` field in the `File` structure. The file system manages *regular files* and *directory files* in exactly the same way, except that it does not interpret the contents of the *data blocks* associated with *regular files* at all, whereas the file system interprets the contents of a *directory file* as a series of `File` structures describing the files and subdirectories within the directory.

The superblock in our file system contains a `File` structure (the `root` field in `struct Super`) that holds the meta-data for the file system's root directory. The contents of this directory-file is a sequence of `File` structures describing the files and directories located within the root directory of the file system. Any subdirectories in the root directory may in turn contain more `File` structures representing sub-subdirectories, and so on.

# The File System

Note that the *GNUmakefile* file in this lab sets up QEMU to use the file *lab/obj/kern/kernel.img* as the image for disk 0 as before, and to use the (new) file *lab/obj/fs/fs.img* as the image for disk 1. In this lab our file system should only ever touch disk 1; disk 0 is used only to boot the kernel. If you corrupt either disk image in some way, you can reset both of them to their original, *pristine* versions simply by typing:

```shell
$ make clean && make
```

## Disk Access

We implement the IDE disk driver as part of the user-level file system environment.

The x86 processor uses the `IOPL` bits in the `EFLAGS` register to determine whether protected-mode code is allowed to perform special device I/O instructions such as the `IN` and `OUT` instructions. Since all of the IDE disk registers we need to access are located in the x86's I/O space rather than being memory-mapped, giving *I/O privilege* to the file system environment is the only thing we need to do in order to allow the file system to access these registers. In effect, the `IOPL` bits in the `EFLAGS` register provides the kernel with a simple *all-or-nothing* method of controlling whether user-mode code can access I/O space. In our case, we want the file system environment to be able to access I/O space, but we do not want any other environments to be able to access I/O space at all.

## Exercise 1

> i386\_init identifies the file system environment by passing the type ENV\_TYPE\_FS to your environment creation function, env\_create. Modify env_create in env.c, so that it gives the file system environment I/O privilege, but never gives that privilege to any other environment.
> 
> Make sure you can start the file environment without causing a General Protection fault. You should pass the "fs i/o" test in make grade.

The I/O privileges is controlled by `FL_IOPL_MASK`, which is defined in *lab/inc/mmu.h*:

```c
109:#define FL_IOPL_MASK	0x00003000	// I/O Privilege Level bitmask
```

The change is as follows:

```diff
diff --git a/lab/kern/env.c b/lab/kern/env.c
index a3091b9..74cd15e 100644
--- a/lab/kern/env.c
+++ b/lab/kern/env.c
@@ -386,7 +386,7 @@ void env_create(uint8_t *binary, enum EnvType type) {
 
 	// If this is the file server (type == ENV_TYPE_FS) give it I/O privileges.
 	// LAB 5: Your code here.
-
+	if (type == ENV_TYPE_FS) env->env_tf.tf_eflags |= FL_IOPL_MASK;
 }
 
 //

```

Now the grade passes one:

```shell
$ cd ~/mitos/lab && make grade
    ⋮
OK (2.0s)
  fs i/o: OK
    ⋮
```

## Question

> Do you have to do anything else to ensure that this I/O privilege setting is saved and restored properly when you subsequently switch from one environment to another? Why?

No. The `FL_IOPL_MASK` bit of the `EFLAGS` register is used to set the I/O privilege, and the `EFLAGS` register content is stored and is restored in user space when environments switch.

## The Block Cache

In our file system, we will implement a simple *buffer cache* (really just a *block cache*) with the help of the processor's *virtual memory system*. The code for the block cache is in *lab/fs/bc.c*.

Our file system will be limited to handling disks of size `3`GB or less. We reserve a large, fixed `3`GB region of the file system environment's address space, from `0x10000000` (`DISKMAP`) up to `0xD0000000` (`DISKMAP + DISKMAX`), as a *memory mapped* version of the disk. For example, disk block `0` is mapped at virtual address `0x10000000`, disk block `1` is mapped at virtual address `0x10001000`, and so on. The `diskaddr` function in *lab/fs/bc.c* implements this translation from disk block numbers to virtual addresses (along with some sanity checking).

Of course, it would take a long time to read the entire disk into memory, so instead we'll implement a form of *demand paging*, wherein we only allocate pages in the disk map region and read the corresponding block from the disk in response to a page fault in this region. This way, we can pretend that the entire disk is in memory.

## Exercise 2

> Implement the `bc_pgfault` and `flush_block` functions in *lab/fs/bc.c*.
> 
> `bc_pgfault` is a page fault handler, just like the one your wrote in the previous lab for *copy-on-write* `fork`, except that its job is to load pages in from the disk in response to a page fault. When writing this, keep in mind that:
> 
> - `addr` may not be aligned to a block boundary;
> - `ide_read` operates in sectors, not blocks.
> 
> The `flush_block` function should write a block out to disk if necessary. `flush_block` should not do anything if the block is not even in the block cache (that is, the page is not mapped) or if it is not dirty. We will use the VM hardware to keep track of whether a disk block has been modified since it was last read from or written to disk. To see whether a block needs writing, we can just look to see if the `PTE_D` *dirty* bit is set in the `uvpt` entry. (The `PTE_D` bit is set by the processor in response to a write to that page; see [5.2.4.3 in chapter 5 of the 386 reference manual](http://pdos.csail.mit.edu/6.828/2018/readings/i386/s05_02.htm).) After writing the block to disk, `flush_block` should clear the `PTE_D` bit using `sys_page_map`.
> 
> Use *make grade* to test your code. Your code should pass *check_bc*, *check_super*, and *check_bitmap*.

The first task is to complete the `bc_pgfault` function for I/O read. This task involves in rounding down `addr`, allocating page, and reading from disk. The functions `ROUNDDOWN`, `sys_page_alloc`, and `ide_read` are used. The implementation is as follows:

```diff
diff --git a/lab/fs/bc.c b/lab/fs/bc.c
index e3922c4..d000da5 100644
--- a/lab/fs/bc.c
+++ b/lab/fs/bc.c
@@ -48,6 +48,9 @@ bc_pgfault(struct UTrapframe *utf)
 	// the disk.
 	//
 	// LAB 5: you code here:
+	addr = ROUNDDOWN(addr, PGSIZE);
+	if ((r = sys_page_alloc(0, addr, PTE_W | PTE_U | PTE_P)) != 0) panic("ERROR: failed to allocate pages: %e", r);
+	if ((r = ide_read(blockno * BLKSECTS, addr, BLKSECTS))) panic("ERROR: failed to read IDE sector: %e", r); // read one block from disk (`BLKSECTS': sectors per block)
 
 	// Clear the dirty bit for the disk block page since we just read the
 	// block from disk
```

The second task is to implement the function `flush_block` for I/O write. During I/O write operation, the `PTE_D` *dirty* bit in the second-level page table is set by the processor before a write to an address covered by that page table entry:

![Format of A Page Table Entry](../_resources/c9de56c453044347ad8524369bc4ec34.png)

The dirty bit in directory entries is undefined. An operating system that supports paged virtual memory can use these bits to determine what pages to eliminate from physical memory when the demand for memory exceeds the physical memory available. The operating system is responsible for testing and clearing these bits.

This task involves in rounding down `addr`, checking the validation of `addr`, and resetting the `PTE_D` *dirty* bit. The comment on the `flush_block` function says clear:

```c
// Flush the contents of the block containing VA out to disk if
// necessary, then clear the PTE_D bit using sys_page_map.
// If the block is not in the block cache or is not dirty, does
// nothing.
// Hint: Use va_is_mapped, va_is_dirty, and ide_write.
// Hint: Use the PTE_SYSCALL constant when calling sys_page_map.
// Hint: Don't forget to round addr down.
```

The implementation is as follows:

```diff
diff --git a/lab/fs/bc.c b/lab/fs/bc.c
index e3922c4..d000da5 100644
--- a/lab/fs/bc.c
+++ b/lab/fs/bc.c
@@ -77,7 +80,12 @@ flush_block(void *addr)
 		panic("flush_block of bad va %08x", addr);
 
 	// LAB 5: Your code here.
-	panic("flush_block not implemented");
+	addr = ROUNDDOWN(addr, PGSIZE);
+	if (va_is_mapped(addr) && va_is_dirty(addr)) {
+		int r;
+		if ((r = ide_write(blockno * BLKSECTS, addr, BLKSECTS)) != 0) panic("ERROR: failed to flush to disk: %e", r);
+		if ((r = sys_page_map(0, addr, 0, addr, uvpt[PGNUM(addr)] & PTE_SYSCALL)) < 0) panic("ERROR: failed to clear the DIRTY bit: %e", r);
+	}
 }
 
 // Test that the block cache works, by smashing the superblock and
```

Verify the work in the directory *lab*:

```shell
$ cd mitos/lab && make grade
    ⋮
make[1]: warning:  Clock skew detected.  Your build may be incomplete.
OK (2.5s)
  fs i/o: OK
  check_bc: OK
  check_super: OK
  check_bitmap: OK
    ⋮
```

The code passes `check_bc`, `check_super`, and `check_bitmap` this time.

## The Block Bitmap

After `fs_init` sets the bitmap pointer, we can treat bitmap as a packed array of bits, one for each block on the disk. See, for example, `block_is_free`, which simply checks whether a given block is marked free in the bitmap.

## Exercise 3

> Use `free_block` as a model to implement `alloc_block` in *lab/fs/fs.c*, which should find a free disk block in the bitmap, mark it used, and return the number of that block. When you allocate a block, you should immediately flush the changed bitmap block to disk with `flush_block`, to help file system consistency.
> 
> Use `make grade` to test your code. Your code should now pass `alloc_block`.

`bitmap` is a `uint32_t` array (`32` bit integer) defined in *lab/fs/fs.h*. To trace a block's status, locate its position in the `bitmap` first: `blockno / 32`. And then locate its bit at this position: `blockno % 32`. This is how to mark a block to be fred: `bitmap[blockno / 32] |= 1 << (blockno%32)`.

One may scan the `bitmap` to detect a free block. The total size of the blocks is limited by `super->s_nblocks`, `block_is_free` can check whether a blockno is free, `flush_block` can flush the just allocated block to disk. My implementation is as follows:

```c
int alloc_block(void) {
    if (super == 0) return 0;
    uint32_t blockno;
    for (blockno = 0; blockno < super->s_nblocks; blockno++)
        if (block_is_free(blockno)) {
            bitmap[blockno / 32] &= ~(1 << (blockno % 32));
            flush_block(bitmap + blockno / 32);
            return blockno;
        }
    return -E_NO_DISK;
}
```

Verify the work in the directory *~/mitos/lab*:

```shell
$ cd ~/mitos/lab && make grade
    ⋮
OK (2.4s)
  fs i/o: OK
  check_bc: OK
  check_super: OK
  check_bitmap: OK
  alloc_block: OK
    ⋮
```

This time `alloc_block` passes.

## File Operations

We have provided a variety of functions in *lab/fs/fs.c* to implement the basic facilities you will need to interpret and manage `File` structures, scan and manage the entries of directory-files, and walk the file system from the root to resolve an absolute pathname. Read through all of the code in *lab/fs/fs.c* and make sure you understand what each function does before proceeding.

## Exercise 4

> Implement `file_block_walk` and `file_get_block`. `file_block_walk` maps from a block offset within a file to the pointer for that block in the `struct File` or the indirect block, very much like what `pgdir_walk` did for *page tables*. `file_get_block` goes one step further and maps to the actual disk block, allocating a new one if necessary.
> 
> Use `make grade` to test your code. Your code should pass `file_open`, `file_get_block`, and `file_flush/file_truncated/file rewrite`, and `testfile`.

The function `file_block_walk` is implemented as follows:

```c
// Find the disk block number slot for the 'filebno'th block in file 'f'.
// Set '*ppdiskbno' to point to that slot.
// The slot will be one of the f->f_direct[] entries,
// or an entry in the indirect block.
// When 'alloc' is set, this function will allocate an indirect block
// if necessary.
//
// Returns:
//	0 on success (but note that *ppdiskbno might equal 0).
//	-E_NOT_FOUND if the function needed to allocate an indirect block, but
//		alloc was 0.
//	-E_NO_DISK if there's no space on the disk for an indirect block.
//	-E_INVAL if filebno is out of range (it's >= NDIRECT + NINDIRECT).
//
// Analogy: This is like pgdir_walk for files.
// Hint: Don't forget to clear any block you allocate.
static int file_block_walk(struct File *f, uint32_t filebno, uint32_t **ppdiskbno, bool alloc) {
    if (filebno >= (NDIRECT + NINDIRECT)) return -E_INVAL;
    if (filebno < NDIRECT) *ppdiskbno = f->f_direct + filebno;
    else {
        if (!f->f_indirect) {
            if (!alloc) return -E_NOT_FOUND;
            else {
                uint32_t blockno = alloc_block();
                if (blockno < 0) return -E_NO_DISK;
                memset(diskaddr(blockno), 0, BLKSIZE);
                f->f_indirect = blockno;
            }
        }
        *ppdiskbno = (uint32_t *)diskaddr(f->f_indirect) + (filebno - NDIRECT);
    }
    return 0;
}
```

The function `file_get_block` is implemented as follows:

```c
// Set *blk to the address in memory where the filebno'th
// block of file 'f' would be mapped.
//
// Returns 0 on success, < 0 on error.  Errors are:
//	-E_NO_DISK if a block needed to be allocated but the disk is full.
//	-E_INVAL if filebno is out of range.
//
// Hint: Use file_block_walk and alloc_block.
int file_get_block(struct File *f, uint32_t filebno, char **blk) {
    uint32_t *pDiskBlockNo;
    int r;
    if ((r = file_block_walk(f, filebno, &pDiskBlockNo, 1)) != 0) return r;
    if (!*pDiskBlockNo) {
        uint32_t blockno = alloc_block();
        if (blockno < 0) return -E_NO_DISK;
        memset(diskaddr(blockno), 0, BLKSIZE);
        *pDiskBlockNo = blockno;
    }
    *blk = diskaddr(*pDiskBlockNo);
 	return 0;
}
```

N.B.: the block may not be allocated yet if it is at `f->f_direct` (see `file_block_walk`), so `alloc_block` is needed here.

Verify the work in the directory *~/mitos/lab*:

```shell
$ cd ~/mitos/lab && make grade
    ⋮
OK (3.1s)
  fs i/o: OK
  check_bc: OK
  check_super: OK
  check_bitmap: OK
  alloc_block: OK
  file_open: OK
  file_get_block: OK
  file_flush/file_truncate/file rewrite: OK
testfile: OK (2.4s)
    ⋮
```

This time, `file_open`, `file_get_block`, and `file_flush/file_truncated/file rewrite`, and `testfile` pass.

## The File System Interface

Since other environments can not directly call functions in the file system environment, we will expose access to the file system environment via a *remote procedure call* (*RPC*) abstraction, built atop JOS's *IPC* mechanism. Graphically, here's what a call to the file system server (say, read) looks like:

```
      Regular env           FS env
   +---------------+   +---------------+
   |      read     |   |   file_read   |
   |   (lib/fd.c)  |   |   (fs/fs.c)   |
...|.......|.......|...|.......^.......|...............
   |       v       |   |       |       | RPC mechanism
   |  devfile_read |   |  serve_read   |
   |  (lib/file.c) |   |  (fs/serv.c)  |
   |       |       |   |       ^       |
   |       v       |   |       |       |
   |     fsipc     |   |     serve     |
   |  (lib/file.c) |   |  (fs/serv.c)  |
   |       |       |   |       ^       |
   |       v       |   |       |       |
   |   ipc_send    |   |   ipc_recv    |
   |       |       |   |       ^       |
   +-------|-------+   +-------|-------+
           |                   |
           +-------------------+
```

Everything below the dotted line is simply the mechanics of getting a read request from the regular environment to the file system environment. Starting at the beginning, `read` works on any file descriptor and simply dispatches to the appropriate device `read` function, in this case `devfile_read` (we can have more device types, like pipes). `devfile_read` implements `read` specifically for on-disk files. This and the other `devfile_*` functions in *lib/file.c* implement the client side of the file system operations, and all work in roughly the same way, bundling up arguments in a request structure, calling `fsipc` to send the IPC request, and unpacking and returning the results. The `fsipc` function simply handles the common details of sending a request to the server and receiving the reply.

The file system server code can be found in *lab/fs/serv.c*. It loops in the `serve` function, endlessly receiving a request over IPC, dispatching that request to the appropriate handler function, and sending the result back via IPC. In the `read` example, `serve` will dispatch to `serve_read`, which will take care of the IPC details specific to `read` requests such as unpacking the `request` structure and finally call `file_read` to actually perform the file read.

JOS's IPC mechanism lets an environment send a single 32-bit number and, optionally, share a page. To send a request from the client to the server, we use the 32-bit number for the request type (the file system server RPCs are numbered, just like how syscalls were numbered) and store the arguments to the request in a `union Fsipc` on the page shared via the IPC. On the client side, we always share the page at `fsipcbuf`; on the server side, we map the incoming request page at `fsreq` (`0x0ffff000`).

The server also sends the response back via IPC. We use the 32-bit number for the function's return code. For most RPCs, this is all they return. `FSREQ_READ` and `FSREQ_STAT` also return data, which they simply write to the page that the client sent its request on. There's no need to send this page in the response IPC, since the client shared it with the file system server in the first place. Also, in its response, `FSREQ_OPEN` shares with the client a new `Fd` page. We'll return to the file descriptor page shortly.

## Exercise 5

> Implement `serve_read` in *lab/fs/serv.c*.
> 
> `serve_read`'s heavy lifting will be done by the already-implemented `file_read` in *lab/fs/fs.c* (which, in turn, is just a bunch of calls to `file_get_block`). `serve_read` just has to provide the RPC interface for file reading. Look at the comments and code in `serve_set_size` to get a general idea of how the server functions should be structured.
> 
> Use `make grade` to test your code. Your code should pass `serve_open/file_stat/file_close` and `file_read` for a score of `70/150`.

This assignment involves two structures. `struct OpenFile` is defined in the file *lab/fs/serv.c*:

```c
struct OpenFile {
    uint32_t o_fileid;	// file id
    struct File *o_file;	// mapped descriptor for open file
    int o_mode;		// open mode
    struct Fd *o_fd;	// Fd page
};
```

It has a `struct Fd` field that is defined in the file *lab/inc/fd.h*:

```c
struct Fd {
    int fd_dev_id;
    off_t fd_offset;
    int fd_omode;
    union {
        // File server files
        struct FdFile fd_file;
    };
};
```

The `serve_read` function implementation is as follows:

```c
// Read at most ipc->read.req_n bytes from the current seek position
// in ipc->read.req_fileid. Return the bytes read from the file to
// the caller in ipc->readRet, then update the seek position. Returns
// the number of bytes successfully read, or < 0 on error.
int serve_read(envid_t envid, union Fsipc *ipc) {
    struct Fsreq_read *req = &ipc->read;
    struct Fsret_read *ret = &ipc->readRet;
    if (debug) cprintf("serve_read %08x %08x %08x\n", envid, req->req_fileid, req->req_n);
    struct OpenFile *of;
    int r = openfile_lookup(envid, req->req_fileid, &of);
    if (r != 0) return r;
    r = file_read(of->o_file, ret->ret_buf, req->req_n, of->o_fd->fd_offset);
    if (r > 0) of->o_fd->fd_offset += r;
    return r;
}
```

Verify the work in the directory *mitos/lab*:

```shell
$ cd ~/mitos/lab && make grade
    ⋮
OK (2.2s)
  fs i/o: OK
  check_bc: OK
  check_super: OK
  check_bitmap: OK
  alloc_block: OK
  file_open: OK
  file_get_block: OK
  file_flush/file_truncate/file rewrite: OK
testfile: OK (2.8s)
  serve_open/file_stat/file_close: OK
  file_read: OK	
    ⋮
Score: 70/150
    ⋮
```

This time, `serve_open/file_stat/file_close` and `file_read` pass.

## Exercise 6

> Implement `serve_write` in *lab/fs/serv.c* and `devfile_write` in *lab/lib/file.c*.
> 
> Use `make grade` to test your code. Your code should pass `file_write`, `file_read after file_write`, `open`, and `large file` for a score of `90/150`.

The function `serve_write` involves another structure:

```c
struct Fsreq_write {
    int req_fileid;
    size_t req_n;
    char req_buf[PGSIZE - (sizeof(int) + sizeof(size_t))];
}
```

The `serve_write` implementation is pretty similar to `serve_read`:

```c
// Write req->req_n bytes from req->req_buf to req_fileid, starting at
// the current seek position, and update the seek position
// accordingly.  Extend the file if necessary.  Returns the number of
// bytes written, or < 0 on error.
int serve_write(envid_t envid, struct Fsreq_write *req) {
    if (debug) cprintf("serve_write %08x %08x %08x\n", envid, req->req_fileid, req->req_n);
    struct OpenFile *of;
    int r = openfile_lookup(envid, req->req_fileid, &of);
    if (r != 0) return r;
    r = file_write(of->o_file, req->req_buf, req->req_n, of->o_fd->fd_offset);
    if (r > 0) of->o_fd->fd_offset += r;
    return r;
}
```

`serve_write` is actually the handler for `FSREQ_WRITE`: `[FSREQ_WRITE] = (fshandler)serve_write` (see *lab/fs/serv.c*). Making an `FSREQ_WRITE` request to the file system server is actually required to implement the function `devfile_write`. The function `fsipc` is used to fire `FSREQ_WRITE` and is defined in *lab/lib/file.c*:

```c
// Send an inter-environment request to the file server, and wait for
// a reply.  The request body should be in fsipcbuf, and parts of the
// response may be written back to fsipcbuf.
// type: request code, passed as the simple integer IPC value.
// dstva: virtual address at which to receive reply page, 0 if none.
// Returns result from the file server.
static int fsipc(unsigned type, void *dstva)
```

Also, `fsipcbuf` in *lab/lib/file.c* is used to store the IPC request/response data, and understand it by checking `union Fsipc` in *lab/inc/fs.h*. N.B.: `struct Fsreq_write` is `write` part of `union Fsipc`.

The implementation for `devfile_write` is as follows:

```c
// Write at most 'n' bytes from 'buf' to 'fd' at the current seek position.
//
// Returns:
//	 The number of bytes successfully written.
//	 < 0 on error.
static ssize_t devfile_write(struct Fd *fd, const void *buf, size_t n) {
    // Make an FSREQ_WRITE request to the file system server.  Be
    // careful: fsipcbuf.write.req_buf is only so large, but
    // remember that write is always allowed to write *fewer*
    // bytes than requested.
    fsipcbuf.write.req_fileid = fd->fd_file.id;
    fsipcbuf.write.req_n = n;
    memmove(fsipcbuf.write.req_buf, buf, n);
    int r = fsipc(FSREQ_WRITE, NULL);
    if (r < 0) cprintf("ERROR: failed for FSREQ_WRITE");
    return r;
}
```

Verify the work in the directory *mitos/lab*:
```shell
$ cd ~/mitos/lab && make grade
    ⋮
OK (3.3s)
  fs i/o: OK
  check_bc: OK
  check_super: OK
  check_bitmap: OK
  alloc_block: OK
  file_open: OK
  file_get_block: OK
  file_flush/file_truncate/file rewrite: OK
testfile: OK (2.9s)
  serve_open/file_stat/file_close: OK
  file_read: OK
  file_write: OK
  file_read after file_write: OK
  open: OK
  large file: OK
    ⋮
Score: 90/150
    ⋮
```

This time, `file_write`, `file_read after file_write`, `open`, and `large file` pass.

## Spawning Processes

We have given you the code for 

We implemented spawn rather than a UNIX-style exec because spawn is easier to implement from user space in "exokernel fashion", without special help from the kernel. Think about what you would have to do in order to implement exec in user space, and be sure you understand why it is harder.

## Exercise 7

>`spawn` relies on the new syscall `sys_env_set_trapframe` to initialize the state of the newly created environment. Implement `sys_env_set_trapframe` in *lab/kern/syscall.c* (don't forget to dispatch the new system call in `syscall()`).
>
>Test your code by running the `user/spawnhello` program from *lab/kern/init.c*, which will attempt to `spawn` `/hello` from the file system.
>
>Use `make grade` to test your code. 

`spawn` is defined in *lab/lib/spawn.c*. `spawn` creates a new environment, loads an *executable program image* from the *file system* into it, and then starts the child environment running this program. The parent process then continues running independently of the child. The `spawn` function effectively acts like a `fork` in UNIX followed by an immediate `exec` in the child process.

The difference is that `fork` does clone an environment/process and that does not load an *executable program image* from the *file system* into the new environment to execute.

The implementation needs to validate the address with the function `user_mem_assert` which is defined in the file *lab/kern/pmap.c*.

The implementation is as follows:
```c
// Set envid's trap frame to 'tf'.
// tf is modified to make sure that user environments always run at code
// protection level 3 (CPL 3), interrupts enabled, and IOPL of 0.
//
// Returns 0 on success, < 0 on error.  Errors are:
//	-E_BAD_ENV if environment envid doesn't currently exist,
//		or the caller doesn't have permission to change envid.
static int sys_env_set_trapframe(envid_t envid, struct Trapframe *tf) {
	tf->tf_cs |= 3; // run at code protection level 3 (CPL 3)
	tf->tf_ss |= 3; // run at code protection level 3 (CPL 3)
	tf->tf_eflags |= FL_IF; // interrupts enabled
	tf->tf_eflags &= ~FL_IOPL_3; // IOPL of 0
	struct Env *env;
	int r = envid2env(envid, &env, 1);
	if (r != 0) return -E_BAD_ENV;
	user_mem_assert(env, tf, sizeof(struct Trapframe), PTE_W); // validate the address
	env->env_tf = *tf;
	return 0;
}
```

Also, add the system call to `ssycall`:
```diff
diff --git a/lab/kern/syscall.c b/lab/kern/syscall.c
index eab2159..555641d 100644
--- a/lab/kern/syscall.c
+++ b/lab/kern/syscall.c
@@ -356,6 +361,7 @@ int32_t syscall(uint32_t syscallno, uint32_t a1, uint32_t a2, uint32_t a3, uint3
        case SYS_env_set_pgfault_upcall: return sys_env_set_pgfault_upcall(a1, (void *)a2);
        case SYS_ipc_recv: return sys_ipc_recv((void *)a1);
        case SYS_ipc_try_send: return sys_ipc_try_send(a1, a2, (void *)a3, a4);
+       case SYS_env_set_trapframe: return sys_env_set_trapframe(a1, (struct Trapframe *)a2);
        default: return -E_INVAL;
        }
 }
```

Verify the work in the directory *~/mitos/lab*:
```shell
$ make clean && make run-spawnhello-nox
    ⋮
enabled interrupts: 1 2 4
FS is running
FS can do I/O
Device 1 presence: 1
i am parent environment 00001001
block cache is good
superblock is good
bitmap is good
alloc_block is good
file_open is good
file_get_block is good
file_flush is good
file_truncate is good
file rewrite is good
hello, world	
    ⋮	
```

Do `make grade` in the directory *~/mitos/lab*:
```shell
$ cd ~/mitos/lab && make grade
    ⋮
OK (4.7s)
  fs i/o: OK
  check_bc: OK
  check_super: OK
  check_bitmap: OK
  alloc_block: OK
  file_open: OK
  file_get_block: OK
  file_flush/file_truncate/file rewrite: OK
testfile: OK (5.4s)
  serve_open/file_stat/file_close: OK
  file_read: OK
  file_write: OK
  file_read after file_write: OK
  open: OK
  large file: OK
spawn via spawnhello: OK (3.2s)
Protection I/O space: OK (3.3s)
    ⋮
Score: 100/150
    ⋮
```

This time, `file_write`, `file_read after file_write`, `open`, and `large file` pass.

## Sharing Library State across `fork` and `spawn`

The UNIX file descriptors are a general notion that also encompasses pipes, console I/O, etc. In JOS, each of these device types has a corresponding `struct Dev`, with pointers to the functions that implement `read`, `write`, etc. for that device type. *lab/lib/fd.c* implements the general UNIX-like file descriptor interface on top of this. Each `struct Fd` indicates its device type, and most of the functions in *lab/lib/fd.c* simply dispatch operations to functions in the appropriate `struct Dev`.

`struct Fd` and `struct Dev` can be found in the file *lab/inc/fd.h*.

*lab/lib/fd.c* also maintains the file descriptor table region in each application environment's address space, starting at `FDTABLE`. This area reserves a page's worth (`4`KB) of address space for each of the up to `MAXFD` (currently 32) file descriptors the application can have open at once. At any given time, a particular file descriptor table page is mapped if and only if the corresponding file descriptor is in use. Each file descriptor also has an optional *data page* in the region starting at `FILEDATA`, which devices can use if they choose.

We would like to share file descriptor state across `fork` and `spawn`, but file descriptor state is kept in user-space memory. Right now, on `fork`, the memory will be marked *copy-on-write*, so the state will be duplicated rather than shared. (This means environments won't be able to seek in files they didn't open themselves and that pipes won't work across a `fork`.) On `spawn`, the memory will be left behind, not copied at all. (Effectively, the spawned environment starts with no open file descriptors.)

We will change `fork` to know that certain regions of memory are used by the *library operating system* and should always be shared. Rather than hard-code a list of regions somewhere, we will set an otherwise-unused bit in the page table entries (just like we did with the `PTE_COW` bit in fork).

We have defined a new `PTE_SHARE` bit in *lab/inc/lib.h*. This bit is one of the three `PTE` bits that are marked *available for software use* in the Intel and AMD manuals. We will establish the convention that if a page table entry has this bit set, the `PTE` should be copied directly from parent to child in both `fork` and `spawn`. Note that this is different from marking it copy-on-write: as described in the first paragraph, we want to make sure to share updates to the page.

## Exercise 8

>Change `duppage` in *lab/lib/fork.c* to follow the new convention. If the page table entry has the `PTE_SHARE` bit set, just copy the mapping directly. (You should use `PTE_SYSCALL`, not `0xfff`, to mask out the relevant bits from the page table entry. `0xfff` picks up the accessed and dirty bits as well.)
>
>Likewise, implement `copy_shared_pages` in *lab/lib/spawn.c*. It should loop through all page table entries in the current process (just like `fork` did), copying any page mappings that have the `PTE_SHARE` bit set into the child process.

The first task is relatively easy and just needs to set the `PTE_SYSCALL` bit if the `PTE_SHARE` flag is set. One may follow the previous code:
```diff
diff --git a/lab/lib/fork.c b/lab/lib/fork.c
index 80d4229..43e65d4 100644
--- a/lab/lib/fork.c
+++ b/lab/lib/fork.c
@@ -55,11 +55,13 @@ static void pgfault(struct UTrapframe *utf) {
 //
 static int duppage(envid_t envid, unsigned pn) {
        int r;
        envid_t envid_parent = sys_getenvid();
        void *va = (void *)(pn * PGSIZE);
-       if (((uvpt[pn] & PTE_W) == PTE_W)
-           || ((uvpt[pn] & PTE_COW) == PTE_COW)) { // the page is writable or copy-on-write
+       if ((uvpt[pn] & PTE_SHARE) == PTE_SHARE) { // copy the mapping directly
+               r = sys_page_map(envid_parent, va, envid, va, uvpt[pn] & PTE_SYSCALL);
+               if (r != 0) panic("ERROR: failed to copy the mapping directly");
+       } else  if (((uvpt[pn] & PTE_W) == PTE_W)
+                   || ((uvpt[pn] & PTE_COW) == PTE_COW)) { // the page is writable or copy-on-write
                r = sys_page_map(envid_parent, va, envid, va, PTE_COW | PTE_U | PTE_P);
                if (r != 0) panic("failed to map copy-on-write page for a child");
                r = sys_page_map(envid_parent, va, envid_parent, va, PTE_COW | PTE_U | PTE_P); // the parent page is marked copy-on-write
```

The second task requires to scan all pages up to `USTACKTOP`, and directly copy the address if the `PTE_SHARE` as well as `PTE_P` flag are set:
```c
// Copy the mappings for shared pages into the child address space.
static int copy_shared_pages(envid_t child) {
	int r;
	envid_t envid_parent = sys_getenvid();
	uint32_t addr;
	for (addr = 0; addr < USTACKTOP; addr += PGSIZE) {
		if (((uvpd[PDX(addr)] & PTE_P) == PTE_P) &&
		    ((uvpt[PGNUM(addr)] & PTE_P) == PTE_P) &&
		    ((uvpt[PGNUM(addr)] & PTE_SHARE) == PTE_SHARE)) {
			r = sys_page_map(envid_parent, (void *)addr, child, (void *)addr, uvpt[PGNUM(addr)] & PTE_SYSCALL);
			if (r != 0) panic("ERROR: failed to copy shared pages");
		}
	}
	return 0;
}
```

Now veryfy the work as the assignment mentiones:
```shell
$ make clean && make run-testpteshare-nox
    ⋮
fork handles PTE_SHARE right
    ⋮
spawn handles PTE_SHARE right
    ⋮
```
And run `testfdsharing`:
```shell
$ make clean && make run-testfdsharing-nox
    ⋮
file rewrite is good
going to read in child (might page fault if your sharing is buggy)
read in child succeeded
read in parent succeeded
Welcome to the JOS kernel monitor!
    ⋮
```

Do `make grade` in the directory *~/mitos/lab*:
```shell
$ cd ~/mitos/lab && make grade
    ⋮
PTE_SHARE [testpteshare]: OK (2.0s)
PTE_SHARE [testfdsharing]: OK (2.2s)
    ⋮
```

## The Keyboard Interface

For the shell to work, we need a way to type at it. QEMU has been displaying output we write to the CGA display and the serial port, but so far we've only taken input while in the kernel monitor. In QEMU, input typed in the graphical window appear as input from the keyboard to JOS, while input typed to the console appear as characters on the serial port. *lab/kern/console.c* already contains the keyboard and serial drivers that have been used by the kernel monitor since lab 1, but now you need to attach these to the rest of the system.

## Exercise 9

>In your *lab/kern/trap.c*, call `kbd_intr` to handle trap `IRQ_OFFSET + IRQ_KBD` and `serial_intr` to handle trap `IRQ_OFFSET + IRQ_SERIAL`.
>
>We implemented the console `input`/`output` file type for you, in *lab/lib/console.c*. `kbd_intr` and `serial_intr` fill a buffer with the recently read input while the `console` file type drains the buffer (the `console` file type is used for `stdin`/`stdout` by default unless the user redirects them).
>
>Test your code by running `make run-testkbd` and type a few lines. The system should echo your lines back to you as you finish them. Try typing in both the console and the graphical window, if you have both available.

`kbd_intr` and `serial_intr` are defined in the file *lab/kern/console.c*.

This assignment is to hook `kbd_intr` and `serial_intr` in the function `trap_dispatch`:
```diff
diff --git a/lab/kern/trap.c b/lab/kern/trap.c
index 9dc9ac1..7b71fce 100644
--- a/lab/kern/trap.c
+++ b/lab/kern/trap.c
@@ -246,6 +246,12 @@ static void trap_dispatch(struct Trapframe *tf) {
       	case IRQ_OFFSET + IRQ_SPURIOUS:
       		// Handle spurious interrupts
       		// The hardware sometimes raises these because of noise on the
       		// IRQ line or other reasons. We don't care.
       		cprintf("Spurious interrupt on irq 7\n");
       		print_trapframe(tf);
       		return;

        // Handle keyboard and serial interrupts.
        // LAB 5: Your code here.
+       case IRQ_OFFSET + IRQ_KBD:
+               kbd_intr();
+               return;
+       case IRQ_OFFSET + IRQ_SERIAL:
+               serial_intr();
+               return;

        default:
                // Unexpected trap: The user process or the kernel has a bug.
```

Verify the work in the directory *~/mitos/lab* and type some characters:
```shell
$ make clean && make run-testkbd-nox
    ⋮
Type a line: hello jos :-)
hello jos :-)
    ⋮
```

My virtual machine does not serve graphical windows, so I only test the console functionality.

## The Shell

Run `make run-icode` or `make run-icode-nox`. This will run your kernel and start `user/icode`. `icode` execs `init`, which will set up the *console* as file descriptors `0` and `1` (standard input and standard output). It will then spawn `sh`, the shell. You should be able to run the following commands:
>echo hello world | cat
>cat lorem |cat
>cat lorem |num
>cat lorem |num |num |num |num |num
>lsfd
Note that the user library routine `cprintf` prints straight to the console, without using the file descriptor code. This is great for debugging but not great for piping into other programs. To print output to a particular file descriptor (for example, `1`, standard output), use `fprintf(1, "...", ...)`. `printf("...", ...)` is a short-cut for printing to the file descriptor `1`. See *lab/user/lsfd.c* for examples.

My test is as follows:
```shell
$ cd ~/mitos/lab && make clean && make run-icode-nox
    ⋮
Welcome to the JOS kernel, now with a file system!

icode: close /motd
icode: spawn /init
icode: exiting
init: running
init: data seems okay
init: bss seems okay
init: args: 'init' 'initarg1' 'initarg2'
init: running sh
init: starting sh
$ echo hello world | cat
hello world
$ cat lorem | cat
Lorem ipsum dolor sit amet, consectetur
adipisicing elit, sed do eiusmod tempor
incididunt ut labore et dolore magna
aliqua. Ut enim ad minim veniam, quis
nostrud exercitation ullamco laboris
nisi ut aliquip ex ea commodo consequat.
Duis aute irure dolor in reprehenderit
in voluptate velit esse cillum dolore eu
fugiat nulla pariatur. Excepteur sint
occaecat cupidatat non proident, sunt in
culpa qui officia deserunt mollit anim
id est laborum.
$ cat lorem | num
    1 Lorem ipsum dolor sit amet, consectetur
    2 adipisicing elit, sed do eiusmod tempor
    3 incididunt ut labore et dolore magna
    4 aliqua. Ut enim ad minim veniam, quis
    5 nostrud exercitation ullamco laboris
    6 nisi ut aliquip ex ea commodo consequat.
    7 Duis aute irure dolor in reprehenderit
    8 in voluptate velit esse cillum dolore eu
    9 fugiat nulla pariatur. Excepteur sint
   10 occaecat cupidatat non proident, sunt in
   11 culpa qui officia deserunt mollit anim
   12 id est laborum.
$ cat lorem | num | num | num | num | num
    1     1     1     1     1 Lorem ipsum dolor sit amet, consectetur
    2     2     2     2     2 adipisicing elit, sed do eiusmod tempor
    3     3     3     3     3 incididunt ut labore et dolore magna
    4     4     4     4     4 aliqua. Ut enim ad minim veniam, quis
    5     5     5     5     5 nostrud exercitation ullamco laboris
    6     6     6     6     6 nisi ut aliquip ex ea commodo consequat.
    7     7     7     7     7 Duis aute irure dolor in reprehenderit
    8     8     8     8     8 in voluptate velit esse cillum dolore eu
    9     9     9     9     9 fugiat nulla pariatur. Excepteur sint
   10    10    10    10    10 occaecat cupidatat non proident, sunt in
   11    11    11    11    11 culpa qui officia deserunt mollit anim
   12    12    12    12    12 id est laborum.
$ lsfd
fd 0: name <cons> isdir 0 size 0 dev cons
fd 1: name <cons> isdir 0 size 0 dev cons
$
```

## Exercise 10

>The shell does not support I/O redirection. It would be nice to run `sh <script` instead of having to type in all the commands in the script by hand, as you did above. Add I/O redirection for `<` to `user/sh.c`.
>
>Test your implementation by typing `sh <script` into your shell.
>
>Run `make run-testshell` to test your shell. `testshell` simply feeds the above commands (also found in *lab/fs/testshell.sh*) into the shell and then checks that the output matches *lab/fs/testshell.key*.

This assignment requires to complete the `runcmd` function in *lab/user/sh.c*, the *input redirection* case. The source comment says clear how to implement the general purpose input redirection:
- check whether the file descriptor is `0`, the default standard input;
- if the file descriptor is not `0`, duplicate it onto the file descriptor `0`, and then close the original one.

The implementation is as follows:
```diff
diff --git a/lab/user/sh.c b/lab/user/sh.c
index 26f501a..70d63c0 100644
--- a/lab/user/sh.c
+++ b/lab/user/sh.c
@@ -55,7 +55,12 @@ again:
                        // then close the original 'fd'.

                        // LAB 5: Your code here.
-                       panic("< redirection not implemented");
+                       fd = open(t, O_RDONLY);
+                       if (fd < 0) panic("ERROR: failed to open the file.");
+                       if (fd != 0) {
+                               dup(fd, 0);
+                               close(fd);
+                       }
                        break;

                case '>':       // Output redirection
```

Verify the work by running `testshell`:
```shell
$ cd ~/mitos/lab && make clean && make run-testshell-nox
    ⋮
running sh -x < testshell.sh | cat
shell ran correctly
    ⋮
```

Now `make grade` can pass all test cases successfully:
```shell
$ cd ~/mitos/lab && make grade
    ⋮
internal FS tests [fs/test.c]: OK (2.7s)
  fs i/o: OK
  check_bc: OK
  check_super: OK
  check_bitmap: OK
  alloc_block: OK
  file_open: OK
  file_get_block: OK
  file_flush/file_truncate/file rewrite: OK
testfile: OK (1.5s)
  serve_open/file_stat/file_close: OK
  file_read: OK
  file_write: OK
  file_read after file_write: OK
  open: OK
  large file: OK
spawn via spawnhello: OK (2.1s)
Protection I/O space: OK (2.2s)
PTE_SHARE [testpteshare]: OK (2.3s)
PTE_SHARE [testfdsharing]: OK (2.2s)
start the shell [icode]:
Timeout! OK (31.5s)
testshell: OK (12.8s)
primespipe: OK (6.3s)
Score: 150/150
```

## Challenge

>Add more features to the shell. Possibilities include (a few require changes to the file system too):
>- backgrounding commands (ls &)
>- multiple commands per line (ls; echo hi)
>- command grouping ((ls; echo hi) | cat > out)
>- environment variable expansion (echo $hello)
>- quoting (echo "a | b")
>- command-line history and/or editing
>- tab completion
>- directories, cd, and a PATH for command-lookup.
>- file creation
>- ctl-c to kill the running environment 
>but feel free to do something not on this list.

Most shell features are implemented in the file *lab/user/sh.c*. I implement two new features: *backgrounding commands* and *multiple commands on one line*.

The *backgrounding commands* feature can be implemented with `fork`:
```diff
diff --git a/lab/user/sh.c b/lab/user/sh.c
index 70d63c0..c5331b7 100644
--- a/lab/user/sh.c
+++ b/lab/user/sh.c
@@ -106,9 +107,17 @@ again:
 				close(p[0]);
 				goto runit;
 			}
 			break;
 
+		case '&': // backgrounding commands
+			r = fork();
+			if (r == 0) goto runit;
+			else exit();
+
 		case 0:		// String is complete
 			// Run the current command!
 			goto runit;
```

Verify the work:
```shell
$ cd ~/mitos/lab && make clean && make qemu-nox
    ⋮
$ ls &
$ newmotd
motd
lorem
script
testshell.key
testshell.sh
init
cat
echo
init
ls
lsfd
num
forktree
primes
primespipe
sh
testfdsharing
testkbd
testpipe
testpteshare
testshell
hello
faultio

```

For *multiple commands on one line*, we need to control all file descriptors for each command. I define a flag `isMulti` to indicate whether the input is for multiple commands. The implementation is as follows:
```diff
diff --git a/lab/user/sh.c b/lab/user/sh.c
index 2f7c399..c5331b7 100644
--- a/lab/user/sh.c
+++ b/lab/user/sh.c
@@ -3,6 +3,7 @@
 #define BUFSIZ 1024		/* Find the buffer overrun bug! */
 int debug = 0;
 
+int isMulti = 0; /* indicator of multiple commands on one line */
 
 // gettoken(s, 0) prepares gettoken for subsequent calls and returns 0.
 // gettoken(0, token) parses a shell token from the previously set string,
@@ -112,6 +113,11 @@ again:
 			r = fork();
 			if (r == 0) goto runit;
 			else exit();
+
+		case ';': // multiple commands on one line
+			isMulti = 1;
+			goto runit;
+
 		case 0:		// String is complete
 			// Run the current command!
 			goto runit;
@@ -156,7 +162,7 @@ runit:
 
 	// In the parent, close all file descriptors and wait for the
 	// spawned command to exit.
-	close_all();
+	if (!isMulti) close_all();
 	if (r >= 0) {
 		if (debug)
 			cprintf("[%08x] WAIT %s %08x\n", thisenv->env_id, argv[0], r);
@@ -175,6 +181,11 @@ runit:
 			cprintf("[%08x] wait finished\n", thisenv->env_id);
 	}
 
+	if (isMulti) {
+		isMulti = 0;
+		goto again;
+	}
+
 	// Done!
 	exit();
 }
```

Verify the work:
```shell
$ cd ~/mitos/lab && make clean && make qemu-nox
    ⋮
$ ls; echo --------------------------; echo hello, world
newmotd
motd
lorem
script
testshell.key
testshell.sh
init
cat
echo
init
ls
lsfd
num
forktree
primes
primespipe
sh
testfdsharing
testkbd
testpipe
testpteshare
testshell
hello
faultio
--------------------------
hello, world
$
```








✌️
