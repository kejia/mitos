
[Lab 6](http://pdos.csail.mit.edu/6.828/2018/labs/lab6/)

# Introduction

In this lab, we will add the network driver to JOS.

After the Lab 6 branch is pulled and is merged, the Lab 5 grader fails. There is no clue in the lab description about whether the Lab 5 grader should work or not.

## QEMU's Virtual Network

QEMU's user mode network stack requires no administrative privileges to run. QEMU provides a virtual network card *E1000*, and JOS implements related configuration in the files *lab/hw/net/e1000.c* and *lab/net/ns.h*.

JOS serves *echo* on port `7` and *http* on port `80`.

All incoming and outgoing packets are recorded in the file *lab/qemu.pcap*, and one can read as follows:

```shell
$ tcpdump -XXnr qemu.pcap
```

## The Network Server

The network stack is implemented with *lwIP* as a BSD socket interface. The network server is actually a combination of four environments:

- core network server environment (includes socket call dispatcher and lwIP)
- input environment
- output environment
- timer environment

The following diagram shows the different environments and their relationships. In this lab, you will implement the parts highlighted in green.

![2925ec570f03697e6853cc2ebfc51f93.png](../_resources/fa07f599d79e4236a95e5e618b7a60ba.png)

### The Core Network Server Environment

The core network server environment is composed of the *socket call dispatcher* and *lwIP* itself. The socket call dispatcher works exactly like the file server. User environments use stubs (found in *lib/nsipc.c*) to send IPC messages to the core network environment. If you look at *lib/nsipc.c* you will see that we find the core network server the same way we found the file server: `i386_init` created the `NS` environment with `NS_TYPE_NS`, so we scan `envs`, looking for this special environment type. For each user environment IPC, the dispatcher in the network server calls the appropriate BSD socket interface function provided by *lwIP* on behalf of the user.

Regular user environments do not use the `nsipc_*` calls directly. Instead, they use the functions in *lib/sockets.c*, which provides a file descriptor-based sockets API. Thus, user environments refer to sockets via file descriptors, just like how they referred to on-disk files. A number of operations (`connect`, `accept`, etc.) are specific to sockets, but `read`, `write`, and close go through the normal file descriptor device-dispatch code in *lib/fd.c*. Much like how the file server maintained internal unique ID's for all open files, *lwIP* also generates unique ID's for all open sockets. In both the file server and the network server, we use information stored in struct `Fd` to map per-environment file descriptors to these unique ID spaces.

Even though it may seem that the IPC dispatchers of the file server and network server act the same, there is a key difference. BSD socket calls like `accept` and `recv` can block indefinitely. If the dispatcher were to let *lwIP* execute one of these blocking calls, the dispatcher would also block and there could only be one outstanding network call at a time for the whole system. Since this is unacceptable, the network server uses *user-level threading* to avoid blocking the entire server environment. For every incoming IPC message, the dispatcher creates a thread and processes the request in the newly created thread. If the thread blocks, then only that thread is put to sleep while other threads continue to run.

In addition to the core network environment there are three helper environments. Besides accepting messages from *user applications* (the `output` environment), the core network environment's dispatcher also accepts messages from the `input` and `timer` environments.

### The Output Environment

When servicing user environment socket calls, *lwIP* will generate packets for the network card to transmit. *LwIP* will send each packet to be transmitted to the `output` helper environment using the `NSREQ_OUTPUT` IPC message with the packet attached in the page argument of the IPC message. The `output` environment is responsible for accepting these messages and forwarding the packet on to the device driver via the system call interface that you will soon create.

### The Input Environment

Packets received by the network card need to be injected into *lwIP*. For every packet received by the device driver, the `input` environment pulls the packet out of kernel space (using kernel system calls that you will implement) and sends the packet to the core server environment using the `NSREQ_INPUT` IPC message.

The packet input functionality is separated from the core network environment because JOS makes it hard to simultaneously accept IPC messages and poll or wait for a packet from the device driver. We do not have a select system call in JOS that would allow environments to monitor multiple input sources to identify which input is ready to be processed.

If one takes a look at *net/input.c* and *net/output.c*, one will see that both need to be implemented. This is mainly because the implementation depends on your system call interface. You will write the code for the two helper environments after you implement the driver and system call interface.

### The Timer Environment

The `timer` environment periodically sends messages of type `NSREQ_TIMER` to the core network server notifying it that a timer has expired. The timer messages from this thread are used by lwIP to implement various network timeouts.

# Part A: Initialization and Transmitting Packets

## Exercise 1

> Add a call to `time_tick` for every clock interrupt in *kern/trap.c*. Implement `sys_time_msec` and add it to syscall in *kern/syscall.c* so that user space has access to the time.

The kernel does not have a notion of time, so we need to add it. There is currently a clock interrupt that is generated by the hardware every `10`ms. On every clock interrupt we can increment a variable to indicate that time has advanced by `10`ms. This is implemented in *lab/kern/time.c*, and this exercise requires to hook it.

The implementation is as follows:

```diff
diff --git a/lab/kern/syscall.c b/lab/kern/syscall.c
index 7a62691..e5aac24 100644
--- a/lab/kern/syscall.c
+++ b/lab/kern/syscall.c
@@ -349,7 +349,7 @@ static int
 sys_time_msec(void)
 {
 	// LAB 6: Your code here.
-	panic("sys_time_msec not implemented");
+	return time_msec();
 }
 
 // Dispatches to the correct kernel function, passing the arguments.
@@ -371,6 +371,7 @@ int32_t syscall(uint32_t syscallno, uint32_t a1, uint32_t a2, uint32_t a3, uint3
 	case SYS_ipc_recv: return sys_ipc_recv((void *)a1);
 	case SYS_ipc_try_send: return sys_ipc_try_send(a1, a2, (void *)a3, a4);
 	case SYS_env_set_trapframe: return sys_env_set_trapframe(a1, (struct Trapframe *)a2);
+	case SYS_time_msec: return sys_time_msec();
 	default: return -E_INVAL;
 	}
 }
diff --git a/lab/kern/trap.c b/lab/kern/trap.c
index 0634aaf..b1b737f 100644
--- a/lab/kern/trap.c
+++ b/lab/kern/trap.c
@@ -235,6 +235,13 @@ static void trap_dispatch(struct Trapframe *tf) {
 		// interrupt using lapic_eoi() before calling the scheduler!
 		// LAB 4: Your code here.
 		lapic_eoi();
+
+		// Add time tick increment to clock interrupts.
+		// Be careful! In multiprocessors, clock interrupts are
+		// triggered on every CPU.
+		// LAB 6: Your code here.
+		if (thiscpu->cpu_id == 0) time_tick();
+
 		sched_yield();
 		return;
 	case IRQ_OFFSET + IRQ_SPURIOUS:
```

Verify the work:

```shell
$ cd ~/mitos/lab && make clean && make INIT_CFLAGS=-DTEST_NO_NS run-testtime-nox
    ⋮
starting count down: 5 4 3 2 1 0
    ⋮
```

## Exercise 2

> Browse [Intel's Software Developer's Manual](http://pdos.csail.mit.edu/6.828/2018/readings/hardware/8254x_GBe_SDM.pdf) for the *E1000*. This manual covers several closely related Ethernet controllers. QEMU emulates the *82540EM*.
> 
> You should skim over chapter 2 now to get a feel for the device. To write your driver, you will need to be familiar with chapters [3](http://pdos.csail.mit.edu/6.828/2018/readings/hardware/8254x_GBe_SDM.pdf#G5.1327691) and [14](http://pdos.csail.mit.edu/6.828/2018/readings/hardware/8254x_GBe_SDM.pdf#G16.37111), as well as [4.1](http://pdos.csail.mit.edu/6.828/2018/readings/hardware/8254x_GBe_SDM.pdf#G6.1014343) (though not 4.1's subsections). You will also need to use chapter [13](http://pdos.csail.mit.edu/6.828/2018/readings/hardware/8254x_GBe_SDM.pdf#G15.1491295) as reference. The other chapters mostly cover components of the *E1000* that your driver will not have to interact with. Do not worry about the details right now; just get a feel for how the document is structured so you can find things later.
> 
> While reading the manual, keep in mind that the *E1000* is a sophisticated device with many advanced features. A working *E1000* driver only needs a fraction of the features and interfaces that the NIC provides. Think carefully about the easiest way to interface with the card. We strongly recommend that you get a basic driver working before taking advantage of the advanced features.

*E1000* is a PCI device which plugs into the PCI bus on the motherboard. The PCI bus has address, data, and interrupt lines, and allows the CPU to communicate with PCI devices and PCI devices to read and write memory. A PCI device needs to be discovered and initialized before it can be used. Discovery is the process of walking the PCI bus looking for attached devices. Initialization is the process of allocating I/O and memory space as well as negotiating the IRQ line for the device to use.

The PCI code is mainly in *lab/kern/pci.c*. When it finds a device, it reads its vendor ID and device ID and uses these two values as a key to search the `pci_attach_vendor` array. The array is composed of `struct pci_driver` entries like this:

```c
struct pci_driver {
    uint32_t key1, key2;
    int (*attachfn) (struct pci_func *pcif);
};
```

If the discovered device's vendor ID and device ID match an entry in the array, the PCI code calls that entry's `attachfn` to perform device initialization. Devices can also be identified by class, which is what the other driver table in *lab/kern/pci.c* is for. The attach function is passed a PCI function to initialize. A PCI card can expose multiple functions, though the *E1000* exposes only one. Here is how we represent a PCI function in JOS:

```c
struct pci_func {
    struct pci_bus *bus;

    uint32_t dev;
    uint32_t func;

    uint32_t dev_id;
    uint32_t dev_class;

    uint32_t reg_base[6];
    uint32_t reg_size[6];
    uint8_t irq_line;
};
```

The above structure reflects some of the entries found in Table 4-1 of [Section 4.1](http://pdos.csail.mit.edu/6.828/2018/readings/hardware/8254x_GBe_SDM.pdf#G6.1014343) of the developer manual. The last three entries of `struct pci_func` are of particular interest to us, as they record the negotiated memory, I/O, and interrupt resources for the device. The `reg_base` and `reg_size` arrays contain information for up to six *Base Address Registers* or *BARs*. `reg_base` stores the base memory addresses for memory-mapped I/O regions (or base I/O ports for I/O port resources), `reg_size` contains the size in bytes or number of I/O ports for the corresponding base values from `reg_base`, and `irq_line` contains the IRQ line assigned to the device for interrupts. The specific meanings of the *E1000* *BARs* are given in the second half of table 4-2.

When the attach function of a device is called, the device has been found but not yet enabled. This means that the PCI code has not yet determined the resources allocated to the device, such as address space and an IRQ line, and, thus, the last three elements of the `struct pci_func` structure are not yet filled in. The attach function should call `pci_func_enable`, which will enable the device, negotiate these resources, and fill in the `struct pci_func`.

## Exercise 3

> Implement an attach function to initialize the *E1000*. Add an entry to the `pci_attach_vendor` array in *lab/kern/pci.c* to trigger your function if a matching PCI device is found (be sure to put it before the `{0, 0, 0}` entry that mark the end of the table). You can find the vendor ID and device ID of the *82540EM* that QEMU emulates in [section 5.2](http://pdos.csail.mit.edu/6.828/2018/readings/hardware/8254x_GBe_SDM.pdf#G7.1041348). You should also see these listed when JOS scans the PCI bus while booting.
> 
> For now, just enable the *E1000* device via `pci_func_enable`. We will add more initialization throughout the lab.
> 
> We have provided the *lab/kern/e1000.c* and *lab/kern/e1000.h* files for you so that you do not need to mess with the build system. They are currently blank; you need to fill them in for this exercise. You may also need to include the *e1000.h* file in other places in the kernel.
> 
> When you boot your kernel, you should see it print that the PCI function of the *E1000* card was enabled. Your code should now pass the pci attach test of make grade.

The implementation is as follows:

```diff
diff --git a/lab/kern/e1000.h b/lab/kern/e1000.h
index 8b5a513..4949b42 100644
--- a/lab/kern/e1000.h
+++ b/lab/kern/e1000.h
@@ -1,3 +1,9 @@
+#include <kern/pci.h>
+
 #ifndef JOS_KERN_E1000_H
 #define JOS_KERN_E1000_H
+#define E1000_VENDOR_ID            0x8086
+#define E1000_DEVICE_ID_82540EM    0x100E
 #endif  // SOL >= 6
+
+int e1000_attach(struct pci_func *pciFunc);
diff --git a/lab/kern/pci.c b/lab/kern/pci.c
index 784e072..028cc27 100644
--- a/lab/kern/pci.c
+++ b/lab/kern/pci.c
@@ -31,6 +31,7 @@ struct pci_driver pci_attach_class[] = {
 // pci_attach_vendor matches the vendor ID and device ID of a PCI device. key1
 // and key2 should be the vendor ID and device ID respectively
 struct pci_driver pci_attach_vendor[] = {
+	{E1000_VENDOR_ID, E1000_DEVICE_ID_82540EM, &e1000_attach},
 	{ 0, 0, 0 },
 };
diff --git a/lab/kern/e1000.c b/lab/kern/e1000.c
index 192f317..9d9b571 100644
--- a/lab/kern/e1000.c
+++ b/lab/kern/e1000.c
@@ -1,4 +1,9 @@
 #include <kern/e1000.h>
 #include <kern/pmap.h>
+#include <kern/pci.h>
 
 // LAB 6: Your driver code here
+int e1000_attach(struct pci_func *pciFunc) {
+	pci_func_enable(pciFunc);
+	return 0;
+}
```

Verify the work:

```shell
$ cd ~/mitos/lab && make grade
    ⋮
testtime: OK (9.0s)
pci attach: OK (2.7s)
    ⋮
Part A score: 10/35
```

## Memory-mapped I/O

Software communicates with the *E1000* via memory-mapped I/O (MMIO). In JOS both the CGA console and the LAPIC are devices that you control and query by writing to and reading from "memory", but these reads and writes do not go to DRAM; they go directly to these devices.

`pci_func_enable` negotiates an MMIO region with the *E1000* and stores its base and size in BAR 0 (that is, `reg_base[0]` and `reg_size[0]`). This is a range of physical memory addresses assigned to the device, which means you will have to do something to access it via virtual addresses. Since MMIO regions are assigned very high physical addresses (typically above `3`GB), you can not use `KADDR` to access it because of JOS's `256`MB limit. Thus, you will have to create a new memory mapping. We will use the area above `MMIOBASE` (your `mmio_map_region` from lab 4 will make sure we do not overwrite the mapping used by the LAPIC). Since PCI device initialization happens before JOS creates user environments, you can create the mapping in `kern_pgdir`, and it will always be available.

## Exercise 4

> In your attach function, create a virtual memory mapping for the *E1000*'s BAR 0 by calling `mmio_map_region` (which you wrote in lab 4 to support memory-mapping the LAPIC).
> 
> You will want to record the location of this mapping in a variable so you can later access the registers you just mapped. Take a look at the lapic variable in *lab/kern/lapic.c* for an example of one way to do this. If you do use a pointer to the device register mapping, be sure to declare it `volatile`; otherwise, the compiler is allowed to cache values and reorder accesses to this memory.
> 
> To test your mapping, try printing out the device status register ([section 13.4.2](http://pdos.csail.mit.edu/6.828/2018/readings/hardware/8254x_GBe_SDM.pdf)). This is a `4` byte register that starts at byte `8` of the register space. You should get `0x80080783`, which indicates a full duplex link is up at `1000` MB/s, among other things.

The attach function should call `pci_func_enable`, which will enable the device, will negotiate resources, and will fill in the `struct pci_func` (see [section Exercise 2](#exercise_2)). This assignment requires to initialize the MMIO for the area recorded by the fields of the `struct pci_func`: `reg_base` and `reg_size`:

```c
struct pci_func {
    struct pci_bus *bus;

    uint32_t dev;
    uint32_t func;

    uint32_t dev_id;
    uint32_t dev_class;

    uint32_t reg_base[6];
    uint32_t reg_size[6];
    uint8_t irq_line;
};
```

To test the device status register, we need to dump out `4` bytes at the `8` byte offset from the start of the MMIO of `E1000`.

The implementation is as follows:

```diff
diff --git a/lab/kern/e1000.h b/lab/kern/e1000.h
index 4949b42..3b46522 100644
--- a/lab/kern/e1000.h
+++ b/lab/kern/e1000.h
@@ -6,4 +6,6 @@
 #define E1000_DEVICE_ID_82540EM    0x100E
 #endif  // SOL >= 6
 
+volatile uintptr_t e1000_mmio_base;
+
 int e1000_attach(struct pci_func *pciFunc);
diff --git a/lab/kern/e1000.c b/lab/kern/e1000.c
index 9d9b571..127aff2 100644
--- a/lab/kern/e1000.c
+++ b/lab/kern/e1000.c
@@ -5,5 +5,7 @@
 // LAB 6: Your driver code here
 int e1000_attach(struct pci_func *pciFunc) {
 	pci_func_enable(pciFunc);
+	e1000_mmio_base = (uintptr_t) mmio_map_region(pciFunc->reg_base[0], pciFunc->reg_size[0]);
+	cprintf("e1000 status register: 0x%08x\n", (*(uint32_t *)(e1000_mmio_base + 8))); // test mmio: the dump value should be 0x80080783
 	return 0;
 }
```

Verify the work:

```shell
$ cd ~/mitos/lab && make clean && make qemu-nox
    ⋮
e1000 status register: 0x80080783
    ⋮
```

## DMA

*E1000* uses *Direct Memory Access* (*DMA*) to read and write data without involving CPU. The driver is responsible for allocating memory for the transmit and receive queues, for setting up DMA descriptors, and for configuring the *E1000* with the location of these queues, but everything after that is asynchronous. To transmit a packet, the driver copies it into the next DMA descriptor in the transmit queue and informs the *E1000* that another packet is available; the *E1000* will copy the data out of the descriptor when there is time to send the packet. Likewise, when the *E1000* receives a packet, it copies it into the next DMA descriptor in the receive queue, which the driver can read from at its next opportunity.

The receive and transmit queues are very similar at a high level. Both consist of a sequence of *descriptors*. The queues are implemented as *circular arrays*, meaning that when the card or the driver reach the end of the array, it wraps back around to the beginning. Both have a head pointer and a tail pointer, and the contents of the queue are the descriptors between these two pointers. The hardware always consumes descriptors from the head and moves the head pointer, while the driver always add descriptors to the tail and moves the tail pointer. The descriptors in the transmit queue represent packets waiting to be sent (hence, in the steady state, the transmit queue is empty). For the receive queue, the descriptors in the queue are free descriptors that the card can receive packets into (hence, in the steady state, the receive queue is full of all available receive descriptors).

The pointers to these arrays as well as the addresses of the packet buffers in the descriptors must all be *physical addresses* because hardware performs DMA directly to and from physical RAM without going through the MMU.

## Transmitting Packets

The transmit and receive functions of the *E1000* are basically independent of each other, so we can work on one at a time. Here, we start with transmit.

First, one will have to initialize the card to transmit, following the steps described in [section 14.5](http://pdos.csail.mit.edu/6.828/2018/readings/hardware/8254x_GBe_SDM.pdf#G16.37215) (one does not have to worry about the subsections). The first step of transmit initialization is setting up the transmit queue. The precise structure of the queue is described in [section 3.4](http://pdos.csail.mit.edu/6.828/2018/readings/hardware/8254x_GBe_SDM.pdf#G5.1029304) and the structure of the descriptors is described in [section 3.3.3](http://pdos.csail.mit.edu/6.828/2018/readings/hardware/8254x_GBe_SDM.pdf#G5.1318952). We will not be using the TCP offload features of the *E1000*, so one can focus on the *legacy transmit descriptor format*.

Byte addresses within the descriptor buffer are computed as follows: `address = base + (ptr * 16)`, where `ptr` is the value in the hardware head or tail register.

## C Structures

One will find it convenient to use C structs to describe the *E1000*'s structures. As an example, consider the legacy transmit descriptor given in table 3-8 of the manual and reproduced here:

```
  63            48 47   40 39   32 31   24 23   16 15             0
  +---------------------------------------------------------------+
  |                         Buffer address                        |
  +---------------+-------+-------+-------+-------+---------------+
  |    Special    |  CSS  | Status|  Cmd  |  CSO  |    Length     |
  +---------------+-------+-------+-------+-------+---------------+
```

It is coded into C as follows:

```c
struct tx_desc
{
    uint64_t addr;
    uint16_t length;
    uint8_t cso;
    uint8_t cmd;
    uint8_t status;
    uint8_t css;
    uint16_t special;
};
```

The driver will have to reserve memory for the transmit descriptor array and the packet buffers pointed to by the transmit descriptors. There are several ways to do this, ranging from dynamically allocating pages to simply declaring them in global variables. Whatever one chooses, keep in mind that the *E1000* accesses physical memory directly, which means any buffer it accesses must be contiguous in physical memory.

There are also multiple ways to handle the packet buffers. The simplest, which the lab recommends starting with, is to reserve space for a packet buffer for each descriptor during driver initialization and simply copy packet data into and out of these pre-allocated buffers. The maximum size of an Ethernet packet is `1518` bytes, which bounds how big these buffers need to be. More sophisticated drivers could dynamically allocate packet buffers (e.g., to reduce memory overhead when network usage is low) or even pass buffers directly provided by user space (a technique known as *zero copy*), but it's good to start simple.

## Exercise 5

> Perform the initialization steps described in [section 14.5](http://pdos.csail.mit.edu/6.828/2018/readings/hardware/8254x_GBe_SDM.pdf#G16.37215) (but not its subsections). Use [section 13](http://pdos.csail.mit.edu/6.828/2018/readings/hardware/8254x_GBe_SDM.pdf#G15.1491295) as a reference for the registers the initialization process refers to and [section 3.3.3](http://pdos.csail.mit.edu/6.828/2018/readings/hardware/8254x_GBe_SDM.pdf#G5.1318952) and [section 3.4](http://pdos.csail.mit.edu/6.828/2018/readings/hardware/8254x_GBe_SDM.pdf#G5.1029304) for reference to the transmit descriptors and transmit descriptor array.
> 
> Be mindful of the alignment requirements on the transmit descriptor array and the restrictions on length of this array. Since `TDLEN` must be `128`-byte aligned and each transmit descriptor is `16` bytes, your transmit descriptor array will need some multiple of `8` transmit descriptors. However, don't use more than `64` descriptors or our tests will not be able to test transmit ring overflow.
> 
> For the `TCTL.COLD`, you can assume full-duplex operation. For `TIPG`, refer to the default values described in table 13-77 of [section 13.4.34](http://pdos.csail.mit.edu/6.828/2018/readings/hardware/8254x_GBe_SDM.pdf#G15.1471971) for the IEEE 802.3 standard IPG (do not use the values in the table in [section 14.5](http://pdos.csail.mit.edu/6.828/2018/readings/hardware/8254x_GBe_SDM.pdf#G16.37215)).

### Transmit Initialization

Transmit initialization involves memory allocation and flags setting. The memory must be aligned on a `16`-byte boundary. The process mainly includes the following operations:

- Program the Transmit Descriptor Base Address (`TDBAL`/`TDBAH`) register(s) with the address of the region. `TDBAL` is used for `32`-bit addresses and both `TDBAL` and `TDBAH` are used for `64`-bit addresses.
- Set the *Transmit Descriptor Length* (`TDLEN`) register to the size (in bytes) of the descriptor ring. This register must be `128`-byte aligned.
- The Transmit Descriptor Head and Tail (`TDH`/`TDT`) registers are initialized (by hardware) to `0`b after a power-on or a software initiated Ethernet controller reset. Software should write `0`b to both these registers to ensure this.
- Initialize the Transmit Control Register (`TCTL`) for desired operation to include the following:
    - Set the Enable (`TCTL.EN`) bit to `1`b for normal operation.
    - Set the Pad Short Packets (`TCTL.PSP`) bit to `1`b.
    - Configure the Collision Threshold (`TCTL.CT`) to the desired value. Ethernet standard is `10`h. This setting only has meaning in half duplex mode.
    - Configure the Collision Distance (`TCTL.COLD`) to its expected value. For full duplex operation, this value should be set to `40`h. For gigabit half duplex, this value should be set to `200`h. For `10/100` half duplex, this value should be set to `40`h.
- Program the Transmit IPG (`TIPG`) register.

The implementation uses GNU C variable attribute, [`__attribute__`](http://gcc.gnu.org/onlinedocs/gcc/Variable-Attributes.html), which aligns the declared `struct` by `16`. See [Common Variable Attributes](http://gcc.gnu.org/onlinedocs/gcc/Common-Variable-Attributes.html#Common-Variable-Attributes).

There are a number of constant variables declaration, I find the original ones mainly from [*e1000_hw.h*](http://pdos.csail.mit.edu/6.828/2018/labs/lab6/e1000_hw.h) and [*e1000.h*](http://github.com/qemu/u-boot/blob/master/drivers/net/e1000.h), and I place all in the unit *lab/kern/e1000.h*. The definition for the `struct e1000_tx_desc` is given in [Section Exercise 4](#section_exercise_4) and is copied from [*axle OS*](http://axleos.com/docs/html/e1000_8h_source.html). The `struct e1000_tx_desc` is defined as follows:

```c
struct e1000_tx_desc {
    volatile uint64_t addr;
    volatile uint16_t length;
    volatile uint8_t cso;
    volatile uint8_t cmd;
    volatile uint8_t status;
    volatile uint8_t css;
    volatile uint16_t special;
} __attribute__((packed));
```

The implementation is as follows:

```diff
diff --git a/lab/kern/e1000.h b/lab/kern/e1000.h
index 3b46522..d7cb670 100644
--- a/lab/kern/e1000.h
+++ b/lab/kern/e1000.h
@@ -1,11 +1,64 @@
 #include <kern/pci.h>
 
 #ifndef JOS_KERN_E1000_H
+
 #define JOS_KERN_E1000_H
 #define E1000_VENDOR_ID            0x8086
 #define E1000_DEVICE_ID_82540EM    0x100E
-#endif  // SOL >= 6
 
 volatile uintptr_t e1000_mmio_base;
 
 int e1000_attach(struct pci_func *pciFunc);
+
+#define E1000_TCTL     0x00400  /* TX Control - RW */
+#define E1000_TIPG     0x00410  /* TX Inter-packet gap -RW */
+#define E1000_TDBAL    0x03800  /* TX Descriptor Base Address Low - RW */
+#define E1000_TDBAH    0x03804  /* TX Descriptor Base Address High - RW */
+#define E1000_TDLEN    0x03808  /* TX Descriptor Length - RW */
+#define E1000_TDH      0x03810  /* TX Descriptor Head - RW */
+#define E1000_TDT      0x03818  /* TX Descripotr Tail - RW */
+
+/* Transmit Control */
+#define E1000_TCTL_EN   0x00000002  /* enable tx */
+#define E1000_TCTL_PSP  0x00000008  /* pad short packets */
+#define E1000_TCTL_CT   0x00000ff0  /* collision threshold */
+#define E1000_TCTL_COLD 0x003ff000  /* collision distance */
+
+/* Collision related configuration parameters */
+#define E1000_COLLISION_THRESHOLD   0x10
+#define E1000_CT_SHIFT              4
+
+/* Collision distance is a 0-based value that applies to half-duplex-capable hardware only. */
+#define E1000_COLLISION_DISTANCE    0x40
+#define E1000_COLD_SHIFT            12
+
+/* Default values for the transmit IPG register */
+#define E1000_DEFAULT_TIPG_IPGT     10
+#define E1000_DEFAULT_TIPG_IPGR1    4
+#define E1000_DEFAULT_TIPG_IPGR2    6
+#define E1000_TIPG_IPGT_MASK        0x000003FF
+#define E1000_TIPG_IPGR1_MASK       0x000FFC00
+#define E1000_TIPG_IPGR2_MASK       0x3FF00000
+#define E1000_TIPG_IPGR1_SHIFT      10
+#define E1000_TIPG_IPGR2_SHIFT      20
+
+struct e1000_tx_desc {
+	volatile uint64_t addr;
+	volatile uint16_t length;
+	volatile uint8_t cso;
+	volatile uint8_t cmd;
+	volatile uint8_t status;
+	volatile uint8_t css;
+	volatile uint16_t special;
+} __attribute__((packed));
+
+#define E1000_BUFFER_SIZE 1024
+#define E1000_TRANSMIT_SIZE 64
+
+static struct e1000_tx_desc e1000_tx_queue[E1000_TRANSMIT_SIZE] __attribute__((aligned(16)));
+static uint8_t e1000_tx_buffer[E1000_TRANSMIT_SIZE][E1000_BUFFER_SIZE];
+
+#define E1000_ADDR(offset) (*(volatile uint32_t *)(e1000_mmio_base + offset))
+static void e1000_tx_init();
+
+#endif  // SOL >= 6
diff --git a/lab/kern/e1000.c b/lab/kern/e1000.c
index 127aff2..2755025 100644
--- a/lab/kern/e1000.c
+++ b/lab/kern/e1000.c
@@ -1,11 +1,34 @@
 #include <kern/e1000.h>
 #include <kern/pmap.h>
 #include <kern/pci.h>
+#include <inc/string.h>
+
+static void e1000_tx_init() {
+	/* init the transmit queue: */
+	int i = 0;
+	memset(e1000_tx_queue, 0, sizeof(e1000_tx_queue));
+	for (i = 0; i < E1000_TRANSMIT_SIZE; i ++) e1000_tx_queue[i].addr = PADDR(e1000_tx_buffer[i]);
+
+	/* init transmit descriptor registers: */
+	E1000_ADDR(E1000_TDBAL) = PADDR(e1000_tx_queue);
+	E1000_ADDR(E1000_TDBAH) = 0;
+	E1000_ADDR(E1000_TDLEN) = sizeof(e1000_tx_queue);
+	E1000_ADDR(E1000_TDH) = 0;
+	E1000_ADDR(E1000_TDT) = 0;
+
+	/* init transmit control registers */
+	E1000_ADDR(E1000_TCTL) |= E1000_TCTL_EN | E1000_TCTL_PSP; // set the Enable (`TCTL.EN`) bit to `1`b for normal operation, and set the Pad Short Packets (`TCTL.PSP`) bit to `1`b.
+	/// E1000_ADDR(E1000_TCTL) &= ~(E1000_TCTL_CT | E1000_TCTL_COLD); // prepare to configure the Collision Threshold (`TCTL.CT`) and the Collision Threshold (`TCTL.CT`). unclear...
+	E1000_ADDR(E1000_TCTL) |= ((0x10 << 4) | (0x40 << 12)); // configure the Collision Threshold (`TCTL.CT`) and the Collision Threshold (`TCTL.CT`)
+	/// E1000_ADDR(E1000_TIPG) &= ~(0x000003ff | 0x000ffc00 | 0x3ff00000); // prepare to program the Transmit IPG (`TIPG`) register
+	E1000_ADDR(E1000_TIPG) |= (10 | (4 << 10) | (6 << 20)); // program the Transmit IPG (`TIPG`) register
+}
 
 // LAB 6: Your driver code here
 int e1000_attach(struct pci_func *pciFunc) {
 	pci_func_enable(pciFunc);
 	e1000_mmio_base = (uintptr_t) mmio_map_region(pciFunc->reg_base[0], pciFunc->reg_size[0]);
 	cprintf("e1000 status register: 0x%08x\n", (*(uint32_t *)(e1000_mmio_base + 8))); // test mmio: the dump value should be 0x80080783
+	e1000_tx_init();
 	return 0;
 }
```

Verify the work:

```shell
$ cd ~/mitos/lab && make clean && make E1000_DEBUG=TXERR,TX qemu-nox
    ⋮
e1000 status register: 0x80080783
e1000: tx disabled
    ⋮
```

## Exercise 6

> Write a function to transmit a packet by checking that the next descriptor is free, copying the packet data into the next descriptor, and updating TDT. Make sure you handle the transmit queue being full.

Here, *the next descriptor* is the one that is pointed at by the transmit descriptor tail register, `TDT`, which is the *index* in the transmit queue array, not a byte offset.

There are several noticeable points at transmit.

Checking the head and the tail registers only cannot ensure whether the transmit queue is full: the documentation explicitly states that reading this register from software is *unreliable*.

If the `RS` bit in the command field of a transmit descriptor is set, then, when the card has transmitted the packet in that descriptor, the card will set the `DD` bit in the status field of the descriptor. If a descriptor's `DD` bit is set, it is safe to recycle that descriptor and to use it to transmit another packet. This trick is used to detect whether the transmit queue is full: if the next descriptor, which is accessible via `TDT`, has its `DD` bit unset, the transmit queue is full.

If the user calls the transmit system call, but the `DD` bit of the next descriptor is not set, indicating that the transmit queue is full, the packet may be simply dropped. Network protocols are resilient to this, but if a large burst of packets are dropped, the protocol may not recover. One could instead tell the user environment that it has to retry, much like one did for `sys_ipc_try_send`. This has the advantage of pushing back on the environment generating the data.

The new function for transmitting is implemented as follows:

```c
int e1000_transmit(const void *buffer, size_t size) {
    if (size > E1000_PACKET_BUFFER_SIZE) return -E1000_ERROR_PACKET_BUFFER_TOO_LARGE;
    uint32_t tail = E1000_ADDR(E1000_TDT);
    if ((e1000_tx_queue[tail].cmd & E1000_TXD_CMD_RS) &&
        !(e1000_tx_queue[tail].status & E1000_TXD_STAT_DD))
        return -E1000_ERROR_TRANSMIT_QUEUE_FULL; // the transmit queue is full.
    e1000_tx_queue[tail].status &= ~E1000_TXD_STAT_DD; // unset the `DD` bit to re-use the descriptor
    memcpy(e1000_tx_buffer[tail], buffer, size);
    e1000_tx_queue[tail].length = size;
    e1000_tx_queue[tail].cmd |= (E1000_TXD_CMD_RS | E1000_TXD_CMD_EOP);
    E1000_ADDR(E1000_TDT) = (tail + 1) % E1000_TRANSMIT_SIZE;
    return 0;
}
```

The full change is as follows:

```diff
diff --git a/lab/kern/e1000.h b/lab/kern/e1000.h
index fb3c630..bef8bd9 100644
--- a/lab/kern/e1000.h
+++ b/lab/kern/e1000.h
@@ -61,4 +61,15 @@ static uint8_t e1000_tx_buffer[E1000_TRANSMIT_SIZE][E1000_BUFFER_SIZE];
 #define E1000_ADDR(offset) (*(volatile uint32_t *)(e1000_mmio_base + offset))
 static void e1000_tx_init();
 
+/* Transmit Descriptor bit definitions */
+#define E1000_TXD_CMD_EOP   0x01  /* End of Packet */
+#define E1000_TXD_CMD_RS    0x08  /* Report Status */
+#define E1000_TXD_STAT_DD   0x01  /* Descriptor Done */
+
+#define E1000_PACKET_BUFFER_SIZE 2048
+#define E1000_ERROR_PACKET_BUFFER_TOO_LARGE 1
+#define E1000_ERROR_TRANSMIT_QUEUE_FULL 2
+
+int e1000_transmit(const void *buffer, size_t size);
+
 #endif  // SOL >= 6
diff --git a/lab/kern/e1000.c b/lab/kern/e1000.c
index 2755025..26bc51d 100644
--- a/lab/kern/e1000.c
+++ b/lab/kern/e1000.c
@@ -24,11 +24,27 @@ static void e1000_tx_init() {
 	E1000_ADDR(E1000_TIPG) |= (10 | (4 << 10) | (6 << 20)); // program the Transmit IPG (`TIPG`) register
 }
 
+int e1000_transmit(const void *buffer, size_t size) {
+	if (size > E1000_PACKET_BUFFER_SIZE) return -E1000_ERROR_PACKET_BUFFER_TOO_LARGE;
+	uint32_t tail = E1000_ADDR(E1000_TDT);
+	if ((e1000_tx_queue[tail].cmd & E1000_TXD_CMD_RS) &&
+	    !(e1000_tx_queue[tail].status & E1000_TXD_STAT_DD))
+		return -E1000_ERROR_TRANSMIT_QUEUE_FULL; // the transmit queue is full.
+	e1000_tx_queue[tail].status &= ~E1000_TXD_STAT_DD; // unset the `DD` bit to re-use the descriptor
+	memcpy(e1000_tx_buffer[tail], buffer, size);
+	e1000_tx_queue[tail].length = size;
+	e1000_tx_queue[tail].cmd |= (E1000_TXD_CMD_RS | E1000_TXD_CMD_EOP);
+	E1000_ADDR(E1000_TDT) = (tail + 1) % E1000_TRANSMIT_SIZE;
+	return 0;
+}
+
 // LAB 6: Your driver code here
 int e1000_attach(struct pci_func *pciFunc) {
 	pci_func_enable(pciFunc);
 	e1000_mmio_base = (uintptr_t) mmio_map_region(pciFunc->reg_base[0], pciFunc->reg_size[0]);
 	cprintf("e1000 status register: 0x%08x\n", (*(uint32_t *)(e1000_mmio_base + 8))); // test mmio: the dump value should be 0x80080783
 	e1000_tx_init();
+	char *msg = "e1000 is transmitting...";
+	e1000_transmit(msg, 25); // hotspot test
 	return 0;
 }
```

Verify the work:

```shell
$ cd ~/mitos/lab && make clean && make E1000_DEBUG=TXERR,TX qemu-nox
    ⋮
e1000 status register: 0x80080783
e1000: tx disabled
e1000: index 0: 0x2b0e80 : 9000019 0
    ⋮
$ tcpdump -XXnr qemu.pcap
reading from file qemu.pcap, link-type EN10MB (Ethernet)
16:11:52.962309 69:73:20:74:72:61 > 65:31:30:30:30:20, ethertype Unknown (0x6e73), length 25:
    0x0000:  6531 3030 3020 6973 2074 7261 6e73 6d69  e1000.is.transmi
    0x0010:  7474 696e 672e 2e2e 00                   tting....
```

## Exercise 7

> Add a system call that lets you transmit packets from user space. The exact interface is up to you. Do not forget to check any pointers passed to the kernel from user space.

This assignment requires to add a system call for the *E1000 transmit* function. The following units are changed:

- *lab/inc/lib.h*
- *lab/inc/syscall.h*
- *lab/kern/syscall.c*
- *lab/lib/syscall.c*

The implementation is as follows:

```diff
diff --git a/lab/inc/lib.h b/lab/inc/lib.h
index 66740e8..fd3d19f 100644
--- a/lab/inc/lib.h
+++ b/lab/inc/lib.h
@@ -60,6 +60,7 @@ int	sys_page_unmap(envid_t env, void *pg);
 int	sys_ipc_try_send(envid_t to_env, uint32_t value, void *pg, int perm);
 int	sys_ipc_recv(void *rcv_pg);
 unsigned int sys_time_msec(void);
+int sys_transmit(const void *buffer, size_t size);
 
 // This must be inlined.  Exercise for reader: why?
 static inline envid_t __attribute__((always_inline))
diff --git a/lab/inc/syscall.h b/lab/inc/syscall.h
index 36f26de..53a0ca8 100644
--- a/lab/inc/syscall.h
+++ b/lab/inc/syscall.h
@@ -18,6 +18,7 @@ enum {
 	SYS_ipc_try_send,
 	SYS_ipc_recv,
 	SYS_time_msec,
+	SYS_transmit,
 	NSYSCALLS
 };
 
diff --git a/lab/kern/syscall.c b/lab/kern/syscall.c
index e5aac24..98be624 100644
--- a/lab/kern/syscall.c
+++ b/lab/kern/syscall.c
@@ -12,6 +12,7 @@
 #include <kern/console.h>
 #include <kern/sched.h>
 #include <kern/time.h>
+#include <kern/e1000.h>
 
 // Print a string to the system console.
 // The string is exactly 'len' characters long.
@@ -352,6 +353,11 @@ sys_time_msec(void)
 	return time_msec();
 }
 
+static int sys_transmit(const void *buffer, size_t size) {
+	user_mem_assert(curenv, buffer, size, PTE_U);
+	return e1000_transmit(buffer, size);
+}
+
 // Dispatches to the correct kernel function, passing the arguments.
 int32_t syscall(uint32_t syscallno, uint32_t a1, uint32_t a2, uint32_t a3, uint32_t a4, uint32_t a5) {
 	// Call the function corresponding to the 'syscallno' parameter.
@@ -372,6 +378,7 @@ int32_t syscall(uint32_t syscallno, uint32_t a1, uint32_t a2, uint32_t a3, uint3
 	case SYS_ipc_try_send: return sys_ipc_try_send(a1, a2, (void *)a3, a4);
 	case SYS_env_set_trapframe: return sys_env_set_trapframe(a1, (struct Trapframe *)a2);
 	case SYS_time_msec: return sys_time_msec();
+	case SYS_transmit: return sys_transmit((void *)a1, a2);
 	default: return -E_INVAL;
 	}
 }
diff --git a/lab/lib/syscall.c b/lab/lib/syscall.c
index 9e1a1d9..ac22030 100644
--- a/lab/lib/syscall.c
+++ b/lab/lib/syscall.c
@@ -122,3 +122,7 @@ sys_time_msec(void)
 {
 	return (unsigned int) syscall(SYS_time_msec, 0, 0, 0, 0, 0, 0);
 }
+
+int sys_transmit(const void *buffer, size_t size) {
+	return syscall(SYS_transmit, 0, (uint32_t)buffer, size, 0, 0, 0);
+}
```

## Exercise 8

> Implement *lab/net/output.c*.

The *output* helper environment's goal is to do the following in a loop: accept `NSREQ_OUTPUT` IPC messages from the core network server and send the packets accompanying the IPC message to the network device driver using the system call added above. The `NSREQ_OUTPUT` IPC's are sent by the `low_level_output` function in *lab/net/lwip/jos/jif/jif.c*, which glues the `lwIP` stack to JOS's network system. Each IPC will include a page consisting of a `union Nsipc` with the packet in its `struct jif_pkt pkt` field (see *lab/inc/ns.h*). `struct jif_pkt` looks like

```c
struct jif_pkt {
    int jp_len;
    char jp_data[0];
};
```

`jp_len` represents the length of the packet. All subsequent bytes on the IPC page are dedicated to the packet contents. Using a zero-length array like `jp_data` at the end of a `struct` is a common C trick for representing buffers without pre-determined lengths. Since C does not do array bounds checking, as long as you ensure there is enough unused memory following the struct, you can use `jp_data` as if it were an array of any size.

Be aware of the interaction between the device driver, the `output` environment and the core network server when there is no more space in the device driver's transmit queue. The core network server sends packets to the `output` environment using IPC. If the `output` environment is suspended due to a send packet system call because the driver has no more buffer space for new packets, the core network server will block waiting for the output server to accept the IPC call.

The implementation is as follows:

```diff
diff --git a/lab/kern/e1000.c b/lab/kern/e1000.c
index 26bc51d..db99bf8 100644
--- a/lab/kern/e1000.c
+++ b/lab/kern/e1000.c
@@ -44,7 +44,5 @@ int e1000_attach(struct pci_func *pciFunc) {
 	e1000_mmio_base = (uintptr_t) mmio_map_region(pciFunc->reg_base[0], pciFunc->reg_size[0]);
 	cprintf("e1000 status register: 0x%08x\n", (*(uint32_t *)(e1000_mmio_base + 8))); // test mmio: the dump value should be 0x80080783
 	e1000_tx_init();
-	char *msg = "e1000 is transmitting...";
-	e1000_transmit(msg, 25); // hotspot test
 	return 0;
 }
diff --git a/lab/net/output.c b/lab/net/output.c
index f577c4e..c8fffc4 100644
--- a/lab/net/output.c
+++ b/lab/net/output.c
@@ -1,4 +1,5 @@
 #include "ns.h"
+#include <kern/e1000.h>
 
 extern union Nsipc nsipcbuf;
 
@@ -6,8 +7,22 @@ void
 output(envid_t ns_envid)
 {
 	binaryname = "ns_output";
-
 	// LAB 6: Your code here:
 	// 	- read a packet from the network server
 	//	- send the packet to the device driver
+	uint32_t ipc_val, whom;
+	int r;
+	while (1) {
+		// receive a packet message from the network server. see `lib/ipc.c' for `ipc_recv'.
+		ipc_val = ipc_recv((int32_t *) &whom, &nsipcbuf, NULL);
+		if (ipc_val == NSREQ_OUTPUT) {
+			// send the network packet to the device driver
+			while ((r = sys_transmit(nsipcbuf.pkt.jp_data, nsipcbuf.pkt.jp_len)) == -E1000_ERROR_TRANSMIT_QUEUE_FULL)
+				sys_yield(); // wait when the transmit queue is full
+			if (r == -E1000_ERROR_PACKET_BUFFER_TOO_LARGE)
+				panic("ERROR: %s: packet is too large (%d bytes) to be sent\n", binaryname, nsipcbuf.pkt.jp_len);
+			else if (r < 0)
+				panic("ERROR: the network failed to send transmit packats: the return value is %d\n", r);
+		}
+	}
 }
```

Verify the work:

```shell
$ cd ~/mitos/lab && make clean && make E1000_DEBUG=TXERR,TX run-net_testoutput-nox
    ⋮
e1000 status register: 0x80080783
e1000: tx disabled
Transmitting packet 0
FS is running
FS can do I/O
Device 1 presence: 1
Transmitting packet 1
e1000: index 0: 0x2b0e80 : 9000009 0
Transmitting packet 2
e1000: index 1: 0x2b1280 : 9000009 0
block cache is good
superblock is good
Transmitting packet 3
e1000: index 2: 0x2b1680 : 9000009 0
bitmap is good
alloc_block is good
Transmitting packet 4
e1000: index 3: 0x2b1a80 : 9000009 0
file_open is good
file_get_block is good
Transmitting packet 5
e1000: index 4: 0x2b1e80 : 9000009 0
file_flush is good
file_truncate is good
Transmitting packet 6
e1000: index 5: 0x2b2280 : 9000009 0
file rewrite is good
Transmitting packet 7
e1000: index 6: 0x2b2680 : 9000009 0
Transmitting packet 8
e1000: index 7: 0x2b2a80 : 9000009 0
Transmitting packet 9
e1000: index 8: 0x2b2e80 : 9000009 0
e1000: index 9: 0x2b3280 : 9000009 0
No runnable environments in the system!
Welcome to the JOS kernel monitor!
Type 'help' for a list of commands.
K>
$ cd ~/mitos/lab && tcpdump -XXnr qemu.pcap
reading from file qemu.pcap, link-type EN10MB (Ethernet)
22:10:00.006391 [|ether]
    0x0000:  5061 636b 6574 2030 30                   Packet.00
22:10:00.025506 [|ether]
    0x0000:  5061 636b 6574 2030 31                   Packet.01
22:10:00.045849 [|ether]
    0x0000:  5061 636b 6574 2030 32                   Packet.02
22:10:00.065333 [|ether]
    0x0000:  5061 636b 6574 2030 33                   Packet.03
22:10:00.085640 [|ether]
    0x0000:  5061 636b 6574 2030 34                   Packet.04
22:10:00.105439 [|ether]
    0x0000:  5061 636b 6574 2030 35                   Packet.05
22:10:00.125459 [|ether]
    0x0000:  5061 636b 6574 2030 36                   Packet.06
22:10:00.135390 [|ether]
    0x0000:  5061 636b 6574 2030 37                   Packet.07
22:10:00.145415 [|ether]
    0x0000:  5061 636b 6574 2030 38                   Packet.08
22:10:00.145915 [|ether]
    0x0000:  5061 636b 6574 2030 39                   Packet.09
```

Now grade the work:

```shell
$ cd ~/mitos/lab && make grade
    ⋮
testtime: OK (8.6s)
pci attach: OK (2.5s)
testoutput [5 packets]: OK (2.6s)
testoutput [100 packets]: OK (3.4s)
Part A score: 35/35
    ⋮
```

## Question 1

> How did you structure your transmit implementation? In particular, what do you do if the transmit ring is full?

In the `output` helper environment, the system call `sys_transmit` calls the function `e1000_transmit`, which detects whether the transmit ring is full. If the transmit ring is full, the `output` environment yields until a descriptor is available.

# Part B: Receiving Packets and the Web Server

## Exercise 9

> Read [Section 3.2](http://pdos.csail.mit.edu/6.828/2018/readings/hardware/8254x_GBe_SDM.pdf#G5.1327495). You can ignore anything about interrupts and checksum offloading (you can return to these sections if you decide to use these features later), and you do not have to be concerned with the details of thresholds and how the card's internal caches work.

[Section 3.2](http://pdos.csail.mit.edu/6.828/2018/readings/hardware/8254x_GBe_SDM.pdf#G5.1327495) describes how packet reception works, including the receive queue structure and receive descriptors, and the initialization process is detailed in [Section 14.4](http://pdos.csail.mit.edu/6.828/2018/readings/hardware/8254x_GBe_SDM.pdf#G16.37177).

Hardware stores incoming packets in host memory subject to the following filter modes. If there is insufficient space in the receive FIFO, hardware drops them and indicates the missed packet in the appropriate *statistics registers*.

The following filter modes are supported:

- Exact Unicast/Multicast: The destination address must exactly match one of 16 stored addresses. These addresses can be unicast or multicast.
- Promiscuous Unicast: Receive all unicasts.
- Multicast: The upper bits of the incoming packet's destination address index a bit vector that indicates whether to accept the packet; if the bit in the vector is one, accept the packet; otherwise, reject it. The controller provides a `4096` bit vector. Software provides four choices of which bits are used for indexing. These are `[47:36]`, `[46:35]`, `[45:34]`, or `[43:32]` of the internally stored representation of the destination address.
- Promiscuous Multicast: Receive all multicast packets.
- VLAN: Receive all VLAN packets that are for this station, and have the appropriate bit set in the VLAN filter table.

The *store–bad–packet* bit in the *Device Control register*, `RCTL.SBP`, controls whether bad packets are received. Packet errors are indicated by the *error bits* in the *receive descriptor*, `RDESC.ERRORS`. Setting the promiscuous enables (`RCTL.UPE/MPE`) and the *store–bad–packet* bit `RCTL.SBP` will allow receive all packets.

The receive queue is very similar to the transmit queue, except that it consists of empty packet buffers waiting to be filled with incoming packets. Hence, when the network is idle, the transmit queue is empty (because all packets have been sent), but the receive queue is full of *empty* packet buffers.

When the *E1000* receives a packet, it first checks if it matches the card's configured filters (for example, to see if the packet is addressed to this *E1000*'s MAC address) and ignores the packet if it does not match any filters. Otherwise, the *E1000* tries to retrieve the next receive descriptor from the head of the receive queue. If the head (`RDH`) has caught up with the tail (`RDT`), then the receive queue is out of free descriptors, so the card *drops* the packet. If there is a free receive descriptor, it copies the packet data into the buffer pointed to by the descriptor, sets the descriptor's `DD` (*Descriptor Done*) and `EOP` (*End of Packet*) status bits, and increments the `RDH`.

If the *E1000* receives a packet that is larger than the packet buffer in one receive descriptor, it will retrieve as many descriptors as necessary from the receive queue to store the entire contents of the packet. To indicate that this has happened, it will set the `DD` status bit on all of these descriptors, but only set the `EOP` status bit on the last of these descriptors. One can either deal with this possibility in the driver, or simply configure the card to not accept *long packets* (also known as *jumbo frames*) and make sure your receive buffers are large enough to store the largest possible standard Ethernet packet (`1518` bytes).

## Exercise 10

> Set up the receive queue and configure the *E1000* by following the process in [Section 14.4](http://pdos.csail.mit.edu/6.828/2018/readings/hardware/8254x_GBe_SDM.pdf#G16.37177). You do not have to support *long packets* or *multicast*. For now, do not configure the card to use interrupts; you can change that later if you decide to use receive interrupts. Also, configure the *E1000* to strip the Ethernet `CRC`, since the grade script expects it to be stripped.
> 
> By default, the card will filter out all packets. You have to configure the *Receive Address Registers* (`RAL` and `RAH`) with the card's own MAC address in order to accept packets addressed to that card. You can simply hard-code QEMU's default MAC address of `52:54:00:12:34:56` (we already hard-code this in `lwIP`, so doing it here too does not make things any worse). Be very careful with the byte order; MAC addresses are written from lowest-order byte to highest-order byte, so `52:54:00:12` are the low-order `32` bits of the MAC address and `34:56` are the high-order `16` bits.
> 
> The *E1000* only supports a specific set of receive buffer sizes (given in the description of `RCTL.BSIZE` in [Section 13.4.22](http://pdos.csail.mit.edu/6.828/2018/readings/hardware/8254x_GBe_SDM.pdf#G15.1353657)). If you make your receive packet buffers large enough and disable long packets, you will not have to worry about packets spanning multiple receive buffers. Also, remember that, just like for transmit, the receive queue and the packet buffers must be contiguous in physical memory.
> 
> You should use at least `128` receive descriptors.

This assignment requires to initialize receive, mainly including the following operations:

- Program the *Receive Address Register(s) (`RAL`/`RAH`)* with the desired Ethernet addresses. `RAL[0]`/`RAH[0]` should always be used to store the Individual Ethernet MAC address of the Ethernet controller. As mentioned the MAC address of `52:54:00:12:34:56` is going to be hard-coded. Make sure to get the byte ordering right and did not forget to set the *Address Valid* bit in `RAH`.
- Initialize the `MTA` (*Multicast Table Array*) to `0`b. See [Section 13.5.1](http://pdos.csail.mit.edu/6.828/2018/readings/hardware/8254x_GBe_SDM.pdf#G15.1123398) for details. This looks unnecessary: The `E1000_MTA` definition is only found for *82543* and *82544*, so *Intel 82540EM* may be initialized to `0`.
- Allocate a region of memory for the receive descriptor list. Software should ensure this memory is aligned on a paragraph (`16`-byte) boundary. Program the *Receive Descriptor Base Address* (`RDBAL`/`RDBAH`) register(s) with the address of the region. `RDBAL` is used for `32`-bit addresses and both `RDBAL` and `RDBAH` are used for `64`-bit addresses.
- Set the *Receive Descriptor Length* (`RDLEN`) register to the size (in bytes) of the descriptor ring. This register must be `128`-byte aligned.
- The *Receive Descriptor Head and Tail* registers are initialized (by hardware) to `0` after a power-on or a software-initiated Ethernet controller reset. Receive buffers of appropriate size should be allocated and pointers to these buffers should be stored in the receive descriptor ring. Software initializes the *Receive Descriptor Head* (`RDH`) register and *Receive Descriptor Tail* (`RDT`) with the appropriate head and tail addresses. Head should point to the first valid receive descriptor in the descriptor ring and tail should point to one descriptor beyond the last valid descriptor in the descriptor ring.
- Program the *Receive Control* (`RCTL`) register with appropriate values for desired operation to include the following:
    - Set the receiver *Enable* (`RCTL.EN`) bit to `1`b for normal operation. However, it is best to leave the Ethernet controller receive logic disabled (`RCTL.EN` = `0`b) until after the receive descriptor ring has been initialized and software is ready to process received packets.
    - Set the *Long Packet Enable* (`RCTL.LPE`) bit to `1`b when processing packets greater than the standard Ethernet packet size. As mentioned there is no need to enable this feature for now.
    - *Loopback Mode* (`RCTL.LBM`) should be set to `00`b for normal operation.
    - Configure the *Receive Descriptor Minimum Threshold Size* (`RCTL.RDMTS`) bits to the desired value. Set it to `0` in the lab.
    - Configure the *Multicast Offset* (`RCTL.MO`) bits to the desired value. Its initial value is `0`, and there is no need to change in the lab.
    - Set the *Broadcast Accept Mode* (`RCTL.BAM`) bit to `1`b allowing the hardware to accept broadcast packets.
    - Configure the *Receive Buffer Size* (`RCTL.BSIZE`) bits to reflect the size of the receive buffers software provides to hardware. Also configure the *Buffer Extension Size* (`RCTL.BSEX`) bits if receive buffer needs to be larger than `2048` bytes. In my implementation, I set the receive buffer size to `1024` bytes, so `RCTL.BSEX = 0`b and `RCTL.BSIZE = 01`b. See [Section 13.4.22](http://pdos.csail.mit.edu/6.828/2018/readings/hardware/8254x_GBe_SDM.pdf#G15.1353657) for details.
    - Set the *Strip Ethernet CRC* (`RCTL.SECRC`) bit if the desire is for hardware to strip the CRC prior to DMA-ing the receive packet to host memory. As required by the lab, this bit is to set.
    - For the *82541xx* and *82547GI/EI*, program the *Interrupt Mask Set/Read (IMS)* register to enable any interrupt the driver wants to be notified of when the even occurs. Suggested bits include `RXT`, `RXO`, `RXDMT`, `RXSEQ`, and `LSC`. There is no immediate reason to enable the transmit interrupts. Plan to optimize interrupts later, including programming the interrupt moderation registers `TIDV`, `TADV`, `RADV` and `IDTR`. As mentioned there is no need to enable this feature for now.
    - For the *82541xx* and *82547GI/EI*, if software uses the *Receive Descriptor Minimum Threshold Interrupt*, the *Receive Delay Timer* (`RDTR`) register should be initialized with the desired delay time. See [Section 13.4.22](http://pdos.csail.mit.edu/6.828/2018/readings/hardware/8254x_GBe_SDM.pdf#G15.1353657) for the initial values and descriptions of some of the registers mentioned above.

The receive descriptor's layout is given in [Section 3.2.3](http://pdos.csail.mit.edu/6.828/2018/readings/hardware/8254x_GBe_SDM.pdf#G5.1010437):

```
 63            48 47   40 39   32 31           16 15             0
 +---------------------------------------------------------------+
 |                         Buffer address                        |
 +---------------+-------+--------+---------------+--------------+
 |    Special    | Error | Status |   Checksum    |    Length    |
 +---------------+-------+--------+---------------+--------------+
```

Similar to the transmit, there are a number of constant variables declaration, I find the original ones mainly from [*e1000_hw.h*](http://pdos.csail.mit.edu/6.828/2018/labs/lab6/e1000_hw.h) and [*e1000.h*](http://github.com/qemu/u-boot/blob/master/drivers/net/e1000.h), and I place all in the unit *lab/kern/e1000.h*.

The definition for the `struct e1000_rx_desc` is not given in lab and is copied from [*axle OS*](http://axleos.com/docs/html/e1000_8h_source.html). The `struct e1000_rx_desc` is defined as follows:

```c
struct e1000_rx_desc {
         volatile uint64_t addr;
         volatile uint16_t length;
         volatile uint16_t checksum;
         volatile uint8_t status;
         volatile uint8_t errors;
         volatile uint16_t special;
 } __attribute__((packed));
```

The implementation is as follows:

```diff
diff --git a/lab/kern/e1000.c b/lab/kern/e1000.c
index db99bf8..f42f6c4 100644
--- a/lab/kern/e1000.c
+++ b/lab/kern/e1000.c
@@ -38,11 +38,40 @@ int e1000_transmit(const void *buffer, size_t size) {
 	return 0;
 }
 
+static void e1000_rx_init() {
+	/* init the receive queue: */
+	int i = 0;
+	memset(e1000_rx_queue, 0, sizeof(e1000_rx_queue));
+	for (i = 0; i < E1000_RECEIVE_SIZE; i++) e1000_rx_queue[i].addr = PADDR(e1000_rx_buffer[i]);
+
+	/* init the receive address */
+	E1000_ADDR(E1000_RAL) = QEMU_MAC_LOW;
+	E1000_ADDR(E1000_RAH) = QEMU_MAC_HIGH;
+	E1000_ADDR(E1000_RAH) |= E1000_RAH_AV; // set the `Address Valid' bit in `RAH'
+
+	/* init receive descriptor registers: */
+	E1000_ADDR(E1000_RDBAL) = PADDR(e1000_rx_queue);
+	E1000_ADDR(E1000_RDBAH) = 0;
+	E1000_ADDR(E1000_RDLEN) = sizeof(e1000_rx_queue);
+	E1000_ADDR(E1000_RDH) = 0;
+	E1000_ADDR(E1000_RDT) = E1000_RECEIVE_SIZE - 1;
+
+	/* init receive control registers */
+	E1000_ADDR(E1000_RCTL) |= E1000_RCTL_EN; // set the Enable (`TCTL.EN`) bit to `1`b for normal operation.
+	E1000_ADDR(E1000_RCTL) &= ~(E1000_RCTL_LBM); // loopback Mode (`RCTL.LBM`) should be set to `00`b for normal operation.
+	E1000_ADDR(E1000_RCTL) &= ~(E1000_RCTL_RDMTS); // configure the *Receive Descriptor Minimum Threshold Size* (`RCTL.RDMTS') bits to 0.
+	E1000_ADDR(E1000_RCTL) |= E1000_RCTL_BAM; // set the *Broadcast Accept Mode* (`RCTL.BAM') bit to 1 allowing the hardware to accept broadcast packets.
+	E1000_ADDR(E1000_RCTL) &= ~E1000_RCTL_BSEX; // the recive buffer size is `1024` bytes, so `RCTL.BSEX = 0`b and `RCTL.BSIZE = 01`b.
+	E1000_ADDR(E1000_RCTL) |= (E1000_RCTL_SZ & 0x00010000); // the recive buffer size is `1024` bytes, so `RCTL.BSEX = 0`b and `RCTL.BSIZE = 01`b.
+	E1000_ADDR(E1000_RCTL) |= E1000_RCTL_SECRC; // strip Ethernet CRC (`RCTL.SECRC = 1`)
+}
+
 // LAB 6: Your driver code here
 int e1000_attach(struct pci_func *pciFunc) {
 	pci_func_enable(pciFunc);
 	e1000_mmio_base = (uintptr_t) mmio_map_region(pciFunc->reg_base[0], pciFunc->reg_size[0]);
 	cprintf("e1000 status register: 0x%08x\n", (*(uint32_t *)(e1000_mmio_base + 8))); // test mmio: the dump value should be 0x80080783
 	e1000_tx_init();
+	e1000_rx_init();
 	return 0;
 }
diff --git a/lab/kern/e1000.h b/lab/kern/e1000.h
index bef8bd9..fa94a8b 100644
--- a/lab/kern/e1000.h
+++ b/lab/kern/e1000.h
@@ -72,4 +72,45 @@ static void e1000_tx_init();
 
 int e1000_transmit(const void *buffer, size_t size);
 
+/* Receive Address */
+#define E1000_RAL     0x00005400  /* Receive Address Low - RW */
+#define E1000_RAH     0x00005404  /* Receive Address High - RW */
+#define E1000_RAH_AV  0x80000000	/* Receive descriptor valid */
+
+/* RX Descriptor */
+#define E1000_RDBAL    0x02800  /* RX Descriptor Base Address Low - RW */
+#define E1000_RDBAH    0x02804  /* RX Descriptor Base Address High - RW */
+#define E1000_RDLEN    0x02808  /* RX Descriptor Length - RW */
+#define E1000_RDH      0x02810  /* RX Descriptor Head - RW */
+#define E1000_RDT      0x02818  /* RX Descriptor Tail - RW */
+
+/* Receive Control */
+#define E1000_RCTL          0x00000100  /* RX Control - RW */
+#define E1000_RCTL_EN       0x00000002  /* enable */
+#define E1000_RCTL_LBM      0x000000c0  /* loopback mode */
+#define E1000_RCTL_RDMTS    0x00000300  /* rx desc min threshold size */
+#define E1000_RCTL_BAM      0x00008000	/* broadcast enable */
+#define E1000_RCTL_SZ       0x00030000  /* rx buffer size */
+#define E1000_RCTL_SECRC    0x04000000  /* strip ethernet CRC */
+#define E1000_RCTL_BSEX     0x02000000  /* Buffer size extension */
+
+struct e1000_rx_desc {
+         volatile uint64_t addr;
+         volatile uint16_t length;
+         volatile uint16_t checksum;
+         volatile uint8_t status;
+         volatile uint8_t errors;
+         volatile uint16_t special;
+ } __attribute__((packed));
+
+#define E1000_RECEIVE_SIZE 128
+
+static struct e1000_rx_desc e1000_rx_queue[E1000_RECEIVE_SIZE] __attribute__((aligned(16)));
+static uint8_t e1000_rx_buffer[E1000_RECEIVE_SIZE][E1000_BUFFER_SIZE];
+
+#define QEMU_MAC_LOW  0x12005452
+#define QEMU_MAC_HIGH 0x00005634
+
+static void e1000_rx_init();
+
 #endif  // SOL >= 6
```

Verify the work:

```shell
$ cd ~/mitos/lab && make clean &&  make E1000_DEBUG=TX,TXERR,RX,RXERR,RXFILTER run-net_testinput-nox
    ⋮
Sending ARP announcement...
e1000: index 0: 0x2d1680 : 900002a 0
e1000: unicast match[0]: 52:54:00:12:34:56
    ⋮
```

## Exercise 11

>Write a function to receive a packet from the *E1000* and expose it to user space by adding a system call. Make sure you handle the receive queue being empty.

Similar to transmit, the documentation states that the `RDH` register cannot be reliably read from software, so in order to determine if a packet has been delivered to this descriptor's packet buffer, one will have to read the `DD` status bit in the descriptor. If the `DD` bit is set, one can copy the packet data out of that descriptor's packet buffer and then tell the card that the descriptor is free by updating the queue's tail index, `RDT`.

If the `DD` bit is not set, then no packet has been received. This is the receive-side equivalent of when the transmit queue was full, and there are several things one can do in this situation. One can simply return a *try again* error and require the caller to retry. While this approach works well for full transmit queues because that is a transient condition, it is less justifiable for empty receive queues because the receive queue may remain empty for long stretches of time. A second approach is to suspend the calling environment until there are packets in the receive queue to process. This tactic is very similar to `sys_ipc_recv`. Just like in the *IPC* case, since there is only one kernel stack per CPU, as soon as we leave the kernel the state on the stack will be lost. We need to set a flag indicating that an environment has been suspended by receive queue underflow and record the system call arguments. The drawback of this approach is complexity: the *E1000* must be instructed to generate receive interrupts and the driver must handle them in order to resume the environment blocked waiting for a packet.

In brief, when `RDH` and `RDT` both point to the same descriptor, it means that the receive descriptor array is full: it is full of processed descriptor packets, or it is full of unprocessed descriptor packets. Using the `DD` bit of a descriptor to determine whether its packet is processed or not: the `DD` bit is set if a receive descriptor is unprocessed.

The implementation is as follows:
```diff
diff --git a/lab/kern/e1000.h b/lab/kern/e1000.h
index fa94a8b..2badedf 100644
--- a/lab/kern/e1000.h
+++ b/lab/kern/e1000.h
@@ -69,6 +69,7 @@ static void e1000_tx_init();
 #define E1000_PACKET_BUFFER_SIZE 2048
 #define E1000_ERROR_PACKET_BUFFER_TOO_LARGE 1
 #define E1000_ERROR_TRANSMIT_QUEUE_FULL 2
+#define E1000_ERROR_RECEIVE_QUEUE_EMPTY 3
 
 int e1000_transmit(const void *buffer, size_t size);
 
@@ -94,6 +95,9 @@ int e1000_transmit(const void *buffer, size_t size);
 #define E1000_RCTL_SECRC    0x04000000  /* strip ethernet CRC */
 #define E1000_RCTL_BSEX     0x02000000  /* Buffer size extension */
 
+/* Receive Descriptor bit definitions */
+#define E1000_RXD_STAT_DD	0x01  /* Descriptor Done */
+
 struct e1000_rx_desc {
          volatile uint64_t addr;
          volatile uint16_t length;
@@ -113,4 +117,6 @@ static uint8_t e1000_rx_buffer[E1000_RECEIVE_SIZE][E1000_BUFFER_SIZE];
 
 static void e1000_rx_init();
 
+int e1000_receive(void *buffer, size_t size);
+
 #endif  // SOL >= 6
diff --git a/lab/kern/e1000.c b/lab/kern/e1000.c
index ad87c94..63d4648 100644
--- a/lab/kern/e1000.c
+++ b/lab/kern/e1000.c
@@ -66,6 +66,16 @@ static void e1000_rx_init() {
 	E1000_ADDR(E1000_RCTL) |= E1000_RCTL_SECRC; // strip Ethernet CRC (`RCTL.SECRC = 1`)
 }
 
+int e1000_receive(void *buffer, size_t size) {
+	uint32_t next_to_tail = ((E1000_ADDR(E1000_RDT) + 1) % E1000_RECEIVE_SIZE);
+	if (!(e1000_rx_queue[next_to_tail].status & E1000_RXD_STAT_DD)) return -E1000_ERROR_RECEIVE_QUEUE_EMPTY; // the receive queue is empty
+	if (e1000_rx_queue[next_to_tail].length > size) return -E1000_ERROR_PACKET_BUFFER_TOO_LARGE;
+	memcpy(buffer, e1000_rx_buffer[next_to_tail], e1000_rx_queue[next_to_tail].length);
+	e1000_rx_queue[next_to_tail].status &= ~E1000_RXD_STAT_DD; // unset the `DD` bit to re-use the descriptor
+	E1000_ADDR(E1000_RDT) = next_to_tail; // move the tail
+	return e1000_rx_queue[next_to_tail].length;
+}
+
 // LAB 6: Your driver code here
 int e1000_attach(struct pci_func *pciFunc) {
 	pci_func_enable(pciFunc);
diff --git a/lab/kern/syscall.c b/lab/kern/syscall.c
index 98be624..d87efc8 100644
--- a/lab/kern/syscall.c
+++ b/lab/kern/syscall.c
@@ -358,6 +358,11 @@ static int sys_transmit(const void *buffer, size_t size) {
 	return e1000_transmit(buffer, size);
 }
 
+static int sys_receive(void *buffer, size_t size) {
+	user_mem_assert(curenv, buffer, size, PTE_U);
+	return e1000_receive(buffer, size);
+}
+
 // Dispatches to the correct kernel function, passing the arguments.
 int32_t syscall(uint32_t syscallno, uint32_t a1, uint32_t a2, uint32_t a3, uint32_t a4, uint32_t a5) {
 	// Call the function corresponding to the 'syscallno' parameter.
@@ -379,6 +384,7 @@ int32_t syscall(uint32_t syscallno, uint32_t a1, uint32_t a2, uint32_t a3, uint3
 	case SYS_env_set_trapframe: return sys_env_set_trapframe(a1, (struct Trapframe *)a2);
 	case SYS_time_msec: return sys_time_msec();
 	case SYS_transmit: return sys_transmit((void *)a1, a2);
+	case SYS_receive: return sys_receive((void *)a1, a2);
 	default: return -E_INVAL;
 	}
 }
diff --git a/lab/inc/syscall.h b/lab/inc/syscall.h
index 53a0ca8..2de371a 100644
--- a/lab/inc/syscall.h
+++ b/lab/inc/syscall.h
@@ -19,6 +19,7 @@ enum {
 	SYS_ipc_recv,
 	SYS_time_msec,
 	SYS_transmit,
+	SYS_receive,
 	NSYSCALLS
 };
diff --git a/lab/lib/syscall.c b/lab/lib/syscall.c
index ac22030..e998d1b 100644
--- a/lab/lib/syscall.c
+++ b/lab/lib/syscall.c
@@ -126,3 +126,7 @@ sys_time_msec(void)
 int sys_transmit(const void *buffer, size_t size) {
 	return syscall(SYS_transmit, 0, (uint32_t)buffer, size, 0, 0, 0);
 }
+
+int sys_receive(void *buffer, size_t size) {
+	return syscall(SYS_receive, 0, (uint32_t)buffer, size, 0, 0, 0);
+}
diff --git a/lab/inc/lib.h b/lab/inc/lib.h
index fd3d19f..addf19f 100644
--- a/lab/inc/lib.h
+++ b/lab/inc/lib.h
@@ -61,6 +61,7 @@ int	sys_ipc_try_send(envid_t to_env, uint32_t value, void *pg, int perm);
 int	sys_ipc_recv(void *rcv_pg);
 unsigned int sys_time_msec(void);
 int sys_transmit(const void *buffer, size_t size);
+int sys_receive(void *buffer, size_t size);
 
 // This must be inlined.  Exercise for reader: why?
 static inline envid_t __attribute__((always_inline))
```

To verify the work, one needs to finish the Exercise 12.

## Exercise 12

>Implement *lab/net/input.c*.

This assignment requires to receive packets into read buffers from the receive descriptor queue. A received packet needs to be sent to the network server buffer `nsipcbuf`.

When one IPC a page to the network server, it will be reading from it for a while, which may result in missing packets, so one should handle this issue: do not immediately receive another packet in to the same physical page.

The implementation is as follows:
```diff
diff --git a/lab/net/input.c b/lab/net/input.c
index 4e08f0f..eeb0970 100644
--- a/lab/net/input.c
+++ b/lab/net/input.c
@@ -1,7 +1,19 @@
 #include "ns.h"
+#include <inc/lib.h>
+#include <kern/e1000.h>
+
+#define SLEEPTIME 16
 
 extern union Nsipc nsipcbuf;
 
+void sleep(int millisecond) {
+	unsigned now = sys_time_msec();
+	unsigned end = now + millisecond;
+	if ((int)now < 0 && (int)now > -MAXERROR) panic("sys_time_msec: %e", (int)now);
+	if (end < now) panic("sleep: wrap");
+	while (sys_time_msec() < end) sys_yield();
+}
+
 void
 input(envid_t ns_envid)
 {
@@ -13,4 +25,19 @@ input(envid_t ns_envid)
 	// Hint: When you IPC a page to the network server, it will be
 	// reading from it for a while, so don't immediately receive
 	// another packet in to the same physical page.
+	uint8_t inputBuffer[E1000_BUFFER_SIZE];
+	int r, i;
+	while (1) {
+		memset(inputBuffer, 0, sizeof(inputBuffer));
+		// read a packet from the receive descriptors queue
+		while ((r = sys_receive(inputBuffer, sizeof(inputBuffer))) == -E1000_ERROR_RECEIVE_QUEUE_EMPTY) sys_yield();
+		// panic if the received packet length < 0
+		if (r < 0) panic("ERROR: failed to receive packet");
+		// send it to the network server
+		nsipcbuf.pkt.jp_len = r;
+		memcpy(nsipcbuf.pkt.jp_data, inputBuffer, r);
+		ipc_send(ns_envid, NSREQ_INPUT, &nsipcbuf, PTE_P | PTE_U);
+		// wait for IPC
+		sleep(SLEEPTIME);
+	}
 }
```

The function `sleep` is used to wait for IPC read, and there will be missing packets without the calling to it at the last line.

Verify the work:
```shell
$ cd ~/mitos/lab && make E1000_DEBUG=TX,TXERR,RX,RXERR,RXFILTER run-net_testinput-nox
    ⋮
Sending ARP announcement...
e1000: index 0: 0x2d3680 : 900002a 0
e1000: unicast match[0]: 52:54:00:12:34:56
input: 0000   5254 0012 3456 5255  0a00 0202 0806 0001
input: 0010   0800 0604 0002 5255  0a00 0202 0a00 0202
input: 0020   5254 0012 3456 0a00  020f 0000 0000 0000
input: 0030   0000 0000 0000 0000  0000 0000 0000 0000

Waiting for packets...
    ⋮
```

Now the grader should pass the `testinput`:
```shell
$ cd ~/mitos/lab && make grade
    ⋮
testtime: OK (8.9s)
pci attach: OK (2.0s)
testoutput [5 packets]: OK (2.7s)
testoutput [100 packets]: OK (3.5s)
Part A score: 35/35

testinput [5 packets]: OK (3.6s)
testinput [100 packets]: OK (4.6s)
tcp echo server [echosrv]: OK (3.2s)
    ⋮
```

If one sees *missing packet* from the `testinput [100 packets]` test:
```shell
    ⋮
testinput [100 packets]: FAIL (3.6s)
    AssertionError: got ARP reply through packet 8/100
    unexpected packet:
    0000   5254 0012 3456 5255 0a00 0202 0800 4500   RT..4VRU......E.
    0010   0026 0008 0000 4011 62af 0a00 0202 0a00   ........b.......
    0020   020f 9b47 0007 0012 e2c1 5061 636b 6574   ...G......Packet
    0030   2030 3039 0000 0000 0000 0000             .009........
    missing packet 9/100:
    0000   5254 0012 3456 5255 0a00 0202 0800 4500   RT..4VRU......E.
    0010   0026 0008 0000 4011 62af 0a00 0202 0a00   ........b.......
    0020   020f 9b47 0007 0012 e2c1 5061 636b 6574   ...G......Packet
    0030   2030 3038 0000 0000 0000 0000             .008........
    got packet 10/100 through packet 100/100
    ⋮
```
one should increase the `SLEEPTIME` value.

## Question 2

>How did you structure your receive implementation? In particular, what do you do if the receive queue is empty and a user environment requests the next incoming packet?

The receive system call invokes the function `e1000_receive`, which returns the error code `-3` to indicate that the receive queue is empty. The function `e1000_receive` checks the `DD` bit of the next descriptor to the tail of the receive queue: if it is not set, the received packet must have been read, and the receive queue is empty. When the `input` helper detects the empty error code, it yields to other environments.

## Exercise 13

>The web server is missing the code that deals with sending the contents of a file back to the client. Finish the web server by implementing `send_file` and `send_data`.

A web server in its simplest form sends the contents of a file to the requesting client. The lab has provided skeleton code for a very simple web server in *lab/user/httpd.c*. The skeleton code deals with incoming connections and parses the headers.

As the exercise requires, if the requested file does not exist or is a directory, response the client a `404` error.

The implementation is as follows:
```diff
diff --git a/lab/user/httpd.c b/lab/user/httpd.c
index ede43bf..2f590db 100644
--- a/lab/user/httpd.c
+++ b/lab/user/httpd.c
@@ -77,7 +77,14 @@ static int
 send_data(struct http_request *req, int fd)
 {
 	// LAB 6: Your code here.
-	panic("send_data not implemented");
+	int r;
+	char buffer[BUFFSIZE];
+	while ((r = read(fd, buffer, BUFFSIZE)) > 0)
+		if (write(req->sock, buffer, r) != r) {
+			r = -1;
+			die("send_data: failed to response");
+		}
+	return r;
 }
 
 static int
@@ -216,6 +223,7 @@ send_file(struct http_request *req)
 	int r;
 	off_t file_size = -1;
 	int fd;
+	struct Stat stat;
 
 	// open the requested url for reading
 	// if the file does not exist, send a 404 error using send_error
@@ -223,7 +231,18 @@ send_file(struct http_request *req)
 	// set file_size to the size of the file
 
 	// LAB 6: Your code here.
-	panic("send_file not implemented");
+	fd = open(req->url, O_RDONLY);
+	if (fd < 0) {
+		r = send_error(req, 404);
+		goto end;
+	}
+	r = fstat(fd, &stat);
+	if (r < 0) goto end;
+	if (stat.st_isdir) {
+		r = send_error(req, 404);
+		goto end;
+	}
+	file_size = stat.st_size;
 
 	if ((r = send_header(req, 200)) < 0)
 		goto end;
```

Now verify the work. Firstly, start the web server (`ctrl+a x` to terminate it):
```shell
$ cd ~/mitos/lab && make run-httpd-nox
    ⋮
ns: 52:54:00:12:34:56 bound to static IP 10.0.2.15
NS: TCP/IP initialized.
Waiting for http connections...
```
Secondly, check its port in another session:
```shell
$ cd ~/mitos/lab && make which-ports
Local port 26001 forwards to JOS port 7 (echo server)
Local port 26002 forwards to JOS port 80 (web server)
```
The web server's port is `26002` here. Finally, request the *index.html*:
```shell
$ curl http://localhost:26002/index.html
<html>
<head>
	<title>jhttpd on JOS</title>
</head>
<body>
	<center>
		<h2>This file came from JOS.</h2>
		<marquee>Cheesy web page!</marquee>
	</center>
</body>
</html>
```

Now grade the work:
```shell
$ cd ~/mitos/lab && make grade
    ⋮
testtime: OK (9.1s)
pci attach: OK (2.4s)
testoutput [5 packets]: OK (3.6s)
testoutput [100 packets]: OK (3.9s)
Part A score: 35/35

testinput [5 packets]: OK (2.8s)
testinput [100 packets]: OK (4.8s)
tcp echo server [echosrv]: OK (3.2s)
web server [httpd]:
  http://localhost:26002/: OK (3.2s)
  http://localhost:26002/index.html: OK (3.1s)
  http://localhost:26002/random_file.txt: OK (3.7s)
Part B score: 70/70

Score: 105/105
```

## Challenge

>Read about the *EEPROM* in the developer's manual and write the code to load the *E1000*'s MAC address out of the *EEPROM*. Currently, QEMU's default MAC address is hard-coded into both your receive initialization and *lwIP*. Fix your initialization to use the MAC address you read from the *EEPROM*, add a system call to pass the MAC address to *lwIP*, and modify *lwIP* to the MAC address read from the card. Test your change by configuring QEMU to use a different MAC address.

The *EEPROM* device for storing product configuration information. The *EEPROM* is divided into four general regions:
- Hardware accessed: loaded by the Ethernet controller after power-up, PCI Reset deassertion, D3->D0 transition, or software commanded *EEPROM* reset (CTRL_EXT.EE_RST).
- ASF accessed: loaded by the Ethernet controller in ASF mode after power-up, ASF Soft Reset (ASF FRC_RST), or software commanded ASF EEPROM read (ASF FRC_EELD).
- Software accessed: used by software only.
- External BMC (TCO) accessed: loaded by an external BMC (TCO) from the SMBus after power up.

Software can use the *EEPROM Read register* (`EERD`) to cause the Ethernet controller to read a word from the *EEPROM* that the software can then use. To do this, software writes the address to read the *Read Address* (`EERD.ADDR`) field and then simultaneously writes a `1`b to the *Start Read bit* (`EERD.START`). The Ethernet controller then reads the word from the *EEPROM*, sets the *Read Done bit* (`EERD.DONE`), and puts the data in the *Read Data field* (`EERD.DATA`). Software can poll the *EEPROM Read register* until it sees the `EERD.DONE` bit set, then use the data from the `EERD.DATA` field. Any words read this way are not written to hardware’s internal registers.

The *E1000*'s MAC address is stored in *EEPROM* `00`h~`02`h.

I copy some necessary *EEPROM* declarations from QEMU's [e1000.h](http://github.com/qemu/u-boot/blob/master/drivers/net/e1000.h):
```c
#define E1000_EERD     0x00014	/* EEPROM Read - RW */
/* EEPROM Read */
#define E1000_EERD_START      0x00000001	/* Start Read */
#define E1000_EERD_DONE       0x00000010	/* Read Done */
#define E1000_EERD_ADDR_SHIFT 8
#define E1000_EERD_ADDR_MASK  0x0000FF00	/* Read Address */
#define E1000_EERD_DATA_SHIFT 16
#define E1000_EERD_DATA_MASK  0xFFFF0000	/* Read Data */
```

The whole implementation includes reading the MAC address from *EEPROM*, adding the system call, commenting out the hard-coded MAC address, and applying the system call:
```diff
diff --git a/lab/kern/e1000.h b/lab/kern/e1000.h
index 2badedf..208654a 100644
--- a/lab/kern/e1000.h
+++ b/lab/kern/e1000.h
@@ -119,4 +119,22 @@ static void e1000_rx_init();
 
 int e1000_receive(void *buffer, size_t size);
 
+#define E1000_EERD     0x00014	/* EEPROM Read - RW */
+
+/* EEPROM Read */
+#define E1000_EERD_START      0x00000001	/* Start Read */
+#define E1000_EERD_DONE       0x00000010	/* Read Done */
+#define E1000_EERD_ADDR_SHIFT 8
+#define E1000_EERD_ADDR_MASK  0x0000FF00	/* Read Address */
+#define E1000_EERD_DATA_SHIFT 16
+#define E1000_EERD_DATA_MASK  0xFFFF0000	/* Read Data */
+
+/* EEPROM position of MAC Address */
+#define E1000_EEPROM_MAC_0 0x00000000
+#define E1000_EEPROM_MAC_1 0x00000001
+#define E1000_EEPROM_MAC_2 0x00000002
+
+static uint16_t read_eeprom(uint8_t addr);
+int64_t get_mac_from_eeprom();
+
 #endif  // SOL >= 6
diff --git a/lab/kern/e1000.c b/lab/kern/e1000.c
index 63d4648..fa6cebd 100644
--- a/lab/kern/e1000.c
+++ b/lab/kern/e1000.c
@@ -45,8 +45,8 @@ static void e1000_rx_init() {
 	for (i = 0; i < E1000_RECEIVE_SIZE; i++) e1000_rx_queue[i].addr = PADDR(e1000_rx_buffer[i]);
 
 	/* init the receive address */
-	E1000_ADDR(E1000_RAL) = QEMU_MAC_LOW;
-	E1000_ADDR(E1000_RAH) = QEMU_MAC_HIGH;
+	/* E1000_ADDR(E1000_RAL) = QEMU_MAC_LOW; */
+	/* E1000_ADDR(E1000_RAH) = QEMU_MAC_HIGH; */
 	E1000_ADDR(E1000_RAH) |= E1000_RAH_AV; // set the `Address Valid' bit in `RAH'
 
 	/* init receive descriptor registers: */
@@ -76,6 +76,20 @@ int e1000_receive(void *buffer, size_t size) {
 	return e1000_rx_queue[next_to_tail].length;
 }
 
+static uint16_t read_eeprom(uint8_t addr) {
+	E1000_ADDR(E1000_EERD) &= ~(E1000_EERD_ADDR_MASK | E1000_EERD_DATA_MASK); // reset to prepare
+	E1000_ADDR(E1000_EERD) |= (addr << E1000_EERD_ADDR_SHIFT); // write the address to read the *Read Address* (`EERD.ADDR`) field
+	E1000_ADDR(E1000_EERD) |= E1000_EERD_START; // write a `1`b to the *Start Read bit* (`EERD.START`)
+	while (!(E1000_ADDR(E1000_EERD) & E1000_EERD_DONE));  // poll the *EEPROM Read register* until it sees the `EERD.DONE` bit set
+	return (E1000_ADDR(E1000_EERD) & E1000_EERD_DATA_MASK) >> E1000_EERD_DATA_SHIFT; // read the data in the *Read Data field* (`EERD.DATA`)
+}
+
+int64_t get_mac_from_eeprom() {
+	return read_eeprom(E1000_EEPROM_MAC_0) |
+		(read_eeprom(E1000_EEPROM_MAC_1) << 16) |
+		((uint64_t)read_eeprom(E1000_EEPROM_MAC_2) << 32);
+}
+
 // LAB 6: Your driver code here
 int e1000_attach(struct pci_func *pciFunc) {
 	pci_func_enable(pciFunc);
diff --git a/lab/kern/syscall.c b/lab/kern/syscall.c
index d87efc8..84a3125 100644
--- a/lab/kern/syscall.c
+++ b/lab/kern/syscall.c
@@ -363,6 +363,13 @@ static int sys_receive(void *buffer, size_t size) {
 	return e1000_receive(buffer, size);
 }
 
+static int sys_mac_addr(void *buffer, size_t size) {
+    if (size < 6) return -E_INVAL;
+    uint64_t mac_addr = get_mac_from_eeprom();
+    memcpy(buffer, (uint8_t *)&mac_addr, 6);
+    return 0;
+}
+
 // Dispatches to the correct kernel function, passing the arguments.
 int32_t syscall(uint32_t syscallno, uint32_t a1, uint32_t a2, uint32_t a3, uint32_t a4, uint32_t a5) {
 	// Call the function corresponding to the 'syscallno' parameter.
@@ -385,6 +392,7 @@ int32_t syscall(uint32_t syscallno, uint32_t a1, uint32_t a2, uint32_t a3, uint3
 	case SYS_time_msec: return sys_time_msec();
 	case SYS_transmit: return sys_transmit((void *)a1, a2);
 	case SYS_receive: return sys_receive((void *)a1, a2);
+	case SYS_mac_addr: return sys_mac_addr((void *)a1, a2);
 	default: return -E_INVAL;
 	}
 }
diff --git a/lab/inc/syscall.h b/lab/inc/syscall.h
index 2de371a..c354b4c 100644
--- a/lab/inc/syscall.h
+++ b/lab/inc/syscall.h
@@ -20,6 +20,7 @@ enum {
 	SYS_time_msec,
 	SYS_transmit,
 	SYS_receive,
+	SYS_mac_addr,
 	NSYSCALLS
 }; 
diff --git a/lab/lib/syscall.c b/lab/lib/syscall.c
index de0ab2f..c4a7ae3 100644
--- a/lab/lib/syscall.c
+++ b/lab/lib/syscall.c
@@ -130,3 +130,7 @@ int sys_transmit(const void *buffer, size_t size) {
 int sys_receive(void *buffer, size_t size) {
 	return syscall(SYS_receive, 0, (uint32_t)buffer, size, 0, 0, 0);
 }
+
+int sys_mac_addr(void *buffer, size_t size) {
+    return syscall(SYS_mac_addr, 0, (uint32_t)buffer, size, 0, 0, 0);
+}
diff --git a/lab/inc/lib.h b/lab/inc/lib.h
index be432b3..de1fc8a 100644
--- a/lab/inc/lib.h
+++ b/lab/inc/lib.h
@@ -62,6 +62,7 @@ int	sys_ipc_recv(void *rcv_pg);
 unsigned int sys_time_msec(void);
 int sys_transmit(const void *buffer, size_t size);
 int sys_receive(void *buffer, size_t size);
+int sys_mac_addr(void *buffer, size_t size);
 
 // This must be inlined.  Exercise for reader: why?
 static inline envid_t __attribute__((always_inline))
diff --git a/lab/net/lwip/jos/jif/jif.c b/lab/net/lwip/jos/jif/jif.c
index 54f89ab..e584c5c 100644
--- a/lab/net/lwip/jos/jif/jif.c
+++ b/lab/net/lwip/jos/jif/jif.c
@@ -61,12 +61,13 @@ low_level_init(struct netif *netif)
     netif->flags = NETIF_FLAG_BROADCAST;
 
     // MAC address is hardcoded to eliminate a system call
-    netif->hwaddr[0] = 0x52;
-    netif->hwaddr[1] = 0x54;
-    netif->hwaddr[2] = 0x00;
-    netif->hwaddr[3] = 0x12;
-    netif->hwaddr[4] = 0x34;
-    netif->hwaddr[5] = 0x56;
+    /* netif->hwaddr[0] = 0x52; */
+    /* netif->hwaddr[1] = 0x54; */
+    /* netif->hwaddr[2] = 0x00; */
+    /* netif->hwaddr[3] = 0x12; */
+    /* netif->hwaddr[4] = 0x34; */
+    /* netif->hwaddr[5] = 0x56; */
+    if (sys_mac_addr(netif->hwaddr, netif->hwaddr_len) < 0) panic("jif: failed to read mac address from eeprom");
 }
 
 /*
diff --git a/lab/net/testinput.c b/lab/net/testinput.c
index 22cf937..3275ac7 100644
--- a/lab/net/testinput.c
+++ b/lab/net/testinput.c
@@ -16,7 +16,9 @@ announce(void)
 	// listens for very specific ARP requests, such as requests
 	// for the gateway IP.
 
-	uint8_t mac[6] = {0x52, 0x54, 0x00, 0x12, 0x34, 0x56};
+	/* uint8_t mac[6] = {0x52, 0x54, 0x00, 0x12, 0x34, 0x56}; */
+	uint8_t mac[6];
+	if (sys_mac_addr(mac, sizeof(mac)) < 0) panic("failed to read mac address from eeprom");
 	uint32_t myip = inet_addr(IP);
 	uint32_t gwip = inet_addr(DEFAULT);
 	int r;
```

Verify the work:
```shell
$ cd ~/mitos/lab && make grade
    ⋮
testtime: OK (9.8s)
pci attach: OK (2.0s)
testoutput [5 packets]: OK (2.6s)
testoutput [100 packets]: OK (4.4s)
Part A score: 35/35

testinput [5 packets]: OK (2.7s)
testinput [100 packets]: OK (4.6s)
tcp echo server [echosrv]: OK (3.3s)
web server [httpd]:
  http://localhost:26002/: OK (3.1s)
  http://localhost:26002/index.html: OK (3.2s)
  http://localhost:26002/random_file.txt: OK (3.2s)
Part B score: 70/70

Score: 105/105
```

## Question 3

>What does the web page served by JOS's web server say?

As what has been done for the second step of Exercise 13, the response returned is as follows:
```html
<html>
<head>
	<title>jhttpd on JOS</title>
</head>
<body>
	<center>
		<h2>This file came from JOS.</h2>
		<marquee>Cheesy web page!</marquee>
	</center>
</body>
</html>
```

## Question 4

>How long approximately did it take you to do this lab?

It took me about two months.

✌️
