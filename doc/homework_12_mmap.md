Homework 12


[Homework 12](http://pdos.csail.mit.edu/6.828/2018/homework/mmap.html)

In this assignment, the program maintains a very large table of square root values in virtual memory, and the table is too large to fit in physical RAM.

The assginment requires to load a set of square root values into physical RAM dynamically. We will use `mmap` and `munmap` to allocate physical space dynamically. Specifically, we will implement the handler function to handle the `SIGSEGV` page fault. `munmap` is used to release the previous allocated RAM space because the whole table of square root values is too large to fit in RAM.

I place the *mmap.c* file in the directory *homework*.

The signature of the function `calculate_sqrts` is defined as follows:
```c
static void calculate_sqrts(double *sqrt_pos, int start, int nr)
```
It calculates the square root values from `start` to `start + nr - 1` and store them in the physical RAM starting from `sqrt_pos`.

My implementation of the function `handle_sigsegv` is as follows:
```c
static void *cur;

static void handle_sigsegv(int sig, siginfo_t *si, void *ctx) {
	if (cur) {
		int r = 0;
		r = munmap(cur, page_size);
		if (r != 0) {
			fprintf(stderr, "ERROR: failed to munmap() region for sqrt table; %s\n", strerror(errno));
			exit(EXIT_FAILURE);
		}
	}
	uintptr_t fault_addr = (uintptr_t)si->si_addr;
	cur = (void *)align_down(fault_addr, page_size);
	cur = mmap(cur, page_size, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, 0, 0);
	if (cur < 0) {
		fprintf(stderr, "ERROR: failed to mmap() region for sqrt table; %s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
	calculate_sqrts(cur, (cur - (void *)sqrts) / sizeof(double), page_size / sizeof(double));
}
```
Only the argument `si` is actually used here to grab the position of the page fault. `cur` is used to trace the current start RAM address to store square root values. Firstly, the previous allocated RAM space used to store previous squared root values is released by calling `munmap`. Then, `mmap` is called to allocate a new page of RAM, and the start address `cur` is for the first parameter of `calculate_sqrts`. `(cur - (void *)sqrts) / sizeof(double)` calculates the new start number that will be computed to its square root value. `page_size / sizeof(double)` calculates how many squared root values need to be calculated for the new allocated page.

Verify the work:
```shell
$ cd ~/mitos/homework && gcc mmap.c -lm -o mmap && ./mmap
page_size is 4096
Validating square root table contents...
All tests passed!
```

✌️
