
[Homework 4](http://pdos.csail.mit.edu/6.828/2018/homework/xv6-zero-fill.html)

# Homework: xv6 Lazy Page Allocation

## Part One: Eliminate Allocation from `sbrk()`

The system call `sbrk` is used to maintain the *heap* area of a process. Do the change as hint:
```diff
diff --git a/homework/xv6-public/sysproc.c b/homework/xv6-public/sysproc.c
index 597783a..dd5507d 100644
--- a/homework/xv6-public/sysproc.c
+++ b/homework/xv6-public/sysproc.c
@@ -51,8 +51,8 @@ sys_sbrk(void)
   if(argint(0, &n) < 0)
     return -1;
   addr = myproc()->sz;
-  if(growproc(n) < 0)
-    return -1;
   return addr;
 }
```
N.B.: the process's size still needs to be increased as is mentioned in the assignment:
>but you still need to increase the process's size!

Otherwise, *Part Two* may be in trouble. So, the final change should be:
```diff
diff --git a/homework/xv6-public/sysproc.c b/homework/xv6-public/sysproc.c
index 597783a..d628959 100644
--- a/homework/xv6-public/sysproc.c
+++ b/homework/xv6-public/sysproc.c
@@ -51,8 +51,9 @@ sys_sbrk(void)
   if(argint(0, &n) < 0)
     return -1;
   addr = myproc()->sz;
-  if(growproc(n) < 0)
-    return -1;
+  /* if(growproc(n) < 0) */
+  /*   return -1; */
+  myproc()->sz += n;
   return addr;
 }
```

In the directory `homework/xv6-public` run the following command to start the modified xv6 shell:
```shell
$ make clean && make qemu-nox
```
Then, run the command:
```shell
$ echo hi
pid 3 sh: trap 14 err 6 on cpu 0 eip 0x111c addr 0x4004--kill proc
```

The system call `sbrk` uses `growproc` to shrink or to grow the user heap, i.e. deallocating and allocating memory. Now this is removed, no memory will be actually maintained. Then, when MMU locates an absent page table entry, the *page fault* interrupt/trap happends.

## Part Two: Lazy Allocation

This part requires to allocate user memory of the process in the *page fault* trap handler, `trap` in *homework/xv6-public/trap.c*. The work copies most of the function `allocuvm` in *homework/xv6-public/vm.c*, and follows the hints:
>Hint: look at the cprintf arguments to see how to find the virtual address that caused the page fault.
>
>Hint: steal code from allocuvm() in vm.c, which is what sbrk() calls (via growproc()).
>
>Hint: use PGROUNDDOWN(va) to round the faulting virtual address down to a page boundary.
>
>Hint: break or return in order to avoid the cprintf and the myproc()->killed = 1.
>
>Hint: you'll need to call mappages(). In order to do this you'll need to delete the static in the declaration of mappages() in vm.c, and you'll need to declare mappages() in trap.c. Add this declaration to trap.c before any call to mappages():
>
>      int mappages(pde_t *pgdir, void *va, uint size, uint pa, int perm);
>
>Hint: you can check whether a fault is a page fault by checking if tf->trapno is equal to T_PGFLT in trap(). 

N.B.: here use `PGROUNDDOWN`, instead of `PGROUNDUP`.

The change is as follows:
```diff
diff --git a/homework/xv6-public/vm.c b/homework/xv6-public/vm.c
index 7134cff..0275cec 100644
--- a/homework/xv6-public/vm.c
+++ b/homework/xv6-public/vm.c
@@ -57,7 +57,7 @@ walkpgdir(pde_t *pgdir, const void *va, int alloc)
 // Create PTEs for virtual addresses starting at va that refer to
 // physical addresses starting at pa. va and size might not
 // be page-aligned.
-static int
+int
 mappages(pde_t *pgdir, void *va, uint size, uint pa, int perm)
 {
   char *a, *last;
diff --git a/homework/xv6-public/trap.c b/homework/xv6-public/trap.c
index 41c66eb..76fa77c 100644
--- a/homework/xv6-public/trap.c
+++ b/homework/xv6-public/trap.c
@@ -13,6 +13,7 @@ struct gatedesc idt[256];
 extern uint vectors[];  // in vectors.S: array of 256 entry pointers
 struct spinlock tickslock;
 uint ticks;
+int mappages(pde_t *pgdir, void *va, uint size, uint pa, int perm);

 void
 tvinit(void)
@@ -77,7 +78,26 @@ trap(struct trapframe *tf)
             cpuid(), tf->cs, tf->eip);
     lapiceoi();
     break;
-
+  case T_PGFLT: {
+    char* mem;
+    uint a = PGROUNDDOWN(rcr2());
+    for (; a < myproc()->sz; a += PGSIZE) {
+      mem = kalloc();
+      if(mem == 0){
+        cprintf("allocuvm out of memory\n");
+        deallocuvm(myproc()->pgdir, myproc()->sz, myproc()->tf->eax);
+        return;
+      }
+      memset(mem, 0, PGSIZE);
+      if(mappages(myproc()->pgdir, (char*)a, PGSIZE, V2P(mem), PTE_W|PTE_U) < 0){
+        cprintf("allocuvm out of memory (2)\n");
+        deallocuvm(myproc()->pgdir, myproc()->sz, myproc()->tf->eax);
+        kfree(mem);
+        return;
+      }
+    }
+    break;
+  }
   //PAGEBREAK: 13
   default:
     if(myproc() == 0 || (tf->cs&3) == 0){
```

In the directory `homework/xv6-public` run the following command to start the modified xv6 shell:
```shell
$ make clean && make qemu-nox
```
Then, run the command:
```shell
$ echo hi
hi
```

✌
