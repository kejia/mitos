
[Homework 9](http://pdos.csail.mit.edu/6.828/2018/homework/barrier.html)

# Barriers

> Implement a *barrier* using condition variables provided by the *pthread* library.

A barrier is a point in an application at which all threads must wait until all other threads reach that point, too. Condition variables are a sequence coordination technique similar to xv6's sleep and wakeup.

In this assignment, multiple threads are created and access the same memory (`bstate.round`). Each of the threads mainly executes a loop, and a *round* is when all threads immediately finish the same time of iteration. The assignment expects every thread is blocked at a barrier at a single iteration and proceeds after all threads wait at the barrier (finish one `round`).

To make the program work, the following *clock* related `pthread` functions should be used:
```c
pthread_mutex_t lock;     // declare a lock
pthread_mutex_init(&lock, NULL);   // initialize the lock
pthread_mutex_lock(&lock);  // acquire lock
pthread_mutex_unlock(&lock);  // release lock
```
And another two functions are needed:
```c
pthread_cond_wait(&cond, &mutex);  // go to sleep on cond, releasing lock mutex
pthread_cond_broadcast(&cond);     // wake up every thread sleeping on cond
```
Check with `man pthread` for details on the functions.

The function `barrier` should be completed only:
```c
static void barrier() {
	pthread_mutex_lock(&bstate.barrier_mutex);
	if (bstate.nthread + 1 != nthread) {
		bstate.nthread++;
		pthread_cond_wait(&bstate.barrier_cond, &bstate.barrier_mutex);
	} else { // the last thread arrives
		bstate.nthread = 0;
		bstate.round++;
		if (nthread > 1) pthread_cond_broadcast(&bstate.barrier_cond);
	}
	pthread_mutex_unlock(&bstate.barrier_mutex);
}
```

Verify the work in the directory *homework*:
```shell
$ gcc -g -O2 -pthread barrier.c && ./a.out 1
OK; passed
$ gcc -g -O2 -pthread barrier.c && ./a.out 2
OK; passed
$ gcc -g -O2 -pthread barrier.c && ./a.out 3
OK; passed
$ gcc -g -O2 -pthread barrier.c && ./a.out 4
OK; passed
$ gcc -g -O2 -pthread barrier.c && ./a.out 5
OK; passed
```

✌️
