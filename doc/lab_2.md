
[Lab 2](http://pdos.csail.mit.edu/6.828/2018/labs/lab2/#Part-1--Physical-Page-Management)

In this lab, two components are required. The first one is a *physical memory allocator*. It should operate in units of `4096` bytes, called `pages`. The second component is *virtual memory management*.

One of the purpose of *virtual addressing* is unifiying external storage addressing with internal (high speed) storage addressing.

# Part 1: Physical Page Management

## Exercise 1

> In the file kern/pmap.c, you must implement code for the following functions (probably in the order given).
> 
> - boot_alloc()
> - mem\_init() (only up to the call to check\_page\_free\_list(1))
> - page_init()
> - page_alloc()
> - page_free()
> 
> check\_page\_free\_list() and check\_page\_alloc() test your physical page allocator. You should boot JOS and see whether check\_page\_alloc() reports success. Fix your code so that it passes. You may find it helpful to add your own assert()s to verify that your assumptions are correct.

`boot_alloc()`:
```c
	result = nextfree;
	char* new_next_free = ROUNDUP(nextfree + n, PGSIZE);
	if ((uint32_t)new_next_free > KERNBASE + (PGSIZE * npages))
		panic("boot memory allocation panic\n");
	else
		nextfree = new_next_free;
		
	return result;
```

`mem_init()`:
```c
	size_t boot_alloc_size = sizeof(struct PageInfo) * npages;
	pages = (struct PageInfo*) boot_alloc(boot_alloc_size);
	memset(pages, 0, boot_alloc_size);
```

`page_init()`:
```c
void page_init(void) {
	pages[0].pp_ref = 1;
	pages[0].pp_link = NULL;
	size_t io_start = IOPHYSMEM / PGSIZE - 1, io_end = PADDR(boot_alloc(0)) / PGSIZE;
	size_t i;
	for (i = 1; i < npages; i++) {
		if (i > io_start && i < io_end) {
			pages[i].pp_ref = 1;
			pages[i].pp_link = NULL;
		} else {
			pages[i].pp_ref = 0;
			pages[i].pp_link = page_free_list;
			page_free_list = &pages[i];
		}
	}
}
```

`page_alloc()`:
```c
struct PageInfo* page_alloc(int alloc_flags) {
	struct PageInfo* r = page_free_list;
	if (r != NULL) {
		page_free_list = page_free_list->pp_link;
		r->pp_link = NULL;
		if (alloc_flags & ALLOC_ZERO)
			memset(page2kva(r), '\0', PGSIZE);
	}
	return r;
}
```

`page_free()`:
```c
void page_free(struct PageInfo* pp) {
	if (!(pp->pp_ref || pp->pp_link)) {
		pp->pp_link = page_free_list;
		page_free_list = pp;
	} else panic("cannot free the page\n");
}
```

# Part 2: Virtual Memory

The statement of this part is useful for understanding *virtual*, *linear*, and *physical address*:

> In x86 terminology, a *virtual address* consists of a segment selector and an offset within the segment. A *linear address* is what you get after segment translation but before page translation. A *physical address* is what you finally get after both segment and page translation and what ultimately goes out on the hardware bus to your RAM.
> 
> ```
>            Selector  +--------------+         +-----------+
>           ---------->|              |         |           |
>                      | Segmentation |         |  Paging   |
> Software             |              |-------->|           |---------->  RAM
>             Offset   |  Mechanism   |         | Mechanism |
>           ---------->|              |         |           |
>                      +--------------+         +-----------+
>             Virtual                   Linear                Physical
> ```
> 
> A C pointer is the "*offset*" component of the *virtual address*.

See [x86 Manual: Chapter 5: Page Translation](http://pdos.csail.mit.edu/6.828/2018/readings/i386/s05_02.htm) for illustration on *Format of a Linear Address* (which is identical to a *virtual* address in JOS):

![截屏2020-11-01 18.30.11.png](../_resources/038b1e9b7d2140a59d8cb8541a4658e4.png)

## Exercise 2

> Look at chapters 5 and 6 of the [Intel 80386 Reference Manual](http://pdos.csail.mit.edu/6.828/2018/readings/i386/toc.htm), if you haven't done so already. Read the sections about page translation and page-based protection closely (5.2 and 6.4). We recommend that you also skim the sections about segmentation; while JOS uses the paging hardware for virtual memory and protection, segment translation and segment-based protection cannot be disabled on the x86, so you will need a basic understanding of it.

## Exercise 3

> While GDB can only access QEMU's memory by virtual address, it's often useful to be able to inspect physical memory while setting up virtual memory. Review the [QEMU monitor commands](http://pdos.csail.mit.edu/6.828/2018/labguide.html#qemu) from the lab tools guide, especially the `xp` command, which lets you inspect physical memory. To access the QEMU monitor, press `Ctrl-a c` in the terminal (the same binding returns to the serial console).
> 
> Use the `xp` command in the QEMU monitor and the `x` command in GDB to inspect memory at corresponding physical and virtual addresses and make sure you see the same data.
> 
> Our patched version of QEMU provides an `info pg` command that may also prove useful: it shows a compact but detailed representation of the current page tables, including all mapped memory ranges, permissions, and flags. Stock QEMU also provides an `info mem` command that shows an overview of which ranges of virtual addresses are mapped and with what permissions.

The translation from *virtual* to *physical* addresses in JOS is simplified. In JOS, a *linear* address is equal to the *offset* of its *virtual* address, and the start *virtual* address `0xf0000000` is mapped to the *physical* address `0x00000000`. Thus, there is the mapping: $\mathit{pa} = \mathit{va} - \mathbf{0xf0000000} + \mathbf{0x00000000} = \mathit{va} - \mathbf{0xf0000000}$. Here, $\mathit{pa}$ is a *physical* address, and $\mathit{va}$ is a *virtual* address. In JOS, the function `KADDR(pa)` is used to translate a *physical* address to a *virtual* address, and the function `PADDR(va)` is used to translate a *virtual* address to a *physical* address.

Now verify that mapping, e.g. check whether the contents of the *virtual* address `0xf0100000` is identical to the contents of the *physical* address `0x100000 = 0xf0100000 - 0xf0000000`.

Start a qemu-gdb session:
```shell
$ make qemu-nox-gdb
```
and then start a GDB session:
```shell
$ make gdb
	⋮
(gdb) c
Continuing.
```
and then the qemu-gdb session is in JOS:
```shell
Welcome to the JOS kernel monitor!
Type 'help' for a list of commands.
K>
```
In the GDB session, input the following command after interuption via `ctrl+c`:
```shell
(gdb) x/8x 0xf0100000
0xf0100000 <_start+4026531828>:	0x1badb002	0x00000000	0xe4524ffe	0x7205c766
0xf0100010 <entry+4>:	0x34000004	0x1000b812	0x220f0011	0xc0200fd8
(gdb)
```
In the qemu-gdb session, input the following command after interuption via `ctrl+a c`:
```shell
(qemu) xp/x 0x100000
0000000000100000: 0x1badb002
(qemu) xp/8x 0x100000
0000000000100000: 0x1badb002 0x00000000 0xe4524ffe 0x7205c766
0000000000100010: 0x34000004 0x1000b812 0x220f0011 0xc0200fd8
(qemu)
```
It is verified that the contents of the *virtual* address `0xf0100000` is identical to the contents of the *physical* address `0x100000 = 0xf0100000 - 0xf0000000`.

N.B.: GDB can only access QEMU's memory by virtual address.

The excerpt on *Memory Management Unit (MMU)* is useful to understand address translation:
>From code executing on the CPU, once we're in protected mode (which we entered first thing in boot/boot.S), there's no way to directly use a linear or physical address. All memory references are interpreted as virtual addresses and translated by the MMU, which means all pointers in C are virtual addresses.

The following is an example on a simplified version of address translation [[1]](#1):

![mmu_example.png](../_resources/f293d855030641c8aa8aa8bc91707497.png)

And another simplified demonstration:

![截屏2020-10-30 13.24.32.png](../_resources/96a92edadcdb45e49a9afa9c56f3ed6c.png)

### Question 1

>Assuming that the following JOS kernel code is correct, what type should variable x have, uintptr_t or physaddr_t?
>
>```c
>	mystery_t x;
>	char* value = return_a_pointer();
>	*value = 10;
>	x = (mystery_t) value;
>```

`value` is a pointer, which is a *virtual address* as mentioned before, so `x` should be a *virtual* address, i.e. `uintptr_t` in JOS.

## Exercise 4

>In the file kern/pmap.c, you must implement code for the following functions.
>
>```c
>        pgdir_walk()
>        boot_map_region()
>        page_lookup()
>        page_remove()
>        page_insert()
>```
>
>check_page(), called from mem_init(), tests your page table management routines. You should make sure it reports success before proceeding. 

[Page Translation](http://pdos.csail.mit.edu/6.828/2018/readings/i386/s05_02.htm) should be studied in order to finish the exercise. The *two-level page table structure* is shown as follows (see [Proteced-mode Address Translation](http://pdos.csail.mit.edu/6.828/2018/lec/x86_translation_and_registers.pdf)):

![截屏2020-10-24 20.11.37.png](../_resources/92ffa9d8446b4ca1ae02a7faa6d185b9.png)

`pgdir_walk()`:
```c
pte_t* pgdir_walk(pde_t* pgdir, const void* va, int create) {
	pde_t* page_dir_entry = pgdir + PDX(va);
	if (!(*page_dir_entry & PTE_P)) {
		if (!create) return NULL;
		struct PageInfo* page_info = page_alloc(ALLOC_ZERO);
		if (page_info == NULL) return NULL;
		page_info->pp_ref++;
		*page_dir_entry = page2pa(page_info) | PTE_P | PTE_W | PTE_U; // get the physical address with the page_info and set the flags
	}
	pte_t* page_table_entry = KADDR(PTE_ADDR(*page_dir_entry)); // convert to the virtual address
	page_table_entry += PTX(va);
	return page_table_entry;
}
```

`boot_map_region()`:
```c
static void boot_map_region(pde_t* pgdir, uintptr_t va, size_t size, physaddr_t pa, int perm) {
	int end = size / PGSIZE;
	pte_t* page_table_entry;
	for (int i = 0; i < end; i++) {
		page_table_entry = pgdir_walk(pgdir, (void*) va, 1);
		if (page_table_entry == NULL) panic("fail to memory allocation\n");
		*page_table_entry = pa | perm | PTE_P;
		va += PGSIZE;
		pa += PGSIZE;
	}
}
```

`page_lookup()`:
```c
struct PageInfo* page_lookup(pde_t* pgdir, void* va, pte_t** pte_store) {
	pte_t* page_table_entry = pgdir_walk(pgdir, va, 0);
	if (page_table_entry == NULL || !(*page_table_entry & PTE_P)) return NULL;
	if (pte_store) *pte_store = page_table_entry; // **pte_store may be modified out of page_lookup, e.g. in page_remove
	struct PageInfo* page_info = pa2page(PTE_ADDR(*page_table_entry));
	return page_info;
}
```

`page_remove()`:
```c
void page_remove(pde_t* pgdir, void* va) {
	struct PageInfo* page_info;
	pte_t* page_table_entry;
	page_info = page_lookup(pgdir, va, &page_table_entry);
	if (page_info == NULL) return;
	*page_table_entry = 0;
	page_decref(page_info);
	tlb_invalidate(pgdir, va);
}
```
N.B.: *TLB*, Translation-Lookasice Buffer, is another type of Page-table cache, generally faster than main memory.

`page_insert()`:
```c
int page_insert(pde_t* pgdir, struct PageInfo* pp, void* va, int perm) {
	pte_t* page_table_entry = pgdir_walk(pgdir, va, 1);
	if (page_table_entry == NULL) return -E_NO_MEM;
	pp->pp_ref++; // tackle the re-insertion corner case
	if (*page_table_entry & PTE_P) page_remove(pgdir, va);
	*page_table_entry = (page2pa(pp) | perm | PTE_P);
	return 0;
}
```

# Part 3: Kernel Address Space

The memory partition is shown in the diagram below (see: *lab/inc/memlayout.h*):

```text
 Virtual memory map:                                Permissions
                                                    kernel/user

    4 Gig -------->  +------------------------------+
                     |                              | RW/--
                     ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                     :              .               :
                     :              .               :
                     :              .               :
                     |~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~| RW/--
                     |                              | RW/--
                     |   Remapped Physical Memory   | RW/--
                     |                              | RW/--
    KERNBASE, ---->  +------------------------------+ 0xf0000000      --+
    KSTACKTOP        |     CPU0's Kernel Stack      | RW/--  KSTKSIZE   |
                     | - - - - - - - - - - - - - - -|                   |
                     |      Invalid Memory (*)      | --/--  KSTKGAP    |
                     +------------------------------+                   |
                     |     CPU1's Kernel Stack      | RW/--  KSTKSIZE   |
                     | - - - - - - - - - - - - - - -|                 PTSIZE
                     |      Invalid Memory (*)      | --/--  KSTKGAP    |
                     +------------------------------+                   |
                     :              .               :                   |
                     :              .               :                   |
    MMIOLIM ------>  +------------------------------+ 0xefc00000      --+
                     |       Memory-mapped I/O      | RW/--  PTSIZE
 ULIM, MMIOBASE -->  +------------------------------+ 0xef800000
                     |  Cur. Page Table (User R-)   | R-/R-  PTSIZE
    UVPT      ---->  +------------------------------+ 0xef400000
                     |          RO PAGES            | R-/R-  PTSIZE
    UPAGES    ---->  +------------------------------+ 0xef000000
                     |           RO ENVS            | R-/R-  PTSIZE
 UTOP,UENVS ------>  +------------------------------+ 0xeec00000
 UXSTACKTOP -/       |     User Exception Stack     | RW/RW  PGSIZE
                     +------------------------------+ 0xeebff000
                     |       Empty Memory (*)       | --/--  PGSIZE
    USTACKTOP  --->  +------------------------------+ 0xeebfe000
                     |      Normal User Stack       | RW/RW  PGSIZE
                     +------------------------------+ 0xeebfd000
                     |                              |
                     |                              |
                     ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                     .                              .
                     .                              .
                     .                              .
                     |~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~|
                     |     Program Data & Heap      |
    UTEXT -------->  +------------------------------+ 0x00800000
    PFTEMP ------->  |       Empty Memory (*)       |        PTSIZE
                     |                              |
    UTEMP -------->  +------------------------------+ 0x00400000      --+
                     |       Empty Memory (*)       |                   |
                     | - - - - - - - - - - - - - - -|                   |
                     |  User STAB Data (optional)   |                 PTSIZE
    USTABDATA ---->  +------------------------------+ 0x00200000        |
                     |       Empty Memory (*)       |                   |
    0 ------------>  +------------------------------+                 --+

```

## Exercise 5

>Fill in the missing code in mem_init() after the call to check_page().
>
>Your code should now pass the check_kern_pgdir() and check_page_installed_pgdir() checks. 

This part mainly asks to allocate space for *UPAGES*, the kernel stack, and the whole kernel. The tasks are done with the function *boot_map_region* in terms of question hints:

```c
// allocate for UPAGES
boot_map_region(kern_pgdir, UPAGES, PTSIZE, PADDR(pages), PTE_U | PTE_P);

// allocate for the kernel stack
boot_map_region(kern_pgdir, KSTACKTOP - KSTKSIZE, KSTKSIZE, PADDR(bootstack), PTE_W | PTE_P);

// allocate for the whole kernel
boot_map_region(kern_pgdir, KERNBASE, 4294967295 - KERNBASE, 0, PTE_W | PTE_P);
```

Test the work:
```shell
$ make qemu-nox
+ cc kern/pmap.c
+ ld obj/kern/kernel
ld: warning: section `.bss' type changed to PROGBITS
+ mk obj/kern/kernel.img
***
*** Use Ctrl-a x to exit qemu
***
/home/vagrant/mitos/qemu/dist/bin/qemu-system-i386 -nographic -drive file=obj/kern/kernel.img,index=0,media=disk,format=raw -serial mon:stdio -gdb tcp::26000 -D qemu.log
6828 decimal is XXX octal!
Physical memory: 131072K available, base = 640K, extended = 130432K
check_page_free_list() succeeded!
check_page_alloc() succeeded!
check_page() succeeded!
check_kern_pgdir() succeeded!
check_page_free_list() succeeded!
check_page_installed_pgdir() succeeded!
entering test_backtrace 5
entering test_backtrace 4
entering test_backtrace 3
entering test_backtrace 2
entering test_backtrace 1
entering test_backtrace 0
Stack backtrace:
  ebp f0114f18  eip f010007b  args 00000000 00000000 00000000 00000000 f0102738
         kern/init.c:20: test_backtrace+59
  ebp f0114f38  eip f0100068  args 00000000 00000001 f0114f78 00000000 f0102738
         kern/init.c:18: test_backtrace+40
  ebp f0114f58  eip f0100068  args 00000001 00000002 f0114f98 00000000 f0102738
         kern/init.c:18: test_backtrace+40
  ebp f0114f78  eip f0100068  args 00000002 00000003 f0114fd8 00000000 f01047dc
         kern/init.c:18: test_backtrace+40
  ebp f0114f98  eip f0100068  args 00000003 00000004 f0119000 00040000 00000000
         kern/init.c:18: test_backtrace+40
  ebp f0114fb8  eip f0100068  args 00000004 00000005 00000000 00010094 00010094
         kern/init.c:18: test_backtrace+40
  ebp f0114fd8  eip f01000d9  args 00000005 00001aac 00000640 00000000 00000000
         kern/init.c:44: i386_init+69
  ebp f0114ff8  eip f010003e  args 00116021 00000000 00000000 00000000 00000000
         kern/entry.S:83: <unknown>+0
leaving test_backtrace 0
leaving test_backtrace 1
leaving test_backtrace 2
leaving test_backtrace 3
leaving test_backtrace 4
leaving test_backtrace 5
Welcome to the JOS kernel monitor!
Type 'help' for a list of commands.
K>
```

Grade the lab:
```shell
$ make grade
make: Warning: File 'obj/.deps' has modification time 0.97 s in the future
make clean
make[1]: Entering directory '/home/vagrant/mitos/lab'
make[1]: Warning: File 'obj/.deps' has modification time 0.94 s in the future
rm -rf obj .gdbinit jos.in qemu.log
make[1]: warning:  Clock skew detected.  Your build may be incomplete.
make[1]: Leaving directory '/home/vagrant/mitos/lab'
./grade-lab2
make[1]: Entering directory '/home/vagrant/mitos/lab'
make[1]: Warning: File 'obj/.deps' has modification time 0.79 s in the future
+ as kern/entry.S
+ cc kern/entrypgdir.c
+ cc kern/init.c
+ cc kern/console.c
+ cc kern/monitor.c
+ cc kern/pmap.c
+ cc kern/kclock.c
+ cc kern/printf.c
+ cc kern/kdebug.c
+ cc lib/printfmt.c
+ cc lib/readline.c
+ cc lib/string.c
+ ld obj/kern/kernel
ld: warning: section `.bss' type changed to PROGBITS
+ as boot/boot.S
+ cc -Os boot/main.c
+ ld boot/boot
boot block is 390 bytes (max 510)
+ mk obj/kern/kernel.img
make[1]: warning:  Clock skew detected.  Your build may be incomplete.
make[1]: Leaving directory '/home/vagrant/mitos/lab'
running JOS: (1.3s)
  Physical page allocator: OK
  Page management: OK
  Kernel page directory: OK
  Page management 2: OK
Score: 70/70
```

### Question 2

>What entries (rows) in the page directory have been filled in at this point? What addresses do they map and where do they point? In other words, fill out the table as much as possible.

To this question, one needs to print out page-direction of the two-level pages with `info pg`:
```shell
VPN range     Entry         Flags        Physical page
[ef000-ef3ff]  PDE[3bc]     -------UWP
[ef400-ef7ff]  PDE[3bd]     -------U-P
[efc00-effff]  PDE[3bf]     -------UWP
[f0000-f03ff]  PDE[3c0]     ----A--UWP
[f0400-f7fff]  PDE[3c1-3df] ----A--UWP
[f8000-ffbff]  PDE[3e0-3fe] -------UWP
[ffc00-fffff]  PDE[3ff]     -------UWP
```

The entry `1023 = 0x3ff` is ranged in `[0xffc00, 0xfffff]`, so its start *virtual* address is `0xffc00000` (remember that the high `20` bits of a *physical* address in JOS, are from a page-directory entry (`10` bits) and from a page-table entry (`10` bits), and the low `12` bits come from the *offset* of the *virtual* address), which locates a *page table*, as is shown by the pre-filled hint. Here, this *page table* entry can hold $2^{10} = 1024$ (the middle `10` bits of an address) values, so, combined with the `12`-bit *offset*, this table can address $1024 \times 2^{12} = 4$MB bytes.

Similarly, The entry `1022 = 0x3fe` is mentioned in this line:
```shell
[f8000-ffbff]  PDE[3e0-3fe] -------UWP
```
There are $31$ entries on `[0x3e0, 0x3fe]`, and $31744$ values on `[0xf8000, 0xffbff]`, so `PDE[3fe]`, which ends at `0xffbff`, holds $1024$ values, so it starts at `0xffbff - 1023 = 0xff800`, i.e. the base virtual address is `0xff800000`. `0xffc00000 - 0xff800000 = 4194304 = 4`MB, so this page-directory entry points to a page table following the top 4MB of physical memory.

There's no information about page-directory entries `0`, `1`, and `2`, so they are invalid.

| Entry | Base Virtual Address | Points to (logically) |
| --- | --- | --- |
| 1023 | `0xffc00000` | page table for top 4MB of physical memory |
| 1022 | `0xff800000` | page table for the second top 4MB physical memory |
| 2 | `0x00800000` | invalid |
| 1 | `0x00400000` | invalid |
| 0 | `0x00000000` | invalid |

### Question 3

> We have placed the kernel and user environment in the same address space. Why will user programs not be able to read or write the kernel's memory? What specific mechanisms protect the kernel memory?

See [x86 Manual: Chapter 6: Protection](http://pdos.csail.mit.edu/6.828/2018/readings/i386/s06_04.htm).

Page-directories and page-tables entries each have permission flag bits.

>When the processor is executing at user level, only pages that belong to user level and are marked for read/write access are writable; pages that belong to supervisor level are neither readable nor writable from user level.

And the kernel address space is set as non-user accessible:
```c
boot_map_region(kern_pgdir, UPAGES, PTSIZE, PADDR(pages), PTE_U | PTE_P);
```

### Question 4

> What is the maximum amount of physical memory that this operating system can support? Why?

The system uses $4$M space for `PageInfo`. Each `PageInfo` uses $8$ bytes = $8$B, so there may be at most `4MB / 8B = 512K` `PageInfo`s. Each `PageInfo` is for $4$M space, so the maximum amount of physical memory is `4M * 512K = 2G`.

N.B.:
- The comment of `PageInfo` meintions `UPAGES`:
	```c
	Page descriptor structures, mapped at UPAGES.
	```
- The *memlayout* shows the size of the space:
	```plaintext
		UVPT      ---->  +------------------------------+ 0xef400000
						 |          RO PAGES            | R-/R-  PTSIZE
		UPAGES    ---->  +------------------------------+ 0xef000000
	```
- Use the following way to get the size of `PageInfo`:
	```c
	panic("size of PageInfo: %d\n", sizeof(struct PageInfo));
	```

### Question 5

>How much space overhead is there for managing memory, if we actually had the maximum amount of physical memory? How is this overhead broken down?

`PageInfo` uses $4$M, as mentioned above. The page-directory uses $4096$B = $4$KB, which can locate $1024$ page-tables. However, at most $2$G memory can be allocated (see Question 3), each page-table-entry addresses $2^{12} = 4096$ bytes = $4$KB in physical memory, each page-table holds $1024$ entries, so at most $\frac{2\text{G}}{4096\text{B} \times 1024} = 512$ page-tables are needed. Each page-table uses $4096$B = $4$KB, so totally $4\text{KB} \times 512 = 2048\text{KB} = 2\text{M}$ memory is allocated for page-tables.

Thus, the total overhead is: $4\text{M} + 4\text{KB} + 2\text{M} = 6\text{M} + 4\text{KB}$.

### Question 6

>Revisit the page table setup in kern/entry.S and kern/entrypgdir.c. Immediately after we turn on paging, EIP is still a low number (a little over 1MB). At what point do we transition to running at an EIP above KERNBASE? What makes it possible for us to continue executing at a low EIP between when we enable paging and when we begin running at an EIP above KERNBASE? Why is this transition necessary?

In *kern/entry.S*, the `jmp	*%eax` instruction directs to go to the address contained in the register `eax` and makes the transition to running at an `EIP` above `KERNBASE`. The comment in *kern/entrypgdir.c* is useful:

>The entry.S page directory maps the first 4MB of physical memory starting at virtual address KERNBASE (that is, it maps virtual addresses [KERNBASE, KERNBASE+4MB) to physical addresses [0, 4MB)). We choose 4MB because that's how much we can map with one page table and it's enough to get us through early boot.  We also map virtual addresses [0, 4MB) to physical addresses [0, 4MB); this region is critical for a few instructions in entry.S and then we never use it again.

It is saying that `entry_pgdir` maps *virtual* addresses `[0, 4MB)` to *physical* addresses `[0, 4MB)`. This happens before starting paging. When paging is enabled by `jmp	*%eax` before running at an `EIP` above `KERNBASE`, the low $4$M *virtual* addresses are identical to the low $4$M *physical* addresses, and this addressing way does not change after the full paging begins; otherwise, the system had two inconsistent virtual memory address maps before and after beginning paging.

# Reference

<a id="1">[1]</a>
Tanenbaum, Andrew S. and Bos, Herbert. Modern Operating Systems. 4 Boston, MA: Pearson, 2014.



✌️

KADDR: This macro takes a physical address and returns the corresponding kernel virtual address. It panics if you pass an invalid physical address.
