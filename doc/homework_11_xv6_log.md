
[Homework 11: xv6 Log](http://pdos.csail.mit.edu/6.828/2018/homework/xv6-new-log.html)

# xv6 Log

The file *homework/xv6-public/log.c* records the log functionality:
```c
struct logheader {
  int n;
  int block[LOGSIZE];
};

struct log {
  struct spinlock lock;
  int start;
  int size;
  int outstanding; // how many FS sys calls are executing.
  int committing;  // in commit(), please wait.
  int dev;
  struct logheader lh;
};
```
N.B.: `logheader` keeps log blocks (in memory or on disk); `start` and `size` are used for tracking the log buffer in the super-block; the variable `log` is alive in memory.

# Crash Recovery

In this assignmen, the crash recovery functionality provided in *homework/xv6-public/log.c* is disabled. Do the changes mentioned in the assignment, and test in the direcotry *homework/xv6-public/log.c*:
```shell
$ rm fs.img && make clean && make qemu-nox
    ⋮
xv6...
cpu0: starting 0
sb: size 20000 nblocks 19937 ninodes 200 nlog 30 logstart 2 inodestart 32 bmap start 58
recovery: n=0 but ignoring
init: starting sh
$ echo hi > a
lapicid 0: panic: commit mimicking crash
 80102e00 801051a2 80104917 801059d9 8010573c 0 0 0 0 0	
```
A crash occurred, and the `panic` from `commit()` was hit.

Now re-start xv6, keeping the same *fs.img*:
```shell
$ make qemu-nox
    ⋮
xv6...
cpu0: starting 0
sb: size 20000 nblocks 19937 ninodes 200 nlog 30 logstart 2 inodestart 32 bmap start 58
recovery: n=2 but ignoring
init: starting sh
$ cat a
lapicid 0: panic: ilock: no type
 801017f7 801050f8 80104917 801059d9 8010573c 0 0 0 0 0
```

Now fix `recover_from_log()` as mentioned in the assignment, and re-run xv6, keeping the same `fs.img`:
```shell
$ make qemu-nox
    ⋮
xv6...
cpu0: starting 0
sb: size 20000 nblocks 19937 ninodes 200 nlog 30 logstart 2 inodestart 32 bmap start 58
recovery: n=2 but ignoring
init: starting sh
$ cat a
$
```

This time the file *a* is empty. That is because the log header `block[0]` is set to `0`, leaving the on-disk inode still marked unallocated when the crash happens. After xv6 restarts, the log recovers the crash, treating the on-disk inode unallocated, and so nothing is written to the file *a*.

After having this assignment done, restore the source code to make the log recovery work:
```shell
$ rm fs.img && make clean && make qemu-nox
    ⋮
xv6...
cpu0: starting 0
sb: size 20000 nblocks 19937 ninodes 200 nlog 30 logstart 2 inodestart 32 bmap start 58
init: starting sh
$ echo hi > a
$ cat a
hi
$
```

# Streamlinting Commit

The original implementation of the function `install_trans` does unnecessary read from the log, and this assignment requires to improve this function to avoid the needless read from the log.

The synchronization of the log is controlled by the `lock` field of `struct log`. An I/O transaction using the log looks like this:
```c
begin_op();
    ⋮
bp = bread(...);
bp->data[...] = ...;
log_write(bp);
    ⋮
end_op();
```
 
Here, `log_write` sets the buffer `flags` to `B_DIRTY`, which means that `data` needs to be written out and so will not be evicted. `end_op` calls `commit`, and `commit` calls `install_trans`:
```c
static void install_trans(void) {
  int tail;
  for (tail = 0; tail < log.lh.n; tail++) {
    struct buf *lbuf = bread(log.dev, log.start+tail+1); // read log block
    struct buf *dbuf = bread(log.dev, log.lh.block[tail]); // read dst
    memmove(dbuf->data, lbuf->data, BSIZE);  // copy block to dst
    bwrite(dbuf);  // write dst to disk
    brelse(lbuf);
    brelse(dbuf);
  }
}
```
In a log transaction, `log_write` sets the buffer `flags` to `B_DIRTY`, so the destination buffer (i.e. `dbuf`) in `install_trans` is still kept valid, no need to re-read from the log blocks. Therefore, the redundant log-block-read should be removed:
```diff
diff --git a/homework/xv6-public/log.c b/homework/xv6-public/log.c
index a64c0f6..65b55a3 100644
--- a/homework/xv6-public/log.c
+++ b/homework/xv6-public/log.c
@@ -72,11 +72,8 @@ install_trans(void)
   int tail;

   for (tail = 0; tail < log.lh.n; tail++) {
-    struct buf *lbuf = bread(log.dev, log.start+tail+1); // read log block
     struct buf *dbuf = bread(log.dev, log.lh.block[tail]); // read dst
-    memmove(dbuf->data, lbuf->data, BSIZE);  // copy block to dst
     bwrite(dbuf);  // write dst to disk
-    brelse(lbuf);
     brelse(dbuf);
   }
 }
```

Verify the work in the directory *homework/xv6-public*:
```shell
$ make clean && make qemu-nox
    ⋮
xv6...
cpu0: starting 0
sb: size 20000 nblocks 19937 ninodes 200 nlog 30 logstart 2 inodestart 32 bmap start 58
init: starting sh
$ echo hi > a
$ cat a
hi
$
```

✌️
